package com.techled.teacheranalytics.util;

import android.content.Intent;
import android.util.Log;

import com.techled.teacheranalytics.LoginActivity;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

public class Util {

    /* localhost */
    private String strServletUrl = "";
    private String strMainUrl;
    private boolean isDemoVersion = false;

    public Util() {
        strMainUrl = LoginActivity.SERVER_URL;
        //strMainUrl = "http://192.168.8.37:8080/webresources/";
        strServletUrl = strMainUrl.replace("webresources/", "");
    }

    //change password
    public String changePwd(Integer userId, String old_password, String new_password, String confirm_password) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("users/changePwd?");
            {
                sb.append("usrId=").append(userId);
                sb.append("&old_password=").append(URLEncoder.encode(old_password, "UTF-8"));
                sb.append("&new_password=").append(URLEncoder.encode(new_password, "UTF-8"));
                sb.append("&confirm_password=").append(URLEncoder.encode(confirm_password, "UTF-8"));

            }
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //live lecture
    public String getLiveLecture(Integer orgId, Integer insId, Integer dscId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        // sb.append("http://192.168.2.25:8080/webresources/");
        sb.append("lecture/getLiveLectures?");
        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);


        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get student info
    public String getStudentInfo(Integer orgId, Integer insId,
                                 Integer ayr, Integer stu) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("student/getStudentInfo?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("ayr=").append(ayr).append("&");
        sb.append("stu=").append(stu).append("&");
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    // standard-division
    public String getStandardDivisionJSON(Integer orgId, Integer insId,
                                          Integer dscId, Integer ayr, Integer stfId) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("standardDivision/getStandardDivisionJSON?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("ayr=").append(ayr).append("&");
        sb.append("stf=").append(stfId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //leave the school request
    public String GetLeaveTheSchoolRequest(Integer orgId, Integer insId) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("lcconsent/getStuAppliedLc?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get timeline data
    public String getTimelineData(Integer orgId, Integer insId,
                                  String stuStfSe, Integer stuId, String readYN) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("timeline/getTimelineDetails?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("stuStfSe=").append(stuStfSe).append("&");
        sb.append("stuId=").append(stuId).append("&");
        sb.append("tilReadYn=").append(readYN);
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //academic year
    public String getAcademicYears(Integer orgId, Integer insId, Integer dscId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("academicYear/getAcademicYears?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getAyrYears(Integer orgId, Integer insId, Integer dscId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("academicYear/getAyrYears?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //syllabus
    public String getSyallabusDetails(Integer orgId, Integer insId, Integer dscId,Integer ayr,Integer stdId,Integer subId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("syllabus/getStdSubUnits?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");;
        sb.append("ayr=").append(ayr).append("&");;
        sb.append("std=").append(stdId).append("&");;
        sb.append("sub=").append(subId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //syllabus sub-units details
    public String getSubUnitsSyallabusDetails(Integer orgId, Integer insId, Integer dscId,Integer ayr,Integer stdId,Integer subId,Integer uni) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("syllabus/getStdSubUniSubUnitsV1?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");;
        sb.append("ayr=").append(ayr).append("&");;
        sb.append("std=").append(stdId).append("&");;
        sb.append("sub=").append(subId).append("&");;
        sb.append("uni=").append(uni);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //delte syllabus unit details
    public String deleteUnitSyallabusDetails(Integer orgId, Integer insId, Integer dscId,Integer ayr,Integer stdId,Integer subId,Integer uniId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("syllabus/deleteStdSubUnit?");
        sb.append("orgId=").append(orgId).append("&");
        sb.append("insId=").append(insId).append("&");
        sb.append("dscId=").append(dscId).append("&");;
        sb.append("ayrYear=").append(ayr).append("&");;
        sb.append("stdId=").append(stdId).append("&");;
        sb.append("subId=").append(subId).append("&");;
        sb.append("uniId=").append(uniId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //delete syllabus sub-unit details
    public String deleteSubUnitSyallabusDetails(Integer orgId, Integer insId, Integer dscId,Integer ayr,Integer stdId,Integer subId,Integer uniId,Integer sunId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("syllabus/deleteStdSubUniSubUnits?");
        sb.append("orgId=").append(orgId).append("&");
        sb.append("insId=").append(insId).append("&");
        sb.append("dscId=").append(dscId).append("&");;
        sb.append("ayrYear=").append(ayr).append("&");;
        sb.append("stdId=").append(stdId).append("&");;
        sb.append("subId=").append(subId).append("&");;
        sb.append("uniId=").append(uniId).append("&");;
        sb.append("sunId=").append(sunId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //save syllabus unit details
    public String saveStdSubUnit(Integer orgId,Integer insId,Integer dscId,Integer ayrYear,
                                 Integer stdId,Integer subId,String unitDescription,
                                 String unitNumber,String unitActiveYn ,
                                 String unitShowContentYn ,
                                 String unitNoOfUnitsAlloted,
                                 String unitPeriodFrom ,String unitPeriodTo,Integer uniId) {

        try {

            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("syllabus/saveStdSubUnit?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            sb.append("ayrYear=").append(ayrYear).append("&");
            sb.append("stdId=").append(stdId).append("&");
            sb.append("subId=").append(subId).append("&");;
            sb.append("&unitDescription=").append(URLEncoder.encode(unitDescription, "UTF-8")).append("&");;
            sb.append("&unitNumber=").append(URLEncoder.encode(unitNumber, "UTF-8")).append("&");;
            sb.append("unitActiveYn=").append(unitActiveYn).append("&");
            sb.append("unitShowContentYn=").append(unitShowContentYn).append("&");
            sb.append("unitNoOfUnitsAlloted=").append(URLEncoder.encode(unitNoOfUnitsAlloted, "UTF-8")).append("&");
            sb.append("unitPeriodFrom=").append(URLEncoder.encode(unitPeriodFrom, "UTF-8")).append("&");
            sb.append("unitPeriodTo=").append(URLEncoder.encode(unitPeriodTo, "UTF-8")).append("&");
            sb.append("uniId=").append(uniId);

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    //save syllabus subunit details
    public String saveStdSubUniSubUnits(Integer orgId,Integer insId,Integer dscId,Integer ayrYear,
                                 Integer stdId,Integer subId,String unitNumber,String unitActiveYn ,String unitShowContentYn ,
                                 String unitNoOfUnitsAlloted,String unitPeriodFrom ,String unitPeriodTo,Integer uniId,Integer sunId, String content, String activities) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("syllabus/saveStdSubUniSubUnits?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            sb.append("ayrYear=").append(ayrYear).append("&");
            sb.append("stdId=").append(stdId).append("&");
            sb.append("subId=").append(subId).append("&");
            sb.append("subUnitDescription=").append(URLEncoder.encode(unitNumber, "UTF-8")).append("&");
            sb.append("subUnitActiveYn=").append(unitActiveYn).append("&");
            sb.append("unitShowContentYn=").append(unitShowContentYn).append("&");
            sb.append("subUnitNoOfUnitsAlloted=").append(URLEncoder.encode(unitNoOfUnitsAlloted, "UTF-8")).append("&");
            sb.append("subUnitPeriodFrom=").append(URLEncoder.encode(unitPeriodFrom, "UTF-8")).append("&");
            sb.append("subUnitPeriodTo=").append(URLEncoder.encode(unitPeriodTo, "UTF-8")).append("&");
            sb.append("uniId=").append(uniId).append("&");
            sb.append("sunId=").append(sunId).append("&");
            sb.append("subUnitContent=").append(content).append("&");
            sb.append("subUnitActivities=").append(activities);

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    //get academic performance get standard
    public String getAcadmicPerformance(Integer orgId, Integer insId,
                                        Integer dscId, Integer ayr, Integer std, Integer div, Integer sub, String examTypes) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        //  sb.append("http://192.168.2.21:8080/webresources/");
        sb.append("analytics/getSchoolAnalytics?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("ayr=").append(ayr).append("&");
        sb.append("std=").append(std).append("&");
        sb.append("div=").append(div).append("&");
        sb.append("sub=").append(sub).append("&");
        sb.append("examTypes=").append(examTypes);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    // get teacher standard division info
    public String getTeacherBySubStdDiv(Integer orgId, Integer insId,
                                        Integer dscId, Integer ayr, Integer std, Integer div, Integer sub) {

        StringBuilder sb = new StringBuilder();
        // strMainUrl = "http://192.168.2.21:8080/webresources/";
        sb.append(strMainUrl);
        sb.append("analytics/getTeacherBySubStdDiv?");
        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayr);
        sb.append("&std=").append(std);
        sb.append("&div=").append(div);
        sb.append("&sub=").append(sub);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get class wise performance analytics
    public String getClassAnalytics(Integer orgId, Integer insId,
                                    Integer dscId, Integer ayr, Integer std, Integer div, Integer sub, String examTypes) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        //sb.append("http://192.168.2.21:8080/webresources/");
        sb.append("analytics/getClassAnalytics?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("ayr=").append(ayr).append("&");
        sb.append("std=").append(std).append("&");
        sb.append("div=").append(div).append("&");
        sb.append("subId=").append(sub).append("&");
        sb.append("examTypes=").append(examTypes);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //save system log
    public String SaveSystemLog(Integer orgId, Integer depId, String description, String category, Integer usrId, String module) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("systemLogWs/logEntry?");
            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(depId);
            sb.append("&desc=").append(URLEncoder.encode(description, "UTF-8"));
            sb.append("&category=").append(URLEncoder.encode(category, "UTF-8"));
            sb.append("usrId=").append(usrId);
            sb.append("&module=").append(URLEncoder.encode(module, "UTF-8"));


            Webservice webservice = new Webservice(sb.toString());
            webservice.start();

            try {
                webservice.join(30000);
                Log.d("addUrl", "" + sb);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    //My info
    public String getMyInfo(int usrId, int ayrYear) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("users/getUserInfoV1?");
        sb.append("usrId=").append(usrId);
        sb.append("&ayrYear=").append(ayrYear);
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //fetch student marked attendance
    public String fetchMarkedAttendanceV1(Integer orgId, Integer insId,
                                          Integer dscId, Integer ayrYear, Integer stdId, Integer divId, String date) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("attendance/fetchMarkedAttendanceV1?");
            sb.append("org=").append(orgId);
            sb.append("&ins=").append(insId);
            sb.append("&dsc=").append(dscId);
            sb.append("&std=").append(stdId);
            sb.append("&div=").append(divId);
            sb.append("&ayr=").append(ayrYear);
            sb.append("&date=").append(URLEncoder.encode(date, "UTF-8"));
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    //fetch marked sport attendance
    public String fetchMarkedSportAttendance(Integer orgId, Integer insId,
                                          Integer dscId, Integer ayrYear, Integer stdId, Integer divId, String date) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("attendance/fetchMarkedSportAttendance?");
            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&dscId=").append(dscId);
            sb.append("&stdId=").append(stdId);
            sb.append("&divId=").append(divId);
            sb.append("&ayrYear=").append(ayrYear);
            sb.append("&date=").append(URLEncoder.encode(date, "UTF-8"));
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }
    //fetch sport attendance history
    public String FetchSportAttendance(Integer orgId, Integer insId,
                                       Integer dscId, Integer ayrYear, Integer stdId, Integer divId, String date) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("attendance/fetchSportAttdMarkedDays?");
            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&dscId=").append(dscId);
            sb.append("&stdId=").append(stdId);
            sb.append("&divId=").append(divId);
            sb.append("&ayrYear=").append(ayrYear);
           // sb.append("&date=").append(URLEncoder.encode(date, "UTF-8"));

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }
    //check it is sport day or not
    public String checkIsSportDay(Integer orgId, Integer insId,
                                          Integer dscId, Integer ayrYear, Integer stdId, Integer divId, String date) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("attendance/checkSportAttendance?");
            sb.append("org=").append(orgId);
            sb.append("&ins=").append(insId);
            sb.append("&dsc=").append(dscId);
            sb.append("&std=").append(stdId);
            sb.append("&div=").append(divId);
            sb.append("&ayr=").append(ayrYear);
            sb.append("&date=").append(URLEncoder.encode(date, "UTF-8"));
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    //load sport teacher std div
    public String loadSportStdDiv(Integer orgId, Integer insId,
                                  Integer dscId, Integer ayrYear, Integer stf) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("standardDivision/getResponsibleStaffStdDiv?");
            sb.append("org=").append(orgId);
            sb.append("&ins=").append(insId);
            sb.append("&dsc=").append(dscId);
            sb.append("&ayr=").append(ayrYear);
            sb.append("&stf=").append(stf);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }


    //save mark as read
    public String saveTimelineMarkAsRead(Integer orgId, Integer insId,
                                         Integer yearMonthId, String stuStfSe, Integer stfId, Integer stuIdLogin, Integer tilId, String tilReadYn) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("timeline/saveTimelineDetails?");
            sb.append("org=").append(orgId);
            sb.append("&ins=").append(insId);
            sb.append("&yearMonthId=").append(yearMonthId);
            sb.append("&stuStfSe=").append(stuStfSe);
            sb.append("&stuId=").append(stfId);
            sb.append("&stuIdLogin=").append(stuIdLogin);
            sb.append("&tilId=").append(tilId);
            sb.append("&tilReadYn=").append(tilReadYn);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    //fetch attendance
    public String fetchAttdMarkedDaysV1(Integer orgId, Integer insId,
                                        Integer dscId, Integer stdId, Integer divId, Integer ayrYear) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("attendance/fetchAttdMarkedDaysV1?");
            sb.append("org=").append(orgId);
            sb.append("&ins=").append(insId);
            sb.append("&dsc=").append(dscId);
            sb.append("&std=").append(stdId);
            sb.append("&div=").append(divId);
            sb.append("&ayr=").append(ayrYear);
           // sb.append("&month=").append(month);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    // Student For Attendance
    public String getAttdStudentsString(Integer orgId, Integer insId,
                                        Integer dscId, Integer ayrYear, Integer stdId, Integer divId) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("attendance/getStudents?");
        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);
        sb.append("&std=").append(stdId);
        sb.append("&div=").append(divId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get leave balance
    public String getMyLeaveBalance(Integer orgId, Integer insId, Integer dscId,
                                    Integer ayrYear, Integer stfId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("leave/getMyLeaveBalance?");
        if (isDemoVersion) {

        } else {
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("stf=").append(stfId);
        }
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get disciplinary rules
    public String getDisciplinaryRulesV1(Integer orgId, Integer insId, String type) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("disciplinaryAction/getDisciplinaryRulesV1?");

        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("type=").append(type);


        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //send student remark
    public String sendRemark(Integer orgId, Integer insId, Integer ayr,
                             Integer stfStuId, Integer disRuleId, String stuStfSe, String disDescription,
                             String currDate, Integer stuId) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("disciplinaryAction/saveDisciplinaryActionsV1?");
            sb.append("org=").append(orgId);
            sb.append("&ins=").append(insId);
            sb.append("&ayr=").append(ayr);
            sb.append("&stfStuId=").append(stfStuId);
            sb.append("&disRuleId=").append(disRuleId);
            sb.append("&stuStfSe=").append(stuStfSe);
            sb.append("&disDescription=").append(URLEncoder.encode(disDescription, "UTF-8"));
            sb.append("&currDate=").append(URLEncoder.encode(currDate, "UTF-8"));
            sb.append("&assignId=").append(stuId);

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //notification broadcast servlet
    public String sendNotification(Integer orgId, Integer insId, Integer dscId, Integer ayr,
                                   Integer stdId, Integer divId, String uName, String informParents, String informStudents, String forTsd, String from, String to, String typeNca,
                                   String addToTimeline, String url, String uploadType, String title, String message, String notifyAllDivision, String imageString, String ext, String stfGrades, String stfIds, String notifiedStdName, String notifiedDivName, Integer usrId, String hwkS3Url) {
        try {
           /* StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("news/saveNotificationV1?");


            sb.append("org=").append(orgId);
            sb.append("&ins=").append(insId);
            sb.append("&dsc=").append(dscId);

            sb.append("&ayr=").append(ayr);
            sb.append("&std=").append(stdId);
            sb.append("&div=").append(divId);

            sb.append("&usrName=").append(URLEncoder.encode(uName, "UTF-8"));
            sb.append("&informParents=").append("Y");
            sb.append("&forTsd=").append(forTsd);

            sb.append("&from=").append(from);
            sb.append("&to=").append(to);
            sb.append("&typeNca=").append(typeNca);

            sb.append("&addToTimeLine=").append(addToTimeline);
            sb.append("&url=").append(url);
            sb.append("&uploadType=").append(uploadType);
            sb.append("&notificationTitle=").append(URLEncoder.encode(title, "UTF-8"));
            sb.append("&notificationMessage=").append(URLEncoder.encode(message, "UTF-8"));
            sb.append("&notifyAllDivisions=").append(notifyAllDivision);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();

            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return webservice.output.toString();*/


            StringBuilder sb = new StringBuilder();
            // sb.append("http://182.72.44.211:3000/");
            sb.append(strServletUrl);
            sb.append("notificationBroadcastServlet");
            String urlW = sb.toString();
            WebservletForNoficationBroadCast webservlet = new WebservletForNoficationBroadCast(urlW, orgId, insId, dscId, ayr, stdId, divId, uName, informParents, informStudents, forTsd, from, to, typeNca, addToTimeline, uploadType, title, message, notifyAllDivision, imageString, ext, stfGrades, stfIds, notifiedStdName, notifiedDivName, usrId, hwkS3Url);
            webservlet.start();
            try {
                webservlet.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservlet.output == null) {
                return null;
            } else {
                return webservlet.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //past events
    public String getPastEvents(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, String usrRole, Integer usrId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("events/getPastEvents?");

        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);
        sb.append("&usrRole=").append(usrRole);
        sb.append("&id=").append(usrId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get notification broadcast
    public String getNotificationBroadcast(Integer usrId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("newsNotification/getNotification?");
        sb.append("usr=").append(usrId);


        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get division by standard
    public String getDivisionByStandardId(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer usrId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("standardDivision/getDivisionByStandardId?");

        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);
        sb.append("&std=").append(usrId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    // Upcoming events data
    public String getUpcomingEvents(Integer orgId, Integer insId, Integer dscId,
                                    Integer ayrYear, String usrRole, Integer id) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("events/getUpcomingEvents?");


        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);
        sb.append("&usrRole=").append(usrRole);
        sb.append("&id=").append(id);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }


    //apply staff leave
    public String applyStaffLeave(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stfId, Integer lvrId,
                                  String lvrFrom, String lvrTo, String balance,
                                  String halfDayYn, String whichhalf, String woffHdayYn,
                                  String leaveReason, String leaveAddress,
                                  String sendSms, String sendEmail, String sendNotification) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("leave/applyStaffLeave?");
        if (isDemoVersion) {

        } else {
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("stf=").append(stfId).append("&");
            sb.append("lvr=").append(lvrId).append("&");
            sb.append("lvrFrom=").append(lvrFrom).append("&");
            sb.append("lvrTo=").append(lvrTo).append("&");
            sb.append("balance=").append(balance).append("&");
            sb.append("halfDayYn=").append(halfDayYn).append("&");
            sb.append("whichHalf=").append(whichhalf).append("&");
            sb.append("woffHdayYn=").append(woffHdayYn).append("&");
            String encodedReason = URLEncoder.encode(leaveReason, "UTF-8");
            String encodedAddress = URLEncoder.encode(leaveAddress, "UTF-8");
            sb.append("leaveReason=").append(encodedReason).append("&");
            sb.append("leaveAddress=").append(encodedAddress).append("&");
            sb.append("sendSms=").append(sendSms).append("&");
            sb.append("sendEmail=").append(sendEmail).append("&");
            sb.append("sendNotification=").append(sendNotification);

        }
        //{"status":"LEAVE_SUCCESSFULLY_APPLIED"}
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //pending stu leave
    public String getPendingStuLeaves(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stf, Integer std, Integer div) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        //  sb.append("http://192.168.2.36:8080/webresources/");
        sb.append("leave/getStudentLeaves?");
        //    sb.append("leave/approveStuLeaves?");
        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);
        sb.append("&stf=").append(stf);
        sb.append("&std=").append(std);
        sb.append("&div=").append(div);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get homework list
    public String getHomeworkList(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stf) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);

        sb.append("homework/getStdDivHomeworkOnStfId?");
        sb.append("orgId=").append(orgId);
        sb.append("&insId=").append(insId);
        sb.append("&dscId=").append(dscId);
        sb.append("&ayrYear=").append(ayrYear);
        sb.append("&stfId=").append(stf);
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    // Approve Leave
    public String approveStuLeave(Integer orgId, Integer insId, Integer dscId, Integer stuId,
                                  Integer salApplicationNo, String status, String remark, Integer stfId, String sendSms, String sendEmail) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        //leave/approveRejectLeave?org=&ins=&dsc=&stf=&lvr=&appNo=&remark=&appStatus=applicationStatus
        sb.append("leave/approveStudentLeaveV1?");


        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&stu=").append(stuId);
        sb.append("&salApplicationNo=").append(salApplicationNo);
        sb.append("&status=").append(status);
        String encodedRemark = URLEncoder.encode(remark, "UTF-8");
        sb.append("&remark=").append(encodedRemark);
        sb.append("&stf=").append(stfId);
        sb.append("&sendSms=").append(sendSms);
        sb.append("&sendEmail=").append(sendEmail);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //save fcm token
    public String setAndroidAnalyticsRegId(int usrId, String androidId, int orgId, int insId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        //   sb.append("http://192.168.2.30:8080/webresources/");
        sb.append("users/setAndroidAnalyticsRegIdV1?");
        sb.append("usr=").append(usrId);
        sb.append("&androidId=").append(androidId);
        sb.append("&org=").append(orgId);
        sb.append("&ins=").append(insId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //monthly attendance
    public String getDayWiseMonthInfo(int orgId, int insId, int dscId,
                                      int ayrYear, int stfId) {
        StringBuilder sb = new StringBuilder();
        //  sb.append("http://192.168.2.21:8080/webresources/");
        sb.append(strMainUrl);
        sb.append("attendance/getDayWiseMonthInfoStaffV1?");

        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);
        sb.append("&stf=").append(stfId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //monthly attendance
    public String getStudentMonthlyAttendance(int orgId, int insId, int dscId,
                                              int ayrYear, int stfId) {
        StringBuilder sb = new StringBuilder();
        //sb.append("http://192.168.2.21:8080/webresources/");
        sb.append(strMainUrl);
        sb.append("attendance/getStaffPresentCountV1?");

        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);
        sb.append("&stf=").append(stfId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //timetable url
    public String getTimetableUrl(int orgId, int insId, int dscId,
                                  int ayrYear, int stdId, int divId) {
        StringBuilder sb = new StringBuilder();
        // sb.append("http://192.168.2.21:8080/webresources/");
        sb.append(strMainUrl);
        sb.append("lecture/getTimetable?");

        sb.append("orgId=").append(orgId);
        sb.append("&insId=").append(insId);
        sb.append("&dscId=").append(dscId);
        sb.append("&ayrYear=").append(ayrYear);
        sb.append("&stdId=").append(stdId);
        sb.append("&divId=").append(divId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //module data
    public String getModuleData() {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        //sb.append("http://192.168.2.21:8080/webresources/");
        sb.append("module/findModulesOpenForReportingError?");

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //save suggestion request
    public String SaveSuggestionRequest(int orgId, int insId, int mdlId,
                                        String title, String description, String expDueDate, String priority,
                                        int usrId, String typeBe, String status) {
        StringBuilder sb = new StringBuilder();
        // sb.append("http://192.168.2.21:8080/webresources/");
        try {
            sb.append(strMainUrl);
            sb.append("schoolCustomization/insertSchoolCustomizationNew?");

            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&mdlId=").append(mdlId);
            sb.append("&title=").append(URLEncoder.encode(title, "UTF-8"));
            sb.append("&description=").append(URLEncoder.encode(description, "UTF-8"));
            sb.append("&expDueDate=").append(URLEncoder.encode(expDueDate, "UTF-8"));
            sb.append("&priority=").append(priority);
            sb.append("&usrId=").append(usrId);
            sb.append("&typeBe=").append(typeBe);
            sb.append("&status=").append(status);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    // Mark Student Attendance
    public String markStudentAttd(Integer orgId, Integer insId, Integer dscId,
                                  Integer ayrYear, Integer std, Integer div, String strRolls,
                                  String date, String notifyParents, String insName, Integer assetId) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("attendance/markStudentAttdV2?");

            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("std=").append(std).append("&");
            sb.append("div=").append(div).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("attArray=").append(URLEncoder.encode(strRolls, "UTF-8")).append("&");
            sb.append("date=").append(URLEncoder.encode(date, "UTF-8")).append("&");
            sb.append("notifyParents=").append(notifyParents).append("&");
            sb.append("insName=").append(URLEncoder.encode(insName, "UTF-8")).append("&");
            sb.append("stfId=").append(assetId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    //mark sport attd
    public String markStudentSportAttd(Integer orgId, Integer insId, Integer dscId,
                                  Integer ayrYear, Integer std, Integer div, String strRolls,
                                  String date, String notifyParents, Integer assetId) {
        try {
            //org=4&ins=1&dsc=1&std=1&div=1&ayr=2018&date=2019-03-04&rolls=1,2,3,4
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("attendance/markSportAttendance?");

            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("std=").append(std).append("&");
            sb.append("div=").append(div).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("rolls=").append(URLEncoder.encode(strRolls, "UTF-8")).append("&");
            sb.append("date=").append(URLEncoder.encode(date, "UTF-8")).append("&");
            sb.append("notifyParents=").append(notifyParents).append("&");
            sb.append("stfId=").append(assetId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    //sms
    public String getSmsLog(Integer orgId, Integer insId, Integer dscId, Integer usrId, String staffStuSe
    ) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("sms/getSmsLogs?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("usrId=").append(usrId).append("&");
        sb.append("staffStuSe").append(staffStuSe);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getIssuesDetails(Integer orgId, Integer insId, Integer usrId) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("schoolCustomization/getMyCustomizationRequests?");
        sb.append("orgId=").append(orgId).append("&");
        sb.append("insId=").append(insId).append("&");
        sb.append("usrId=").append(usrId).append("&");

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get email log
    public String getEmailLog(Integer orgId, Integer insId, Integer dscId, Integer usrId, String staffStuSe
    ) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("sms/getEmailLogs?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("usrId=").append(usrId).append("&");
        sb.append("staffStuSe").append(staffStuSe);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get timeline data
    public String getTimelineData(int orgId, int insId, String stuStfSe, int usrId
    ) {
        StringBuilder sb = new StringBuilder();
        //sb.append("http://192.168.2.6:8080/webresources/");
        sb.append(strMainUrl);
        sb.append("timeline/getTimeLineNews?");

        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&stuStfSe=").append(stuStfSe);
        sb.append("&stuId=").append(usrId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //user event log
    public String saveUserEventLog(Integer orgId, Integer insId,
                                   Integer dscId, Integer ugpId, Integer usrId, String usrName, String session, String ip, String event, String usrNotificationYn) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            //   sb.append("http://192.168.4.86:8080/webresources/");
            sb.append("userEventLog/insertUserLogData?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("ugp=").append(ugpId).append("&");
            sb.append("usr=").append(usrId).append("&");
            sb.append("usrName=").append(URLEncoder.encode(usrName, "UTF-8")).append("&");
            sb.append("session=").append(URLEncoder.encode(session, "UTF-8")).append("&");
            sb.append("ip=").append(ip).append("&");
            sb.append("event=").append(event).append("&");
            sb.append("usrNotificationYn=").append(usrNotificationYn);
            //   sb.append("orderBySubjectExamSe=").append(orderBySubjectExamSe);

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //login
    public String authenticateUserVersion2(String id, String userName, String password) {
        try {
            Webservice webservice = new Webservice(strMainUrl
                    + "users/authenticateUserV1?login=" + URLEncoder.encode(id, "UTF-8") + "&usrName=" + URLEncoder.encode(userName, "UTF-8")
                    + "&pwd=" + URLEncoder.encode(password, "UTF-8"));
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //get subjects
    public String getStaffClassSubjects(Integer orgId, Integer insId, Integer dscId,
                                        Integer ayrYear, Integer stfId, Integer stdId, Integer divId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("subject/getStaffClassSubjects?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("stf=").append(stfId).append("&");
        sb.append("std=").append(stdId).append("&");
        sb.append("div=").append(divId);
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get exams details
    public String getStfExamsScheduleV1(Integer orgId, Integer insId, Integer dscId,
                                        Integer stdId, Integer divId, Integer ayrYear, Integer stfId, Integer subId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("examSchedule/getStfExamScheduleV1?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");

        sb.append("std=").append(stdId).append("&");
        sb.append("div=").append(divId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("stf=").append(stfId).append("&");
        sb.append("subId=").append(subId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get exam marks
    public String getExamMarksV1(Integer orgId, Integer insId, Integer dscId,
                                 Integer stdId, Integer divId, Integer ayrYear, Integer examId, Integer subId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("examSchedule/getExamMarksV1?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");

        sb.append("std=").append(stdId).append("&");
        sb.append("div=").append(divId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("exm=").append(examId).append("&");
        sb.append("subId=").append(subId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get exam marks
    public String getExamMarksV2(Integer orgId, Integer insId, Integer dscId,
                                 Integer stdId, Integer divId, Integer ayrYear, Integer examId, Integer subId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("examSchedule/getExamMarksV2?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");

        sb.append("std=").append(stdId).append("&");
        sb.append("div=").append(divId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("exm=").append(examId).append("&");
        sb.append("subId=").append(subId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //notify parent after upload homework
    public String notifyParentsAboutHomework(Integer orgId, Integer insId, Integer dscId,
                                             Integer stdId, Integer divId, Integer ayrYear, Integer hwkId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("homework/notifyParentsAboutHomework?");
        sb.append("orgId=").append(orgId).append("&");
        sb.append("insId=").append(insId).append("&");
        sb.append("dscId=").append(dscId).append("&");
        sb.append("stdId=").append(stdId).append("&");
        sb.append("divId=").append(divId).append("&");
        sb.append("ayrYear=").append(ayrYear).append("&");
        sb.append("hwkId=").append(hwkId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get timeline read details
    public String getTimelineReadDetails(Integer orgId, Integer insId, Integer ayr, String stuStfSe, String tilIndentifier, String date, String timelineType) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("timeline/getTimelineReadDetails?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("ayrYear=").append(ayr).append("&");
        sb.append("stuStfSe=").append(stuStfSe).append("&");
        sb.append("tilIdentifier=").append(tilIndentifier).append("&");
        sb.append("date=").append(date).append("&");
        sb.append("timelineType=").append(timelineType);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //save exam marks
    public String saveEnteredExamMarksV1(Integer orgId, Integer insId, Integer dscId,
                                         Integer stdId, Integer divId, Integer ayrYear, Integer examId, Integer subId, String marksInfo, Integer usrId, Integer ugpId, String stdName, String divName) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("examSchedule/enterMarksV1?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("std=").append(stdId).append("&");
            sb.append("div=").append(divId).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("exm=").append(examId).append("&");
            sb.append("subId=").append(subId).append("&");

            sb.append("marksInfo=").append(URLEncoder.encode(marksInfo, "UTF-8")).append("&");
            sb.append("usrId=").append(usrId).append("&");
            sb.append("ugpId=").append(ugpId).append("&");
            sb.append("stdName=").append(URLEncoder.encode(stdName, "UTF-8")).append("&");
            sb.append("divName=").append(URLEncoder.encode(divName, "UTF-8"));
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //upload homework servlet
    public String uploadHomeworkByStaff(Integer orgId, Integer insId, Integer dscId,
                                        Integer stdId,
                                        Integer divId, Integer subId, Integer ayrYear, Integer hwkId, String hwkTTitle, String hwkDesc, String hwkType,
                                        String imageString, String contentType, String dueDate, Integer stfId, String flippedClassroom, String assignDate, String fileExtension, String notifyAllDivision, String notifyParents, String hwkS3Url) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strServletUrl);
            sb.append("homeworkServlet");
            String urlW = sb.toString();
            Webservlet webservlet = new Webservlet(urlW, orgId, insId, dscId, stdId, divId, subId, ayrYear, hwkId, hwkTTitle,
                    hwkDesc, hwkType, imageString, contentType, dueDate, stfId, flippedClassroom, assignDate, fileExtension, notifyAllDivision, notifyParents, hwkS3Url);
            webservlet.start();
            try {
                webservlet.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservlet.output == null) {
                return null;
            } else {
                return webservlet.output.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //get staff grades
    public String getStfGrades(Integer orgId, Integer insId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("staffGrade/getGrade?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId);
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }


    //get teaching log for day, My Lectures details
    public String findTeachingLogForDay(Integer orgId, Integer insId, Integer dscId,Integer stfId,
                                        Integer ayrYear, String startDate, String endDate){

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("teachingLog/findTeachingLogForDay?");
        sb.append("orgId=").append(orgId).append("&");
        sb.append("insId=").append(insId).append("&");
        sb.append("dscId=").append(dscId).append("&");
        sb.append("stfId=").append(stfId).append("&");
        sb.append("ayrYear=").append(ayrYear).append("&");
        //sb.append("ayrYear=").append("2019&");
        sb.append("startDate=").append(startDate).append("&");
        sb.append("endDate=").append(endDate);
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get staff leaved details
    public String getMyLeavesStaff(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stfId){

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("leave/getMyLeavesStf?");
        sb.append("orgId=").append(orgId).append("&");
        sb.append("insId=").append(insId).append("&");
        sb.append("dscId=").append(dscId).append("&");
        sb.append("ayrYear=").append(ayrYear).append("&");
        sb.append("stfId=").append(stfId);
        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    //get teaching log for day, My Lectures details
    public String saveTeachingLogForDay(Integer orgId, Integer insId, Integer dscId, Integer stdId, Integer stfId,
                                        Integer divId, Integer ayrYear, Integer prdId, Integer subId, String telSpecialLogYn,
                                        String telActualTeaching, String telComment, Integer tepPrdIndex, String telSlbBokSb, String uniSubUnitId,
                                        String scheduledTeaching, String lectureConducted, String eventBased,String selectedDate, String telEvalEctSubEctIds,String PlannedSyllabusIds, String lectureNotConductedReason, String completedSyllabusIds){

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("teachingLog/saveTeachingLogForDay?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            sb.append("stdId=").append(stdId).append("&");
            sb.append("stfId=").append(stfId).append("&");
            sb.append("divId=").append(divId).append("&");
            sb.append("ayrYear=").append(ayrYear).append("&");
            sb.append("prdId=").append(prdId).append("&");
            sb.append("subId=").append(subId).append("&");
            sb.append("telSpecialLogYn=").append(telSpecialLogYn).append("&");
            sb.append("telActualTeaching=").append(URLEncoder.encode(telActualTeaching, "UTF-8")).append("&");
            sb.append("telComment=").append(URLEncoder.encode(telComment, "UTF-8")).append("&");
            sb.append("tepPrdIndex=").append(tepPrdIndex).append("&");
            sb.append("telSlbBokSb=").append(telSlbBokSb).append("&");
            sb.append("telUniSun=").append(uniSubUnitId).append("&");
            sb.append("telScheduleTeaching=").append(URLEncoder.encode(scheduledTeaching, "UTF-8")).append("&");
            sb.append("telLectureConductedYn=").append(lectureConducted).append("&");
            sb.append("telLectureEventBased=").append(eventBased).append("&");
            sb.append("scheduleTeachingDate=").append(selectedDate).append("&");
            sb.append("telEvalEctSubectIds=").append(telEvalEctSubEctIds).append("&");
            sb.append("telPlannedUniSun=").append(PlannedSyllabusIds).append("&");
            sb.append("telLectureNotConductedReason=").append(lectureNotConductedReason).append("&");
            sb.append("telCompletedUniSun=").append(completedSyllabusIds);


            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    //get Syllabus details unit and sub-unit for teaching log
    public String getStdUnitsSubUnits(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stdId,
                                        Integer subId){

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("syllabus/getStdUnitsSubUnits?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("std=").append(stdId).append("&");
        sb.append("sub=").append(subId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }


    //get Tools and method for teaching log
    public String getEvaluationForLessonPlan(Integer orgId, Integer insId, Integer dscId, Integer ayrYear){

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("evaluation/getEvaluationForLessonPlan?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String findPendingTasks(int orgId, int insId, int dscId, int stfId, String taskAssignedToMe) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("tasks/getMyTasks?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            sb.append("stfId=").append(stfId).append("&");
            sb.append("taskAssignedToMe=").append(taskAssignedToMe);

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String loadStandards(int orgId, int insId, int dscId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("standardDivision/getStandards?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String loadStandardsV2(int orgId, int insId, int dscId, int ayrYear, int stfId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("evaluation/loadStandards?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("stf=").append(stfId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String loadDivisionsV2(int orgId, int insId, int dscId, int stdId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("evaluation/loadDivisions?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("std=").append(stdId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getStandardsForMarkEntry(int orgId, int insId, int dscId, String ayrYear, String stfId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("standardDivision/getStandardsForMarkEntry?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("stf=").append(stfId).append("&");
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getStandardDivisionForMarksEntry(int orgId, int insId, int dscId, String ayrYear, String stfId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("standardDivision/getStandardDivisionForMarksEntry?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("stf=").append(stfId).append("&");
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getStudents(int orgId, int insId, int dscId, int ayrYear, String selectedStdId, String selectedDivId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("student/getStudentList?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("ayr=").append(ayrYear).append("&");
            sb.append("std=").append(selectedStdId).append("&");
            sb.append("div=").append(selectedDivId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getStaff(int orgId, int insId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("staff/getActiveStaffList?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getPredefinedTask(int orgId, int insId, int dscId, int stfId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("tasks/getMyPredefinedTasks?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            sb.append("stfId=").append(stfId);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getTaskDetails(int orgId, int insId, int dscId, int stfId, int tskId, int tssId, int assignerId, boolean isTsk) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("tasks/getTaskDetails?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            if(isTsk){
                sb.append("tskId=").append(tskId).append("&");
                sb.append("stfId=").append(stfId);
            }else {
                sb.append("tskId=").append(tskId).append("&");
                sb.append("tssId=").append(tssId).append("&");
                sb.append("stfId=").append(assignerId);
            }
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String updateTaskByMeDetails(int orgId, int insId, int dscId, int stfId, int tskId, String subject, String description, String startDate, String endDate, String progressNote, String priority, String tStatus, int evtId) {

        try {
            StringBuilder sb = new StringBuilder();
            try{
                sb.append(strMainUrl);
                sb.append("tasks/saveTaskAssignedByMe?");
                sb.append("orgId=").append(orgId).append("&");
                sb.append("insId=").append(insId).append("&");
                sb.append("dscId=").append(dscId).append("&");
                sb.append("stfId=").append(stfId).append("&");
                sb.append("tskId=").append(tskId).append("&");
                sb.append("evtId=").append(evtId).append("&");
                sb.append("tskSubject=").append(URLEncoder.encode(subject, "UTF-8")).append("&");
                sb.append("tskDescription=").append(URLEncoder.encode(description, "UTF-8")).append("&");
                sb.append("startDate=").append(startDate).append("&");
                sb.append("endDate=").append(endDate).append("&");
                sb.append("progressNote=").append(URLEncoder.encode(progressNote, "UTF-8")).append("&");
                sb.append("priorityHml=").append(priority).append("&");
                sb.append("tskStatus=").append(tStatus);
            }catch (Exception e){
                e.printStackTrace();
            }

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String updateTaskToMeDetails(int orgId, int insId, int dscId, int stfId, int tskId, int tssId, String progressNote, String percentComplete, String lastActionDate, int assignerId, boolean isTsk, int evtId) {

        try {
            StringBuilder sb = new StringBuilder();
            try{
                sb.append(strMainUrl);
                sb.append("tasks/saveTaskAssignedToMe?");
                sb.append("orgId=").append(orgId).append("&");
                sb.append("insId=").append(insId).append("&");
                sb.append("dscId=").append(dscId).append("&");
                sb.append("evtId=").append(evtId).append("&");

                if(isTsk){
                    sb.append("stfId=").append(stfId).append("&");
                }else {
                    sb.append("stfId=").append(assignerId).append("&");
                }

                sb.append("tskId=").append(tskId).append("&");
                sb.append("tssId=").append(tssId).append("&");
                sb.append("lastActionDate=").append(lastActionDate).append("&");
                sb.append("percentComplete=").append(percentComplete).append("&");
                sb.append("progressNote=").append(URLEncoder.encode(progressNote, "UTF-8"));
            }catch (Exception e){
                e.printStackTrace();
            }

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String forgotPassword(String emailId, String userName, String desCode) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("users/forgotPassword?");
            sb.append("emailId=").append(emailId).append("&");
            sb.append("userName=").append(userName).append("&");
            sb.append("dscCode=").append(desCode);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getLevelSubsForTeacher(String orgId, String insId, String dscId, String stfId, String lvlHwk) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("homework/getLevelSubsForTeacher?");
            sb.append("org=").append(orgId).append("&");
            sb.append("ins=").append(insId).append("&");
            sb.append("dsc=").append(dscId).append("&");
            sb.append("stf=").append(stfId).append("&");
            sb.append("lvlHwk=").append(lvlHwk);
            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //upload homework servlet
    public String uploadHomeworkByStaffV1(Integer orgId, Integer insId, Integer dscId,
                                          Integer stdId,
                                          Integer divId, Integer subId, Integer ayrYear, Integer hwkId, String hwkTTitle, String hwkDesc, String hwkType,
                                          String imageString, String contentType, String dueDate, Integer stfId, String flippedClassroom, String assignDate, String fileExtension, String notifyAllDivision, String notifyParents, String hwkS3Url) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strServletUrl);
            sb.append("homeworkServletV1");
            String urlW = sb.toString();
            Webservlet webservlet = new Webservlet(urlW, orgId, insId, dscId, stdId, divId, subId, ayrYear, hwkId, hwkTTitle,
                    hwkDesc, hwkType, imageString, contentType, dueDate, stfId, flippedClassroom, assignDate, fileExtension, notifyAllDivision, notifyParents, hwkS3Url);
            webservlet.start();
            try {
                webservlet.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservlet.output == null) {
                return null;
            } else {
                return webservlet.output.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //add new task servlet
    public String addNewTaskByStaff(Integer orgId, Integer insId, Integer dscId,
                                    Integer stfId,
                                    Integer ayrYear, String ptkId, String taskMeeting, String subject, String description, String creationDate,
                                    String firstScheduleDate, String endDate, String stfStu, String selectedStfId, String selectedStuId, String sendSms, String selectedStuIds, String selectedStfIds, String priorityHml, String multipleAssignmentsYn, String assignGroupTcgn, String progressNote, String percentComplete, String imageString, String ext, String contentType, String fileName, int evtId) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strServletUrl);
            sb.append("taskServlet");
            String urlW = sb.toString();
            TaskWebservlet webServlet = new TaskWebservlet(urlW, orgId, insId, dscId, stfId, ayrYear, ptkId, taskMeeting, subject, description,
                    creationDate, firstScheduleDate, endDate, stfStu, selectedStfId, selectedStuId, sendSms, selectedStuIds, selectedStfIds, priorityHml, multipleAssignmentsYn, assignGroupTcgn, progressNote, percentComplete, imageString, ext, contentType, fileName, evtId);
            webServlet.start();
            try {
                webServlet.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webServlet.output == null) {
                return null;
            } else {
                return webServlet.output.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //get teaching log for day, My Lectures details
    public String saveMeetingLinkTeachingLog(Integer orgId, Integer insId, Integer dscId, Integer stdId, Integer stfId,
                                        Integer divId, Integer ayrYear, Integer prdId, Integer subId, Integer tepPrdIndex, String telSlbBokSb, String selectedDate, String link){

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("teachingLog/saveMeetingLinkForTeachingLog?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            sb.append("stdId=").append(stdId).append("&");
            sb.append("stfId=").append(stfId).append("&");
            sb.append("divId=").append(divId).append("&");
            sb.append("ayrYear=").append(ayrYear).append("&");
            sb.append("prdId=").append(prdId).append("&");
            sb.append("subId=").append(subId).append("&");
            sb.append("tepPrdIndex=").append(tepPrdIndex).append("&");
            sb.append("telSlbBokSb=").append(telSlbBokSb).append("&");
            sb.append("scheduleTeachingDate=").append(selectedDate).append("&");
            //sb.append("link=").append(URLEncoder.encode(link, "UTF-8"));
            sb.append("link=").append(link.trim());

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String createMeetingLinkForTeachingLog(Integer orgId, Integer insId, Integer dscId, Integer stdId, Integer stfId,
                                             Integer divId, Integer ayrYear, Integer prdId, Integer subId, Integer tepPrdIndex, String telSlbBokSb, String selectedDate, String lectureStartTime, String lectureEndTime, String meetingTopic, String meetingTypeZG){

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("teachingLog/createMeetingLinkForTeachingLog?");
            sb.append("orgId=").append(orgId).append("&");
            sb.append("insId=").append(insId).append("&");
            sb.append("dscId=").append(dscId).append("&");
            sb.append("stdId=").append(stdId).append("&");
            sb.append("stfId=").append(stfId).append("&");
            sb.append("divId=").append(divId).append("&");
            sb.append("ayrYear=").append(ayrYear).append("&");
            sb.append("prdId=").append(prdId).append("&");
            sb.append("subId=").append(subId).append("&");
            sb.append("tepPrdIndex=").append(tepPrdIndex).append("&");
            sb.append("telSlbBokSb=").append(telSlbBokSb).append("&");
            sb.append("scheduleTeachingDate=").append(selectedDate).append("&");
            sb.append("lectureStartTime=").append(URLEncoder.encode(lectureStartTime, "UTF-8")).append("&");
            sb.append("lectureEndTime=").append(URLEncoder.encode(lectureEndTime, "UTF-8")).append("&");
            sb.append("meetingTopic=").append(URLEncoder.encode(meetingTopic, "UTF-8")).append("&");
            sb.append("meetingTypeZG=").append(URLEncoder.encode(meetingTypeZG, "UTF-8"));

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String saveLectureAttd(String orgId, String insId, String dscId, String divId, String stdId, String stuId, String ayrYear, String date, String prdId, String stfId, String subId, String tepPrdIndex, String startTime, String endTime, String isStudent, String dateTime) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("lecture/saveLectureAttd?");
            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&dscId=").append(dscId);
            sb.append("&divId=").append(divId);
            sb.append("&stdId=").append(stdId);
            sb.append("&stuId=").append(stuId);
            sb.append("&ayrYear=").append(ayrYear);
            sb.append("&date=").append(date);
            sb.append("&prdId=").append(prdId);
            sb.append("&stfId=").append(stfId);
            sb.append("&subId=").append(subId);
            sb.append("&tepPrdIndex=").append(tepPrdIndex);
            sb.append("&startTime=").append(startTime);
            sb.append("&endTime=").append(endTime);
            sb.append("&isStudent=").append(isStudent);
            sb.append("&joinDateTime=").append(URLEncoder.encode(dateTime, "UTF-8"));
            sb.append("&source=").append("T");

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String associateElearningContent(String orgId, String insId, String dscId, String ayrYear, String stdId, String subId, String uniId, String sunId, String usrId, String elcLink, String elcContentType, String elcDescription, String elcComment, String elcDetails, String elcContentUploadType, String elcLocation) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("elearningContentsWs/associateElearningContent?");
            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&dscId=").append(dscId);
            sb.append("&ayrYear=").append(ayrYear);
            sb.append("&stdId=").append(stdId);
            sb.append("&subId=").append(subId);
            sb.append("&uniId=").append(uniId);
            sb.append("&sunId=").append(sunId);
            sb.append("&usrId=").append(usrId);
            sb.append("&elcLink=").append(elcLink.trim());
            sb.append("&elcContentType=").append(elcContentType);
            sb.append("&elcDescription=").append(URLEncoder.encode(elcDescription, "UTF-8"));
            sb.append("&elcComment=").append(URLEncoder.encode(elcComment, "UTF-8"));
            sb.append("&elcDetails=").append(URLEncoder.encode(elcDetails, "UTF-8"));
            sb.append("&elcContentUploadType=").append(elcContentUploadType);
            sb.append("&elcLocation=").append(elcLocation);

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getElearningContents(String orgId, String insId, String dscId, String elc) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("elearningContentsWs/getElearningContents?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("elc=").append(elc);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getLatestParamsVersion2(Integer userId)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("users/setLatestLoginParameterVersion2?");
        sb.append("usrId=").append(userId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getSubjectsForStaff(String orgId, String insId, String dscId, String divId, String stdId, String ayrYear, String stfId) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("subject/getSubjectsForStaff?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("div=").append(divId).append("&");
        sb.append("std=").append(stdId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("stf=").append(stfId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getExamTypes(String orgId, String insId, String dscId, String divId, String stdId, String ayrYear, String subId) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("examSchedule/getExamTypes?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("div=").append(divId).append("&");
        sb.append("std=").append(stdId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("sub=").append(subId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getStfExamScheduleV2(String orgId, String insId, String dscId, String divId, String stdId, String ayrYear, String subId, String stfId, String ter, String ext) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("examSchedule/getStfExamScheduleV2?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("div=").append(divId).append("&");
        sb.append("std=").append(stdId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("sub=").append(subId).append("&");
        sb.append("stf=").append(stfId).append("&");
        sb.append("ter=").append(ter).append("&");
        sb.append("ext=").append(ext);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getStudentExmDocs(String orgId, String insId, String dscId, String divId, String stdId, String ayrYear, String subId, String stfId, String exm) {

        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("onlineExam/getStudentExmDocs?");
        sb.append("org=").append(orgId).append("&");
        sb.append("ins=").append(insId).append("&");
        sb.append("dsc=").append(dscId).append("&");
        sb.append("div=").append(divId).append("&");
        sb.append("std=").append(stdId).append("&");
        sb.append("ayr=").append(ayrYear).append("&");
        sb.append("sub=").append(subId).append("&");
        sb.append("stf=").append(stfId).append("&");
        sb.append("exm=").append(exm);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getStudentDocToEvaluate(String orgId, String insId, String dscId, String divId, String stdId, String stuId, String ayrYear, String subId, String exmId, String extId) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("onlineExam/getStudentDocToEvaluate?");
            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&dscId=").append(dscId);
            sb.append("&stdId=").append(stdId);
            sb.append("&subId=").append(subId);
            sb.append("&stuId=").append(stuId);
            sb.append("&ayrYear=").append(ayrYear);
            sb.append("&divId=").append(divId);
            sb.append("&exmId=").append(exmId);

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String saveEvaluatedDoc(String orgId, String insId, String dscId, String divId, String stdId, String stuId, String ayrYear, String subId, String exmId, String exdId, String evaluatedDocUrl, String marks, String comment) {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("onlineExam/saveEvaluatedDoc?");
            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&dscId=").append(dscId);
            sb.append("&stdId=").append(stdId);
            sb.append("&subId=").append(subId);
            sb.append("&stuId=").append(stuId);
            sb.append("&ayrYear=").append(ayrYear);
            sb.append("&divId=").append(divId);
            sb.append("&exmId=").append(exmId);
            sb.append("&exdId=").append(exdId);
            sb.append("&evaluatedDocUrl=").append(evaluatedDocUrl);
            sb.append("&marks=").append(marks);
            sb.append("&comment=").append(URLEncoder.encode(comment, "UTF-8"));

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getAwsS3Credentials() {

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("institute/getAwsS3Credentials?");

            Webservice WebService = new Webservice(sb.toString());
            WebService.start();
            try {
                WebService.join(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (WebService.output == null) {
                return null;
            } else {
                return WebService.output.toString();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String findEventsForTask(Integer orgId, Integer insId, Integer dscId, Integer ayrYear) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("events/findEventsForTask?");

        sb.append("org=").append(orgId);
        sb.append("&ins=").append(insId);
        sb.append("&dsc=").append(dscId);
        sb.append("&ayr=").append(ayrYear);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getExamStudentAnalysis(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, String date) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("onlineExam/getExamStudentAnalysis?");

        sb.append("orgId=").append(orgId);
        sb.append("&insId=").append(insId);
        sb.append("&dscId=").append(dscId);
        sb.append("&ayrYear=").append(ayrYear);
        sb.append("&date=").append(date);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getCategories(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stdId, Integer divId, Integer stfId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("loadCategories/getCategories?");

        sb.append("orgId=").append(orgId);
        sb.append("&insId=").append(insId);
        sb.append("&dscId=").append(dscId);
        sb.append("&ayrYear=").append(ayrYear);
        sb.append("&stdId=").append(stdId);
        sb.append("&divId=").append(divId);
        sb.append("&staffId=").append(stfId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String getSubCategories(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stdId, Integer divId, Integer stfId, Integer ectId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("loadCategories/getSubCategories?");

        sb.append("orgId=").append(orgId);
        sb.append("&insId=").append(insId);
        sb.append("&dscId=").append(dscId);
        sb.append("&ayrYear=").append(ayrYear);
        sb.append("&stdId=").append(stdId);
        sb.append("&divId=").append(divId);
        sb.append("&staffId=").append(stfId);
        sb.append("&ectId=").append(ectId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();

        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String loadEvalItems(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stdId, Integer divId, Integer stfId, Integer ectId, Integer ecsId, Integer terId) {
        StringBuilder sb = new StringBuilder();
        sb.append(strMainUrl);
        sb.append("loadCategories/loadEvalItems?");

        sb.append("orgId=").append(orgId);
        sb.append("&insId=").append(insId);
        sb.append("&dscId=").append(dscId);
        sb.append("&ayrYear=").append(ayrYear);
        sb.append("&stdId=").append(stdId);
        sb.append("&divId=").append(divId);
        sb.append("&staffId=").append(stfId);
        sb.append("&ectId=").append(ectId);
        sb.append("&ecsId=").append(ecsId);
        sb.append("&terId=").append(terId);

        Webservice webservice = new Webservice(sb.toString());
        webservice.start();
        try {
            webservice.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (webservice.output == null) {
            return null;
        } else {
            return webservice.output.toString();
        }
    }

    public String saveEvaluation(Integer orgId, Integer insId, Integer dscId, Integer ayrYear, Integer stdId, Integer divId, Integer stfId, String stfName, Integer ectId, Integer ecsId, String ectName, String ecsName, Integer terId, String sceId, Integer stuId, String evalArray, String evalDate) {

        try {

            StringBuilder sb = new StringBuilder();
            sb.append(strMainUrl);
            sb.append("loadCategories/saveEvaluation?");

            sb.append("orgId=").append(orgId);
            sb.append("&insId=").append(insId);
            sb.append("&dscId=").append(dscId);
            sb.append("&ayrYear=").append(ayrYear);
            sb.append("&stdId=").append(stdId);
            sb.append("&divId=").append(divId);
            sb.append("&stfId=").append(stfId);
            sb.append("&stfName=").append(URLEncoder.encode(stfName, "UTF-8"));
            sb.append("&ectId=").append(ectId);
            sb.append("&ecsId=").append(ecsId);
            sb.append("&ectName=").append(URLEncoder.encode(ectName, "UTF-8"));
            sb.append("&ecsName=").append(URLEncoder.encode(ecsName, "UTF-8"));
            sb.append("&terId=").append(terId);
            sb.append("&sceId=").append(sceId);
            sb.append("&stuId=").append(stuId);
            sb.append("&evalArray=").append(URLEncoder.encode(evalArray, "UTF-8"));
            sb.append("&evalDate=").append(URLEncoder.encode(evalDate, "UTF-8"));
            //URLEncoder.encode(evalDate, "UTF-8")

            Webservice webservice = new Webservice(sb.toString());
            webservice.start();
            try {
                webservice.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (webservice.output == null) {
                return null;
            } else {
                return webservice.output.toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
package com.techled.teacheranalytics.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Created by ajinkya on 23/4/15.
 */
public class Webservlet extends Thread {
    private Integer orgId, insId, dscId,
            stdId, divId, subId, ayrYear, hwkId, stfId;
    private String hwkTTitle, hwkDesc, hwkType,notifyParents,
            imageString, contentType, dueDate, flippedClassroom, assignDate, url, fileExtension, notifyAllDivision, hwkS3Url;
    private byte[] fBytes;


    public Object output;

    public Webservlet(String url, Integer orgId, Integer insId, Integer dscId,
                      Integer stdId,
                      Integer divId, Integer subId, Integer ayrYear, Integer hwkId, String hwkTTitle, String hwkDesc, String hwkType,
                      String imageString, String contentType, String dueDate, Integer stfId, String flippedClassroom, String assignDate, String fileExtension, String notifyAllDivision,String notifyParents, String hwkS3Url) {
        this.url = url;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.stdId = stdId;
        this.divId = divId;
        this.subId = subId;
        this.ayrYear = ayrYear;
        this.hwkId = hwkId;
        this.hwkTTitle = hwkTTitle;
        this.hwkDesc = hwkDesc;
        this.hwkType = hwkType;
        this.imageString = imageString;
        this.contentType = contentType;
        this.dueDate = dueDate;
        this.stfId = stfId;
        this.flippedClassroom = flippedClassroom;
        this.assignDate = assignDate;
        this.fileExtension = fileExtension;
        this.notifyAllDivision = notifyAllDivision;
        this.notifyParents = notifyParents;
        this.hwkS3Url = hwkS3Url;

    }


    public String postMultipartUrlTest() {
        String output = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            MultipartEntity entity = new MultipartEntity();
            entity.addPart("orgId", new StringBody(String.valueOf(orgId)));
            entity.addPart("insId", new StringBody(String.valueOf(insId)));
            entity.addPart("dscId", new StringBody(String.valueOf(dscId)));
            entity.addPart("stdId", new StringBody(String.valueOf(stdId)));
            entity.addPart("divId", new StringBody(String.valueOf(divId)));
            entity.addPart("subId", new StringBody(String.valueOf(subId)));
            entity.addPart("ayrYear", new StringBody(String.valueOf(ayrYear)));
            entity.addPart("hwkId", new StringBody(String.valueOf(hwkId)));
            entity.addPart("hwkTitle", new StringBody(String.valueOf(hwkTTitle), Charset.forName("UTF-8")));
            entity.addPart("hwkDesc", new StringBody(String.valueOf(hwkDesc), Charset.forName("UTF-8")));
            entity.addPart("hwkType", new StringBody(String.valueOf(hwkType)));
            entity.addPart("imageString", new StringBody(String.valueOf(imageString)));
            entity.addPart("contentType", new StringBody(String.valueOf(contentType)));
            entity.addPart("dueDate", new StringBody(String.valueOf(dueDate)));
            entity.addPart("stfId", new StringBody(String.valueOf(stfId)));
            entity.addPart("flippedClassroom", new StringBody(String.valueOf(flippedClassroom)));
            entity.addPart("assignDate", new StringBody(String.valueOf(assignDate)));
            entity.addPart("ext", new StringBody(String.valueOf(fileExtension)));
            entity.addPart("notifyAllDivisions",new StringBody(String.valueOf(notifyAllDivision)));
            entity.addPart("notifyParents",new StringBody(String.valueOf(notifyParents)));
            entity.addPart("hwkS3Url",new StringBody(String.valueOf(hwkS3Url)));
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpclient.execute(httpPost);
           /* StatusLine statusLine = httpResponse.getStatusLine();
            int code = statusLine.getStatusCode();
            if (code == 200) {
                output = "SUCCESS";
            } else {
                output = "FAILURE";
            }*/
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpResponse.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            output = s.toString();
            return output;
        } catch (Exception e) {
            e.printStackTrace();
            return output;
        }
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub

        super.run();
        output = postMultipartUrlTest();
    }
}

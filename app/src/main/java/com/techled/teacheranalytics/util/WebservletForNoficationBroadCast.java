package com.techled.teacheranalytics.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class WebservletForNoficationBroadCast extends Thread {
    private Integer orgId, insId, dscId,
            stdId, divId, subId, ayrYear, hwkId, stfId,usrId;
    private String notifiedStdName,notifiedDivName,url, usrName,informParents, informStudents, ext, notifyAllDivision,forTsd,from,to,typeNca,addToTimeline,uploadType,notificationTitle,notificationMessage,notifyAllDivisions,imageString,stfGrades,stfIds, notificationS3Url;
    private byte[] fBytes;


    public Object output;

    public WebservletForNoficationBroadCast(String url, Integer orgId, Integer insId, Integer dscId,Integer ayrYear,
                                            Integer stdId,
                                            Integer divId, String usrName,String informParents, String informStudents,String forTsd,String from,
                                             String to,String typeNca,String addToTimeline,String uploadType,
                                             String notificationTitle,String notificationMessage,
                                            String notifyAllDivision, String imageString,String ext,String stfGrades,String stfIds,String notifiedStdName,String notifiedDivName,Integer usrId, String notificationS3Url) {
        this.url = url;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.ayrYear = ayrYear;
        this.stdId = stdId;
        this.divId = divId;
        this.usrName = usrName;
        this.informParents = informParents;
        this.informStudents = informStudents;
        this.forTsd =forTsd;
        this.from =from;
        this.to = to;
        this.typeNca = typeNca;
        this.addToTimeline = addToTimeline;
        this.uploadType = uploadType;
        this.notificationTitle = notificationTitle;
        this.notificationMessage = notificationMessage;
        this.notifyAllDivision = notifyAllDivision;
        this.imageString = imageString;
        this.ext = ext;
        this.stfGrades = stfGrades;
        this.stfIds = stfIds;
        this.notifiedStdName = notifiedStdName;
        this.notifiedDivName = notifiedDivName;
        this.usrId = usrId;
        this.notificationS3Url = notificationS3Url;

        }


    public String postMultipartUrlTest() {
        String output = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            MultipartEntity entity = new MultipartEntity();
            entity.addPart("orgId", new StringBody(String.valueOf(orgId)));
            entity.addPart("insId", new StringBody(String.valueOf(insId)));
            entity.addPart("dscId", new StringBody(String.valueOf(dscId)));
            entity.addPart("ayrYear", new StringBody(String.valueOf(ayrYear)));
            entity.addPart("stdId", new StringBody(String.valueOf(stdId)));
            entity.addPart("divId", new StringBody(String.valueOf(divId)));
            entity.addPart("usrName", new StringBody(String.valueOf(usrName)));
            entity.addPart("informParents", new StringBody(String.valueOf(informParents)));
            entity.addPart("informStudentsYN", new StringBody(String.valueOf(informStudents)));
            entity.addPart("forTsd", new StringBody(String.valueOf(forTsd)));
            entity.addPart("from", new StringBody(String.valueOf(from)));
            entity.addPart("to", new StringBody(String.valueOf(to)));
            entity.addPart("typeNca", new StringBody(String.valueOf(typeNca)));
            entity.addPart("addToTimeLine", new StringBody(String.valueOf(addToTimeline)));
            entity.addPart("uploadType", new StringBody(String.valueOf(uploadType)));
            entity.addPart("notificationTitle", new StringBody(String.valueOf(notificationTitle)));
            entity.addPart("notificationMessage", new StringBody(String.valueOf(notificationMessage)));
            entity.addPart("notifyAllDivisions", new StringBody(String.valueOf(notifyAllDivisions)));
            entity.addPart("imageString", new StringBody(String.valueOf(imageString)));
            entity.addPart("ext",new StringBody(String.valueOf(ext)));
            entity.addPart("stfGrades",new StringBody(String.valueOf(stfGrades)));
            entity.addPart("stfIds",new StringBody(String.valueOf(stfIds)));
            entity.addPart("stdName",new StringBody(String.valueOf(notifiedStdName)));
            entity.addPart("divName",new StringBody(String.valueOf(notifiedDivName)));
            entity.addPart("usrId", new StringBody(String.valueOf(usrId)));
            entity.addPart("nntS3Url", new StringBody(String.valueOf(notificationS3Url)));
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpclient.execute(httpPost);

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpResponse.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder s = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            output = s.toString();
            return output;
        } catch (Exception e) {
            e.printStackTrace();
            return output;
        }
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub

        super.run();
        output = postMultipartUrlTest();
    }
}

package com.techled.teacheranalytics.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Created by ajinkya on 23/4/15.
 */
public class TaskWebservlet extends Thread {
    private int orgId, insId, dscId, stfId, ayrYear, evtId;
    private String url, ptkId, taskMeeting, subject, description, creationDate,
            firstScheduleDate, endDate, stfStu, selectedStfId, selectedStuId, sendSms, selectedStuIds, selectedStfIds, priorityHml, multipleAssignmentsYn, assignGroupTcgn, progressNote, percentComplete, imageString, ext, contentType, fileName;
    private byte[] fBytes;
    public Object output;

    public TaskWebservlet(String url, int orgId, int insId, int dscId, int stfId, int ayrYear, String ptkId, String taskMeeting, String subject, String description, String creationDate, String firstScheduleDate, String endDate, String stfStu, String selectedStfId, String selectedStuId, String sendSms, String selectedStuIds, String selectedStfIds, String priorityHml, String multipleAssignmentsYn, String assignGroupTcgn, String progressNote, String percentComplete, String imageString, String ext, String contentType, String fileName, int evtId) {
        this.url = url;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.stfId = stfId;
        this.ayrYear = ayrYear;
        this.ptkId = ptkId;
        this.taskMeeting = taskMeeting;
        this.subject = subject;
        this.description = description;
        this.creationDate = creationDate;
        this.firstScheduleDate = firstScheduleDate;
        this.endDate = endDate;
        this.stfStu = stfStu;
        this.selectedStfId = selectedStfId;
        this.selectedStuId = selectedStuId;
        this.sendSms = sendSms;
        this.selectedStuIds = selectedStuIds;
        this.selectedStfIds = selectedStfIds;
        this.priorityHml = priorityHml;
        this.multipleAssignmentsYn = multipleAssignmentsYn;
        this.assignGroupTcgn = assignGroupTcgn;
        this.progressNote = progressNote;
        this.percentComplete = percentComplete;
        this.imageString = imageString;
        this.ext = ext;
        this.contentType = contentType;
        this.fileName = fileName;
        this.evtId = evtId;
    }


    public String postMultipartUrlTest() {
        String output = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            MultipartEntity entity = new MultipartEntity();
            entity.addPart("orgId", new StringBody(String.valueOf(orgId)));
            entity.addPart("insId", new StringBody(String.valueOf(insId)));
            entity.addPart("dscId", new StringBody(String.valueOf(dscId)));
            entity.addPart("stfId", new StringBody(String.valueOf(stfId)));
            entity.addPart("ayrYear", new StringBody(String.valueOf(ayrYear)));
            entity.addPart("ptkId", new StringBody(String.valueOf(ptkId)));
            entity.addPart("taskMeeting", new StringBody(String.valueOf(taskMeeting)));
            entity.addPart("subject", new StringBody(String.valueOf(subject)));
            entity.addPart("description", new StringBody(String.valueOf(description)));
            entity.addPart("creationDate", new StringBody(String.valueOf(creationDate)));
            entity.addPart("firstScheduleDate", new StringBody(String.valueOf(firstScheduleDate)));
            entity.addPart("endDate", new StringBody(String.valueOf(endDate)));
            entity.addPart("stfStu", new StringBody(String.valueOf(stfStu)));
            entity.addPart("selectedStfId", new StringBody(String.valueOf(selectedStfId)));
            entity.addPart("selectedStuId", new StringBody(String.valueOf(selectedStuId)));
            entity.addPart("sendSms", new StringBody(String.valueOf(sendSms)));
            entity.addPart("selectedStuIds", new StringBody(String.valueOf(selectedStuIds)));
            entity.addPart("selectedStfIds", new StringBody(String.valueOf(selectedStfIds)));
            entity.addPart("priorityHml",new StringBody(String.valueOf(priorityHml)));
            entity.addPart("multipleAssignmentsYn", new StringBody(String.valueOf(multipleAssignmentsYn)));
            entity.addPart("assignGroupTcgn", new StringBody(String.valueOf(assignGroupTcgn)));
            entity.addPart("progressNote", new StringBody(String.valueOf(progressNote)));
            entity.addPart("percentComplete",new StringBody(String.valueOf(percentComplete)));
            entity.addPart("imageString",new StringBody(String.valueOf(imageString)));
            entity.addPart("ext",new StringBody(String.valueOf(ext)));
            entity.addPart("contentType",new StringBody(String.valueOf(contentType)));
            entity.addPart("fileName",new StringBody(String.valueOf(fileName)));
            entity.addPart("evtId",new StringBody(String.valueOf(evtId)));
            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpclient.execute(httpPost);
           /* StatusLine statusLine = httpResponse.getStatusLine();
            int code = statusLine.getStatusCode();
            if (code == 200) {
                output = "SUCCESS";
            } else {
                output = "FAILURE";
            }*/
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpResponse.getEntity().getContent(), StandardCharsets.UTF_8));
            String sResponse;
            StringBuilder s = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            output = s.toString();
            return output;
        } catch (Exception e) {
            e.printStackTrace();
            return output;
        }
    }

    @Override
    public void run() {
        super.run();
        output = postMultipartUrlTest();
    }
}

package com.techled.teacheranalytics.util;

import com.techled.teacheranalytics.BuildConfig;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

public class Webservice extends Thread {
    private static final String SOAP_ACTION = "http://www.naveenbalani.com/webservices/WassupAndroidService/todaysMessage";

    private static final String OPERATION_NAME = "todaysMessage";

    private static final String WSDL_TARGET_NAMESPACE = "http://www.naveenbalani.com/webservices/WassupAndroidService/";

    private static final String SOAP_ADDRESS = "http://naveenbalani.com/WassupAndroid.asmx";

    public String url;

    public Object output;

    public Webservice(String url) {
        this.url = url;
        this.url = this.url.concat("&appSrc=Android").concat("&appVersion=").concat(BuildConfig.VERSION_NAME);
    }

    public Object callRestfulWebService() {
        HttpClient httpclient = new DefaultHttpClient();

        try {
            HttpGet request = new HttpGet(url);

            ResponseHandler<String> handler = new BasicResponseHandler();
            Object result;
            try {
                result = httpclient.execute(request, handler);

            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            }
            httpclient.getConnectionManager().shutdown();

            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        output = callRestfulWebService();
        super.run();
    }
}

package com.techled.teacheranalytics;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.techled.teacheranalytics.ActivityList.DashboardModule.DashboardActivity;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LoginActivity extends AppCompatActivity {

    private EditText edtUname, edtUpassword, edtCorpId;
    private Button btnLogin;
    private String name, password;
    private Util util;
    private String result = null;
    private boolean doubleBackToExitPressedOnce;
    private ConnectivityManager connectivityManager;
    private NetworkInfo info;
    private String corpId;
    private String androidId, imei;
    private TextView childText;
    private Context context;
    private ProgressDialog progDailog;
    private String currentVersion;
    private LinearLayout layParent;

    // SERVER_URL
    public static final String PARAMS_FILE = "ParamsFile";
    public static String SERVER_URL = "-";
    private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();

    public boolean checkConnection(Context context) {
        boolean flag = false;
        try {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            info = connectivityManager.getActiveNetworkInfo();

            if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                System.out.println(info.getTypeName());
                flag = true;
            }
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                System.out.println(info.getTypeName());
                flag = true;
            }
        } catch (Exception exception) {
            System.out.println("Exception at network connection....."
                    + exception);
        }
        return flag;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {

            //check internet connection
            boolean checkConnection = checkConnection(LoginActivity.this);
            if (checkConnection) {
            } else {
                Toast.makeText(LoginActivity.this,
                        "Please check your internet connection or try again later!", Toast.LENGTH_LONG).show();
            }

            //set font
            childText = (TextView) findViewById(R.id.childText);
            Typeface face = Typeface.createFromAsset(getAssets(),
                    "fonts/handwriting.ttf");
            childText.setTypeface(face);

            //get reference
            context = this;
            edtCorpId = (EditText) findViewById(R.id.edtCorpId);
            edtUname = (EditText) findViewById(R.id.uName);
            edtUpassword = (EditText) findViewById(R.id.upassword);
            btnLogin = (Button) findViewById(R.id.btnLogin);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //get current version of app
            SharedPreferences versionPref = getSharedPreferences("VersionDetails", MODE_PRIVATE);
            currentVersion = versionPref.getString("CurrentVersion", "");

            //load data for URL
            loadSharedPreferance();

            //check permission for tracking imei
            requestPhoneStateAccessPermission();

            //check if already login
            SharedPreferences loingDetails = getSharedPreferences("user", 0);
            final String finalResult = loingDetails.getString("params", "");
            Log.d("finalResult", "" + finalResult);

            if (finalResult != null && !finalResult.isEmpty()) {
                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                intent.putExtra("params", finalResult);
                intent.putExtra("timeline", getIntent().getBooleanExtra("timeline", false));
                startActivity(intent);
            }

            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Log.w("StudentConnect", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = Objects.requireNonNull(task.getResult()).getToken();

                        SharedPreferences settings = getSharedPreferences("REG_TOKEN", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("token",token );
                        editor.commit();
                    });

            //auto login when complete action
            edtUpassword.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnLogin.performClick();
                    return true;
                }
                return false;
            });

            //login
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //get values
                    name = edtUname.getText().toString().trim();
                    password = edtUpassword.getText().toString().trim();
                    corpId = edtCorpId.getText().toString().trim();

                    //set error to edittext
                    edtCorpId.setError(null);
                    edtUname.setError(null);
                    edtUpassword.setError(null);

                    if (name.equals("") && password.equals("") && corpId.equals("")) {
                        setErrorToEditText(edtCorpId, "Please enter corporate id !");
                        setErrorToEditText(edtUname, "Please enter username !");
                        setErrorToEditText(edtUpassword, "Please enter password !");
                        return;
                    }

                    if (corpId.equals("")) {
                        setErrorToEditText(edtCorpId, "Please enter corporate id !");
                        return;
                    }

                    if (name.equals("")) {
                        setErrorToEditText(edtUname, "Please enter username !");
                        return;
                    }
                    if (password.equals("")) {
                        setErrorToEditText(edtUpassword, "Please enter password !");
                        return;
                    }

                    password = strToHex(password.getBytes());

                    Pattern pattern = Pattern.compile("\\s");
                    Matcher matcher = pattern.matcher(corpId);
                    Matcher matcher2 = pattern.matcher(name);
                    boolean found = matcher.find();
                    boolean found2 = matcher2.find();

                    if (found) {

                        Toast.makeText(context, "School code should not contain any white space.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (found2) {

                        Toast.makeText(context, "Username should not contain any white space.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //get url as per co-operate id
                    if (edtCorpId.getText().toString().trim().equalsIgnoreCase("JPPPUNE")) {
                         //SERVER_URL = "http://192.168.8.36:8080/webresources/";
                        SERVER_URL = "http://jpp.etechschoolonline.com/webresources/";
                        SharedPreferences settings = getSharedPreferences(PARAMS_FILE, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("SERVER_URL", SERVER_URL);
                        editor.commit();

                    } else {
                        SERVER_URL = "http://etechschoolonline.com/webresources/";
                        //SERVER_URL = "http://192.168.8.37:8080/webresources/";
                        //SERVER_URL = "http://192.168.8.36:8080/webresources/";
                        SharedPreferences settings = getSharedPreferences(PARAMS_FILE, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("SERVER_URL", SERVER_URL);
                        editor.commit();


                    }
                    //check permission for tracking imei
                    requestPhoneStateAccessPermission();

                    //login
                    LoginActivity.LoginUser loginUser = new LoginActivity.LoginUser();
                    loginUser.execute(null, null);


                }
            });

            //get imei from pref
            SharedPreferences imeiPref = getSharedPreferences("REG_IMEI", MODE_PRIVATE);
            if (imeiPref != null) {
                imei = imeiPref.getString("IMEI", "-");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    //Requesting permission
    private void requestPhoneStateAccessPermission() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
                return;

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_PHONE_STATE)) {
                //If the user has denied the permission previously your code will come to this block
                //Here you can explain why you need this permission
                //Explain here why you need this permission
            }
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    // permission was granted, yay! do the
                    // calendar task you need to do.
                    try {
//                        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//                            // TODO: Consider calling
//                            //    ActivityCompat#requestPermissions
//                            // here to request the missing permissions, and then overriding
//                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                            //                                          int[] grantResults)
//                            // to handle the case where the user grants the permission. See the documentation
//                            // for ActivityCompat#requestPermissions for more details.
//                            return;
//                        }
//                        //get imei
//                        imei = telephonyManager.getDeviceId();

                        imei = "1234567890";
                        Log.d("IMEI", "" + imei);

                        //store imei
                        SharedPreferences settings = getSharedPreferences("REG_IMEI", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("IMEI", imei);
                        editor.commit();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //  Toast.makeText(this, "You just denied the permission!", Toast.LENGTH_LONG).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "You just denied the permission!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }

    public void loadSharedPreferance() {
        // Load Data from Shared Preferance
        // Restore from preferences
        SharedPreferences settings = getSharedPreferences(PARAMS_FILE, 0);
        SERVER_URL = settings.getString("SERVER_URL", "-");

    }

    // Method to convert string to hexadecimal
    private String strToHex(byte[] buf) {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i) {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }


    public class LoginUser extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progDailog != null) {
                progDailog.dismiss();
            }
            try {
                if (result == null || result.equals("")) {
                    //Toast.makeText(context, "Invalid login details!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Invalid login details!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;

                }

                try {

                    JSONArray jsonArray = new JSONArray(result);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {

                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");
                        JSONObject object = array.getJSONObject(0);

                        int orgId = object.getInt("orgId");
                        int insId = object.getInt("insId");
                        int usrId = object.getInt("usrId");
                        int ugpId = object.getInt("ugpId");
                        int dscId = object.getInt("dscId");
                        String uName = object.getString("usrName");

                        // authantication only for teachers having ugpId is 3
                        if (ugpId == 3) {

                            //store login details
                            SharedPreferences loginDetails = getSharedPreferences("user", 0);
                            loginDetails.edit().putString("params", result).commit();

                            Intent intent = new Intent(context, DashboardActivity.class);
                            intent.putExtra("params", result);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            //get FCM token
                            SharedPreferences tokenDetails = getSharedPreferences("REG_TOKEN", 0);
                            androidId = tokenDetails.getString("token", "");
                            Log.d("TOKEN", "" + androidId);

                            //save data for fcm notification

                            String response = util.setAndroidAnalyticsRegId(usrId, androidId, orgId, insId);
                            Log.d("response", "" + response);

                            if (response == null) {

                                // Toast.makeText(context, "We're sorry. We were not able to save android id!", Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(layParent, "We're sorry. We were not able to save android id!!", Snackbar.LENGTH_LONG);
                                snackbar.setDuration(3000);
                                snackbar.show();
                                return;
                            }

                            //save user log

                            //imei = imei + " - " + currentVersion;
                            //String resUserLog = util.saveUserEventLog(orgId, insId, dscId, ugpId, usrId, uName, imei, "", "LOGIN", "N");

                            //continue
                            startActivity(intent);
                        } else {
                            // Toast.makeText(context, "This facility is not available with your access priviledges!", Toast.LENGTH_SHORT).show();

                            Snackbar snackbar = Snackbar
                                    .make(layParent, "This facility is not available with your access priviledges!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;

                        }


                    } else {
                        // Toast.makeText(context, "Invalid login details !", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Invalid login details!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }


                } catch (Exception e) {
                    //   Toast.makeText(context, "Invalid login details !", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Invalid login details!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    e.printStackTrace();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                util = new Util();
                result = util.authenticateUserVersion2(corpId, name, password);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    protected void setErrorToEditText(EditText editText, String errorMesssage) {
        editText.setError(errorMesssage);
        editText.requestFocus();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }
}

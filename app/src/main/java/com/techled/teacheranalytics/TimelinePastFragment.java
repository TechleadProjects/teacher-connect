package com.techled.teacheranalytics;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.adapter.TimelineRecyAdapter;
import com.techled.teacheranalytics.pojo.TimelineDataDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class TimelinePastFragment extends Fragment {
    
    private Context context;
    private Util util;
    private String params,resMyInfo;
    private int orgId, insId, dscId, ayr, usrId,usrAssetId;
    private RecyclerView timelineRecyView;
    private ProgressDialog progDailog;
    private String resTimeline;
    private ArrayList<TimelineDataDetails> timelineDataDetailsArrayList;
    private SwipeRefreshLayout swipeLay;
    private LinearLayout layDataNotFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_past_timeline, container, false);

        try {

            context = getActivity();
            util = new Util();

            swipeLay = (SwipeRefreshLayout) view.findViewById(R.id.swipeLay);
            layDataNotFound = (LinearLayout) view.findViewById(R.id.layDataNotFound);
            timelineRecyView = (RecyclerView) view.findViewById(R.id.timelineRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            timelineRecyView.setLayoutManager(layoutManager);

            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
            }


            //my info
            SharedPreferences prefsForStdDiv = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefsForStdDiv != null) {
                resMyInfo = prefsForStdDiv.getString("response", null);
            }else {
                resMyInfo = util.getMyInfo(usrId, ayr);
            }


            if (resMyInfo != null) {
                JSONArray array = new JSONArray(resMyInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");


                    if(jsonObject.getInt("payrollInsId")!= 0) {
                        insId = jsonObject.getInt("payrollInsId");
                    }
                    if(jsonObject.getInt("payrollStfId") != 0)
                    {
                        usrAssetId = jsonObject.getInt("payrollStfId");
                    }
                }


            }
            //refresh on swipe down
            swipeLay.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                   LoadTimelineData();

                    if (swipeLay.isRefreshing()) {
                        swipeLay.setRefreshing(false);
                    }
                }
            });


           //load timeline data
           LoadTimelineData();

        } catch (Exception e) {
            e.printStackTrace();
        }
       return view;
    }
    public void LoadTimelineData(){
        new TimelinePastFragment.LoadTimelineData().execute();
    }


    public static Fragment newInstance(int i, String timeline) {
        TimelinePastFragment fragmentFirst = new TimelinePastFragment();
        return fragmentFirst;
    }

    public class LoadTimelineData extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();

            try {
                if (resTimeline != null) {
                    JSONArray array = new JSONArray(resTimeline);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {
                        timelineDataDetailsArrayList = new ArrayList<>();
                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject object = jsonArray.getJSONObject(i);
                            TimelineDataDetails timelineDataDetails = new TimelineDataDetails();
                            timelineDataDetails.setStuTitle(object.getString("stuTitle"));
                            timelineDataDetails.setTilAnefhct(object.getString("tilAnefhct"));
                            timelineDataDetails.setTilDate(object.getString("tilDate"));
                            timelineDataDetails.setTilPriority(object.getString("tilPriority"));
                            timelineDataDetails.setTilS3Url(object.getString("tilS3Url"));
                            timelineDataDetails.setTilUploadedContentTypeVadi(object.getString("tilUploadedContentTypeVadi"));
                            timelineDataDetails.setTilDescription(object.getString("tilDescription"));
                            timelineDataDetails.setTilId(object.getInt("tilId"));
                            timelineDataDetails.setOldStfId(object.getInt("stuId"));
                            timelineDataDetails.setYearMonthId(object.getInt("yearMonthId"));
                            timelineDataDetails.setTilReadYn(object.getString("tilReadYn"));
                            timelineDataDetails.setAssigneeName(object.getString("asigneeName"));
                            timelineDataDetailsArrayList.add(timelineDataDetails);
                        }


                        if (timelineDataDetailsArrayList.size() != 0) {

                            //set adapter
                            layDataNotFound.setVisibility(View.GONE);
                            swipeLay.setVisibility(View.VISIBLE);
                            TimelineRecyAdapter adapter = new TimelineRecyAdapter(timelineDataDetailsArrayList, context,orgId,insId,usrAssetId);
                            timelineRecyView.setAdapter(adapter);

                        } else {

                            layDataNotFound.setVisibility(View.VISIBLE);
                            swipeLay.setVisibility(View.GONE);
                           // Toast.makeText(context, "No read timeline message!", Toast.LENGTH_LONG).show();
                            Snackbar snackbar = Snackbar
                                    .make(swipeLay, "No read timeline message!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }
                    } else {

                        layDataNotFound.setVisibility(View.VISIBLE);
                        swipeLay.setVisibility(View.GONE);
                      //  Toast.makeText(context, "No read timeline message!", Toast.LENGTH_LONG).show();
                        Snackbar snackbar = Snackbar
                                .make(swipeLay, "No read timeline message!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }


                } else {
                    layDataNotFound.setVisibility(View.VISIBLE);
                    swipeLay.setVisibility(View.GONE);
                    //Toast.makeText(context, "No read timeline message!", Toast.LENGTH_LONG).show();
                    Snackbar snackbar = Snackbar
                            .make(swipeLay, "No read timeline message!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resTimeline = util.getTimelineData(orgId, insId, "E", usrAssetId,"Y");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }





}

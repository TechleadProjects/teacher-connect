package com.techled.teacheranalytics;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import com.techled.teacheranalytics.util.TypefaceUtil;

import org.jsoup.Jsoup;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends AppCompatActivity {

    private ConnectivityManager connectivityManager;
    private NetworkInfo info;
    private Context context;
    private TextView childText;
    private boolean doubleBackToExitPressedOnce;
    private String currentVersion;

    private class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {

            String newVersion = null;

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashScreenActivity.this.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();

                return newVersion;

            } catch (Exception e) {

                return newVersion;

            }

        }

        @Override

        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            try {
                if (onlineVersion != null && !onlineVersion.isEmpty()) {

                    if (!(currentVersion).equals(onlineVersion)) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
                        builder.setTitle("New Version Available");
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage("There is a new version available for download!");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();

                                try {
                                    //exit from app
                                    finish();

                                    //update
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        builder.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                //continue execution
                                Timer t = new Timer();
                                t.schedule(new splash(), 3000);
                            }
                        });


                        builder.show();

                    } else {

                        //continue execution
                        Timer t = new Timer();
                        t.schedule(new splash(), 3000);
                    }

                } else {

                    finish();

                    Timer t = new Timer();
                    t.schedule(new splash(), 500);
                }

                Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            //set font to whole application
            TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/text_font.ttf");

            setContentView(R.layout.activity_splash);
            context = this;

            //get current version of app
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

            //store version
            SharedPreferences.Editor versionPref = getSharedPreferences("VersionDetails", MODE_PRIVATE).edit();
            versionPref.putString("CurrentVersion", currentVersion);
            versionPref.commit();

            //set font
            childText = (TextView) findViewById(R.id.childText);
            Typeface face = Typeface.createFromAsset(getAssets(),
                    "fonts/handwriting.ttf");
            childText.setTypeface(face);

            //check connection
            Timer t = new Timer();
            boolean checkConnection = checkConnection(this);
            if (checkConnection) {

                //compare version name and force to update
                SplashScreenActivity.GetVersionCode getVersionCode = new SplashScreenActivity.GetVersionCode();
                getVersionCode.execute().get();
            } else {

                Toast.makeText(SplashScreenActivity.this,
                        "Please check your internet connection or try again later!", Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    class splash extends TimerTask {
        @Override
        public void run() {

            boolean isTimeline = false;
            Intent i = getIntent();
            if(i.getExtras() != null && i.getExtras().containsKey("timeline")) {
                isTimeline = true;
            }
            //continue login
            Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
            intent.putExtra("timeline", isTimeline);
            startActivity(intent);
        }
    }

    public boolean checkConnection(Context context) {
        boolean flag = false;
        try {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            info = connectivityManager.getActiveNetworkInfo();

            if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                System.out.println(info.getTypeName());
                flag = true;
            }
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                System.out.println(info.getTypeName());
                flag = true;
            }
        } catch (Exception exception) {
            System.out.println("Exception at network connection....."
                    + exception);
        }
        return flag;
    }

}

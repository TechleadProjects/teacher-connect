package com.techled.teacheranalytics.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.SyllabusModule.AddNewUnitActivity;
import com.techled.teacheranalytics.ActivityList.SyllabusModule.SubUnitsDetailsActivity;
import com.techled.teacheranalytics.ActivityList.SyllabusModule.SyllabusHomeActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.SyllabusDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SyllabusDetailsRecyAdapter extends RecyclerView.Adapter<SyllabusDetailsRecyAdapter.SyllabusDetailsHolder> {

    private ArrayList<SyllabusDetails> syllabusDetailsArrayList;
    private Context context;
    private Util util;
    private ProgressDialog progDailog;
    private int orgId, insId, dscId, ayr, stdId, subId, unitId;

    public SyllabusDetailsRecyAdapter(Integer orgId, Integer insId, Integer dscId, Integer ayr, Integer stdId, Integer subId, ArrayList<SyllabusDetails> syllabusDetailsArrayList, Context context) {
        this.syllabusDetailsArrayList = syllabusDetailsArrayList;
        this.context = context;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.ayr = ayr;
        this.stdId = stdId;
        this.subId = subId;
    }

    public class SyllabusDetailsHolder extends RecyclerView.ViewHolder {
        private TextView txtUnitNumber, txtUnitDesc, txtFromDate, txtToDate;
        private Button btnEdit, btnDelete, btnSubUnits;

        public SyllabusDetailsHolder(@NonNull View itemView) {
            super(itemView);

            txtUnitNumber = (TextView) itemView.findViewById(R.id.txtUnitNumber);
            txtUnitDesc = (TextView) itemView.findViewById(R.id.txtUnitDesc);
            txtFromDate = (TextView) itemView.findViewById(R.id.txtFromDate);
            txtToDate = (TextView) itemView.findViewById(R.id.txtToDate);
            btnEdit = (Button) itemView.findViewById(R.id.btnEdit);
            btnDelete = (Button) itemView.findViewById(R.id.btnDelete);
            btnSubUnits = (Button) itemView.findViewById(R.id.btnSubUnits);

        }
    }

    @NonNull
    @Override
    public SyllabusDetailsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_syllabus_details, viewGroup, false);
        SyllabusDetailsHolder holder = new SyllabusDetailsHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SyllabusDetailsHolder syllabusDetailsHolder, int i) {

        final SyllabusDetails syllabusDetails = syllabusDetailsArrayList.get(i);
        util = new Util();


        syllabusDetailsHolder.txtUnitNumber.setText("Unit Number: " + syllabusDetails.getUniNumber());


        //Description
        if (syllabusDetails.getUniDescription() == null || syllabusDetails.getUniDescription().equals("")) {
            syllabusDetailsHolder.txtUnitDesc.setText("Description: -");
        } else {
            syllabusDetailsHolder.txtUnitDesc.setText(syllabusDetails.getUniDescription());

        }

        //from date
        if (syllabusDetails.getFromDate() == null || syllabusDetails.getFromDate().equals("")) {
            syllabusDetailsHolder.txtFromDate.setText("From Date: - ");
        } else {
            syllabusDetailsHolder.txtFromDate.setText("From Date: " + syllabusDetails.getFromDate());
        }

        //to date
        if (syllabusDetails.getToDate() == null || syllabusDetails.getToDate().equals("")) {
            syllabusDetailsHolder.txtToDate.setText("To Date: - ");
        } else {
            syllabusDetailsHolder.txtToDate.setText("To Date: " + syllabusDetails.getToDate());
        }

        //edit
        syllabusDetailsHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(context, AddNewUnitActivity.class);
                    intent.putExtra("stdId",stdId);
                    intent.putExtra("ayr",ayr);
                    intent.putExtra("subId",subId);
                    intent.putExtra("unitNumber", syllabusDetails.getUniNumber());
                    intent.putExtra("unitDesc", syllabusDetails.getUniDescription());
                    intent.putExtra("fromDate", syllabusDetails.getFromDate());
                    intent.putExtra("toDate", syllabusDetails.getToDate());
                    intent.putExtra("unitId", syllabusDetails.getUniId());
                    intent.putExtra("editable", "Y");
                    intent.putExtra("showActive",syllabusDetails.getShowActiveYn());
                    intent.putExtra("showContent",syllabusDetails.getShowContentYn());
                    intent.putExtra("count",syllabusDetails.getCount());


                    ((Activity) context).startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //delete
        syllabusDetailsHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                unitId = syllabusDetails.getUniId();
                DeleteUnit deleteUnit = new DeleteUnit();
                deleteUnit.execute(null, null);
            }
        });


        //sub units
        syllabusDetailsHolder.btnSubUnits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SubUnitsDetailsActivity.class);
                intent.putExtra("unitNumber", syllabusDetails.getUniNumber());
                intent.putExtra("unitDesc", syllabusDetails.getUniDescription());
                intent.putExtra("fromDate", syllabusDetails.getFromDate());
                intent.putExtra("toDate", syllabusDetails.getToDate());
                intent.putExtra("unitId", syllabusDetails.getUniId());
                intent.putExtra("ayr", ayr);
                intent.putExtra("subId", subId);
                intent.putExtra("stdId", stdId);
                intent.putExtra("unitDesc", syllabusDetails.getUniDescription());
                ((Activity) context).startActivity(intent);
            }
        });
    }

    public class DeleteUnit extends AsyncTask<String, String, String> {
        private String resSave;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {
                        Toast.makeText(context, "Unit details deleted successfully!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, SyllabusHomeActivity.class);
                        ((Activity) context).startActivity(intent);
                        ((Activity) context).finish();
                    } else {
                        Toast.makeText(context, "Failed to delete the unit record!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Toast.makeText(context, "Failed to delete the unit record!", Toast.LENGTH_SHORT).show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                resSave = util.deleteUnitSyallabusDetails(orgId, insId, dscId, ayr,
                        stdId, subId, unitId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return syllabusDetailsArrayList.size();
    }
}

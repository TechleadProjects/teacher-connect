package com.techled.teacheranalytics.adapter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.StudentSelectionActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.TimelineReadStatusActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadHomeworkActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.ViewHomeworkActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.EmailLog;
import com.techled.teacheranalytics.pojo.HomeworkDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by sonal on 2/12/15.
 */
public class HomeworkDetailsRecyclerAdapter extends RecyclerView
        .Adapter<HomeworkDetailsRecyclerAdapter
        .DataObjectHolder> {
    private ArrayList<HomeworkDetails> homeworkDetailsArrayList;
    private Dialog dialog;
    private Context context;
    private String hwkTitle, hwkSubType;
    private int orgId, insId, dscId, stdId, divId, ayr, hwkId;
    private Util util;
    private ProgressDialog progressDialog;
    private String resNotifyParentsAbtHwk;
    private String resTimReadStatus;
    private String tilIdentifier, assignedDate;

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private TextView txtNotifyParents, txtReadStatus, txtClass, txtSubType, txtAssDate, txtDueDate, txtLink, txtTitle, txtSubject, txtEvaluate;
        private LinearLayout layOption;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txtReadStatus = (TextView) itemView.findViewById(R.id.txtReadStatus);
            txtClass = (TextView) itemView.findViewById(R.id.txtClass);
            txtSubType = (TextView) itemView.findViewById(R.id.txtSubType);
            txtAssDate = (TextView) itemView.findViewById(R.id.txtAssDate);
            txtDueDate = (TextView) itemView.findViewById(R.id.txtDueDate);
            txtLink = (TextView) itemView.findViewById(R.id.txtLink);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSubject = (TextView) itemView.findViewById(R.id.txtSubject);
            txtNotifyParents = (TextView) itemView.findViewById(R.id.txtNotifyParents);
            layOption = (LinearLayout) itemView.findViewById(R.id.layOption);
            txtEvaluate = (TextView) itemView.findViewById(R.id.txtEvaluate);
        }

    }


    public HomeworkDetailsRecyclerAdapter(ArrayList<HomeworkDetails> homeworkDetailsArrayList, Context context, Integer orgId, Integer insId, Integer dscId, Integer stdId, Integer divId, Integer ayr) {
        this.homeworkDetailsArrayList = homeworkDetailsArrayList;
        this.context = context;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.stdId = stdId;
        this.divId = divId;
        this.ayr = ayr;
        //dialog = new Dialog(context);
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_homework_details, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, int position) {
        try {
            final HomeworkDetails homeworkDetails = homeworkDetailsArrayList.get(position);
            util = new Util();
            if(homeworkDetails.getAddToTimeline().equals("") || homeworkDetails.getAddToTimeline() == null){
                holder.layOption.setVisibility(View.GONE);
            }else {
                holder.layOption.setVisibility(View.VISIBLE);
            }

//            if (homeworkDetails.getAddToTimeline().equals("N")) {
//
//                holder.txtNotifyParents.setVisibility(View.VISIBLE);
//                holder.txtReadStatus.setVisibility(View.GONE);
//            } else {
//
//                holder.txtNotifyParents.setVisibility(View.GONE);
//                holder.txtReadStatus.setVisibility(View.VISIBLE);
//            }


            //class
            if (homeworkDetails.getStdName() == null || homeworkDetails.getStdName().equals("") || homeworkDetails.getStdName().equals("null")) {
                holder.txtClass.setText("Class: - ");
            } else {
                holder.txtClass.setText("Class: " + homeworkDetails.getStdName());
            }


            //assigned date
            if (homeworkDetails.getHwkAssDate() == null || homeworkDetails.getHwkAssDate().equals("") || homeworkDetails.getHwkAssDate().equals("null")) {
                holder.txtAssDate.setText("Assigned Date: - ");
            } else {
                holder.txtAssDate.setText("Assigned Date: " + homeworkDetails.getHwkAssDate());
            }

            //subject
            if (homeworkDetails.getHwkSubject() == null || homeworkDetails.getHwkSubject().equals("null") || homeworkDetails.getHwkSubject().equals("")) {
                holder.txtSubject.setText("Subject: -");

            } else {
                holder.txtSubject.setText("Subject: " + homeworkDetails.getHwkSubject());
            }

            //due date
            if (homeworkDetails.getHwkDueDate() == null || homeworkDetails.getHwkDueDate().equals("") || homeworkDetails.getHwkDueDate().equals("null")) {
                holder.txtDueDate.setText("Due Date: - ");

            } else {
                holder.txtDueDate.setText("Due Date: " + homeworkDetails.getHwkDueDate());
            }

            //link
            if (homeworkDetails.getHwkURL() == null || homeworkDetails.getHwkURL().equals("null") || homeworkDetails.getHwkURL().equals("")) {
                holder.txtLink.setVisibility(View.GONE);
            } else {
                holder.txtLink.setVisibility(View.VISIBLE);
                holder.txtLink.setText("Attachment: " + homeworkDetails.getHwkURL());
            }

            //title
            if (homeworkDetails.getHwkTitle() == null || homeworkDetails.getHwkTitle().equals("null") || homeworkDetails.getHwkTitle().equals("")) {
                holder.txtTitle.setText("Title: -");
            } else {
                holder.txtTitle.setText("Title: " + homeworkDetails.getHwkTitle());
            }

            //sub type
            if (homeworkDetails.getSubType() == null || homeworkDetails.getSubType().equals("null") || homeworkDetails.getSubType().equals("")) {
                holder.txtSubType.setText("Submission Type: -  ");
            } else {
                holder.txtSubType.setText("Submission Type: " + homeworkDetails.getSubType());
            }

            //view attachment
            holder.txtLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (homeworkDetails.getHwkURL().equals("null") || homeworkDetails.getHwkURL() == null || homeworkDetails.getHwkURL().equals(" ") || homeworkDetails.getHwkURL().equals("")) {
                            Toast.makeText(context, "Unable to load attachment!", Toast.LENGTH_SHORT).show();

                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(homeworkDetails.getHwkURL()));
                            context.startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(context, "Unable to load attachment!", Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            });

            //
            holder.txtReadStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        //read status
                        assignedDate = homeworkDetails.getHwkAssDate();
                        tilIdentifier = homeworkDetails.getTilIndentifier();

                        new ViewReadStatus().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            //
            holder.txtEvaluate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        Gson gson = new Gson();
                        String json = gson.toJson(homeworkDetails);

                        Intent intent = new Intent(context, StudentSelectionActivity.class);
                        intent.putExtra("homeworkObject", json);
                        context.startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            //notify parents
            holder.txtNotifyParents.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        stdId = homeworkDetails.getStdId();
                        divId = homeworkDetails.getDivId();
                        hwkId = homeworkDetails.getHwkId();

                        //upload homework

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                        // Setting Dialog Title
                        alertDialog.setTitle("Notify Parents...");

                        // Setting Dialog Message
                        alertDialog.setMessage("Are you sure you want to notify parents?");

                        // Setting Icon to Dialog
                        alertDialog.setIcon(R.drawable.notification);

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                //notify parents
                                new NotifyParents().execute();
                            }
                        });

                        // Setting Negative "NO" Button
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to invoke NO event
                                dialog.cancel();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ViewReadStatus extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                if (resTimReadStatus != null) {
                    Intent intent = new Intent(context, TimelineReadStatusActivity.class);
                    intent.putExtra("ReadStatus", resTimReadStatus);
                    ((Activity) context).startActivity(intent);

                } else {
                    Toast.makeText(context, "Failed to get read status!", Toast.LENGTH_SHORT).show();
                    util.SaveSystemLog(orgId,insId,"Failed to get read status of homework"+tilIdentifier,"Teacher Connect",0,"Homework");
                    return;
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while reading status of homework"+tilIdentifier,"Teacher Connect",0,"Homework");
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                resTimReadStatus = util.getTimelineReadDetails(orgId, insId, ayr,"S", tilIdentifier, assignedDate, "H");

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while reading status of homework"+tilIdentifier,"Teacher Connect",0,"Homework");
            }
            return null;
        }
    }

    public class NotifyParents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
//            progDailog.setIndeterminate(false);
            progressDialog.show();


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                if (resNotifyParentsAbtHwk == null || resNotifyParentsAbtHwk.equals("")) {
                    Toast.makeText(context, "Failed notify parents!", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(context, HomeworkDetails.class);
                    ((Activity) context).startActivity(intent);
                    ((Activity) context).finish();
                    util.SaveSystemLog(orgId,insId,"Failed to Notify parents about homework"+tilIdentifier,"Teacher Connect",0,"Homework");
                    return;
                } else {
                    JSONArray jsonArray = new JSONArray(resNotifyParentsAbtHwk);
                    JSONObject statusObj = jsonArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {
                        Toast.makeText(context, "Successfully notified parents!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, ViewHomeworkActivity.class);
                        ((Activity) context).startActivity(intent);
                        ((Activity) context).finish();
                        return;

                    } else {
                        Toast.makeText(context, "Failed notify parents!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, ViewHomeworkActivity.class);
                        ((Activity) context).startActivity(intent);
                        ((Activity) context).finish();
                        util.SaveSystemLog(orgId,insId,"Failed to Notify parents about homework"+tilIdentifier,"Teacher Connect",0,"Homework");
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while Notify parents about homework"+tilIdentifier,"Teacher Connect",0,"Homework");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resNotifyParentsAbtHwk = util.notifyParentsAboutHomework(orgId, insId, dscId, stdId, divId, ayr, hwkId);


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while Notify parents about homework"+tilIdentifier,"Teacher Connect",0,"Homework");
            }
            return null;
        }
    }


    @Override
    public int getItemCount() {
        return homeworkDetailsArrayList.size();
    }


}

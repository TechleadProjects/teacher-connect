package com.techled.teacheranalytics.adapter;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.AttendanceManagementDetails;
import com.techled.teacheranalytics.pojo.MarkAttendance;
import com.techled.teacheranalytics.pojo.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonal on 1/3/18.
 */

public class AttendanceManagementRecyAdapter2 extends RecyclerView.Adapter<AttendanceManagementRecyAdapter2.AttMagtHolder> {
    private Context context;
    private ArrayList<MarkAttendance> managementDetailsArrayList;
    final MarkAttendance markAttendance = new MarkAttendance();
    private ArrayList<MarkAttendance> list = null;
    private Dialog dialog = null;
    private String date;
    private String comment;
    private boolean flagA= true,flagP=true;

    private static RecyclerViewClickListener itemListener;

    public AttendanceManagementRecyAdapter2(Context context, ArrayList<MarkAttendance> managementDetailsArrayList, String date) {
        this.context = context;
        this.managementDetailsArrayList = managementDetailsArrayList;
        this.date = date;


    }

    public class AttMagtHolder extends RecyclerView.ViewHolder {
        private TextView txtStuName;
        private Button btnPresent, btnLeave, btnAbsent, btnSickLeave, btnExtraCurricular;
        private ImageView imgComment;



        public AttMagtHolder(View itemView) {
            super(itemView);
            txtStuName = (TextView) itemView.findViewById(R.id.txtStuName);
            btnPresent = (Button) itemView.findViewById(R.id.btnPresent);
            btnLeave = (Button) itemView.findViewById(R.id.btnLeave);
            btnAbsent = (Button) itemView.findViewById(R.id.btnAbsent);
            btnSickLeave = (Button) itemView.findViewById(R.id.btnSickLeave);
            btnExtraCurricular = (Button) itemView.findViewById(R.id.btnExtraCurricular);
            imgComment = (ImageView) itemView.findViewById(R.id.imgComment);

        }
    }

    @Override
    public AttMagtHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_attendance_mark, parent, false);
        AttMagtHolder holder = new AttMagtHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final AttMagtHolder holder, final int position) {

        final MarkAttendance managementDetails = managementDetailsArrayList.get(position);
       // holder.txtStuName.setText(managementDetails.getStuName());
        holder.txtStuName.setText("("+managementDetails.getRollNo()+") "+managementDetails.getStuName());
        if(managementDetails.getFlagPAL().equals("A"))
        {
            holder.btnAbsent.setBackgroundResource(R.color.red);
        }else {
            holder.btnAbsent.setBackgroundResource(R.color.lightgrey);
        }
        if(managementDetails.getFlagPAL().equals("P")) {
            holder.btnPresent.setBackgroundResource(R.color.green);
        }else {
            holder.btnPresent.setBackgroundResource(R.color.lightgrey);
        }
        if(managementDetails.getFlagPAL().equals("L")) {
            holder.btnLeave.setBackgroundResource(R.color.orange);
        }else {
            holder.btnLeave.setBackgroundResource(R.color.lightgrey);
        }
        if(managementDetails.getFlagPAL().equals("S")) {
            holder.btnSickLeave.setBackgroundResource(R.color.blue);
        }else {
            holder.btnSickLeave.setBackgroundResource(R.color.lightgrey);
        }
        if(managementDetails.getFlagPAL().equals("X")) {
            holder.btnExtraCurricular.setBackgroundResource(R.color.gray);
        }else {
            holder.btnExtraCurricular.setBackgroundResource(R.color.lightgrey);
        }



        //set absent
        holder.btnAbsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));
                holder.btnAbsent.setBackgroundResource(R.color.red);
                holder.btnAbsent.setTextColor(Color.WHITE);
                holder.btnLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnPresent.setBackgroundResource(R.color.lightgrey);
                holder.btnSickLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnExtraCurricular.setBackgroundResource(R.color.lightgrey);
                managementDetailsArrayList.get(position).setFlagPAL("A");
                managementDetailsArrayList.get(position).setComment("");



            }
        });

        //set present
        holder.btnPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));
                holder.btnPresent.setBackgroundResource(R.color.green);
                holder.btnPresent.setTextColor(Color.WHITE);
                holder.btnLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnAbsent.setBackgroundResource(R.color.lightgrey);
                holder.btnSickLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnExtraCurricular.setBackgroundResource(R.color.lightgrey);
                managementDetailsArrayList.get(position).setFlagPAL("P");
                managementDetailsArrayList.get(position).setComment("");

                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));

            }
        });

        //set leave
        holder.btnLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.btnPresent.setBackgroundResource(R.color.lightgrey);
                holder.btnLeave.setBackgroundResource(R.color.orange);
                holder.btnLeave.setTextColor(Color.WHITE);
                holder.btnAbsent.setBackgroundResource(R.color.lightgrey);
                holder.btnSickLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnExtraCurricular.setBackgroundResource(R.color.lightgrey);
                managementDetailsArrayList.get(position).setFlagPAL("L");
                managementDetailsArrayList.get(position).setComment("");
                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));

            }
        });

        //set sick leave
        holder.btnSickLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));
                holder.btnSickLeave.setBackgroundResource(R.color.blue);
                holder.btnSickLeave.setTextColor(Color.WHITE);
                holder.btnPresent.setBackgroundResource(R.color.lightgrey);
                holder.btnLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnAbsent.setBackgroundResource(R.color.lightgrey);
                holder.btnExtraCurricular.setBackgroundResource(R.color.lightgrey);
                managementDetailsArrayList.get(position).setFlagPAL("S");
                managementDetailsArrayList.get(position).setComment("");

                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));
            }
        });

        //set extra curricular activity
        holder.btnExtraCurricular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));
                holder.btnExtraCurricular.setBackgroundResource(R.color.gray);
                holder.btnExtraCurricular.setTextColor(Color.WHITE);
                holder.btnLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnAbsent.setBackgroundResource(R.color.lightgrey);
                holder.btnSickLeave.setBackgroundResource(R.color.lightgrey);
                holder.btnPresent.setBackgroundResource(R.color.lightgrey);
                managementDetailsArrayList.get(position).setFlagPAL("X");
                managementDetailsArrayList.get(position).setComment("");

                holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment1));

            }
        });

        //add comment
        holder.imgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(  managementDetailsArrayList.get(position).getFlagPAL().equals("L")) {
                    holder.imgComment.setImageDrawable(context.getResources().getDrawable(R.drawable.comment2));

                    dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_add_comment_againt_attenadance);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    dialog.setCanceledOnTouchOutside(false);

                    TextView txtDate = (TextView) dialog.findViewById(R.id.txtDate);
                    TextView txtStuName = (TextView) dialog.findViewById(R.id.txtStuName);
                    TextView txtStatus = (TextView) dialog.findViewById(R.id.txtStatus);

                    txtDate.setText(""+date);
                    txtStuName.setText(""+managementDetailsArrayList.get(position).getStuName());
                    txtStatus.setText("Mark Late In");
                    final EditText edtComment = (EditText) dialog.findViewById(R.id.edtComment);
                    Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            comment = edtComment.getText().toString().trim();
                            if (comment == null || comment.equals("")) {
                                Toast.makeText(context, "Please add comment to mark attendance!", Toast.LENGTH_SHORT).show();

                                return;
                            } else {
                                dialog.dismiss();

                                managementDetailsArrayList.get(position).setComment(comment);
                            }

                        }
                    });
                }else {
                    Toast.makeText(context, "Remark can only be made for Mark Late In!", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return managementDetailsArrayList.size();
    }


    // method to access in activity after updating selection
    public List<MarkAttendance> getList() {
        return managementDetailsArrayList;
    }
}


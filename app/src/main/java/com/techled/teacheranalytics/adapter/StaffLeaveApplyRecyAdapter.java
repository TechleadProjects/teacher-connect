package com.techled.teacheranalytics.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.ActivityList.StaffLeaveApplyModule.StaffLeaveApplicationActivity;
import com.techled.teacheranalytics.pojo.StaffLeaveBalance;

import java.util.ArrayList;

/**
 * Created by sonal on 7/3/18.
 */

public class StaffLeaveApplyRecyAdapter extends RecyclerView.Adapter<StaffLeaveApplyRecyAdapter.StaffLeaveDataHolder> {
    private ArrayList<StaffLeaveBalance> balanceArrayList;
    private Context context;
    private String leaveBalance;

    public StaffLeaveApplyRecyAdapter(ArrayList<StaffLeaveBalance> balanceArrayList, Context context) {
        this.balanceArrayList = balanceArrayList;
        this.context = context;
    }

    public  class StaffLeaveDataHolder extends RecyclerView.ViewHolder
    {
        private TextView leaveName,leaveType,curDebit,curCredit,
                totalCredit,applied,encash,leaveBalance;
        private Button btnApply;
        public StaffLeaveDataHolder(View itemView) {
            super(itemView);
            btnApply = (Button) itemView.findViewById(R.id.btnApply);
            leaveName = (TextView) itemView.findViewById(R.id.leaveName);
            leaveType = (TextView) itemView.findViewById(R.id.leaveType);
            curDebit = (TextView) itemView.findViewById(R.id.curDebit);
            curCredit = (TextView) itemView.findViewById(R.id.curCredit);

            applied = (TextView) itemView.findViewById(R.id.applied);
            encash = (TextView) itemView.findViewById(R.id.encash);
            leaveBalance = (TextView) itemView.findViewById(R.id.leaveBalance);

        }
    }
    @Override
    public StaffLeaveDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.leavebalance_row,parent,false);
        StaffLeaveDataHolder holder = new StaffLeaveDataHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(StaffLeaveDataHolder holder, int position) {
        final StaffLeaveBalance asn = balanceArrayList.get(position);
        try {
            holder.leaveName.setText("Leave Type : " + asn.getName());
            holder.leaveBalance.setText("Leave Balance : " + asn.getLeaveBalance());
            holder.curDebit.setText("Leave Taken : " + asn.getCurrentDebit());
            holder.encash.setText("Encash : " + asn.getEncash());
            holder.curCredit.setText("Current Credit : " + asn.getCurCredit());
            holder.applied.setText("Leave Applied : " + asn.getApplied());

            leaveBalance = holder.leaveBalance.getText().toString();
            String leaveBalance = asn.getBalance();
            if (leaveBalance == null || leaveBalance.equals("0.0") && asn.getCreditEventCe().equals("C")) {
                holder.btnApply.setBackgroundResource(R.drawable.rounded_shape_white);
                holder.btnApply.setTextColor(Color.BLACK);

             }
            if ( leaveBalance.equals("0.0") && asn.getCreditEventCe().equals("E")) {
                holder.leaveBalance.setVisibility(View.GONE);
            }
                holder.btnApply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //	Toast.makeText(context, "coming soon", Toast.LENGTH_SHORT).show();
                    try {

                        String  leaveBalance = asn.getBalance();
                        asn.getCreditEventCe();
                        if (leaveBalance == null || leaveBalance.equals("0.0") && asn.getCreditEventCe().equals("C")) {
                            String msg = "You can't apply a leave because Leave balance for " + asn.getName() + " is 0.";
                            Toast.makeText(context, msg, Toast.LENGTH_LONG)
                                    .show();
                            return;
                        }


                        Bundle b = new Bundle();
                        b.putInt("lvrId", asn.getLvrId());
                        b.putString("lvrName", asn.getName());
                        b.putString("creditCe", asn.getCreditEventCe());
                        b.putString("halfDay", asn.getHalfDayYn());
                        b.putString("woffHDay", asn.getWoffHDay());
                        b.putString("leaveBalance",  asn.getBalance());

                        Intent intent = new Intent(context,
                                StaffLeaveApplicationActivity.class);
                        intent.putExtras(b);
                        context.startActivity(intent);
                        ((Activity)context).finish();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            String creditCe = asn.getCreditEventCe();
            int ce = 0;
            if (creditCe.equals("C")) {
                ce = 0;
            } else {
                ce = 1;
            }
            String leaveType = "";
            switch (ce) {
                case 0:
                    leaveType = "Leave Creditable";
                    break;
                case 1:
                    leaveType = "Event Based (Not Credited)";
                    break;
            }
            holder.leaveType.setText("Type: " + leaveType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public int getItemCount() {
        return balanceArrayList.size();
    }
}

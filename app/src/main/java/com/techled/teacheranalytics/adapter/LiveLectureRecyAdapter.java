package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.LiveLecture;

import java.util.ArrayList;

public class LiveLectureRecyAdapter extends RecyclerView.Adapter<LiveLectureRecyAdapter.LiveLectureDetailsHolder> {


    private ArrayList<LiveLecture> liveLectureArrayList;
    private Context context;

    public LiveLectureRecyAdapter(ArrayList<LiveLecture> liveLectureArrayList, Context context) {
        this.liveLectureArrayList = liveLectureArrayList;
        this.context = context;
    }

    public class LiveLectureDetailsHolder extends RecyclerView.ViewHolder {
        private TextView txtLlectureStdDiv, txtLlectureSubject, txtLlectureTeacher, txtLlectureTimings;

        public LiveLectureDetailsHolder(View itemView) {


            super(itemView);
            txtLlectureStdDiv = (TextView) itemView.findViewById(R.id.txtLlectureStdDiv);
            txtLlectureSubject = (TextView) itemView.findViewById(R.id.txtLlectureSubject);
            txtLlectureTeacher = (TextView) itemView.findViewById(R.id.txtLlectureTeacher);
            txtLlectureTimings = (TextView) itemView.findViewById(R.id.txtLlectureTimings);

        }
    }

    @Override
    public LiveLectureDetailsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_live_lecture_details,parent,false);
        LiveLectureDetailsHolder holder = new LiveLectureDetailsHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LiveLectureDetailsHolder holder, int position) {
        LiveLecture liveLecture = liveLectureArrayList.get(position);
        holder.txtLlectureStdDiv.setText(liveLecture.getStdName() + " (" + liveLecture.getDivName() + ")");
        holder.txtLlectureSubject.setText(liveLecture.getSubName());
        holder.txtLlectureTeacher.setText(liveLecture.getStfName());
        holder.txtLlectureTimings.setText(liveLecture.getStartTime() + "-" + liveLecture.getEndTime());


    }

    @Override
    public int getItemCount() {
        return liveLectureArrayList.size();
    }
}

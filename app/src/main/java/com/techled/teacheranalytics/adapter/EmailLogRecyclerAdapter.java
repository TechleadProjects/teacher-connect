package com.techled.teacheranalytics.adapter;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.EmailLog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by sonal on 2/12/15.
 */
public class EmailLogRecyclerAdapter extends RecyclerView
        .Adapter<EmailLogRecyclerAdapter
        .DataObjectHolder>
{
    private ArrayList<EmailLog> emailArrayList;
    private Dialog dialog;
    private Context context;
    private String TO,CC,BCC,SUB,MSG,TREQUESTED,TPROCESS;



    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private TextView emailSub,date,txtTo,txtCC,txtBCC,txtMessage;
        private LinearLayout layParent,layChild;
        private ImageView imgArrow;

        public DataObjectHolder(View itemView) {
            super(itemView);
            emailSub = (TextView) itemView.findViewById(R.id.subject);

            date=(TextView)itemView.findViewById(R.id.date);
            layParent = (LinearLayout) itemView.findViewById(R.id.layParent);
            layChild = (LinearLayout)itemView.findViewById(R.id.layChild);
            imgArrow = (ImageView) itemView.findViewById(R.id.imgArrow);

        }

    }


    public EmailLogRecyclerAdapter(ArrayList<EmailLog> myDataset, Context context) {
        emailArrayList = myDataset;
        this.context=context;
        //dialog = new Dialog(context);
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_email_subject, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, int position) {
        try {
            final EmailLog data=emailArrayList.get(position);

            holder.emailSub.setText(data.getAscSubject());
            String input = data.getAscTimestampRequested();
            final  DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm a");
            System.out.println(outputFormat.format(inputFormat.parse(input)));
            holder.date.setText(outputFormat.format(inputFormat.parse(input)));

            holder.layParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        dialog = new Dialog(context);
                        dialog.setContentView(R.layout.row_email_log);
                        dialog.show();
                        Window window = dialog.getWindow();
                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

                        dialog.setCanceledOnTouchOutside(false);

                        TextView emailTo = (TextView) dialog.findViewById(R.id.emailTo);
                        TextView emailCc = (TextView) dialog.findViewById(R.id.emailCc);
                        TextView emailBcc = (TextView) dialog.findViewById(R.id.emailBcc);
                        TextView emailSub = (TextView) dialog.findViewById(R.id.emailSub);
                        TextView emailMsg = (TextView) dialog.findViewById(R.id.emailMsg);
                        TextView emailRequested = (TextView) dialog.findViewById(R.id.emailRequested);
                        TextView emailProcessing = (TextView) dialog.findViewById(R.id.emailProcessing);
                        emailTo.setText("To : " + Html.fromHtml(data.getAscTo()));
                        emailCc.setText("Cc : " + Html.fromHtml(data.getAscCc()));
                        emailBcc.setText("Bcc : " + Html.fromHtml(data.getAscBcc()));
                        emailSub.setText("Subject : " + data.getAscSubject());
                        emailMsg.setText("Message : " + Html.fromHtml(data.getAscMessage()));

                        String timeRequested = data.getAscTimestampRequested();

                        System.out.println(outputFormat.format(inputFormat.parse(timeRequested)));
                        emailRequested.setText("Timestamp Requested : " + outputFormat.format(inputFormat.parse(timeRequested)));
                        String timeProcessing = data.getAscTimestampProcessed();
                        emailProcessing.setText("Timestamp Processing : " +outputFormat.format(inputFormat.parse(timeProcessing)));

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            });


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return emailArrayList.size();
    }


}

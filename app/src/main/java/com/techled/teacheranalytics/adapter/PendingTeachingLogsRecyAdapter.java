package com.techled.teacheranalytics.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.techled.teacheranalytics.ActivityList.TeachingLogModule.FillTeachingLogsActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.TeachingLogs;

import java.util.ArrayList;

public class PendingTeachingLogsRecyAdapter extends RecyclerView.Adapter<PendingTeachingLogsRecyAdapter.TeachingLogsHolder> {
    private ArrayList<TeachingLogs> teachingLogsArrayList;
    private Context context;

    public PendingTeachingLogsRecyAdapter(ArrayList<TeachingLogs> teachingLogsArrayList, Context context) {
        this.teachingLogsArrayList = teachingLogsArrayList;
        this.context = context;
    }

    public class TeachingLogsHolder extends RecyclerView.ViewHolder
    {
        private TextView txtPeriod,txtStdDiv,txtSubject,txtDate,txtEvent;
        private Button btnViewLogs;
        public TeachingLogsHolder(@NonNull View itemView) {
            super(itemView);

            txtPeriod = (TextView)itemView.findViewById(R.id.txtPeriod);
            txtStdDiv = (TextView)itemView.findViewById(R.id.txtStdDiv);
            txtSubject = (TextView)itemView.findViewById(R.id.txtSubject);
            txtDate = (TextView)itemView.findViewById(R.id.txtDate);
            txtEvent = (TextView)itemView.findViewById(R.id.txtEvent);
            btnViewLogs = (Button)itemView.findViewById(R.id.btnViewLogs);
        }
    }
    @NonNull
    @Override
    public TeachingLogsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_pending_teaching_log,viewGroup,false);
        TeachingLogsHolder holder = new TeachingLogsHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TeachingLogsHolder teachingLogsHolder, int i) {

        TeachingLogs logs = teachingLogsArrayList.get(i);
        teachingLogsHolder.txtDate.setText("Date: "+logs.getTime());
        teachingLogsHolder.txtEvent.setText(""+logs.getEvent());
        teachingLogsHolder.txtStdDiv.setText(""+logs.getStdDiv());
        teachingLogsHolder.txtPeriod.setText("Period Lecture 1");
        teachingLogsHolder.txtSubject.setText(""+logs.getSubject());

        teachingLogsHolder.btnViewLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FillTeachingLogsActivity.class);
                ((Activity)context).startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return teachingLogsArrayList.size();
    }
}

package com.techled.teacheranalytics.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.techled.teacheranalytics.ActivityList.TeachingLogModule.FillTeachingLogsActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.TeachingLog;
import com.techled.teacheranalytics.util.Util;

import java.util.ArrayList;

public class TeachingLogDetailsRecyAdapter extends RecyclerView.Adapter<TeachingLogDetailsRecyAdapter.TeachingLogDetailsHolder>{
    private ArrayList<TeachingLog> teachingLogDetailsArrayList;
    private Context context;
    private Util util;
    private ProgressDialog progDailog;
    private int orgId, insId, dscId, ayr, stdId, unitId;
    private String selectedDate, selectedDateCompare;

    public TeachingLogDetailsRecyAdapter(Integer orgId, Integer insId, Integer dscId, Integer ayr, Integer stdId, ArrayList<TeachingLog> teachingLogDetailsArrayList, Context context, String selectedDate, String selectedDateCompare) {
        this.teachingLogDetailsArrayList = teachingLogDetailsArrayList;
        this.context = context;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.ayr = ayr;
        this.stdId = stdId;
        this.selectedDate = selectedDate;
        this.selectedDateCompare = selectedDateCompare;
    }

    public class TeachingLogDetailsHolder extends RecyclerView.ViewHolder {
        private TextView txtSubName, txtStdDiv, txtTimeSlot, txtLogStatus;
        private Button btnFillLog, btnEditLog, btnViewLog;


        public TeachingLogDetailsHolder(@NonNull View itemView) {
            super(itemView);

            txtSubName = (TextView) itemView.findViewById(R.id.txtSubName);
            txtStdDiv = (TextView) itemView.findViewById(R.id.txtStdDiv);
            txtTimeSlot = (TextView) itemView.findViewById(R.id.txtTimeSlot);
            txtLogStatus = (TextView) itemView.findViewById(R.id.txtLogStatus);
            btnFillLog = itemView.findViewById(R.id.btnFillLog);
            btnEditLog = itemView.findViewById(R.id.btnEditLog);
            btnViewLog = itemView.findViewById(R.id.btnViewLog);
        }
    }

    @NonNull
    @Override
    public TeachingLogDetailsRecyAdapter.TeachingLogDetailsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_teaching_log_details, viewGroup, false);
        TeachingLogDetailsRecyAdapter.TeachingLogDetailsHolder holder = new TeachingLogDetailsRecyAdapter.TeachingLogDetailsHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TeachingLogDetailsRecyAdapter.TeachingLogDetailsHolder TeachingLogDetailsHolder, final int i) {

        final TeachingLog teachingLog = teachingLogDetailsArrayList.get(i);
        util = new Util();
        TeachingLogDetailsHolder.txtStdDiv.setText("Class : " + teachingLog.getStd()+"-"+teachingLog.getDiv());
        TeachingLogDetailsHolder.txtSubName.setText("Subject : " + teachingLog.getSubName());
        TeachingLogDetailsHolder.txtTimeSlot.setText("Time slot: " + teachingLog.getLectureStartTime() + " to "+teachingLog.getLectureEndTime());
        if(teachingLog.getAbsent() == false && teachingLog.getEvent() == false && teachingLog.getHoliday() == false && teachingLog.getLectureOff() == false) {
            if(teachingLog.getFilledLog()==true) {
                if (teachingLog.getLogStatus().equals("A"))
                {
                    TeachingLogDetailsHolder.txtLogStatus.setText("Log Status : Approved");
                    TeachingLogDetailsHolder.btnViewLog.setVisibility(View.VISIBLE);
                }
                else if (teachingLog.getLogStatus().equals("P") || teachingLog.getLogStatus().equals("N"))
                {
                    TeachingLogDetailsHolder.txtLogStatus.setText("Log Status : Pending for Approval");
                    TeachingLogDetailsHolder.btnEditLog.setVisibility(View.VISIBLE);
                }
                else
                {
                    TeachingLogDetailsHolder.txtLogStatus.setText("Log Status : Rejected");
                    TeachingLogDetailsHolder.btnEditLog.setVisibility(View.VISIBLE);
                }

            }
            //else if (teachingLog.getLogStatus()=="F") { TeachingLogDetailsHolder.btnEditLog.setVisibility(View.VISIBLE); }
            else {
                TeachingLogDetailsHolder.txtLogStatus.setText("Log Status : Not Fiiled");
                TeachingLogDetailsHolder.btnFillLog.setVisibility(View.VISIBLE);
            }
        }else
        {
            TeachingLogDetailsHolder.txtLogStatus.setText("Absent");
        }


        TeachingLogDetailsHolder.btnFillLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //	Toast.makeText(context, "coming soon", Toast.LENGTH_SHORT).show();
                try {
                    Bundle b = new Bundle();
                    b.putInt("newLog",0);
                    b.putString("ScheduledDate",selectedDate);
                    b.putString("selectedDateCompare",selectedDateCompare);
                    Gson gson = new Gson();
                    String jsonObj = gson.toJson(teachingLog);
                    b.putString("TeachingLogJsonObj", jsonObj);
                    Intent intent = new Intent(context, FillTeachingLogsActivity.class);
                    intent.putExtras(b);
                    context.startActivity(intent);
                    ((Activity)context).finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        TeachingLogDetailsHolder.btnEditLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //	Toast.makeText(context, "coming soon", Toast.LENGTH_SHORT).show();
                try {
                    Bundle b = new Bundle();
                    Gson gson = new Gson();
                    String jsonObj = gson.toJson(teachingLog);
                    b.putString("TeachingLogJsonObj", jsonObj);
                    b.putString("ScheduledDate",selectedDate);
                    b.putString("selectedDateCompare",selectedDateCompare);
                    b.putInt("newLog",1);
                    Intent intent = new Intent(context, FillTeachingLogsActivity.class);
                    intent.putExtras(b);
                    context.startActivity(intent);
                    ((Activity)context).finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        TeachingLogDetailsHolder.btnViewLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //	Toast.makeText(context, "coming soon", Toast.LENGTH_SHORT).show();
                try {
                    Bundle b = new Bundle();
                    Gson gson = new Gson();
                    String jsonObj = gson.toJson(teachingLog);
                    b.putString("TeachingLogJsonObj", jsonObj);
                    b.putString("ScheduledDate",selectedDate);
                    b.putInt("newLog",2);
                    Intent intent = new Intent(context, FillTeachingLogsActivity.class);
                    intent.putExtras(b);
                    context.startActivity(intent);
                    ((Activity)context).finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return teachingLogDetailsArrayList.size();
    }

}

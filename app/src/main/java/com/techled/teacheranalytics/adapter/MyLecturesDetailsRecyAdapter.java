package com.techled.teacheranalytics.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.techled.teacheranalytics.ActivityList.MyLectures.MyLectures;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.MyLectureDetails;
import com.techled.teacheranalytics.util.Util;

import java.util.ArrayList;

public class MyLecturesDetailsRecyAdapter extends RecyclerView.Adapter<MyLecturesDetailsRecyAdapter.MyLecturesDetailsHolder> {
    private ArrayList<MyLectureDetails> myLectureDetailsArrayList;
    private Context context;
    private Util util;
    private ProgressDialog progDailog;
    private int orgId, insId, dscId, ayr, stdId, unitId;

    public MyLecturesDetailsRecyAdapter(Integer orgId, Integer insId, Integer dscId, Integer ayr, Integer stdId, ArrayList<MyLectureDetails> myLectureDetailsArrayList, Context context) {
        this.myLectureDetailsArrayList = myLectureDetailsArrayList;
        this.context = context;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.ayr = ayr;
        this.stdId = stdId;
    }

    public class MyLecturesDetailsHolder extends RecyclerView.ViewHolder {
        private TextView txtSubName, txtStdDiv, txtLectureStartTime, txtLectureEndTime;

        public MyLecturesDetailsHolder(@NonNull View itemView) {
            super(itemView);

            txtSubName = (TextView) itemView.findViewById(R.id.txtSubName);
            txtStdDiv = (TextView) itemView.findViewById(R.id.txtStdDiv);
            txtLectureStartTime = (TextView) itemView.findViewById(R.id.txtLectureStartTime);
            txtLectureEndTime = (TextView) itemView.findViewById(R.id.txtLectureEndTime);
        }
    }

    @NonNull
    @Override
    public MyLecturesDetailsRecyAdapter.MyLecturesDetailsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_my_lectures_details, viewGroup, false);
        MyLecturesDetailsRecyAdapter.MyLecturesDetailsHolder holder = new MyLecturesDetailsRecyAdapter.MyLecturesDetailsHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyLecturesDetailsRecyAdapter.MyLecturesDetailsHolder MyLecturesDetailsHolder, int i) {

        final MyLectureDetails myLectureDetails = myLectureDetailsArrayList.get(i);
        util = new Util();
        MyLecturesDetailsHolder.txtSubName.setText("Subject : " + myLectureDetails.getSubName());
        MyLecturesDetailsHolder.txtStdDiv.setText("Std Div : " + myLectureDetails.getStd()+myLectureDetails.getDiv());
        MyLecturesDetailsHolder.txtLectureStartTime.setText("Start Time : " + myLectureDetails.getLectureStartTime());
        MyLecturesDetailsHolder.txtLectureEndTime.setText("End Time : " + myLectureDetails.getLectureEndTime());
    }

    @Override
    public int getItemCount() {
        return myLectureDetailsArrayList.size();
    }
}
package com.techled.teacheranalytics.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.TimelineHomeActivity;
import com.techled.teacheranalytics.pojo.TimelineDataDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by techlead on 30/5/18.
 */

public class TimelineRecyAdapter extends RecyclerView.Adapter<TimelineRecyAdapter.TimelineDataHolder> {

    private ArrayList<TimelineDataDetails> timelineDataArrayList;
    private Context context;
    String internetUrl = "https://mobiocean.com/content-images/Mobi-move-School.jpg";
    private ProgressDialog progressDialog;
    private Util util;
    private int orgId, insId, stfLoginId;
    private int yearMonthId, stfId, tilId;
    private String tilReadYn, responseRead;


    public TimelineRecyAdapter(ArrayList<TimelineDataDetails> timelineDataArrayList, Context context, int orgId, int insId, int stfLoginId) {
        this.timelineDataArrayList = timelineDataArrayList;
        this.context = context;
        this.orgId = orgId;
        this.insId = insId;
        this.stfLoginId = stfLoginId;
    }

    public class TimelineDataHolder extends RecyclerView.ViewHolder {
        private TextView txtDate, txtType, txtMessage, txtDescription,txtAssigneeName;
        private ImageView imgNotf, timelineImage;
        private LinearLayout layChild, layOption;
        private TextView txtDownload, txtMarkAsRead;

        public TimelineDataHolder(View itemView) {
            super(itemView);
            txtAssigneeName = (TextView) itemView.findViewById(R.id.txtAssigneeName);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtType = (TextView) itemView.findViewById(R.id.txtType);
            txtMessage = (TextView) itemView.findViewById(R.id.txtMessage);
            imgNotf = (ImageView) itemView.findViewById(R.id.imgNotf);
            layChild = (LinearLayout) itemView.findViewById(R.id.layChild);
            timelineImage = (ImageView) itemView.findViewById(R.id.timelineImage);
            txtDescription = (TextView) itemView.findViewById(R.id.txtDescription);
            txtDownload = (TextView) itemView.findViewById(R.id.txtDownload);
            txtMarkAsRead = (TextView) itemView.findViewById(R.id.txtMarkAsRead);
            layOption = (LinearLayout) itemView.findViewById(R.id.layOption);
        }
    }

    @Override
    public TimelineDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_timeline_details, parent, false);
        TimelineDataHolder holder = new TimelineDataHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(TimelineDataHolder holder, int position) {
        final TimelineDataDetails timelineData = timelineDataArrayList.get(position);

        try {


            //if already read
            if (timelineData.getTilReadYn().equals("Y")) {
                holder.txtMarkAsRead.setVisibility(View.GONE);
            }

            //timeline title
            if (timelineData.getStuTitle().equals("") || timelineData.getStuTitle().equals("null") || timelineData.getStuTitle() == null) {
                holder.txtMessage.setText("Title - ");
            } else {
                holder.txtMessage.setText("" + timelineData.getStuTitle());
            }

            //timeline date
            if (timelineData.getTilDate().equals("") || timelineData.getTilDate().equals("null") || timelineData.getTilDate() == null) {
                holder.txtDate.setText("Date - ");
            } else {
                holder.txtDate.setText("" + timelineData.getTilDate());
            }

            //timeline description
            if (timelineData.getTilDescription().equals("") || timelineData.getTilDescription().equals("null") || timelineData.getTilDescription() == null) {
                holder.txtDescription.setVisibility(View.GONE);
            } else {
                holder.txtDescription.setText(("" + Html.fromHtml(timelineData.getTilDescription())));
            }

            //assignee name
            if (timelineData.getAssigneeName() == null || timelineData.getAssigneeName().equals("null") || timelineData.getAssigneeName().equals("")) {
                holder.txtAssigneeName.setVisibility(View.GONE);
            } else {
                holder.txtAssigneeName.setText(("Assigned By - " + timelineData.getAssigneeName()));
            }

            //timeline data is news,circular,annoucement
            if (timelineData.getTilAnefhct().equals("N") || timelineData.getTilAnefhct().equals("A") || timelineData.getTilAnefhct().equals("C")) {
                holder.txtDownload.setVisibility(View.VISIBLE);
                holder.timelineImage.setVisibility(View.GONE);
                //view attachment
                if (timelineData.getTilS3Url().equals(" ") || timelineData.getTilS3Url() == null || timelineData.getTilS3Url().equals("null") || timelineData.getTilS3Url().equals("")) {
                    holder.txtDownload.setVisibility(View.GONE);
                } else {
                    holder.txtDownload.setVisibility(View.VISIBLE);
                }
            } else if (timelineData.getTilUploadedContentTypeVadi().equals("X")) {
                holder.txtDownload.setVisibility(View.VISIBLE);
                holder.timelineImage.setVisibility(View.GONE);
                //view attachment
                if (timelineData.getTilS3Url().equals(" ") || timelineData.getTilS3Url() == null || timelineData.getTilS3Url().equals("null") || timelineData.getTilS3Url().equals("")) {
                    holder.txtDownload.setVisibility(View.GONE);
                } else {
                    holder.txtDownload.setVisibility(View.VISIBLE);
                }
            } else if (timelineData.getTilUploadedContentTypeVadi().equals("P")) {
                holder.txtDownload.setVisibility(View.GONE);
                holder.timelineImage.setVisibility(View.VISIBLE);
                if (timelineData.getTilS3Url().equals("null") || timelineData.getTilS3Url().equals("") || timelineData.getTilS3Url() == null) {
                    Glide.with(context).
                            load(internetUrl).override(200, 200).into(holder.timelineImage);
                } else {
                    Glide.with(context).
                            load(timelineData.getTilS3Url()).override(200, 200).into(holder.timelineImage);
                }
            } else if (timelineData.getTilUploadedContentTypeVadi().equals("null") || timelineData.getTilUploadedContentTypeVadi().equals("")) {
                holder.txtDownload.setVisibility(View.GONE);
                holder.timelineImage.setVisibility(View.GONE);
            }

            //if url is null then hide View Attachement is hide
            if (timelineData.getTilS3Url().equals("null") || timelineData.getTilS3Url() == null || timelineData.getTilS3Url().equals(" ") || timelineData.getTilS3Url().equals("")) {

                holder.txtDownload.setVisibility(View.GONE);
                holder.timelineImage.setVisibility(View.GONE);
            }

            if (timelineData.getTilAnefhct().equals("N")) {
                holder.txtType.setText("News");
                holder.imgNotf.setBackgroundResource(R.drawable.news);
            } else if (timelineData.getTilAnefhct().equals("A")) {
                holder.txtType.setText("Announcement");
                holder.imgNotf.setBackgroundResource(R.drawable.announcement);
            } else if (timelineData.getTilAnefhct().equals("C")) {
                holder.txtType.setText("Circular");
                holder.imgNotf.setBackgroundResource(R.drawable.notification_mail_icon);

            }
            if (timelineData.getTilAnefhct().equals("H")) {
                holder.txtType.setText("Homework");
                holder.imgNotf.setBackgroundResource(R.drawable.homework);
            }
            if (timelineData.getTilAnefhct().equals("T")) {
                holder.txtType.setText("Attendance");
                holder.imgNotf.setBackgroundResource(R.drawable.attend);
            }
            if (timelineData.getTilAnefhct().equals("E")) {
                holder.txtType.setText("Events");
                holder.imgNotf.setBackgroundResource(R.drawable.events);

            }
            if (timelineData.getTilAnefhct().equals("X")) {
                holder.txtType.setText("Exam");
                holder.imgNotf.setBackgroundResource(R.drawable.exam);
            }
            if (timelineData.getTilAnefhct().equals("F")) {
                holder.txtType.setText("Fee");
                holder.imgNotf.setBackgroundResource(R.drawable.fees);
            }
            if (timelineData.getTilAnefhct().equals("R")) {
                holder.txtType.setText("Remark");
                holder.imgNotf.setBackgroundResource(R.drawable.remarks);
            }
            if (timelineData.getTilAnefhct().equals("B")) {
                holder.txtType.setText("Library");
                holder.imgNotf.setBackgroundResource(R.drawable.remarks);
            }
            if (timelineData.getTilAnefhct().equals("L")) {
                holder.txtType.setText("Leave");
                holder.imgNotf.setBackgroundResource(R.drawable.remarks);
            }

            //view attachment
            holder.txtDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (timelineData.getTilS3Url().equals("null") || timelineData.getTilS3Url() == null || timelineData.getTilS3Url().equals(" ") || timelineData.getTilS3Url().equals("")) {
                            Toast.makeText(context, "Unable to load attachment!", Toast.LENGTH_SHORT).show();

                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(timelineData.getTilS3Url()));
                            context.startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(context, "Unable to load attachment!", Toast.LENGTH_SHORT).show();

                        e.printStackTrace();
                    }
                }
            });

            //mark as read
            holder.txtMarkAsRead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    stfId = timelineData.getOldStfId();
                    tilId = timelineData.getTilId();
                    yearMonthId = timelineData.getYearMonthId();

                    //mark as read
                    new MarkAsReadTimeline().execute();


                }
            });

            //hide layout
            if (holder.txtMarkAsRead.getVisibility() == View.GONE && holder.txtDownload.getVisibility() == View.GONE) {
                holder.layOption.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class MarkAsReadTimeline extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                if (responseRead != null) {
                    JSONObject object = new JSONObject(responseRead);
                    String status = object.getString("status");
                    if (status.equals("success")) {
                        Toast.makeText(context, "This message mark as read!", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(context, TimelineHomeActivity.class);
                        ((Activity) context).startActivity(intent);
                        ((Activity) context).finish();

                    } else {
                        Toast.makeText(context, "Failed to mark as read!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                } else {
                    Toast.makeText(context, "Failed to mark as read!", Toast.LENGTH_SHORT).show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                util = new Util();
                responseRead = util.saveTimelineMarkAsRead(orgId, insId, yearMonthId, "E", stfId, stfLoginId, tilId, "Y");

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return timelineDataArrayList.size();
    }
}

package com.techled.teacheranalytics.adapter;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.MarkAttendance;
import com.techled.teacheranalytics.pojo.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonal on 1/3/18.
 */

public class OxymeterReadingRecyAdapter extends RecyclerView.Adapter<OxymeterReadingRecyAdapter.AttMagtHolder> {
    private Context context;
    private ArrayList<MarkAttendance> managementDetailsArrayList;
    final MarkAttendance markAttendance = new MarkAttendance();
    private ArrayList<MarkAttendance> list = null;
    private Dialog dialog = null;
    private String date;
    private String comment;
    private boolean flagA= true,flagP=true;

    private static RecyclerViewClickListener itemListener;

    public OxymeterReadingRecyAdapter(Context context, ArrayList<MarkAttendance> managementDetailsArrayList, String date) {
        this.context = context;
        this.managementDetailsArrayList = managementDetailsArrayList;
        this.date = date;
    }

    public class AttMagtHolder extends RecyclerView.ViewHolder {
        private TextView txtStuName;

        public AttMagtHolder(View itemView) {
            super(itemView);
            txtStuName = (TextView) itemView.findViewById(R.id.txtStuName);
        }
    }

    @Override
    public AttMagtHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_oxymeter_reading, parent, false);
        AttMagtHolder holder = new AttMagtHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final AttMagtHolder holder, final int position) {

        final MarkAttendance managementDetails = managementDetailsArrayList.get(position);
       // holder.txtStuName.setText(managementDetails.getStuName());
        holder.txtStuName.setText("("+managementDetails.getRollNo()+") "+managementDetails.getStuName());

    }

    @Override
    public int getItemCount() {
        return managementDetailsArrayList.size();
    }


    // method to access in activity after updating selection
    public List<MarkAttendance> getList() {
        return managementDetailsArrayList;
    }
}


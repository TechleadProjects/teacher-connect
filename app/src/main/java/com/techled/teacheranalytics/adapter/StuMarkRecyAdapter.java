package com.techled.teacheranalytics.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techled.teacheranalytics.ActivityList.StuInfoActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.StuMarkData;

import java.util.ArrayList;

/**
 * Created by sonal on 14/2/18.
 */

public class StuMarkRecyAdapter extends RecyclerView.Adapter<StuMarkRecyAdapter.StuMarkDataHolder> {

    private ArrayList<StuMarkData> stuMarkDataArrayList;
    private Context context;

    public StuMarkRecyAdapter(ArrayList<StuMarkData> stuMarkDataArrayList, Context context) {
        this.stuMarkDataArrayList = stuMarkDataArrayList;
        this.context = context;
    }

    public class StuMarkDataHolder extends RecyclerView.ViewHolder
    {
        private LinearLayout layParent;
        private TextView stuName,stuMark;

        public StuMarkDataHolder(View itemView) {
            super(itemView);
            layParent = (LinearLayout)itemView.findViewById(R.id.layParent);
            stuName = (TextView) itemView.findViewById(R.id.stuName);
            stuMark = (TextView) itemView.findViewById(R.id.stuMark);



        }
    }
    @Override
    public StuMarkDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(context).inflate(R.layout.row_stu_mark_data,parent,false);
      StuMarkDataHolder holder = new StuMarkDataHolder(view);
      return  holder;
    }

    @Override
    public void onBindViewHolder(StuMarkDataHolder holder, final int position) {
        final StuMarkData stuMarkData = stuMarkDataArrayList.get(position);
        if(stuMarkData.getStuName().equals("null"))
        {
            holder.stuName.setText(" - ");
        }else {
            holder.stuName.setText(""+stuMarkData.getStuName());
        }

        holder.stuMark.setText(""+stuMarkData.getStuMark()+"%");

        if(stuMarkData.getColor().equals("#43A047"))
        {
            holder.layParent.setBackgroundColor(Color.parseColor("#43A047"));
        }else if(stuMarkData.getColor().equals("#FB8C00"))
        {
            holder.layParent.setBackgroundColor(Color.parseColor("#FB8C00"));
        }else  {
            holder.layParent.setBackgroundColor(Color.parseColor("#EF4846"));
        }

        holder.layParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StuInfoActivity.class);
                intent.putExtra("stuId",stuMarkData.getStuId());
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return stuMarkDataArrayList.size();
    }
}

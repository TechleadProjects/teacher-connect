package com.techled.teacheranalytics.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.techled.teacheranalytics.ActivityList.CommunicationPortal.EmailtFragment;
import com.techled.teacheranalytics.ActivityList.CommunicationPortal.SmsFragment;



/**
 * Created by techlead on 21/9/17.
 */

public class CommunicationPortalPagerAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public CommunicationPortalPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
           /* case 0:
                NotificationFragment tab3 = new NotificationFragment();
                return tab3;*/
            case 0:
                EmailtFragment tab1 = new EmailtFragment();
                return tab1;
            case 1:
                SmsFragment tab2 = new SmsFragment();
                return tab2;


            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}
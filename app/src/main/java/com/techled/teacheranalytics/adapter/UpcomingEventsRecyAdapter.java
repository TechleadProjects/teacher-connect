package com.techled.teacheranalytics.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.EventsData;

import java.util.ArrayList;

/**
 * Created by sonal on 2/12/15.
 */
public class UpcomingEventsRecyAdapter extends RecyclerView
        .Adapter<UpcomingEventsRecyAdapter
        .DataObjectHolder>
{
    private ArrayList<EventsData> eventsDataArrayList;

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView evenTitle, eventDesc, eventDate, fsWaived;

        public DataObjectHolder(View itemView) {
            super(itemView);
            evenTitle = (TextView) itemView.findViewById(R.id.evenTitle);
            eventDesc = (TextView) itemView.findViewById(R.id.eventDesc);
            eventDate = (TextView) itemView.findViewById(R.id.eventDate);


        }


    }


    public UpcomingEventsRecyAdapter(ArrayList<EventsData> myDataset) {
        eventsDataArrayList = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        try {
            final EventsData data=eventsDataArrayList.get(position);

            holder.evenTitle.setText(Html.fromHtml(data.getTitle()));
            holder.eventDesc.setText("Description : " + Html.fromHtml(data.getDescription()));
            holder.eventDate.setText( Html.fromHtml(data.getEvtDate() ));

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return eventsDataArrayList.size();
    }


}

package com.techled.teacheranalytics.adapter;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.SyllabusModule.SyllabusHomeActivity;
import com.techled.teacheranalytics.ActivityList.TeachingLogModule.FillTeachingLogsActivity;
import com.techled.teacheranalytics.ActivityList.TeachingLogModule.TeachingLogCalendarSummaryActivity;
import com.techled.teacheranalytics.ActivityList.TimetableModule.MeetingForLecturesActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.MyLectureDetails;
import com.techled.teacheranalytics.pojo.Standards;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LectureTimetableRecyAdapter extends RecyclerView.Adapter<LectureTimetableRecyAdapter.StaffTimetableHolder> {

    private Context context;
    private List<MyLectureDetails> timetableList;
    private Dialog dialog = null;
    private Util util;
    private ProgressDialog progDailog;
    private String enteredLink, response, enteredMeetingTopic, selectedMeetingType, startDate, endDate;
    private MyLectureDetails timetableObj;
    private int orgId, insId, dscId, ayrYear, stfId;
    private Date currentTime;
    private Date startTime;
    private Date endTime;
    private int choice;

    public class StaffTimetableHolder extends RecyclerView.ViewHolder {
        public TextView dayName, subName, timelineDetail;
        public Button add, edit, start, create;

        public StaffTimetableHolder(View view) {
            super(view);
            dayName = view.findViewById(R.id.dayName);
            subName = view.findViewById(R.id.subName);
            timelineDetail = view.findViewById(R.id.lectureDetail);

            add = view.findViewById(R.id.addLink);
            edit = view.findViewById(R.id.editLink);
            start = view.findViewById(R.id.startMeeting);
            create = view.findViewById(R.id.createLink);
        }
    }

    public LectureTimetableRecyAdapter(Context context, List<MyLectureDetails> timetableList, int orgId, int insId, int dscId, int ayrYear, int stfId, String startDate, String endDate) {
        this.context = context;
        this.timetableList = timetableList;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.ayrYear = ayrYear;
        this.stfId = stfId;
        this.startDate = startDate;
        this.endDate = endDate;

        util = new Util();
    }

    @Override
    public StaffTimetableHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_lecture_list, parent, false);

        return new StaffTimetableHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StaffTimetableHolder holder, final int position) {
        final MyLectureDetails timetable = timetableList.get(position);

        if (!timetable.getScheduledDate().equals("") && !timetable.getLectureEndTime().equals("")) {

            String dateToCompare = timetable.getScheduledDate() + " " + timetable.getLectureEndTime() + ":00";

            try {
                SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
                Date date = format.parse(dateToCompare);
                Date currentDate = new Date();

                if (date != null && date.before(currentDate) || timetable.getOnlineLectureYn().equals("") || timetable.getOnlineLectureYn().equals("N")) {
                    holder.add.setVisibility(View.GONE);
                    holder.create.setVisibility(View.GONE);
                    holder.edit.setVisibility(View.GONE);
                    holder.start.setVisibility(View.GONE);
                } else {
                    holder.add.setVisibility(View.VISIBLE);
                    holder.create.setVisibility(View.VISIBLE);
                    holder.edit.setVisibility(View.VISIBLE);
                    holder.start.setVisibility(View.VISIBLE);
                }

                if (timetable.getTelOnlineMeetingLink() == null || timetable.getTelOnlineMeetingLink().equals("")) {
                    holder.add.setVisibility(View.VISIBLE);
                    holder.create.setVisibility(View.VISIBLE);
                    holder.edit.setVisibility(View.GONE);
                    holder.start.setVisibility(View.GONE);
                }else {
                    holder.add.setVisibility(View.GONE);
                    holder.create.setVisibility(View.GONE);
                    holder.edit.setVisibility(View.VISIBLE);
                    holder.start.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        holder.dayName.setText(timetable.getDayName().concat(" - ").concat(timetable.getScheduledDate()));
        holder.subName.setText(timetable.getSubName());

        String time = timetable.getLectureStartTime() + " - " + timetable.getLectureEndTime();
        String standard = timetable.getStd() + " - " + timetable.getDiv();
        holder.timelineDetail.setText(time + " | " + standard);

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_add_link_for_lecture);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                dialog.setCanceledOnTouchOutside(false);

                final EditText link = dialog.findViewById(R.id.link);

                Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
                Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        enteredLink = link.getText().toString().trim();

                        if (enteredLink.equals("")) {
                            Toast.makeText(context, "Please enter link!", Toast.LENGTH_SHORT).show();
                        } else if (!URLUtil.isValidUrl(enteredLink)) {

                            Toast.makeText(context, "Please enter valid link!", Toast.LENGTH_SHORT).show();
                        } else {

                            dialog.dismiss();

                            timetableObj = timetableList.get(position);

                            new SaveLinkForTeachingLog().execute();
                        }
                    }
                });
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] singleChoiceItems = new String[]{"Add new existing link", "Create new meeting link", "Cancel the meeting"};
                int itemSelected = 0;
                new androidx.appcompat.app.AlertDialog.Builder(context)
                        .setTitle("Edit meeting")
                        .setSingleChoiceItems(singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                choice = i;
                            }
                        })
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogs, int which) {
                                if (choice == 0) {

                                    dialog = new Dialog(context);
                                    dialog.setContentView(R.layout.dialog_add_link_for_lecture);
                                    dialog.show();
                                    Window window = dialog.getWindow();
                                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                    dialog.setCanceledOnTouchOutside(false);

                                    final EditText link = dialog.findViewById(R.id.link);

                                    Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
                                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                                    LinearLayout linkLayout = dialog.findViewById(R.id.linkLayout);
                                    TextView previousLink = dialog.findViewById(R.id.previousLink);

                                    linkLayout.setVisibility(View.VISIBLE);
                                    previousLink.setText(timetableList.get(position).getTelOnlineMeetingLink());

                                    btnCancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });

                                    btnSave.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            enteredLink = link.getText().toString().trim();

                                            if (enteredLink.equals("")) {
                                                Toast.makeText(context, "Please enter link!", Toast.LENGTH_SHORT).show();
                                            } else if (URLUtil.isValidUrl(enteredLink)) {

                                                Toast.makeText(context, "Please enter valid link!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                dialog.dismiss();

                                                timetableObj = timetableList.get(position);

                                                new SaveLinkForTeachingLog().execute();

                                            }
                                        }
                                    });
                                } else if (choice == 1) {

                                    enteredMeetingTopic = "";
                                    selectedMeetingType = "";

                                    dialog = new Dialog(context);
                                    dialog.setContentView(R.layout.dialog_create_link_for_lecture);
                                    dialog.show();
                                    Window window = dialog.getWindow();
                                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                    dialog.setCanceledOnTouchOutside(false);

                                    final ArrayList<String> meetingTypesArrayList = new ArrayList<>();
                                    meetingTypesArrayList.add("Zoom");
                                    meetingTypesArrayList.add("Go to meeting");

                                    Button createLink = dialog.findViewById(R.id.createLinkForLecture);
                                    ImageView btnCancel = dialog.findViewById(R.id.closeDialog);
                                    final EditText meetingTopic = dialog.findViewById(R.id.meetingTopic);

                                    Spinner meetingType = dialog.findViewById(R.id.meetingType);

                                    ArrayAdapter<String> meetingTypeAdapter = new ArrayAdapter<>((MeetingForLecturesActivity) context,
                                            android.R.layout.simple_spinner_item, meetingTypesArrayList);
                                    meetingTypeAdapter
                                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    meetingType.setAdapter(meetingTypeAdapter);

                                    meetingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            if (position == 0) {
                                                selectedMeetingType = "Z";
                                            } else {
                                                selectedMeetingType = "G";
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }
                                    });

                                    btnCancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });

                                    createLink.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            enteredMeetingTopic = meetingTopic.getText().toString().trim();

                                            if (enteredMeetingTopic.equals("")) {
                                                Toast.makeText(context, "Please enter meeting topic!", Toast.LENGTH_SHORT).show();
                                            }else if (selectedMeetingType.equals("")) {
                                                Toast.makeText(context, "Please select meeting type!", Toast.LENGTH_SHORT).show();
                                            } else {
                                                dialog.dismiss();

                                                timetableObj = timetableList.get(position);

                                                new CreateNewLinkForTeachingLog().execute();

                                            }
                                        }
                                    });
                                } else if (choice == 2) {

                                    enteredLink = "";
                                    timetableObj = timetableList.get(position);

                                    new SaveLinkForTeachingLog().execute();
                                }
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .show();
            }
        });

        holder.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                enteredMeetingTopic = "";
                selectedMeetingType = "";

                dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_create_link_for_lecture);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                dialog.setCanceledOnTouchOutside(false);

                final ArrayList<String> meetingTypesArrayList = new ArrayList<>();
                meetingTypesArrayList.add("Zoom");
                meetingTypesArrayList.add("Go to meeting");

                Button createLink = dialog.findViewById(R.id.createLinkForLecture);
                ImageView btnCancel = dialog.findViewById(R.id.closeDialog);
                final EditText meetingTopic = dialog.findViewById(R.id.meetingTopic);

                Spinner meetingType = dialog.findViewById(R.id.meetingType);

                ArrayAdapter<String> meetingTypeAdapter = new ArrayAdapter<>((MeetingForLecturesActivity) context,
                        android.R.layout.simple_spinner_item, meetingTypesArrayList);
                meetingTypeAdapter
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                meetingType.setAdapter(meetingTypeAdapter);

                meetingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            selectedMeetingType = "Z";
                        } else {
                            selectedMeetingType = "G";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                createLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        enteredMeetingTopic = meetingTopic.getText().toString().trim();

                        if (enteredMeetingTopic.equals("")) {
                            Toast.makeText(context, "Please enter meeting topic!", Toast.LENGTH_SHORT).show();
                        }else if (selectedMeetingType.equals("")) {
                            Toast.makeText(context, "Please select meeting type!", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();

                            timetableObj = timetableList.get(position);

                            new CreateNewLinkForTeachingLog().execute();
                        }
                    }
                });
            }
        });

        holder.start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    final SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");

                    timetableObj = timetableList.get(position);

                    SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
                    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");

                    Date date = format.parse(timetableObj.getScheduledDate());
                    Date cDate = format.parse(format.format(new Date()));
                    String lectureStartDate = format2.format(date);

                    if (date.equals(cDate)) {

                        try {

                            currentTime = formatter1.parse(formatter1.format(new Date()));
                            startTime = formatter1.parse(timetableObj.getLectureStartTime().concat(":00"));
                            endTime = formatter1.parse(timetableObj.getLectureEndTime().concat(":00"));

                            if(startTime != null) {
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(startTime);
                                cal.add(Calendar.MINUTE, -5);
                                startTime = cal.getTime();
                            }

                            if (currentTime != null && currentTime.equals(startTime)) {

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                                // Setting Dialog Title
                                alertDialog.setTitle("Teacher Connect");

                                // Setting Dialog Message
                                alertDialog.setMessage("Are you sure you want to start this lecture?");

                                // Setting Positive "Yes" Button
                                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            dialog.dismiss();

                                            new MarkAttendanceTask().execute();

                                        } catch (Exception e) {

                                            dialog.dismiss();

                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                                            // Setting Dialog Title
                                            alertDialog.setTitle("Student Connect");

                                            // Setting Dialog Message
                                            alertDialog.setMessage("Could not open the link. Bad link found.");

                                            // Setting Positive "Yes" Button
                                            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    dialog.dismiss();
                                                }
                                            });

                                            // Showing Alert Message
                                            alertDialog.show();

                                        }
                                    }
                                });

                                // Setting Negative "NO" Button
                                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to invoke NO event
                                        dialog.cancel();
                                    }
                                });

                                // Showing Alert Message
                                alertDialog.show();

                            } else if (currentTime != null && currentTime.after(startTime) && currentTime.before(endTime)) {

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                                // Setting Dialog Title
                                alertDialog.setTitle("Teacher Connect");

                                // Setting Dialog Message
                                alertDialog.setMessage("Are you sure you want to start this lecture?");

                                // Setting Positive "Yes" Button
                                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            dialog.dismiss();

                                            new MarkAttendanceTask().execute();

                                        } catch (Exception e) {

                                            dialog.dismiss();

                                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                                            // Setting Dialog Title
                                            alertDialog.setTitle("Student Connect");

                                            // Setting Dialog Message
                                            alertDialog.setMessage("Could not open the link. Bad link found.");

                                            // Setting Positive "Yes" Button
                                            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {

                                                    dialog.dismiss();
                                                }
                                            });

                                            // Showing Alert Message
                                            alertDialog.show();

                                        }
                                    }
                                });

                                // Setting Negative "NO" Button
                                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Write your code here to invoke NO event
                                        dialog.cancel();
                                    }
                                });

                                // Showing Alert Message
                                alertDialog.show();

                            } else {

                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                                // Setting Dialog Title
                                alertDialog.setTitle("Teacher Connect");

                                // Setting Dialog Message
                                alertDialog.setMessage("You can start this lecture only between ".concat(formatter1.format(startTime)).concat(" and ").concat(formatter1.format(endTime)));

                                // Setting Positive "Yes" Button
                                alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                });

                                // Showing Alert Message
                                alertDialog.show();

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                        // Setting Dialog Title
                        alertDialog.setTitle("Teacher Connect");

                        // Setting Dialog Message
                        alertDialog.setMessage("You can start this lecture only on ".concat(lectureStartDate));

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
                    }

                } catch (Exception e) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                    // Setting Dialog Title
                    alertDialog.setTitle("Teacher Connect");

                    // Setting Dialog Message
                    alertDialog.setMessage("Could not open the link. Bad link found.");

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();

                }
            }
        });
    }

    public class SaveLinkForTeachingLog extends AsyncTask<String, String, String> {
        private String resSave;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {

                        if (enteredLink.equals("")) {

                            Toast.makeText(context, "Meeting cancelled for the lecture successfully.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, MeetingForLecturesActivity.class);
                            intent.putExtra("weekStart", startDate);
                            intent.putExtra("weekEnd", endDate);
                            context.startActivity(intent);
                            ((MeetingForLecturesActivity) context).finish();
                        } else {

                            Toast.makeText(context, "Link added for the lecture successfully.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, MeetingForLecturesActivity.class);
                            intent.putExtra("weekStart", startDate);
                            intent.putExtra("weekEnd", endDate);
                            context.startActivity(intent);
                            ((MeetingForLecturesActivity) context).finish();
                        }

                    } else {
                        if (enteredLink.equals("")) {

                            Toast.makeText(context, "Could not cancel the meeting for the lecture. Try again later", Toast.LENGTH_LONG).show();
                        } else {

                            Toast.makeText(context, "Could not add the link for the lecture. Try again later", Toast.LENGTH_LONG).show();
                        }

                    }
                } else {

                    if (enteredLink.equals("")) {

                        Toast.makeText(context, "Could not cancel the meeting for the lecture. Try again later", Toast.LENGTH_LONG).show();
                    } else {

                        Toast.makeText(context, "Could not add the link for the lecture. Try again later", Toast.LENGTH_LONG).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");

                Date date = format.parse(timetableObj.getScheduledDate());
                String scheduledDate = format2.format(date);

                resSave = util.saveMeetingLinkTeachingLog(orgId, insId, dscId, timetableObj.getStdId(), stfId, timetableObj.getDivId(), ayrYear, timetableObj.getPrdId(), timetableObj.getSubId(), 0, timetableObj.getTelSlbBokSb(), scheduledDate, enteredLink);
                return resSave;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }
    }

    public class CreateNewLinkForTeachingLog extends AsyncTask<String, String, String> {
        private String resSave;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {

                        Toast.makeText(context, "Link created for the lecture successfully.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, MeetingForLecturesActivity.class);
                        intent.putExtra("weekStart", startDate);
                        intent.putExtra("weekEnd", endDate);
                        context.startActivity(intent);
                        ((MeetingForLecturesActivity) context).finish();

                    } else {
                        JSONObject dataObj = resArray.getJSONObject(0);
                        String data = dataObj.getString("data");

                        if (data.toLowerCase().contains("zoom")) {

                            Toast.makeText(context, data, Toast.LENGTH_LONG).show();
                        }else {

                            Toast.makeText(context, "Could not create the link for the lecture. Try again later", Toast.LENGTH_LONG).show();
                        }

                    }
                } else {
                    Toast.makeText(context, "Could not create the link for the lecture. Try again later", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");

                Date date = format.parse(timetableObj.getScheduledDate());
                String scheduledDate = format2.format(date);

                String lectureStartTime = timetableObj.getLectureStartTime();
                String lectureEndTime = timetableObj.getLectureEndTime();

                resSave = util.createMeetingLinkForTeachingLog(orgId, insId, dscId, timetableObj.getStdId(), stfId, timetableObj.getDivId(), ayrYear, timetableObj.getPrdId(), timetableObj.getSubId(), 0, timetableObj.getTelSlbBokSb(), scheduledDate, lectureStartTime, lectureEndTime, enteredMeetingTopic, selectedMeetingType);
                return resSave;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }
    }

    public class MarkAttendanceTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {
                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        Toast.makeText(context, "Attendance marked successfully.", Toast.LENGTH_SHORT).show();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(timetableObj.getTelOnlineMeetingLink()));
                        context.startActivity(browserIntent);

                    } else {

                        Toast.makeText(context, "Failed to mark attendance.", Toast.LENGTH_SHORT).show();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(timetableObj.getTelOnlineMeetingLink()));
                        context.startActivity(browserIntent);
                    }

                } else {

                    Toast.makeText(context, "Failed to mark attendance.", Toast.LENGTH_SHORT).show();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(timetableObj.getTelOnlineMeetingLink()));
                    context.startActivity(browserIntent);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String date = formatter1.format(new Date());
                String dateTime = formatter2.format(new Date());

                response = util.saveLectureAttd(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), String.valueOf(timetableObj.getDivId()), String.valueOf(timetableObj.getStdId()), String.valueOf(-1), String.valueOf(ayrYear), date, String.valueOf(timetableObj.getPrdId()), String.valueOf(stfId), String.valueOf(timetableObj.getSubId()), String.valueOf(timetableObj.getTepPrdIndex()), timetableObj.getLectureStartTime().concat(":00"), timetableObj.getLectureEndTime().concat(":00"), "N", dateTime);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return timetableList.size();
    }
}
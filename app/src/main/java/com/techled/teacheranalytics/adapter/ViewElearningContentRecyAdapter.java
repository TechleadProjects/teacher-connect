package com.techled.teacheranalytics.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.ELearningContentData;
import com.techled.teacheranalytics.pojo.PendingTaskDetails;

import java.util.ArrayList;
import java.util.List;

public class ViewElearningContentRecyAdapter extends RecyclerView.Adapter<ViewElearningContentRecyAdapter.ScheduledTaskHolder> {

    private Context context;
    private ArrayList<ELearningContentData> eLearningContentArrayList;

    public ViewElearningContentRecyAdapter(Context context, ArrayList<ELearningContentData> eLearningContentArrayList) {
        this.context = context;
        this.eLearningContentArrayList = eLearningContentArrayList;

    }

    public class ScheduledTaskHolder extends RecyclerView.ViewHolder {
        private TextView name, details;
        private LinearLayout contentLayout;

        public ScheduledTaskHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            details = itemView.findViewById(R.id.details);
            contentLayout = itemView.findViewById(R.id.contentLayout);
        }
    }

    @Override
    public ViewElearningContentRecyAdapter.ScheduledTaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_elearning_content, parent, false);
        return new ViewElearningContentRecyAdapter.ScheduledTaskHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewElearningContentRecyAdapter.ScheduledTaskHolder holder, final int position) {

        final ELearningContentData eLearningContent = eLearningContentArrayList.get(position);

        holder.name.setText(eLearningContent.getEclDescription());
        holder.details.setText(eLearningContent.getEclDetails());

        holder.contentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (eLearningContent.getEclLink() != null && !eLearningContent.getEclLink().equals("")) {

                    try {

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(eLearningContent.getEclLink()));
                        context.startActivity(browserIntent);

                    } catch (Exception e) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                        // Setting Dialog Title
                        alertDialog.setTitle("Student Connect");

                        // Setting Dialog Message
                        alertDialog.setMessage("Could not open the link. Bad link found.");

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();

                    }

                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                    // Setting Dialog Title
                    alertDialog.setTitle("Student Connect");

                    // Setting Dialog Message
                    alertDialog.setMessage("URL for the content is empty.");

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return eLearningContentArrayList.size();
    }

    // method to access in activity after updating selection
    public List<ELearningContentData> getList() {
        return eLearningContentArrayList;
    }
}

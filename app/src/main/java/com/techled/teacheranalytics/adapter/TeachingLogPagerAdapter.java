package com.techled.teacheranalytics.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.techled.teacheranalytics.ActivityList.EventModule.PastEventFragment;
import com.techled.teacheranalytics.ActivityList.EventModule.UpcommingEventFragment;
import com.techled.teacheranalytics.ActivityList.TeachingLogModule.FilledTeachingLogFragment;
import com.techled.teacheranalytics.ActivityList.TeachingLogModule.PendingTeachingLogFragment;


public class TeachingLogPagerAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public TeachingLogPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                PendingTeachingLogFragment tab1 = new PendingTeachingLogFragment();
                return tab1;
            case 1:
                FilledTeachingLogFragment tab2 = new FilledTeachingLogFragment();
                return tab2;

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}
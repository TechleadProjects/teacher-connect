package com.techled.teacheranalytics.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.HomeworkDetails;
import com.techled.teacheranalytics.pojo.StudentHomeworkData;
import com.techled.teacheranalytics.util.Util;

import java.util.ArrayList;

/**
 * Created by sonal on 2/12/15.
 */
public class StudentSelectionRecyclerAdapter extends RecyclerView
        .Adapter<StudentSelectionRecyclerAdapter
        .DataObjectHolder> {

    private HomeworkDetails homeworkDetails;
    private ArrayList<StudentHomeworkData> studentHomeworkDataArrayList;
    private Dialog dialog;
    private Context context;
    private int orgId, insId, dscId, stdId, divId, ayr, hwkId;
    private Util util;
    private ProgressDialog progressDialog;

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private TextView stuName, hwStatus, submissionDate, completedDate, hwEvaluate;
        private LinearLayout mainLayout;


        public DataObjectHolder(View itemView) {
            super(itemView);

            stuName = itemView.findViewById(R.id.stuName);
            hwStatus = itemView.findViewById(R.id.hwStatus);
            submissionDate = itemView.findViewById(R.id.submissionDate);
            completedDate = itemView.findViewById(R.id.completedDate);
            hwEvaluate = itemView.findViewById(R.id.hwEvaluate);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }

    public StudentSelectionRecyclerAdapter(Context context, HomeworkDetails homeworkDetails, ArrayList<StudentHomeworkData> studentHomeworkDataArrayList) {

        this.context = context;
        this.homeworkDetails = homeworkDetails;
        this.studentHomeworkDataArrayList = studentHomeworkDataArrayList;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_student_selection, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, int position) {
        try {
            final StudentHomeworkData studentHomeworkData = studentHomeworkDataArrayList.get(position);

            holder.stuName.setText(studentHomeworkData.getStuName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return studentHomeworkDataArrayList.size();
    }

}

package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.PendingTaskDetails;

import java.util.ArrayList;
import java.util.List;

public class ScheduledTasksRecyAdapter extends RecyclerView.Adapter<ScheduledTasksRecyAdapter.ScheduledTaskHolder> {

    private Context context;
    private ArrayList<PendingTaskDetails> scheduledTaskDetailsArrayList;

    public ScheduledTasksRecyAdapter(Context context, ArrayList<PendingTaskDetails> scheduledTaskDetailsArrayList) {
        this.context = context;
        this.scheduledTaskDetailsArrayList = scheduledTaskDetailsArrayList;

    }

    public class ScheduledTaskHolder extends RecyclerView.ViewHolder {
        private TextView txtTaskDueDate, txtTaskAssignee, txtTaskStartDate, txtTaskStatus, txtTaskPercentComplete, txtTaskProgressNote;

        public ScheduledTaskHolder(View itemView) {
            super(itemView);
            txtTaskDueDate = itemView.findViewById(R.id.txtTaskDueDate);
            txtTaskAssignee = itemView.findViewById(R.id.txtTaskAssignee);
            txtTaskStartDate = itemView.findViewById(R.id.txtTaskStartDate);
            txtTaskStatus = itemView.findViewById(R.id.txtTaskStatus);
            txtTaskPercentComplete = itemView.findViewById(R.id.txtTaskPercentComplete);
            txtTaskProgressNote = itemView.findViewById(R.id.txtTaskProgressNote);

        }
    }

    @Override
    public com.techled.teacheranalytics.adapter.ScheduledTasksRecyAdapter.ScheduledTaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_scheduled_tasks, parent, false);
        return new com.techled.teacheranalytics.adapter.ScheduledTasksRecyAdapter.ScheduledTaskHolder(view);
    }

    @Override
    public void onBindViewHolder(final com.techled.teacheranalytics.adapter.ScheduledTasksRecyAdapter.ScheduledTaskHolder holder, final int position) {

        final PendingTaskDetails pendingTaskDetails = scheduledTaskDetailsArrayList.get(position);

        holder.txtTaskDueDate.setText("Due Date : ".concat(pendingTaskDetails.getDueDate()));
        holder.txtTaskAssignee.setText("Assignee : ".concat(pendingTaskDetails.getAssigneeName()));
        holder.txtTaskStartDate.setText("Start Date : ".concat(pendingTaskDetails.getStartDate()));
        holder.txtTaskStatus.setText("Status : ".concat(pendingTaskDetails.getStatus()));
        holder.txtTaskPercentComplete.setText("Percent Complete : ".concat(pendingTaskDetails.getPercentComplete()).concat("%"));
        holder.txtTaskProgressNote.setText("Progress Note : ".concat(pendingTaskDetails.getProgressNote()));
    }

    @Override
    public int getItemCount() {
        return scheduledTaskDetailsArrayList.size();
    }

    // method to access in activity after updating selection
    public List<PendingTaskDetails> getList() {
        return scheduledTaskDetailsArrayList;
    }
}

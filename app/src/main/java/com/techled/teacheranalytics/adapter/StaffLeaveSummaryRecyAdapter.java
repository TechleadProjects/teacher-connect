package com.techled.teacheranalytics.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.LeaveSummary;

import java.util.List;

public class StaffLeaveSummaryRecyAdapter extends RecyclerView.Adapter<StaffLeaveSummaryRecyAdapter.StaffLeaveSummaryHolder> {

    private List<LeaveSummary> leaveSummaryList;

    public class StaffLeaveSummaryHolder extends RecyclerView.ViewHolder {
        public TextView leaveType, leavePeriod, leaveApplicationDate, leaveAccountedDays, leaveStatus, leaveReason;

        public StaffLeaveSummaryHolder(View view) {
            super(view);
            leaveType = view.findViewById(R.id.leave_type);
            leavePeriod = view.findViewById(R.id.leave_period);
            leaveApplicationDate = view.findViewById(R.id.leave_application_date);
            leaveAccountedDays = view.findViewById(R.id.leave_accounted_days);
            leaveStatus = view.findViewById(R.id.leave_status);
            leaveReason = view.findViewById(R.id.leave_reason);
        }
    }

    public StaffLeaveSummaryRecyAdapter(List<LeaveSummary> leaveSummaryList) {
        this.leaveSummaryList = leaveSummaryList;
    }

    @Override
    public StaffLeaveSummaryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_staff_leave_summary, parent, false);

        return new StaffLeaveSummaryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StaffLeaveSummaryHolder holder, int position) {
        LeaveSummary leaveSummary = leaveSummaryList.get(position);
        holder.leaveType.setText(leaveSummary.getLeaveType());
        holder.leavePeriod.setText("Period : " + leaveSummary.getLeavePeriod());
        holder.leaveApplicationDate.setText("Application Date : " + leaveSummary.getLeaveApplicationDate());
        holder.leaveAccountedDays.setText("Accounted Days : " + leaveSummary.getLeaveAccountedDays());
        holder.leaveStatus.setText("Status : " + leaveSummary.getLeaveStatus());
        holder.leaveReason.setText("Leave Reason : " + leaveSummary.getLeaveReason());
    }

    @Override
    public int getItemCount() {
        return leaveSummaryList.size();
    }
}
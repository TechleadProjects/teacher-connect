package com.techled.teacheranalytics.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.google.gson.Gson;
import com.techled.teacheranalytics.ActivityList.TaskManagementModule.EditTaskActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.ExamAnalysisData;
import com.techled.teacheranalytics.pojo.PendingTaskDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonal on 1/3/18.
 */

public class ExamAnalysisRecyAdapter extends Adapter<ExamAnalysisRecyAdapter.ExamAnalysisHolder> {
    private Context context;
    private ArrayList<ExamAnalysisData> examAnalysisDataArrayList;

    public ExamAnalysisRecyAdapter(Context context, ArrayList<ExamAnalysisData> examAnalysisDataArrayList) {
        this.context = context;
        this.examAnalysisDataArrayList = examAnalysisDataArrayList;
    }

    public class ExamAnalysisHolder extends RecyclerView.ViewHolder {
        private RecyclerView recyclerView;
        private TextView className, examName, totalStudentsCount, studentsAppearedCount, studentNotAppeared;

        public ExamAnalysisHolder(View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.recycler_view);

            className = itemView.findViewById(R.id.className);
            examName = itemView.findViewById(R.id.examName);
            totalStudentsCount = itemView.findViewById(R.id.totalStudentCount);
            studentsAppearedCount = itemView.findViewById(R.id.studentAppearedCount);
            studentNotAppeared = itemView.findViewById(R.id.studentNotAppeared);
        }
    }

    @NonNull
    @Override
    public ExamAnalysisHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_exam_analysis, parent, false);
        return new ExamAnalysisHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ExamAnalysisHolder holder, final int position) {

        final ExamAnalysisData examAnalysisData = examAnalysisDataArrayList.get(position);

        RecyclerView recyclerView = holder.recyclerView;
        TextView className = holder.className;
        TextView examName = holder.examName;
        TextView totalStudentsCount = holder.totalStudentsCount;
        TextView studentsAppearedCount = holder.studentsAppearedCount;
        TextView studentNotAppeared = holder.studentNotAppeared;

        className.setText("Class - ".concat(examAnalysisData.getClassName()));
        examName.setText(examAnalysisData.getExamName());
        totalStudentsCount.setText("Total - ".concat(String.valueOf(examAnalysisData.getTotalStudentCount())));
        studentsAppearedCount.setText("Appeared - ".concat(String.valueOf(examAnalysisData.getStudentsAppearedCount())));

        if (examAnalysisData.getStudentsNotAppeared() != null && examAnalysisData.getStudentsNotAppeared().size() > 0) {

            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            Adapter adapter = new StudentsListRecyclerAdapter(context, examAnalysisData.getStudentsNotAppeared());
            recyclerView.setAdapter(adapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            studentNotAppeared.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return examAnalysisDataArrayList.size();
    }

}
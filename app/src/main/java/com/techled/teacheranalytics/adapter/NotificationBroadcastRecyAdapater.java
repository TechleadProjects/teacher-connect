package com.techled.teacheranalytics.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.SendNorificationBroadcastModule.NotificationReadStatusActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.TimelineReadStatusActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.NotificationBroadcastDetails;
import com.techled.teacheranalytics.util.Util;

import java.util.ArrayList;

public class NotificationBroadcastRecyAdapater extends RecyclerView.Adapter<NotificationBroadcastRecyAdapater.NotificationDetailsHolder> {


    private ArrayList<NotificationBroadcastDetails> notificationBroadcastDetailsArrayList;
    private Context context;
    private String tilIdentifier,assignedDate,nntType;
    private int orgId,insId,ayr;
    private Util util;


    public NotificationBroadcastRecyAdapater(ArrayList<NotificationBroadcastDetails> notificationBroadcastDetailsArrayList, Context context,Integer orgId,Integer insId,Integer ayr) {
        this.notificationBroadcastDetailsArrayList = notificationBroadcastDetailsArrayList;
        this.context = context;
        this.orgId = orgId;
        this.insId= insId;
        this.ayr =ayr;
    }

    public class NotificationDetailsHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtDesc, txtAssignedTo, txtDateTime;
        private TextView btnReadStatus, btnAttachment;

        public NotificationDetailsHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
            txtAssignedTo = (TextView) itemView.findViewById(R.id.txtAssignedTo);
            txtDateTime = (TextView) itemView.findViewById(R.id.txtDateTime);
            btnReadStatus = (TextView) itemView.findViewById(R.id.btnReadStatus);
            btnAttachment = (TextView) itemView.findViewById(R.id.btnAttachment);
        }
    }

    @NonNull
    @Override
    public NotificationDetailsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_notification_details, viewGroup, false);
        NotificationDetailsHolder holder = new NotificationDetailsHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationDetailsHolder notificationDetailsHolder, int i) {

        final NotificationBroadcastDetails details = notificationBroadcastDetailsArrayList.get(i);


        util = new Util();

        if (details.getNntUrl().equals("null") || details.getNntUrl() == null || details.getNntUrl().equals(" ") || details.getNntUrl().equals("")) {

            notificationDetailsHolder.btnAttachment.setVisibility(View.GONE);
        } else {
            notificationDetailsHolder.btnAttachment.setVisibility(View.VISIBLE);
        }
        //send to
        if (details.getNntSendTo() == null || details.getNntSendTo().equals("") || details.getNntSendTo().equals("null")) {
            notificationDetailsHolder.txtAssignedTo.setText("Notify To : -");
        } else {
            notificationDetailsHolder.txtAssignedTo.setText("Notify To : " + details.getNntSendTo());
        }

        //date time
        if (details.getNntDateTime() == null || details.getNntDateTime().equals("") || details.getNntDateTime().equals("null")) {
            notificationDetailsHolder.txtDateTime.setText("Date : - ");
        } else {
            notificationDetailsHolder.txtDateTime.setText("" + details.getNntDateTime());
        }

        //description
        if (details.getNntDescription() == null || details.getNntDescription().equals("") || details.getNntDescription().equals("null")) {
            notificationDetailsHolder.txtDesc.setText("" + details.getNntDescription());
        } else {
            notificationDetailsHolder.txtDesc.setText("" + details.getNntDescription());
        }


        //title
        if (details.getNntTitle() == null || details.getNntTitle().equals("") || details.getNntTitle().equals("null")) {

            notificationDetailsHolder.txtTitle.setText("Title : - ");
        } else {
            notificationDetailsHolder.txtTitle.setText("" + details.getNntTitle());
        }


        //show attachment
        notificationDetailsHolder.btnAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (details.getNntUrl().equals("null") || details.getNntUrl() == null || details.getNntUrl().equals(" ") || details.getNntUrl().equals("")) {
                        Toast.makeText(context, "Unable to load attachment!", Toast.LENGTH_SHORT).show();

                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(details.getNntUrl()));
                        context.startActivity(intent);
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Unable to load attachment!", Toast.LENGTH_SHORT).show();

                    e.printStackTrace();
                }
            }
        });

        //read status
        notificationDetailsHolder.btnReadStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, NotificationReadStatusActivity.class);
//
//                //provide identifier to find read status
//                intent.putExtra("identifier", details.getNnIndentifier());
//                ((Activity) context).startActivity(intent);
//                ((Activity) context).finish();

                nntType = details.getNntType();
                tilIdentifier = details.getNnIndentifier();
                assignedDate = details.getNntDateTime();

                //view read status
                ViewReadStatus readStatus = new ViewReadStatus();
                readStatus.execute();

            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationBroadcastDetailsArrayList.size();
    }

    public class ViewReadStatus extends AsyncTask<String, String, String> {
        private ProgressDialog progressDialog;
        private String resTimReadStatus;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                if (resTimReadStatus != null) {
                    Intent intent = new Intent(context, NotificationReadStatusActivity.class);
                    intent.putExtra("ReadStatus", resTimReadStatus);
                    ((Activity) context).startActivity(intent);

                } else {
                    Toast.makeText(context, "Failed to get read status!", Toast.LENGTH_SHORT).show();
                    util.SaveSystemLog(orgId,insId,"Failed to get read status"+tilIdentifier,"Teacher Connect",0,"Notification Broadcast");
                    return;
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while reading status of notification"+tilIdentifier,"Teacher Connect",0,"Notification Broadcast");
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                resTimReadStatus = util.getTimelineReadDetails(orgId, insId, ayr,"S", tilIdentifier, assignedDate, nntType);


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while reading status of notification"+tilIdentifier,"Teacher Connect",0,"Notification Broadcast");
            }
            return null;
        }
    }
}

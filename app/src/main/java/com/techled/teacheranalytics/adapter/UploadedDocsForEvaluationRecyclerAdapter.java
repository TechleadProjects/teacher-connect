package com.techled.teacheranalytics.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.transition.Transition;
import com.techled.teacheranalytics.ActivityList.EvaluateAnswerPaperModule.SubmittedPaperSelectionActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.EvaluateDocumentData;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ImageEditorIntentBuilder;

public class UploadedDocsForEvaluationRecyclerAdapter extends RecyclerView.Adapter<UploadedDocsForEvaluationRecyclerAdapter.MyViewHolder> {

    private ArrayList<EvaluateDocumentData> dataSet;
    private Context context;
    private float totalMarksGiven;

    private ProgressDialog progDailog;

    private final int PHOTO_EDITOR_REQUEST_CODE = 231;
    private String imagePath = "";
    public static long lastClickTime = 0;
    public static final long DOUBLE_CLICK_TIME_DELTA = 500;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView part, marks, comment, evaluatedOn;
        Button evaluatePaper, showPreviouslyEvaluatedPaper;
        ImageView paperEvaluatedFlagImg;

        public MyViewHolder(View itemView) {
            super(itemView);
            part = itemView.findViewById(R.id.part);
            marks = itemView.findViewById(R.id.marks);
            comment = itemView.findViewById(R.id.comment);
            evaluatedOn = itemView.findViewById(R.id.evaluatedOn);
            paperEvaluatedFlagImg = itemView.findViewById(R.id.paperEvaluatedFlagImg);
            showPreviouslyEvaluatedPaper = itemView.findViewById(R.id.showPreviouslyEvaluatedPaper);
            evaluatePaper = itemView.findViewById(R.id.evaluatePaper);
        }
    }

    public UploadedDocsForEvaluationRecyclerAdapter(Context context, ArrayList<EvaluateDocumentData> data, float totalMarksGiven) {
        this.context = context;
        this.dataSet = data;
        this.totalMarksGiven = totalMarksGiven;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_uploaded_docs_for_evaluation_details, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView part = holder.part;
        TextView marks = holder.marks;
        TextView comment = holder.comment;
        TextView evaluatedOn = holder.evaluatedOn;
        ImageView paperEvaluatedFlagImg = holder.paperEvaluatedFlagImg;
        Button showPreviouslyEvaluatedPaper = holder.showPreviouslyEvaluatedPaper;
        Button evaluatePaper = holder.evaluatePaper;

        part.setText("Part ".concat(String.valueOf(listPosition + 1)));

        if (dataSet.get(listPosition).getExdMarks() == null || dataSet.get(listPosition).getExdMarks().equals("") || dataSet.get(listPosition).getExdMarks().equals("null")) {

            marks.setText("Marks : -");
        } else {

            marks.setText("Marks : ".concat(dataSet.get(listPosition).getExdMarks()));
        }

        if (dataSet.get(listPosition).getExdEvaluatedComment() == null || dataSet.get(listPosition).getExdEvaluatedComment().equals("") || dataSet.get(listPosition).getExdEvaluatedComment().equals("null")) {

            comment.setText("Comment : -");
        } else {

            comment.setText("Comment : ".concat(dataSet.get(listPosition).getExdEvaluatedComment()));
        }

        if (dataSet.get(listPosition).getExdEvaluatedTime() == null || dataSet.get(listPosition).getExdEvaluatedTime().equals("") || dataSet.get(listPosition).getExdEvaluatedTime().equals("null")) {

            evaluatedOn.setText("Evaluated On : -");
        } else {

            evaluatedOn.setText("Evaluated On : ".concat(dataSet.get(listPosition).getExdEvaluatedTime()));
        }

        if (dataSet.get(listPosition).getExdEvaluatedDocLink() == null || dataSet.get(listPosition).getExdEvaluatedDocLink().equals("") || dataSet.get(listPosition).getExdEvaluatedDocLink().equals("null") || !URLUtil.isValidUrl(dataSet.get(listPosition).getExdEvaluatedDocLink())) {

            paperEvaluatedFlagImg.bringToFront();
            paperEvaluatedFlagImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.not_evaluated));
            showPreviouslyEvaluatedPaper.setVisibility(View.GONE);
        } else {

            paperEvaluatedFlagImg.bringToFront();
            paperEvaluatedFlagImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.evaluated));
            showPreviouslyEvaluatedPaper.setVisibility(View.VISIBLE);
        }

        showPreviouslyEvaluatedPaper.setOnClickListener(v -> {

            if (dataSet.get(listPosition).getExdEvaluatedDocLink() == null || dataSet.get(listPosition).getExdEvaluatedDocLink().equals("") || dataSet.get(listPosition).getExdEvaluatedDocLink().equals("null") || !URLUtil.isValidUrl(dataSet.get(listPosition).getExdEvaluatedDocLink())) {

                Toast.makeText(context, "URL for the document is not valid.", Toast.LENGTH_SHORT).show();
            } else {

                String meetingLink = dataSet.get(listPosition).getExdEvaluatedDocLink().trim();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(meetingLink));
                context.startActivity(browserIntent);
            }
        });

        evaluatePaper.setOnClickListener(view -> {

            if (!isDoubleClick()) {

                try {

                    if (dataSet.get(listPosition).getExdDocLink() == null || dataSet.get(listPosition).getExdDocLink().equals("") || dataSet.get(listPosition).getExdDocLink().equals("null") || !URLUtil.isValidUrl(dataSet.get(listPosition).getExdDocLink())) {

                        Toast.makeText(context, "URL for the document is not valid.", Toast.LENGTH_SHORT).show();
                    } else if (dataSet.get(listPosition).getExdMaxMarks() != null && !dataSet.get(listPosition).getExdMaxMarks().equals("") && !dataSet.get(listPosition).getExdMaxMarks().equals("null")) {

                        if (dataSet.get(listPosition).getExdMarks() == null || dataSet.get(listPosition).getExdMarks().equals("") || dataSet.get(listPosition).getExdMarks().equals("null") || dataSet.get(listPosition).getExdMarks().equals("0.0")) {

                            if (Float.parseFloat(dataSet.get(listPosition).getExdMaxMarks()) == totalMarksGiven) {

                                new AlertDialog.Builder(context)
                                        .setTitle("Student Connect")
                                        .setMessage("You can evaluate the part but you cannot give any marks to it since max marks have already been given to the paper.")
                                        .setCancelable(false)
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                loadImageToPhotoEditor(listPosition);
                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).show();
                            } else {
                                loadImageToPhotoEditor(listPosition);
                            }
                        } else {

                            loadImageToPhotoEditor(listPosition);
                        }
                    } else {
                        Toast.makeText(context, "Max marks not found for the exam.", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e("Demo App", e.getMessage()); // This could throw if either `sourcePath` or `outputPath` is blank or Null
                }
            }
        });

    }

    public void loadImageToPhotoEditor(int listPosition) {

        Thread t1 = new Thread(() -> {

            Glide.get(context).clearDiskCache();

            ((SubmittedPaperSelectionActivity)context).runOnUiThread(() -> {
                progDailog = new ProgressDialog(context);
                progDailog.setMessage("Fetching file from server...");
                progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progDailog.setCancelable(false);
                progDailog.setIndeterminate(false);
                progDailog.show();
            });

            Glide.with(context).asBitmap().load(dataSet.get(listPosition).getExdDocLink()).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bitmap,
                                            Transition<? super Bitmap> transition) {
                    int width = bitmap.getWidth();
                    int height = bitmap.getHeight();

                    if (progDailog != null) {
                        progDailog.dismiss();
                    }

                    if (width > 3161 && height > 4608) {

                        new AlertDialog.Builder(context)
                                .setTitle("Student Connect")
                                .setMessage("The image resolution and the size of the image is too high which is not supported for editing on this device. (Image is of resolution " + width + " x " + height + "). Please visit the website for the same.")
                                .setCancelable(false)
                                .setPositiveButton("Ok", (dialog, which) -> dialog.dismiss()).show();

                    } else {

                        Thread t1 = new Thread(() -> {

                            try {

                                Glide.get(context).clearDiskCache();

                                ((SubmittedPaperSelectionActivity)context).runOnUiThread(() -> {
                                    progDailog = new ProgressDialog(context);
                                    progDailog.setMessage("Fetching file from server...");
                                    progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                    progDailog.setCancelable(false);
                                    progDailog.setIndeterminate(false);
                                    progDailog.show();
                                });

                                File file = Glide.with(context).asFile().load(dataSet.get(listPosition).getExdDocLink()).submit().get();
                                imagePath = file.getPath();

                                ((SubmittedPaperSelectionActivity)context).runOnUiThread(() -> {

                                    if (progDailog != null) {
                                        progDailog.dismiss();
                                    }

                                    try {

                                        Intent intent = new ImageEditorIntentBuilder(context, imagePath, imagePath)// Add the features you need
                                                .withPaintFeature()
                                                .withAddText()
                                                .withRotateFeature()
                                                .withBrightnessFeature()
                                                .withCropFeature()
                                                .forcePortrait(true)// Add this to force portrait mode (It's set to false by default)
                                                .build();

                                        SubmittedPaperSelectionActivity.outputImagePath = imagePath;
                                        SubmittedPaperSelectionActivity.index = listPosition;
                                        SubmittedPaperSelectionActivity.exdId = dataSet.get(listPosition).getExdId();
                                        SubmittedPaperSelectionActivity.originalFileName = FilenameUtils.getName(new URL(dataSet.get(listPosition).getExdDocLink()).getPath());

                                        EditImageActivity.start(((SubmittedPaperSelectionActivity)context), intent, PHOTO_EDITOR_REQUEST_CODE);

                                    }catch (Exception e) {

                                        e.printStackTrace();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();

                                ((SubmittedPaperSelectionActivity)context).runOnUiThread(() -> {

                                    if (progDailog != null) {
                                        progDailog.dismiss();
                                    }

                                    try {

                                        Toast.makeText(context, "Link failed to open.", Toast.LENGTH_SHORT).show();

                                    }catch (Exception e1) {

                                        e1.printStackTrace();
                                    }
                                });
                            }
                        });
                        t1.start();
                    }
                }
            });
        });
        t1.start();
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static boolean isDoubleClick(){
        long clickTime = System.currentTimeMillis();
        if(clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA){
            lastClickTime = clickTime;
            return true;
        }
        lastClickTime = clickTime;
        return false;
    }

}


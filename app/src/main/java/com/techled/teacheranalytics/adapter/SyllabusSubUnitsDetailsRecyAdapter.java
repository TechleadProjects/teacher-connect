package com.techled.teacheranalytics.adapter;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.SyllabusModule.AddNewSubUnitActivity;
import com.techled.teacheranalytics.ActivityList.SyllabusModule.AddNewUnitActivity;
import com.techled.teacheranalytics.ActivityList.SyllabusModule.SubUnitsDetailsActivity;
import com.techled.teacheranalytics.ActivityList.SyllabusModule.SyllabusHomeActivity;
import com.techled.teacheranalytics.ActivityList.TimetableModule.MeetingForLecturesActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.ELearningContentData;
import com.techled.teacheranalytics.pojo.SubUnitsSyllabusDetails;
import com.techled.teacheranalytics.pojo.SyllabusDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SyllabusSubUnitsDetailsRecyAdapter extends RecyclerView.Adapter<SyllabusSubUnitsDetailsRecyAdapter.SyllabusDetailsHolder> {

    private ArrayList<SubUnitsSyllabusDetails> syllabusDetailsArrayList;
    private Context context;
    private Util util;
    private ProgressDialog progDailog;
    private int orgId, insId, dscId, ayr, stdId, subId, unitId, sunId, usrId;
    private String selectedContentType, enteredContentName, enteredContentLink, enteredDetails, enteredComment;
    private SubUnitsSyllabusDetails subUnitsSyllabusDetails;
    private ArrayList<ELearningContentData> eLearningContentArrayList;

    public SyllabusSubUnitsDetailsRecyAdapter(Integer orgId, Integer insId, Integer dscId, Integer ayr, Integer stdId, Integer usrId, Integer subId, ArrayList<SubUnitsSyllabusDetails> syllabusDetailsArrayList, Context context) {
        this.syllabusDetailsArrayList = syllabusDetailsArrayList;
        this.context = context;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.ayr = ayr;
        this.stdId = stdId;
        this.usrId = usrId;
        this.subId = subId;
    }

    public class SyllabusDetailsHolder extends RecyclerView.ViewHolder {
        private TextView txtUnitNumber, txtUnitDesc, txtFromDate, txtToDate;
        private ImageButton btnEdit, btnDelete, btnViewContent;
        private Button btnAddLink;

        public SyllabusDetailsHolder(@NonNull View itemView) {
            super(itemView);

            txtUnitNumber = (TextView) itemView.findViewById(R.id.txtUnitNumber);
            txtUnitDesc = (TextView) itemView.findViewById(R.id.txtUnitDesc);
            txtFromDate = (TextView) itemView.findViewById(R.id.txtFromDate);
            txtToDate = (TextView) itemView.findViewById(R.id.txtToDate);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnAddLink = itemView.findViewById(R.id.btnAddLink);
            btnViewContent = itemView.findViewById(R.id.btnViewContent);

        }
    }

    @NonNull
    @Override
    public SyllabusDetailsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_sub_unit_syllabus_details, viewGroup, false);
        SyllabusDetailsHolder holder = new SyllabusDetailsHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SyllabusDetailsHolder syllabusDetailsHolder, final int i) {

        final SubUnitsSyllabusDetails syllabusDetails = syllabusDetailsArrayList.get(i);
        util = new Util();

        // syllabusDetailsHolder.txtUnitNumber.setText("Unit Number: " + syllabusDetails.getUniNumber());
        //Description
        if (syllabusDetails.getSunDescription() == null || syllabusDetails.getSunDescription().equals("")) {
            syllabusDetailsHolder.txtUnitDesc.setText("Description: -");
        } else {

            syllabusDetailsHolder.txtUnitDesc.setText(syllabusDetails.getSunDescription());
        }

        //from date
        if (syllabusDetails.getFrom() == null || syllabusDetails.getFrom().equals("") || syllabusDetails.getFrom().equals("null")) {
            syllabusDetailsHolder.txtFromDate.setText("From Date: - ");
        } else {
            syllabusDetailsHolder.txtFromDate.setText("From Date: " + syllabusDetails.getFrom());
        }

        //to date
        if (syllabusDetails.getTo() == null || syllabusDetails.getTo().equals("") || syllabusDetails.getFrom().equals("null")) {
            syllabusDetailsHolder.txtToDate.setText("To Date: - ");
        } else {
            syllabusDetailsHolder.txtToDate.setText("To Date: " + syllabusDetails.getTo());
        }

        syllabusDetailsHolder.btnAddLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_add_content_for_sub_unit);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                dialog.setCanceledOnTouchOutside(false);

                final ArrayList<String> contentTypesArray = new ArrayList<>();
                contentTypesArray.add("Video");
                contentTypesArray.add("Audio");
                contentTypesArray.add("PPT");
                contentTypesArray.add("PDF");

                Button addContent = dialog.findViewById(R.id.addContent);
                ImageView btnCancel = dialog.findViewById(R.id.closeDialog);
                final EditText contentName = dialog.findViewById(R.id.contentName);
                final EditText contentLink = dialog.findViewById(R.id.contentLink);
                final EditText details = dialog.findViewById(R.id.details);
                final EditText comment = dialog.findViewById(R.id.comment);

                Spinner contentType = dialog.findViewById(R.id.contentType);

                ArrayAdapter<String> meetingTypeAdapter = new ArrayAdapter<>(context,
                        android.R.layout.simple_spinner_item, contentTypesArray);
                meetingTypeAdapter
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                contentType.setAdapter(meetingTypeAdapter);

                contentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            selectedContentType = "V";
                        } else if (position == 1) {
                            selectedContentType = "A";
                        } else if (position == 2) {
                            selectedContentType = "P";
                        } else if (position == 3) {
                            selectedContentType = "D";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                addContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        enteredContentName = contentName.getText().toString().trim();
                        enteredContentLink = contentLink.getText().toString().trim();
                        enteredDetails = details.getText().toString().trim();
                        enteredComment = comment.getText().toString().trim();

                        if (enteredContentName.equals("") || enteredContentLink.equals("") || enteredDetails.equals("") || enteredComment.equals("")) {
                            Toast.makeText(context, "Please don't leave any field blank!", Toast.LENGTH_SHORT).show();
                        }else if (selectedContentType.equals("")) {
                            Toast.makeText(context, "Please select content type!", Toast.LENGTH_SHORT).show();
                        } else {
                            dialog.dismiss();

                            subUnitsSyllabusDetails = syllabusDetailsArrayList.get(i);

                            new AddContentForSubUnit().execute();
                        }
                    }
                });
            }
        });

        syllabusDetailsHolder.btnViewContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                subUnitsSyllabusDetails = syllabusDetailsArrayList.get(i);

                if (!subUnitsSyllabusDetails.getSunElcId().equals("")) {

                    new GetElearningContentTask().execute();
                } else {

                    Toast.makeText(context, "No eLearning contents found!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //edit
        syllabusDetailsHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(context, AddNewSubUnitActivity.class);

                    intent.putExtra("stdId",stdId);
                    intent.putExtra("ayr",ayr);
                    intent.putExtra("subId",subId);

                    intent.putExtra("fromDate", syllabusDetails.getFrom());
                    intent.putExtra("toDate", syllabusDetails.getTo());
                    intent.putExtra("unitId", syllabusDetails.getUniId());
                    intent.putExtra("subunit", syllabusDetails.getSunDescription());
                    intent.putExtra("subUnitId", syllabusDetails.getSunId());
                    intent.putExtra("activities", syllabusDetails.getSunActivities());

                    intent.putExtra("editable", "Y");
                    intent.putExtra("showActive",syllabusDetails.getShowActiveYn());
                    intent.putExtra("showContent",syllabusDetails.getShowContentYn());
                    intent.putExtra("count",syllabusDetails.getCount());

                    ((Activity) context).startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //delete
        syllabusDetailsHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sunId = syllabusDetails.getSunId();
                unitId = syllabusDetails.getUniId();

                DeleteUnit deleteUnit = new DeleteUnit();
                deleteUnit.execute(null, null);
            }
        });
    }

    public class DeleteUnit extends AsyncTask<String, String, String> {
        private String resSave;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {
                        Toast.makeText(context, "Sub-unit details deleted successfully!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, SyllabusHomeActivity.class);
                        ((Activity) context).startActivity(intent);
                        ((Activity) context).finish();
                    } else {
                        Toast.makeText(context, "Failed to delete the sub-unit record!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Toast.makeText(context, "Failed to delete the sub-unit record!", Toast.LENGTH_SHORT).show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                resSave = util.deleteSubUnitSyallabusDetails(orgId, insId, dscId, ayr,
                        stdId, subId, unitId,sunId);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return syllabusDetailsArrayList.size();
    }

    public class AddContentForSubUnit extends AsyncTask<String, String, String> {
        private String resSave;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {

                        Toast.makeText(context, "Content added for the sub unit successfully.", Toast.LENGTH_SHORT).show();
                        ((SubUnitsDetailsActivity) context).finish();

                    } else {
                        Toast.makeText(context, "Could not add the content for the sub unit. Try again later", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, "Could not add the content for the sub unit. Try again later", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resSave = util.associateElearningContent(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), String.valueOf(ayr), String.valueOf(stdId), String.valueOf(subId), String.valueOf(subUnitsSyllabusDetails.getUniId()), String.valueOf(subUnitsSyllabusDetails.getSunId()), String.valueOf(usrId), enteredContentLink, selectedContentType, enteredContentName, enteredComment, enteredDetails, "L", "O");
                return resSave;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class GetElearningContentTask extends AsyncTask<String, String, String> {
        private String resSave;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {

                        JSONObject dataObj = resArray.getJSONObject(1);
                        JSONArray dataArray = dataObj.getJSONArray("data");

                        eLearningContentArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            ELearningContentData eLearningContentData = new ELearningContentData();
                            JSONObject object = dataArray.getJSONObject(i);

                            eLearningContentData.setEclDescription(object.getString("eclDescription"));
                            eLearningContentData.setEclLink(object.getString("eclLink"));
                            eLearningContentData.setEclDetails(object.getString("eclDetails"));

                            eLearningContentArrayList.add(eLearningContentData);

                        }

                        if (eLearningContentArrayList.size() > 0) {

                            final Dialog dialog = new Dialog(context);
                            dialog.setContentView(R.layout.dialog_view_elearning_content);
                            dialog.show();
                            Window window = dialog.getWindow();
                            dialog.setCancelable(false);
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                            dialog.setCanceledOnTouchOutside(false);

                            ImageButton closeButton = dialog.findViewById(R.id.closeButton);
                            RecyclerView scheduledTasksRecyclerView = dialog.findViewById(R.id.recyclerView);

                            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                            scheduledTasksRecyclerView.setLayoutManager(layoutManager);
                            scheduledTasksRecyclerView.setItemAnimator(new DefaultItemAnimator());

                            ViewElearningContentRecyAdapter viewElearningContentRecyAdapter = new ViewElearningContentRecyAdapter(context, eLearningContentArrayList);
                            scheduledTasksRecyclerView.setAdapter(viewElearningContentRecyAdapter);
                            viewElearningContentRecyAdapter.notifyDataSetChanged();

                            closeButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                        } else {

                            Toast.makeText(context, "eLearning Content not found", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(context, "eLearning Content not found", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, "eLearning Content not found", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, "eLearning Content not found", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resSave = util.getElearningContents(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), String.valueOf(subUnitsSyllabusDetails.getSunElcId()));
                return resSave;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }
    }
}

package com.techled.teacheranalytics.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.techled.teacheranalytics.R;

import java.util.ArrayList;

public class StudentsListRecyclerAdapter extends RecyclerView.Adapter<StudentsListRecyclerAdapter.MyViewHolder> {

    private ArrayList<String> dataSet;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView studentName, studentDetails;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.studentName = itemView.findViewById(R.id.studentName);
            studentDetails = itemView.findViewById(R.id.studentDetails);
        }
    }

    public StudentsListRecyclerAdapter(Context context, ArrayList<String> data) {
        this.context = context;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_student_list, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView studentName = holder.studentName;
        TextView studentDetails = holder.studentDetails;

        String[] studentData = dataSet.get(listPosition).split("-");

        studentName.setText(studentData[2]);
        studentDetails.setText("Standard - ".concat(studentData[0]).concat(", Division - ").concat(studentData[1]));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}
package com.techled.teacheranalytics.adapter;

import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.PastEvents;

import java.util.ArrayList;

/**
 * Created by sonal on 2/12/15.
 */
public class PastEventsRecyclerAdapter extends RecyclerView
        .Adapter<PastEventsRecyclerAdapter
        .DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewForCalendarAdapter";
    private ArrayList<PastEvents> mDataset;
    private Context context;
    private boolean flag=false;
    private Integer orgId;
    private Integer insId;
    private Integer dscId;
    private Integer ayrYear,stdId,divId,hwkId,stuId;


    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView evenTitle,evenDsc,evenOrgBy,evenFrom,evenTo,viewImages;


        public DataObjectHolder(View itemView) {
            super(itemView);
            evenTitle = (TextView) itemView.findViewById(R.id.evenTitle);
            evenDsc = (TextView) itemView.findViewById(R.id.evenDsc);
            evenOrgBy = (TextView) itemView.findViewById(R.id.evenOrgBy);
            evenFrom = (TextView) itemView.findViewById(R.id.evenFrom);
            evenTo = (TextView) itemView.findViewById(R.id.evenTo);
            viewImages=(TextView)itemView.findViewById(R.id.viewImages);


        }


    }


    public PastEventsRecyclerAdapter(Context context, ArrayList<PastEvents> myDataset)
    {
        this.context=context;
        mDataset = myDataset;

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_past_events, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        try {
            final PastEvents data = mDataset.get(position);


            orgId = data.getOrgId();
            insId = data.getInsId();
            dscId = data.getDscId();
            ayrYear = data.getAyr();

            try {


                holder.evenTitle.setText(Html.fromHtml(data.getEvtTitle()));
                if (data.getEvtDesc().equals("")) {
                    holder.evenDsc.setText("Description: -");
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        holder.evenDsc.setText(Html.fromHtml(data.getEvtDesc(), Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        holder.evenDsc.setText("Description: " + Html.fromHtml(data.getEvtDesc()));
                    }
                    //  holder.evenDsc.setText("Description : " + Html.fromHtml(new String(data.getEvtDesc().getBytes("ISO-8859-1"), "UTF-8")));

                }
                holder.evenOrgBy.setText("Organized By: " + data.getEvtOrganizedBy());
                if(data.getEvtFrom().equals(data.getEvtTo()))
                {
                    holder.evenTo.setText(""+data.getEvtFrom());
                }else {
                    holder.evenTo.setText(""+data.getEvtFrom()+" - "+data.getEvtTo());
                }
                int imgCount = data.getImgCount();
                if (imgCount == 0) {
                    holder.viewImages.setVisibility(View.GONE);
                } else {
                    holder.viewImages.setVisibility(View.VISIBLE);
                }
               /* holder.viewImages.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context, PastEventsImagesActivity.class);
                        intent.putExtra("orgId", data.getOrgId());
                        intent.putExtra("insId", data.getInsId());
                        intent.putExtra("dscId", data.getDscId());
                        intent.putExtra("evtId", data.getEvtId());

                        intent.putExtra("ayr", data.getAyr());
                        intent.putExtra("evtTitle", data.getEvtTitle());
                        intent.putExtra("evtDsc", data.getEvtDesc());
                        intent.putExtra("evtOrgBy", data.getEvtOrganizedBy());
                        intent.putExtra("evtFrom", data.getEvtFrom());
                        intent.putExtra("evtTo", data.getEvtTo());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    }
                });*/


            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}

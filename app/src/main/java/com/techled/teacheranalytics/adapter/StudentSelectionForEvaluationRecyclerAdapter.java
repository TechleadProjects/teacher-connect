package com.techled.teacheranalytics.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.techled.teacheranalytics.ActivityList.EvaluateAnswerPaperModule.SubmittedPaperSelectionActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.EvaluateAnsPaperStudentData;

import java.io.File;
import java.util.ArrayList;

import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ImageEditorIntentBuilder;

public class StudentSelectionForEvaluationRecyclerAdapter extends RecyclerView.Adapter<StudentSelectionForEvaluationRecyclerAdapter.MyViewHolder> {

    private ArrayList<EvaluateAnsPaperStudentData> dataSet;
    private Context context;
    private String ayrYear, standard, division, subject, examType, examSchedule, examName;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, rollNo, status, marks;
        Button viewUploadedPapers;
        ImageView paperEvaluatedFlagImg;
        LinearLayout studentDataLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            rollNo = itemView.findViewById(R.id.rollNo);
            status = itemView.findViewById(R.id.status);
            marks = itemView.findViewById(R.id.marks);
            paperEvaluatedFlagImg = itemView.findViewById(R.id.paperEvaluatedFlagImg);
            viewUploadedPapers = itemView.findViewById(R.id.viewUploadedPapers);
            studentDataLayout = itemView.findViewById(R.id.studentDataLayout);
        }
    }

    public StudentSelectionForEvaluationRecyclerAdapter(Context context, ArrayList<EvaluateAnsPaperStudentData> data, String ayrYear, String standard, String division, String subject, String examType, String examSchedule, String examName) {
        this.context = context;
        this.dataSet = data;
        this.ayrYear = ayrYear;
        this.standard = standard;
        this.division = division;
        this.subject = subject;
        this.examType = examType;
        this.examSchedule = examSchedule;
        this.examName = examName;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_student_selection_for_evaluation_details, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView name = holder.name;
        TextView rollNo = holder.rollNo;
        TextView status = holder.status;
        TextView marks = holder.marks;
        ImageView paperEvaluatedFlagImg = holder.paperEvaluatedFlagImg;
        Button viewUploadedPapers = holder.viewUploadedPapers;
        LinearLayout studentDataLayout = holder.studentDataLayout;

        name.setText(dataSet.get(listPosition).getStuName());
        rollNo.setText("Roll No: ".concat(dataSet.get(listPosition).getRollNo()));
        status.setText(dataSet.get(listPosition).getUploadStatus());

        if (dataSet.get(listPosition).getMarks() == null || dataSet.get(listPosition).getMarks().equals("") || dataSet.get(listPosition).getMarks().equals("null")) {

            marks.setText("Marks : ".concat("-"));
        } else {

            marks.setText("Marks : ".concat(dataSet.get(listPosition).getMarks()));
        }

        paperEvaluatedFlagImg.bringToFront();
        if (dataSet.get(listPosition).getUploadStatus() == null || dataSet.get(listPosition).getUploadStatus().equalsIgnoreCase("Answer Paper Not Uploaded")) {
            paperEvaluatedFlagImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.not_evaluated));
            studentDataLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.button_background_2));
            viewUploadedPapers.setVisibility(View.GONE);
        } else {
            paperEvaluatedFlagImg.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.evaluated));
            studentDataLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.button_background_15));
            viewUploadedPapers.setVisibility(View.VISIBLE);
        }

        viewUploadedPapers.setOnClickListener(view -> {

            if (dataSet.get(listPosition).getUploadStatus() == null || dataSet.get(listPosition).getUploadStatus().equalsIgnoreCase("Answer Paper Not Uploaded")) {

                Toast.makeText(context, "Answer papers not uploaded.", Toast.LENGTH_SHORT).show();
            } else {

                Intent intent = new Intent(context, SubmittedPaperSelectionActivity.class);
                intent.putExtra("ayrYear", ayrYear);
                intent.putExtra("standard", standard);
                intent.putExtra("division", division);
                intent.putExtra("subject", subject);
                intent.putExtra("examType", examType);
                intent.putExtra("examSchedule", examSchedule);
                intent.putExtra("examName", examName);
                intent.putExtra("stuId", dataSet.get(listPosition).getStuId());
                intent.putExtra("stuName", dataSet.get(listPosition).getStuName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}


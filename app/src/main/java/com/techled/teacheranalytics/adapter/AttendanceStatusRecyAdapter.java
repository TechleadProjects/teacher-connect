package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.AttendanceStatusDetails;
import com.techled.teacheranalytics.pojo.StudentAttendanceStatus;

import java.util.ArrayList;

public class AttendanceStatusRecyAdapter extends RecyclerView.Adapter<AttendanceStatusRecyAdapter.AttendanceStatusHolder> {

    private ArrayList<StudentAttendanceStatus> attendanceStatusDetailsArrayList;
    private Context context;

    public AttendanceStatusRecyAdapter(ArrayList<StudentAttendanceStatus> attendanceStatusDetailsArrayList, Context context) {
        this.attendanceStatusDetailsArrayList = attendanceStatusDetailsArrayList;
        this.context = context;
    }

    public class AttendanceStatusHolder extends RecyclerView.ViewHolder {
        private TextView uName, uConactNo;

        public AttendanceStatusHolder(View itemView) {
            super(itemView);
            uName = (TextView) itemView.findViewById(R.id.uName);
            uConactNo = (TextView) itemView.findViewById(R.id.uConactNo);

        }
    }

    @Override
    public AttendanceStatusHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_attd_status_details, parent, false);
        AttendanceStatusHolder holder = new AttendanceStatusHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(AttendanceStatusHolder holder, int position) {
        StudentAttendanceStatus studentAttendanceStatus = attendanceStatusDetailsArrayList.get(position);

        //name
        if (studentAttendanceStatus.getName() == null || studentAttendanceStatus.getName().equals("") || studentAttendanceStatus.getName().equals("null")) {
            holder.uName.setText("-");
        } else {
            holder.uName.setText(studentAttendanceStatus.getName());
        }

        //contact no
        if (studentAttendanceStatus.getContactNo() == null || studentAttendanceStatus.getContactNo().equals("") || studentAttendanceStatus.getContactNo().equals("null")) {
            holder.uConactNo.setText("-");
        } else {
            holder.uConactNo.setText(studentAttendanceStatus.getContactNo());
        }

    }

    @Override
    public int getItemCount() {
        return attendanceStatusDetailsArrayList.size();
    }
}

package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.StuMarkData;
import com.techled.teacheranalytics.pojo.StudentInfo;
import com.techled.teacheranalytics.pojo.SubjectInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sonal on 9/2/18.
 */

public class SubjectAnalyticsRecyAdapter extends RecyclerView.Adapter<SubjectAnalyticsRecyAdapter.SubjectAnalyticsDataHolder>{

    private ArrayList<SubjectInfo> subjectInfoArrayList;
    private Context context;
    private int countRed=0,countGreen=0,countOrange=0;

    public SubjectAnalyticsRecyAdapter(ArrayList<SubjectInfo> subjectInfoArrayList, Context context) {
        this.subjectInfoArrayList = subjectInfoArrayList;
        this.context = context;
    }

    public class SubjectAnalyticsDataHolder extends RecyclerView.ViewHolder
    {
        private TextView txtSubjectInfo,countGood,countAvg,countBad;
        private RecyclerView gridLayout;
        public SubjectAnalyticsDataHolder(View itemView) {
            super(itemView);
            txtSubjectInfo = (TextView) itemView.findViewById(R.id.txtSubjectInfo);
            gridLayout = (RecyclerView) itemView.findViewById(R.id.gridLayout);
            countGood = (TextView)itemView.findViewById(R.id.countGood);
            countAvg = (TextView)itemView.findViewById(R.id.countAvg);
            countBad = (TextView)itemView.findViewById(R.id.countBad);

        }
    }
    @Override
    public SubjectAnalyticsDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_subject_info,parent,false);
        SubjectAnalyticsDataHolder holder = new SubjectAnalyticsDataHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SubjectAnalyticsDataHolder holder, int position) {
        try {
            SubjectInfo subjectInfo = subjectInfoArrayList.get(position);
            holder.txtSubjectInfo.setText("" + subjectInfo.getSubName());

            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            holder.gridLayout.setLayoutManager(layoutManager);

            List<StudentInfo> studentInfoList = subjectInfo.getStudentInfoArrayList();


            ArrayList<StuMarkData> stuMarkDataArrayList = new ArrayList<>();
            for (int i = 0; i < studentInfoList.size(); i++) {

                 if(studentInfoList.get(i).getColor().equals("#43A047")){
                    countGreen++;
                }else if(studentInfoList.get(i).getColor().equals("#FB8C00"))
                {
                    countOrange++;
                }else {
                     countRed++;
                 }
                StuMarkData stuMarkData = new StuMarkData();
                stuMarkData.setStuName(studentInfoList.get(i).getStuName());
                stuMarkData.setStuId(studentInfoList.get(i).getStuId());

                if(studentInfoList.get(i).getMarks().equals("null"))
                {
                    stuMarkData.setStuMark((float) Integer.parseInt(studentInfoList.get(i).getMarks().replace("null", "0")));

                }else{
                    stuMarkData.setStuMark((float) Integer.parseInt(studentInfoList.get(i).getMarks().replace(".0", "")));

                }
                stuMarkData.setColor(studentInfoList.get(i).getColor());
                stuMarkDataArrayList.add(stuMarkData);

                Collections.sort(stuMarkDataArrayList);


            }
            holder.countAvg.setText("Avg. : "+countOrange);
            holder.countBad.setText("Bad : "+countRed);
            holder.countGood.setText("Good : "+countGreen);



            //set adapter
            StuMarkRecyAdapter adapter = new StuMarkRecyAdapter(stuMarkDataArrayList, context);
            holder.gridLayout.setAdapter(adapter);

        }catch (Exception e)
        {
            e.printStackTrace();
        }


     /*   for(int i =0 ;i<studentInfoList.size();i++)
        {




          *//*  LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams p1 =new LinearLayout.LayoutParams(  ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            p1.setMargins(0,5,0,5);
            linearLayout.setLayoutParams(p1);

            GridLayout gridLayout = new GridLayout(context);
            gridLayout.setAlignmentMode(GridLayout.ALIGN_BOUNDS);
            gridLayout.setColumnCount(2);


            TextView txtStuName = new TextView(context);
            txtStuName.setText(i+1+") "+studentInfoList.get(i).getStuName());
            txtStuName.setTextSize(16);

            TextView txtMark = new TextView(context);
            txtMark.setTextSize(20);
            txtMark.setText("  - "+studentInfoList.get(i).getMarks());

            GridLayout.LayoutParams param1 =new GridLayout.LayoutParams();
            param1.height = GridLayout.LayoutParams.WRAP_CONTENT;
            param1.width = GridLayout.LayoutParams.WRAP_CONTENT;
            param1.setGravity(Gravity.CENTER);

            txtMark.setLayoutParams(param1);
            txtStuName.setPadding(5,5,5,5);
            txtMark.setPadding(5,5,5,5);

            gridLayout.addView(txtStuName);
            gridLayout.addView(txtMark);
            linearLayout.addView(gridLayout);
            linearLayout.setBackgroundResource(R.drawable.rect_shape);
            holder.gridLayout.addView(linearLayout);

*//*
            //


            *//*holder.gridLayout.setColumnCount(2);
            holder.gridLayout.setAlignmentMode(GridLayout.ALIGN_BOUNDS);

            GridLayout.LayoutParams param =new GridLayout.LayoutParams();
            param.height = GridLayout.LayoutParams.WRAP_CONTENT;
            param.width = GridLayout.LayoutParams.WRAP_CONTENT;
            param.setMargins(0,5,0,5);

            TextView txtStuName = new TextView(context);
            txtStuName.setText(i+1+") "+studentInfoList.get(i).getStuName());
            txtStuName.setLayoutParams(param);
          //  txtStuName.setBackgroundResource(R.drawable.rect_shape_orange);
            txtStuName.setTextSize(16);



            TextView txtMark = new TextView(context);
            txtMark.setTextSize(20);
            txtMark.setText("  - "+studentInfoList.get(i).getMarks());
            txtMark.setLayoutParams(param);
          //  txtMark.setBackgroundResource(R.drawable.rect_shape_orange);

            GridLayout.LayoutParams param1 =new GridLayout.LayoutParams();
            param1.height = GridLayout.LayoutParams.WRAP_CONTENT;
            param1.width = GridLayout.LayoutParams.WRAP_CONTENT;
            param1.setGravity(Gravity.CENTER);

            txtMark.setLayoutParams(param1);
            txtStuName.setPadding(5,5,5,5);
            txtMark.setPadding(5,5,5,5);

            holder.gridLayout.setPadding(15,15,15,15);
            holder.gridLayout.addView(txtStuName);
            holder.gridLayout.addView(txtMark);
*//*

        }*/
    }

    @Override
    public int getItemCount() {
        return subjectInfoArrayList.size();
    }
}





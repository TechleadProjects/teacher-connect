package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.LeaveTheSchoolRequestDetails;

import java.util.ArrayList;

public class LeaveTheSchoolRequestRecyAdapter extends RecyclerView.Adapter<LeaveTheSchoolRequestRecyAdapter.LeaveTheSchoolDetailsHolder> {
    private ArrayList<LeaveTheSchoolRequestDetails> leaveTheSchoolRequestDetailsArrayList;
    private Context context;

    public LeaveTheSchoolRequestRecyAdapter(ArrayList<LeaveTheSchoolRequestDetails> leaveTheSchoolRequestDetailsArrayList, Context context) {
        this.leaveTheSchoolRequestDetailsArrayList = leaveTheSchoolRequestDetailsArrayList;
        this.context = context;
    }

    public class LeaveTheSchoolDetailsHolder extends RecyclerView.ViewHolder {
        private TextView txtStuName, txtReason, txtAppliedDate, txtTntDate;

        public LeaveTheSchoolDetailsHolder(@NonNull View itemView) {
            super(itemView);
            txtStuName = (TextView) itemView.findViewById(R.id.txtStuName);
            txtReason = (TextView) itemView.findViewById(R.id.txtReason);
            txtAppliedDate = (TextView) itemView.findViewById(R.id.txtAppliedDate);
            txtTntDate = (TextView) itemView.findViewById(R.id.txtTntDate);
        }
    }

    @NonNull
    @Override
    public LeaveTheSchoolDetailsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_leave_the_school_request, viewGroup, false);
        LeaveTheSchoolDetailsHolder holder = new LeaveTheSchoolDetailsHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull LeaveTheSchoolDetailsHolder leaveTheSchoolDetailsHolder, int i) {

        LeaveTheSchoolRequestDetails leaveTheSchoolRequestDetails = leaveTheSchoolRequestDetailsArrayList.get(i);
        leaveTheSchoolDetailsHolder.txtAppliedDate.setText("Applied Date : " + leaveTheSchoolRequestDetails.getStuAppliedLcDate());
        leaveTheSchoolDetailsHolder.txtReason.setText("Reason : " + leaveTheSchoolRequestDetails.getStuLcReason());
        leaveTheSchoolDetailsHolder.txtStuName.setText(leaveTheSchoolRequestDetails.getStuName());

        if(leaveTheSchoolRequestDetails.getStuTentLcDate() ==null || leaveTheSchoolRequestDetails.getStuTentLcDate().equals("")||leaveTheSchoolRequestDetails.equals("null"))
        {
            leaveTheSchoolDetailsHolder.txtTntDate.setText("Tentative Leaving Date : -" );
        }else {
            leaveTheSchoolDetailsHolder.txtTntDate.setText("Tentative Leaving Date : " + leaveTheSchoolRequestDetails.getStuTentLcDate());
        }


    }

    @Override
    public int getItemCount() {
        return leaveTheSchoolRequestDetailsArrayList.size();
    }
}

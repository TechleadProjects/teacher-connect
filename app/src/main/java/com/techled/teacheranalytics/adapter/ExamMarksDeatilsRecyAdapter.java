package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.MarkAttendance;
import com.techled.teacheranalytics.pojo.StuExamsMarksDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by techlead on 27/6/18.
 */

public class ExamMarksDeatilsRecyAdapter extends RecyclerView.Adapter<ExamMarksDeatilsRecyAdapter.ExamMarksDataHolder> {


    private Context context;
    private ArrayList<StuExamsMarksDetails> stuExamsMarksDetailsArrayList;
    private boolean editable;

    public ExamMarksDeatilsRecyAdapter(Context context, ArrayList<StuExamsMarksDetails> stuExamsMarksDetailsArrayList, boolean editable) {
        this.context = context;
        this.stuExamsMarksDetailsArrayList = stuExamsMarksDetailsArrayList;
        this.editable = editable;
    }

    public class ExamMarksDataHolder extends RecyclerView.ViewHolder {
        private TextView txtStuName, txtStatus;
        private EditText txtMarks;

        public ExamMarksDataHolder(View itemView) {
            super(itemView);
            txtStuName = (TextView) itemView.findViewById(R.id.txtStuName);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtMarks = (EditText) itemView.findViewById(R.id.txtMarks);

        }
    }

    @Override
    public ExamMarksDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_stu_exam_marks, parent, false);
        ExamMarksDataHolder holder = new ExamMarksDataHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ExamMarksDataHolder holder, final int position) {
        StuExamsMarksDetails stuExamsMarksDetails = stuExamsMarksDetailsArrayList.get(position);

        if(stuExamsMarksDetails.getStuMarks().equals("null")|| stuExamsMarksDetails.getStuMarks().equals("")||stuExamsMarksDetails.getStuMarks()==null)
        {
            stuExamsMarksDetails.setStuMarks("");
        }

        stuExamsMarksDetailsArrayList.get(position).setStuOldMark(stuExamsMarksDetails.getStuMarks());
        //student name
        if (stuExamsMarksDetails.getStuName().equals("null") || stuExamsMarksDetails.getStuName().equals("") || stuExamsMarksDetails.getStuName() == null) {
            holder.txtStuName.setText("-");
        } else {
            holder.txtStuName.setText("" + stuExamsMarksDetails.getStuName());

        }
        //student marks
        if (stuExamsMarksDetails.getStuMarks() == null || stuExamsMarksDetails.getStuMarks().equals("") || stuExamsMarksDetails.getStuMarks().equals("null")) {
            holder.txtMarks.setText("-");
        } else {
            holder.txtMarks.setText("" + stuExamsMarksDetails.getStuMarks());
        }

        //status
        if (stuExamsMarksDetails.getStuAttd().equals("") || stuExamsMarksDetails.getStuAttd() == null || stuExamsMarksDetails.getStuAttd().equals("null")) {
            holder.txtStatus.setText("-");
        } else {
            if (stuExamsMarksDetails.getStuAttd().equals("P")) {
                holder.txtStatus.setText("P");
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.green));

            } else if (stuExamsMarksDetails.getStuAttd().equals("A")) {
                holder.txtStatus.setText("A");
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
            } else if (stuExamsMarksDetails.getStuAttd().equals("E")) {
                holder.txtStatus.setText("E");
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.blue));
            } else if (stuExamsMarksDetails.getStuAttd().equals("S")) {
                holder.txtStatus.setText("S");
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.orange));
            }
        }

        if (editable == false) {
            holder.txtMarks.setClickable(false);
            holder.txtMarks.setEnabled(false);
        } else {
            holder.txtMarks.setEnabled(true);
            holder.txtMarks.setClickable(true);
        }
/*
        holder.txtMarks.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(editable == true) {

                    String marks = holder.txtMarks.getText().toString();
                    stuExamsMarksDetailsArrayList.get(position).setStuMarks(marks);
                }
                return false;
            }
        });*/


        holder.txtMarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editable == true) {

                    String marks = holder.txtMarks.getText().toString();
                    stuExamsMarksDetailsArrayList.get(position).setStuMarks(marks);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editable == true) {

                    String marks = holder.txtMarks.getText().toString();
                    if (marks == null){
                        marks = "";

                    }
                    stuExamsMarksDetailsArrayList.get(position).setStuMarks(marks);
                }
            }
        });
        /*holder.txtMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editable == true) {

                    String marks = holder.txtMarks.getText().toString();
                    stuExamsMarksDetailsArrayList.get(position).setStuMarks(marks);
                }
            }
        });
*/


    }

    @Override
    public int getItemCount() {
        return stuExamsMarksDetailsArrayList.size();
    }

    // method to access in activity after updating selection
    public ArrayList<StuExamsMarksDetails> getList() {
        return stuExamsMarksDetailsArrayList;
    }
}

package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.MyLectureDetails;

import java.util.ArrayList;
import java.util.List;

public class StaffTimetableRecyAdapter extends RecyclerView.Adapter<StaffTimetableRecyAdapter.StaffTimetableHolder> {

    private List<MyLectureDetails> detailTimetableList, timetableList;

    public class StaffTimetableHolder extends RecyclerView.ViewHolder {
        public TextView dayName, timelineDetail, scheduledDate;

        public StaffTimetableHolder(View view) {
            super(view);
            dayName = view.findViewById(R.id.dayName);
            scheduledDate = view.findViewById(R.id.scheduledDate);
            timelineDetail = view.findViewById(R.id.timelineDetail);
        }
    }

    public StaffTimetableRecyAdapter(List<MyLectureDetails> detailTimetableList, List<MyLectureDetails> timetableList) {
        this.detailTimetableList = detailTimetableList;
        this.timetableList = timetableList;
    }

    @Override
    public StaffTimetableHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_staff_timetable_list, parent, false);

        return new StaffTimetableHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StaffTimetableHolder holder, int position) {
        MyLectureDetails timetable = detailTimetableList.get(position);
        holder.dayName.setText(timetable.getDayName());
        holder.scheduledDate.setText(timetable.getScheduledDate());

        holder.timelineDetail.setText("");

        for (int i=0; i<timetableList.size(); i++){
            if(timetable.getDayName().equals(timetableList.get(i).getDayName())){
                String time = timetableList.get(i).getLectureStartTime() + " - " + timetableList.get(i).getLectureEndTime();
                String subject = timetableList.get(i).getSubName();
                String standard = timetableList.get(i).getStd() + " - " + timetableList.get(i).getDiv();
                holder.timelineDetail.append(time + " | " + subject + " | " + standard + "\n\n");
            }
        }
    }

    @Override
    public int getItemCount() {
        return detailTimetableList.size();
    }
}
package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.AttendanceManagementDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonal on 1/3/18.
 */

public class AttendanceManagementRecyAdapter extends RecyclerView.Adapter<AttendanceManagementRecyAdapter.AttMagtHolder> {
    private Context context;
    private ArrayList<AttendanceManagementDetails> managementDetailsArrayList;

    public AttendanceManagementRecyAdapter(Context context, ArrayList<AttendanceManagementDetails> managementDetailsArrayList) {
        this.context = context;
        this.managementDetailsArrayList = managementDetailsArrayList;

    }

    public class AttMagtHolder extends RecyclerView.ViewHolder {
        private TextView txtStuName;
        private CheckBox stuCheckbox;

        public AttMagtHolder(View itemView) {
            super(itemView);
            txtStuName = (TextView) itemView.findViewById(R.id.txtStuName);
            stuCheckbox = (CheckBox) itemView.findViewById(R.id.stuCheckbox);

        }
    }

    @Override
    public AttMagtHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_attendance_management, parent, false);
        AttMagtHolder holder = new AttMagtHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final AttMagtHolder holder, final int position) {

        final AttendanceManagementDetails managementDetails = managementDetailsArrayList.get(position);
        //holder.stuCheckbox.setChecked(true);
       // managementDetailsArrayList.get(position).setSelected(true);
        holder.txtStuName.setText(managementDetails.getStuName() + " ( " + managementDetails.getRollNo() + " ) ");

        holder.stuCheckbox.setChecked(managementDetailsArrayList.get(position).isSelected());
        holder.stuCheckbox.setTag(managementDetailsArrayList.get(position));


        holder.stuCheckbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                AttendanceManagementDetails contact = (AttendanceManagementDetails) cb.getTag();

                contact.setSelected(cb.isChecked());
                managementDetailsArrayList.get(position).setSelected(cb.isChecked());


            }
        });


    }

    @Override
    public int getItemCount() {
        return managementDetailsArrayList.size();
    }

    // method to access in activity after updating selection
    public List<AttendanceManagementDetails> getList() {
        return managementDetailsArrayList;
    }
}


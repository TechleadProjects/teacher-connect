package com.techled.teacheranalytics.adapter;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.StaffLeaveApplyModule.StaffLeaveApplicationActivity;
import com.techled.teacheranalytics.ActivityList.StudentLeaveApprovalModule.StudentLeaveApprovalActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.PendingStuLeave;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by sonal on 19/3/18.
 */

public class PendingStuLeavesRecyAdapter extends RecyclerView.Adapter<PendingStuLeavesRecyAdapter.StuLeavesHolder> {

    private ArrayList<PendingStuLeave> stuLeaveArrayList;
    private Context context;
    private int stfId;
    private Dialog dialog2;
    private String sendEmail,sendSms;

    public PendingStuLeavesRecyAdapter(ArrayList<PendingStuLeave> stuLeaveArrayList, Context context, Integer stfId) {
        this.stuLeaveArrayList = stuLeaveArrayList;
        this.context = context;
        this.stfId = stfId;
    }

    public class StuLeavesHolder extends RecyclerView.ViewHolder {
        private TextView txtStuName, txtLeaveReason, txtLeaveTime;
        private Button btnApprove, btnCall;

        public StuLeavesHolder(View itemView) {
            super(itemView);
            txtStuName = (TextView) itemView.findViewById(R.id.txtStuName);
            txtLeaveReason = (TextView) itemView.findViewById(R.id.txtLeaveReason);
            txtLeaveTime = (TextView) itemView.findViewById(R.id.txtLeaveTime);
            btnApprove = (Button) itemView.findViewById(R.id.btnApprove);

        }
    }

    @Override
    public StuLeavesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_pending_leave_approval, parent, false);
        StuLeavesHolder holder = new StuLeavesHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(StuLeavesHolder holder, int position) {
        final PendingStuLeave stuLeave = stuLeaveArrayList.get(position);
        if (stuLeave.getLeaveDuration().equals("1")) {
            holder.txtLeaveTime.setText("Duration : " + stuLeave.getLeaveDuration() + " day");

        } else {
            holder.txtLeaveTime.setText("Duration : " + stuLeave.getLeaveDuration() + " days");
        }

        holder.txtLeaveReason.setText("Reason : " + stuLeave.getLeaveReason());
        holder.txtStuName.setText(stuLeave.getStuName());
        holder.btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                // Include dialog.xml file
                dialog.setContentView(R.layout.dialog);
                // Set dialog title
                dialog.setTitle("Comment");

                // set values for custom dialog components - text and
                // button
                dialog.show();

                Button approveButton = (Button) dialog
                        .findViewById(R.id.dlgapproveButton);

                approveButton.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {

                        try {
                            dialog2 = new Dialog(context);
                            dialog2.setContentView(R.layout.confirm_send_sms_email);
                            Window window = dialog2.getWindow();
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            dialog2.show();

                            final CheckBox chkSms = (CheckBox) dialog2.findViewById(R.id.chkSms);
                            final CheckBox chkEmail = (CheckBox) dialog2.findViewById(R.id.chkEmail);

                            Button btnOk = (Button) dialog2.findViewById(R.id.btnOk);
                            Button btnCancel = (Button) dialog2.findViewById(R.id.btnCancel);

                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        dialog2.dismiss();

                                        //email

                                        if (chkEmail.isChecked() == true) {
                                            sendEmail = "Y";
                                        } else {
                                            sendEmail = "N";
                                        }

                                        //sms
                                        if (chkSms.isChecked() == true) {
                                            sendSms = "Y";
                                        } else {
                                            sendSms = "N";
                                        }


                                        EditText comment = (EditText) dialog
                                                .findViewById(R.id.leaveComment);
                                        String c = (comment.getText() == null || comment
                                                .getText().equals("")) ? "-" : comment
                                                .getText().toString();

                                        Util util = new Util();

                                        String result = null;
                                        try {
                                            result = util.approveStuLeave(stuLeave.getOrgId(), stuLeave.getInsId(), stuLeave.getDscId(), stuLeave.getStuId(), stuLeave.getLeaveId(), "A", c, stfId,sendSms,sendEmail);


                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                        JSONObject object = new JSONObject(result);
                                        String status = object.getString("status");

                                        if (status.equals("Failure")) {
                                            Toast.makeText(context, "Fail to processed leave!", Toast.LENGTH_LONG).show();
                                            dialog.dismiss();
                                            return;
                                        } else {
                                            Toast.makeText(context, "Leave processed successfully!", Toast.LENGTH_LONG).show();

                                            Intent intent = new Intent(context, StudentLeaveApprovalActivity.class);
                                            ((Activity) context).startActivity(intent);
                                            ((Activity) context).finish();
                                            dialog.dismiss();
                                        }
                                    }catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog2.dismiss();
                                }
                            });


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                });

                Button declineButton = (Button) dialog.findViewById(R.id.dlgdeclineButton);
                // if decline button is clicked, close the custom dialog
                declineButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg) {
                        try {


                            dialog2 = new Dialog(context);
                            dialog2.setContentView(R.layout.confirm_send_sms_email);
                            Window window = dialog2.getWindow();
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            dialog2.show();

                            final CheckBox chkSms = (CheckBox) dialog2.findViewById(R.id.chkSms);
                            final CheckBox chkEmail = (CheckBox) dialog2.findViewById(R.id.chkEmail);

                            Button btnOk = (Button) dialog2.findViewById(R.id.btnOk);
                            Button btnCancel = (Button) dialog2.findViewById(R.id.btnCancel);

                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        dialog2.dismiss();
                                        if (chkEmail.isChecked() == true) {
                                            sendEmail = "Y";
                                        } else {
                                            sendEmail = "N";
                                        }

                                        //sms
                                        if (chkSms.isChecked() == true) {
                                            sendSms = "Y";
                                        } else {
                                            sendSms = "N";
                                        }

                                        // Close dialog
                                        EditText comment = (EditText) dialog
                                                .findViewById(R.id.leaveComment);
                                        String c = (comment.getText() == null || comment
                                                .getText().equals("")) ? "-" : comment
                                                .getText().toString();

                                        Util util = new Util();

                                        String result = null;
                                        try {
                                            result = util.approveStuLeave(stuLeave.getOrgId(), stuLeave.getInsId(), stuLeave.getDscId(), stuLeave.getStuId(), stuLeave.getLeaveId(), "R", c, stfId,sendSms,sendEmail);

                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }

                                        JSONObject object = new JSONObject(result);
                                        String status = object.getString("status");

                                        if(status.equals("Failure"))
                                        {
                                            Toast.makeText(context, "Fail to processed leave!", Toast.LENGTH_LONG).show();
                                            dialog.dismiss();
                                            return;
                                        }else {
                                            Toast.makeText(context, "Leave processed successfully!", Toast.LENGTH_LONG).show();

                                            Intent intent = new Intent(context, StudentLeaveApprovalActivity.class);
                                            ((Activity) context).startActivity(intent);
                                            ((Activity) context).finish();
                                            dialog.dismiss();

                                        }
                                    }catch (Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog2.dismiss();
                                }
                            });


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                Button cancelButton = (Button) dialog.findViewById(R.id.dlgcancel);
                // if decline button is clicked, close the custom dialog
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });


            }
        });


//        holder.btnCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                try {
//                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + stuLeave.getMobileNo()));
//                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                        return;
//                    }
//                    context.startActivity(intent);
//                }catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return stuLeaveArrayList.size();
    }
}

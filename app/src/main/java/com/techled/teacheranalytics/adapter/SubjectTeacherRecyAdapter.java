package com.techled.teacheranalytics.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.SubjectTeacherDetails;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sonal on 28/2/18.
 */

public class SubjectTeacherRecyAdapter extends RecyclerView.Adapter<SubjectTeacherRecyAdapter.SubjectTeacherHolder> {

    private Context context;
    private ArrayList<SubjectTeacherDetails> teacherDetailsArrayList;
    public  class  SubjectTeacherHolder extends RecyclerView.ViewHolder
    {
        private TextView txtName,txtStfAdd,txtMobileNo,txtStfEmail;
        private CircleImageView stfImg;
        public SubjectTeacherHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtStfAdd = (TextView) itemView.findViewById(R.id.txtStfAdd);
            txtMobileNo = (TextView) itemView.findViewById(R.id.txtMobileNo);
            txtStfEmail = (TextView) itemView.findViewById(R.id.txtStfEmail);
            stfImg = (CircleImageView) itemView.findViewById(R.id.stfImg);

        }
    }


    @Override
    public SubjectTeacherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_subject_teacher_details,parent,false);
        SubjectTeacherHolder holder = new SubjectTeacherHolder(view);
        return holder;
    }

    public SubjectTeacherRecyAdapter(Context context, ArrayList<SubjectTeacherDetails> teacherDetailsArrayList) {
        this.context = context;
        this.teacherDetailsArrayList = teacherDetailsArrayList;
    }

    @Override
    public void onBindViewHolder(SubjectTeacherHolder holder, int position) {
    SubjectTeacherDetails teacherDetails = teacherDetailsArrayList.get(position);
    try{
        holder.txtName.setText(teacherDetails.getStfName());
        holder.txtMobileNo.setText(teacherDetails.getStfContactNo());
        holder.txtStfAdd.setText(teacherDetails.getStfAddress());
        holder.txtStfEmail.setText(teacherDetails.getStfEmail());
        byte[] byteData = null;
        try {
            // Image Setting
            String[] id = teacherDetails.getStfImage().split(",");
            byteData = new byte[id.length];
            for (int i = 0; i < id.length; i++) {

                byteData[i] = Byte.valueOf(id[i]);
            }
        } catch (Exception e) {
            byteData = null;
        }
        if (byteData != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(
                    byteData);
            Bitmap btMap = BitmapFactory.decodeStream(bais);

            Bitmap circleBitmap = Bitmap.createBitmap(btMap.getWidth(), btMap.getHeight(), Bitmap.Config.ARGB_8888);

            BitmapShader shader = new BitmapShader(btMap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            Paint paint = new Paint();
            paint.setShader(shader);

            Canvas c = new Canvas(circleBitmap);
            c.drawCircle(btMap.getWidth() / 2, btMap.getHeight() / 2, btMap.getWidth() / 2, paint);
            holder.stfImg.setImageBitmap(btMap);


        } else {
            Bitmap noImage = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.defualt_user);

            holder.stfImg.setImageBitmap(noImage);

        }
    }catch (Exception e)
    {
        e.printStackTrace();
    }
    }

    @Override
    public int getItemCount() {
        return teacherDetailsArrayList.size();
    }
}

package com.techled.teacheranalytics.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.IssuesDetails;

import java.util.ArrayList;

public class ViewReportIssuesRecyAdapter extends RecyclerView.Adapter<ViewReportIssuesRecyAdapter.IssuesDetailsHolder> {

    private ArrayList<IssuesDetails> issuesDetailsArrayList;
    private Context context;

    public ViewReportIssuesRecyAdapter(ArrayList<IssuesDetails> issuesDetailsArrayList, Context context) {
        this.issuesDetailsArrayList = issuesDetailsArrayList;
        this.context = context;
    }

    public class IssuesDetailsHolder extends RecyclerView.ViewHolder {

        private TextView txtTitle,txtDesc,txtReportingDate,txtDueDate,txtModule,txtPriority,txtTypeBe, txtIssueStatus;
        public IssuesDetailsHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = (TextView)itemView.findViewById(R.id.txtTitle);
            txtDesc = (TextView)itemView.findViewById(R.id.txtDesc);
            txtReportingDate = (TextView)itemView.findViewById(R.id.txtReportingDate);
            txtDueDate = (TextView)itemView.findViewById(R.id.txtDueDate);
            txtModule = (TextView)itemView.findViewById(R.id.txtModule);
            txtPriority = (TextView)itemView.findViewById(R.id.txtPriority);
            txtTypeBe = (TextView)itemView.findViewById(R.id.txtTypeBe);
            txtIssueStatus = (TextView)itemView.findViewById(R.id.txtIssueStatus);

        }
    }

    @NonNull
    @Override
    public IssuesDetailsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_report_issue, viewGroup, false);
        IssuesDetailsHolder holder = new IssuesDetailsHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull IssuesDetailsHolder issuesDetailsHolder, int i) {

        IssuesDetails issuesDetails = issuesDetailsArrayList.get(i);

        //tittle
        issuesDetailsHolder.txtTitle.setText(""+issuesDetails.getSctTitle());

        //module name
        if(issuesDetails.getMdlName() == null || issuesDetails.getMdlName().equals("") || issuesDetails.getMdlName().equals("null"))
        {
            //issuesDetailsHolder.txtModule.setText("Module: -");
            issuesDetailsHolder.txtModule.setVisibility(View.GONE);
        }else {
            issuesDetailsHolder.txtModule.setText("Module: "+issuesDetails.getMdlName());
        }

        //description
        if(issuesDetails.getSctDescription() == null || issuesDetails.getSctDescription().equals("") || issuesDetails.getSctDescription().equals("null"))
        {
            issuesDetailsHolder.txtDesc.setText("Description: -");
        }else {
            issuesDetailsHolder.txtDesc.setText("Description: "+issuesDetails.getSctDescription());
        }

        //due date
        if(issuesDetails.getSctExpDueDate() == null || issuesDetails.getSctExpDueDate().equals("") || issuesDetails.getSctExpDueDate().equals("null"))
        {
            issuesDetailsHolder.txtDueDate.setText("Due Date: -");
        }else {
            issuesDetailsHolder.txtDueDate.setText("Due Date: "+issuesDetails.getSctExpDueDate());
        }

        //reporting date
        if(issuesDetails.getSctReportingDate() == null || issuesDetails.getSctReportingDate().equals("") || issuesDetails.getSctReportingDate().equals("null"))
        {
            issuesDetailsHolder.txtReportingDate.setText("Reporting Date: -");
        }else {
            issuesDetailsHolder.txtReportingDate.setText("Reporting Date: "+issuesDetails.getSctReportingDate());
        }

        //priority
        if(issuesDetails.getSctPriorityUhml() == null || issuesDetails.getSctPriorityUhml().equals("") || issuesDetails.getSctPriorityUhml().equals("null"))
        {
            issuesDetailsHolder.txtPriority.setText("Priority: -");
        }else {
            issuesDetailsHolder.txtPriority.setText("Priority: "+issuesDetails.getSctPriorityUhml());
        }

        //type
        if(issuesDetails.getSctTypeBe() == null || issuesDetails.getSctTypeBe().equals("") || issuesDetails.getSctTypeBe().equals("null"))
        {
            issuesDetailsHolder.txtTypeBe.setText("Type: -");
        }else {
            issuesDetailsHolder.txtTypeBe.setText("Type: "+issuesDetails.getSctTypeBe());
        }

        //status
        if(issuesDetails.getSctProgressStatusNircj() == null || issuesDetails.getSctProgressStatusNircj().equals("") || issuesDetails.getSctProgressStatusNircj().equals("null"))
        {
            issuesDetailsHolder.txtIssueStatus.setText("Status: -");
        }else {
            issuesDetailsHolder.txtIssueStatus.setText("Status: " + issuesDetails.getSctProgressStatusNircj());
        }

    }

    @Override
    public int getItemCount() {
        return issuesDetailsArrayList.size();
    }
}

package com.techled.teacheranalytics.adapter;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.MarkAttendance;
import com.techled.teacheranalytics.pojo.MarkSportAttendance;
import com.techled.teacheranalytics.pojo.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;


public class SportAttendanceRecyAdapter extends RecyclerView.Adapter<SportAttendanceRecyAdapter.AttMagtHolder> {
    private Context context;
    private ArrayList<MarkSportAttendance> managementDetailsArrayList;
    final MarkSportAttendance markAttendance = new MarkSportAttendance();
    private ArrayList<MarkAttendance> list = null;
    private Dialog dialog = null;
    private String date;
    private String comment;
    private boolean flagA= true,flagP=true;

    private static RecyclerViewClickListener itemListener;

    public SportAttendanceRecyAdapter(Context context, ArrayList<MarkSportAttendance> managementDetailsArrayList, String date) {
        this.context = context;
        this.managementDetailsArrayList = managementDetailsArrayList;
        this.date = date;


    }

    public class AttMagtHolder extends RecyclerView.ViewHolder {
        private TextView txtStuName;
        private Button btnPresent, btnAbsent, btnDailyAttendance;




        public AttMagtHolder(View itemView) {
            super(itemView);
            txtStuName = (TextView) itemView.findViewById(R.id.txtStuName);
            btnPresent = (Button) itemView.findViewById(R.id.btnPresent);
            btnAbsent = (Button) itemView.findViewById(R.id.btnAbsent);
            btnDailyAttendance = (Button) itemView.findViewById(R.id.btnDailyAttendance);

        }
    }

    public AttMagtHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_sport_attendance_mark, parent, false);
        AttMagtHolder holder = new AttMagtHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final AttMagtHolder holder, final int position) {

        final MarkSportAttendance managementDetails = managementDetailsArrayList.get(position);
       // holder.txtStuName.setText(managementDetails.getStuName());

        holder.txtStuName.setText("("+managementDetails.getRollNo()+") "+managementDetails.getStuName());
        holder.btnDailyAttendance.setEnabled(false);
        holder.btnDailyAttendance.setText(managementDetails.getStuStatusDaily());



        if (managementDetails.getFlagPAL().equals("S") || managementDetails.getFlagPAL().equals("A") || managementDetails.getFlagPAL().equals("X"))
        {
            holder.btnAbsent.setBackgroundResource(R.color.red);
        }
        else
        {
            holder.btnPresent.setBackgroundResource(R.color.green);
        }
        //set absent
        holder.btnAbsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.btnAbsent.setBackgroundResource(R.color.red);
                holder.btnAbsent.setTextColor(Color.WHITE);
                holder.btnPresent.setBackgroundResource(R.color.lightgrey);
                managementDetailsArrayList.get(position).setFlagPAL("A");
                managementDetailsArrayList.get(position).setComment("");



            }
        });

        //set present
        holder.btnPresent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               holder.btnPresent.setBackgroundResource(R.color.green);
                holder.btnPresent.setTextColor(Color.WHITE);

                holder.btnAbsent.setBackgroundResource(R.color.lightgrey);
                managementDetailsArrayList.get(position).setFlagPAL("P");
                managementDetailsArrayList.get(position).setComment("");


            }
        });



    }

    @Override
    public int getItemCount() {
        return managementDetailsArrayList.size();
    }


    // method to access in activity after updating selection
    public List<MarkSportAttendance> getList() {
        return managementDetailsArrayList;
    }
}


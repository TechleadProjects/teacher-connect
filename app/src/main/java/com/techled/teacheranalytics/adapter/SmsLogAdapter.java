package com.techled.teacheranalytics.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.SmsLogs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by sonal on 2/12/15.
 */
public class SmsLogAdapter extends RecyclerView
        .Adapter<SmsLogAdapter
        .DataObjectHolder>
{
    private ArrayList<SmsLogs> smsLogsArrayList;

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView assMessage, assProcessId, assContactNo, assTimestamp, assTimestampProcessed;

        public DataObjectHolder(View itemView) {
            super(itemView);
            assMessage = (TextView) itemView.findViewById(R.id.assMessage);
            assContactNo = (TextView) itemView.findViewById(R.id.assContactNo);
            assTimestamp = (TextView) itemView.findViewById(R.id.assTimestamp);

        }


    }


    public SmsLogAdapter(ArrayList<SmsLogs> myDataset) {
        smsLogsArrayList = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sms_log, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        try {
            final SmsLogs data=smsLogsArrayList.get(position);
            holder.assMessage.setText( Html.fromHtml(data.getAssMessage()));
          //  holder.assProcessId.setText("ProcessId :" + data.getAssProcessId());
            holder.assContactNo.setText(  data.getAssContactNo());
            String input = data.getAssTimestampProcessed();
            final DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final DateFormat outputFormat = new SimpleDateFormat("  yyyy-MM-dd KK:mm a");
            System.out.println(outputFormat.format(inputFormat.parse(input)));
            holder.assTimestamp.setText(outputFormat.format(inputFormat.parse(input)));



        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return smsLogsArrayList.size();
    }


}

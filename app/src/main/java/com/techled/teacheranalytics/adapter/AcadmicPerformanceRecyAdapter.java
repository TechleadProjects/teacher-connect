package com.techled.teacheranalytics.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techled.teacheranalytics.ActivityList.PerformanceModule.MainPagerScreenActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.AcadmicData;
import com.techled.teacheranalytics.pojo.SubjectInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonal on 7/2/18.
 */

public class AcadmicPerformanceRecyAdapter extends RecyclerView.Adapter<AcadmicPerformanceRecyAdapter.AcdPerHolder> {
    private Context context;
    private int ayrYear;
    private ArrayList<AcadmicData> acadmicDataArrayList;

    public AcadmicPerformanceRecyAdapter(Context context, int ayrYear, ArrayList<AcadmicData> acadmicDataArrayList) {
        this.context = context;
        this.ayrYear = ayrYear;
        this.acadmicDataArrayList = acadmicDataArrayList;
    }

    public class AcdPerHolder extends RecyclerView.ViewHolder {
        private TextView txtStdDiv, txtSubMark1, txtSubMark2;
        private LinearLayout layout1, layout2, layout3, layout4, layout5, layout6;
        private LinearLayout lay1, lay2, lay3, lay4, lay5, lay6, lay7, lay8, lay9, lay10, lay11, lay12;
        private TextView txtSubject1, txtSubject2, txtSubject3, txtSubject4, txtSubject5, txtSubject6,
                txtSubject7, txtSubject8, txtSubject9, txtSubject10, txtSubject11, txtSubject12;
        private TextView txtMark1, txtMark2, txtMark3, txtMark4, txtMark5, txtMark6, txtMark7,
                txtMark8, txtMark9, txtMark10, txtMark11, txtMark12;

        //  private GridLayout gridLay;
        public AcdPerHolder(View itemView) {
            super(itemView);
            txtStdDiv = (TextView) itemView.findViewById(R.id.txtStdDiv);
            layout1 = (LinearLayout) itemView.findViewById(R.id.layout1);
            layout2 = (LinearLayout) itemView.findViewById(R.id.layout2);
            layout3 = (LinearLayout) itemView.findViewById(R.id.layout3);
            layout4 = (LinearLayout) itemView.findViewById(R.id.layout4);
            layout5 = (LinearLayout) itemView.findViewById(R.id.layout5);
            layout6 = (LinearLayout) itemView.findViewById(R.id.layout6);

            lay1 = (LinearLayout) itemView.findViewById(R.id.lay1);
            lay2 = (LinearLayout) itemView.findViewById(R.id.lay2);
            lay3 = (LinearLayout) itemView.findViewById(R.id.lay3);
            lay4 = (LinearLayout) itemView.findViewById(R.id.lay4);
            lay5 = (LinearLayout) itemView.findViewById(R.id.lay5);
            lay6 = (LinearLayout) itemView.findViewById(R.id.lay6);

            lay7 = (LinearLayout) itemView.findViewById(R.id.lay7);
            lay8 = (LinearLayout) itemView.findViewById(R.id.lay8);
            lay9 = (LinearLayout) itemView.findViewById(R.id.lay9);
            lay10 = (LinearLayout) itemView.findViewById(R.id.lay10);
            lay11 = (LinearLayout) itemView.findViewById(R.id.lay11);
            lay12 = (LinearLayout) itemView.findViewById(R.id.lay12);


            txtSubject1 = (TextView) itemView.findViewById(R.id.txtSubject1);
            txtSubject2 = (TextView) itemView.findViewById(R.id.txtSubject2);
            txtSubject3 = (TextView) itemView.findViewById(R.id.txtSubject3);
            txtSubject4 = (TextView) itemView.findViewById(R.id.txtSubject4);
            txtSubject5 = (TextView) itemView.findViewById(R.id.txtSubject5);
            txtSubject6 = (TextView) itemView.findViewById(R.id.txtSubject6);

            txtSubject7 = (TextView) itemView.findViewById(R.id.txtSubject7);
            txtSubject8 = (TextView) itemView.findViewById(R.id.txtSubject8);
            txtSubject9 = (TextView) itemView.findViewById(R.id.txtSubject9);
            txtSubject10 = (TextView) itemView.findViewById(R.id.txtSubject10);
            txtSubject11 = (TextView) itemView.findViewById(R.id.txtSubject11);
            txtSubject12 = (TextView) itemView.findViewById(R.id.txtSubject12);

            txtMark1 = (TextView) itemView.findViewById(R.id.txtMark1);
            txtMark2 = (TextView) itemView.findViewById(R.id.txtMark2);
            txtMark3 = (TextView) itemView.findViewById(R.id.txtMark3);
            txtMark4 = (TextView) itemView.findViewById(R.id.txtMark4);
            txtMark5 = (TextView) itemView.findViewById(R.id.txtMark5);
            txtMark6 = (TextView) itemView.findViewById(R.id.txtMark6);

            txtMark7 = (TextView) itemView.findViewById(R.id.txtMark7);
            txtMark8 = (TextView) itemView.findViewById(R.id.txtMark8);
            txtMark9 = (TextView) itemView.findViewById(R.id.txtMark9);
            txtMark10 = (TextView) itemView.findViewById(R.id.txtMark10);
            txtMark11 = (TextView) itemView.findViewById(R.id.txtMark11);
            txtMark12 = (TextView) itemView.findViewById(R.id.txtMark12);


        }
    }

    @Override
    public AcdPerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_acadmic_performance, parent, false);
        AcdPerHolder holder = new AcdPerHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(AcdPerHolder holder, final int position) {

        try {
            AcadmicData acadmicData = acadmicDataArrayList.get(position);

            holder.txtStdDiv.setText("" + acadmicData.getStdName() + "  " + acadmicData.getDivName());
            final List<SubjectInfo> subjectInfoArrayList = acadmicData.getSubjectInfoArrayList();
            if (subjectInfoArrayList.size() == 1) {
                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);
                    } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))
                    {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }

                holder.lay2.setVisibility(View.GONE);
                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.GONE);
                holder.layout3.setVisibility(View.GONE);
                holder.layout4.setVisibility(View.GONE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);


                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");


            }
            if (subjectInfoArrayList.size() == 2) {
                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.GONE);
                holder.layout3.setVisibility(View.GONE);
                holder.layout4.setVisibility(View.GONE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);


                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


            }

            if (subjectInfoArrayList.size() == 3) {
                //
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }

                holder.layout1.setVisibility(View.VISIBLE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.GONE);
                holder.layout3.setVisibility(View.GONE);
                holder.layout4.setVisibility(View.GONE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);

                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");

                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

            }

            if (subjectInfoArrayList.size() == 4) {

                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.GONE);
                holder.layout4.setVisibility(View.GONE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);


                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");


            }

            if (subjectInfoArrayList.size() == 5) {

                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }

                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout4.setVisibility(View.GONE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.GONE);

                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");
            }
            if (subjectInfoArrayList.size() == 6) {

                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay6
                if (subjectInfoArrayList.get(5).getColor().toString().equals("orange")) {
                    holder.lay6.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("red")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("green")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_green);

                }
                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.VISIBLE);
                holder.layout4.setVisibility(View.GONE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.VISIBLE);

                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");

                holder.txtSubject6.setText(subjectInfoArrayList.get(5).getSubName());
                holder.txtMark6.setText(subjectInfoArrayList.get(5).getMarks() + "%");


            }
            if (subjectInfoArrayList.size() == 7) {
                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay6
                if (subjectInfoArrayList.get(5).getColor().toString().equals("orange")) {
                    holder.lay6.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("red")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("green")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_green);

                }


                //lay7
                if (subjectInfoArrayList.get(6).getColor().toString().equals("orange")) {
                    holder.lay7.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("red")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("green")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_green);

                }
                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.VISIBLE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.VISIBLE);
                holder.lay7.setVisibility(View.VISIBLE);
                holder.lay8.setVisibility(View.GONE);

                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");

                holder.txtSubject6.setText(subjectInfoArrayList.get(5).getSubName());
                holder.txtMark6.setText(subjectInfoArrayList.get(5).getMarks() + "%");

                holder.txtSubject7.setText(subjectInfoArrayList.get(6).getSubName());
                holder.txtMark7.setText(subjectInfoArrayList.get(6).getMarks() + "%");


            }
            if (subjectInfoArrayList.size() == 8) {
                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay6
                if (subjectInfoArrayList.get(5).getColor().toString().equals("orange")) {
                    holder.lay6.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("red")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("green")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_green);

                }


                //lay7
                if (subjectInfoArrayList.get(6).getColor().toString().equals("orange")) {
                    holder.lay7.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("red")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("green")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay8
                if (subjectInfoArrayList.get(7).getColor().toString().equals("orange")) {
                    holder.lay8.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("red")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("green")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_green);

                }

                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.VISIBLE);
                holder.layout4.setVisibility(View.VISIBLE);
                holder.layout5.setVisibility(View.GONE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.VISIBLE);
                holder.lay7.setVisibility(View.VISIBLE);
                holder.lay8.setVisibility(View.VISIBLE);


                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");

                holder.txtSubject6.setText(subjectInfoArrayList.get(5).getSubName());
                holder.txtMark6.setText(subjectInfoArrayList.get(5).getMarks() + "%");

                holder.txtSubject7.setText(subjectInfoArrayList.get(6).getSubName());
                holder.txtMark7.setText(subjectInfoArrayList.get(6).getMarks() + "%");

                holder.txtSubject8.setText(subjectInfoArrayList.get(7).getSubName());
                holder.txtMark8.setText(subjectInfoArrayList.get(7).getMarks() + "%");


            }
            if (subjectInfoArrayList.size() == 9) {
                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay6
                if (subjectInfoArrayList.get(5).getColor().toString().equals("orange")) {
                    holder.lay6.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("red")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("green")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_green);

                }


                //lay7
                if (subjectInfoArrayList.get(6).getColor().toString().equals("orange")) {
                    holder.lay7.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("red")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("green")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay8
                if (subjectInfoArrayList.get(7).getColor().toString().equals("orange")) {
                    holder.lay8.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("red")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("green")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay9
                if (subjectInfoArrayList.get(8).getColor().toString().equals("orange")) {
                    holder.lay9.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("red")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("green")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_green);

                }
                holder.layout1.setVisibility(View.VISIBLE);
                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.VISIBLE);
                holder.layout4.setVisibility(View.VISIBLE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.VISIBLE);
                holder.lay7.setVisibility(View.VISIBLE);
                holder.lay8.setVisibility(View.VISIBLE);
                holder.lay9.setVisibility(View.VISIBLE);
                holder.lay10.setVisibility(View.GONE);

                holder.layout6.setVisibility(View.GONE);

                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");

                holder.txtSubject6.setText(subjectInfoArrayList.get(5).getSubName());
                holder.txtMark6.setText(subjectInfoArrayList.get(5).getMarks() + "%");

                holder.txtSubject7.setText(subjectInfoArrayList.get(6).getSubName());
                holder.txtMark7.setText(subjectInfoArrayList.get(6).getMarks() + "%");

                holder.txtSubject8.setText(subjectInfoArrayList.get(7).getSubName());
                holder.txtMark8.setText(subjectInfoArrayList.get(7).getMarks() + "%");

                holder.txtSubject9.setText(subjectInfoArrayList.get(8).getSubName());
                holder.txtMark9.setText(subjectInfoArrayList.get(8).getMarks() + "%");


            }
            if (subjectInfoArrayList.size() == 10) {


                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay6
                if (subjectInfoArrayList.get(5).getColor().toString().equals("orange")) {
                    holder.lay6.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("red")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("green")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_green);

                }


                //lay7
                if (subjectInfoArrayList.get(6).getColor().toString().equals("orange")) {
                    holder.lay7.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("red")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("green")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay8
                if (subjectInfoArrayList.get(7).getColor().toString().equals("orange")) {
                    holder.lay8.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("red")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("green")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay9
                if (subjectInfoArrayList.get(8).getColor().toString().equals("orange")) {
                    holder.lay9.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("red")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("green")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay10
                if (subjectInfoArrayList.get(9).getColor().toString().equals("orange")) {
                    holder.lay10.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(9).getColor().toString().equals("red")) {
                    holder.lay10.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(9).getColor().toString().equals("green")) {
                    holder.lay10.setBackgroundResource(R.drawable.rect_shape_green);

                }
                holder.layout1.setVisibility(View.VISIBLE);

                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.VISIBLE);
                holder.layout4.setVisibility(View.VISIBLE);
                holder.layout5.setVisibility(View.VISIBLE);
                holder.layout6.setVisibility(View.GONE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.VISIBLE);
                holder.lay7.setVisibility(View.VISIBLE);
                holder.lay8.setVisibility(View.VISIBLE);
                holder.lay9.setVisibility(View.VISIBLE);
                holder.lay10.setVisibility(View.VISIBLE);

                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");

                holder.txtSubject6.setText(subjectInfoArrayList.get(5).getSubName());
                holder.txtMark6.setText(subjectInfoArrayList.get(5).getMarks() + "%");

                holder.txtSubject7.setText(subjectInfoArrayList.get(6).getSubName());
                holder.txtMark7.setText(subjectInfoArrayList.get(6).getMarks() + "%");

                holder.txtSubject8.setText(subjectInfoArrayList.get(7).getSubName());
                holder.txtMark8.setText(subjectInfoArrayList.get(7).getMarks() + "%");


                holder.txtSubject9.setText(subjectInfoArrayList.get(8).getSubName());
                holder.txtMark9.setText(subjectInfoArrayList.get(8).getMarks() + "%");

                holder.txtSubject10.setText(subjectInfoArrayList.get(9).getSubName());
                holder.txtMark10.setText(subjectInfoArrayList.get(9).getMarks() + "%");


            }
            if (subjectInfoArrayList.size() == 11) {


                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay6
                if (subjectInfoArrayList.get(5).getColor().toString().equals("orange")) {
                    holder.lay6.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("red")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("green")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_green);

                }


                //lay7
                if (subjectInfoArrayList.get(6).getColor().toString().equals("orange")) {
                    holder.lay7.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("red")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("green")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay8
                if (subjectInfoArrayList.get(7).getColor().toString().equals("orange")) {
                    holder.lay8.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("red")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("green")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay9
                if (subjectInfoArrayList.get(8).getColor().toString().equals("orange")) {
                    holder.lay9.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("red")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("green")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay10
                if (subjectInfoArrayList.get(9).getColor().toString().equals("orange")) {
                    holder.lay10.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(9).getColor().toString().equals("red")) {
                    holder.lay10.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(9).getColor().toString().equals("green")) {
                    holder.lay10.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay11
                if (subjectInfoArrayList.get(10).getColor().toString().equals("orange")) {
                    holder.lay11.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(10).getColor().toString().equals("red")) {
                    holder.lay11.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(10).getColor().toString().equals("green")) {
                    holder.lay11.setBackgroundResource(R.drawable.rect_shape_green);

                }
                holder.layout1.setVisibility(View.VISIBLE);

                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.VISIBLE);
                holder.layout4.setVisibility(View.VISIBLE);
                holder.layout5.setVisibility(View.VISIBLE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.VISIBLE);
                holder.lay7.setVisibility(View.VISIBLE);
                holder.lay8.setVisibility(View.VISIBLE);
                holder.lay9.setVisibility(View.VISIBLE);
                holder.lay10.setVisibility(View.VISIBLE);

                holder.lay11.setVisibility(View.VISIBLE);
                holder.lay12.setVisibility(View.GONE);

                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");

                holder.txtSubject6.setText(subjectInfoArrayList.get(5).getSubName());
                holder.txtMark6.setText(subjectInfoArrayList.get(5).getMarks() + "%");

                holder.txtSubject7.setText(subjectInfoArrayList.get(6).getSubName());
                holder.txtMark7.setText(subjectInfoArrayList.get(6).getMarks() + "%");

                holder.txtSubject8.setText(subjectInfoArrayList.get(7).getSubName());
                holder.txtMark8.setText(subjectInfoArrayList.get(7).getMarks() + "%");


                holder.txtSubject9.setText(subjectInfoArrayList.get(8).getSubName());
                holder.txtMark9.setText(subjectInfoArrayList.get(8).getMarks() + "%");

                holder.txtSubject10.setText(subjectInfoArrayList.get(9).getSubName());
                holder.txtMark10.setText(subjectInfoArrayList.get(9).getMarks() + "%");

                holder.txtSubject11.setText(subjectInfoArrayList.get(10).getSubName());
                holder.txtMark11.setText(subjectInfoArrayList.get(10).getMarks() + "%");

            }


            if (subjectInfoArrayList.size() == 12) {
                //lay1
                if (subjectInfoArrayList.get(0).getColor().toString().equals("orange")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_orange);
                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("red")) {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(0).getColor().toString().equals("green"))

                {
                    holder.lay1.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay2
                if (subjectInfoArrayList.get(1).getColor().toString().equals("orange")) {
                    holder.lay2.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("red")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(1).getColor().toString().equals("green")) {
                    holder.lay2.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay3
                if (subjectInfoArrayList.get(2).getColor().toString().equals("orange")) {
                    holder.lay3.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("red")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(2).getColor().toString().equals("green")) {
                    holder.lay3.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay4
                if (subjectInfoArrayList.get(3).getColor().toString().equals("orange")) {
                    holder.lay4.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("red")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(3).getColor().toString().equals("green")) {
                    holder.lay4.setBackgroundResource(R.drawable.rect_shape_green);

                }

                //lay5
                if (subjectInfoArrayList.get(4).getColor().toString().equals("orange")) {
                    holder.lay5.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("red")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(4).getColor().toString().equals("green")) {
                    holder.lay5.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay6
                if (subjectInfoArrayList.get(5).getColor().toString().equals("orange")) {
                    holder.lay6.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("red")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(5).getColor().toString().equals("green")) {
                    holder.lay6.setBackgroundResource(R.drawable.rect_shape_green);

                }


                //lay7
                if (subjectInfoArrayList.get(6).getColor().toString().equals("orange")) {
                    holder.lay7.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("red")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(6).getColor().toString().equals("green")) {
                    holder.lay7.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay8
                if (subjectInfoArrayList.get(7).getColor().toString().equals("orange")) {
                    holder.lay8.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("red")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(7).getColor().toString().equals("green")) {
                    holder.lay8.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay9
                if (subjectInfoArrayList.get(8).getColor().toString().equals("orange")) {
                    holder.lay9.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("red")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(8).getColor().toString().equals("green")) {
                    holder.lay9.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay10
                if (subjectInfoArrayList.get(9).getColor().toString().equals("orange")) {
                    holder.lay10.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(9).getColor().toString().equals("red")) {
                    holder.lay10.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(9).getColor().toString().equals("green")) {
                    holder.lay10.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay11
                if (subjectInfoArrayList.get(10).getColor().toString().equals("orange")) {
                    holder.lay11.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(10).getColor().toString().equals("red")) {
                    holder.lay11.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(10).getColor().toString().equals("green")) {
                    holder.lay11.setBackgroundResource(R.drawable.rect_shape_green);

                }
                //lay12
                if (subjectInfoArrayList.get(11).getColor().toString().equals("orange")) {
                    holder.lay12.setBackgroundResource((R.drawable.rect_shape_orange));
                } else if (subjectInfoArrayList.get(11).getColor().toString().equals("red")) {
                    holder.lay12.setBackgroundResource(R.drawable.rect_shape_red2);

                } else if (subjectInfoArrayList.get(11).getColor().toString().equals("green")) {
                    holder.lay12.setBackgroundResource(R.drawable.rect_shape_green);

                }

                holder.layout1.setVisibility(View.VISIBLE);

                holder.layout2.setVisibility(View.VISIBLE);
                holder.layout3.setVisibility(View.VISIBLE);
                holder.layout4.setVisibility(View.VISIBLE);
                holder.layout5.setVisibility(View.VISIBLE);
                holder.layout6.setVisibility(View.VISIBLE);
                holder.lay1.setVisibility(View.VISIBLE);
                holder.lay2.setVisibility(View.VISIBLE);
                holder.lay3.setVisibility(View.VISIBLE);
                holder.lay4.setVisibility(View.VISIBLE);
                holder.lay5.setVisibility(View.VISIBLE);
                holder.lay6.setVisibility(View.VISIBLE);
                holder.lay7.setVisibility(View.VISIBLE);
                holder.lay8.setVisibility(View.VISIBLE);
                holder.lay9.setVisibility(View.VISIBLE);
                holder.lay10.setVisibility(View.VISIBLE);

                holder.lay11.setVisibility(View.VISIBLE);
                holder.lay12.setVisibility(View.VISIBLE);
                holder.txtSubject1.setText(subjectInfoArrayList.get(0).getSubName());
                holder.txtMark1.setText(subjectInfoArrayList.get(0).getMarks() + "%");

                holder.txtSubject2.setText(subjectInfoArrayList.get(1).getSubName());
                holder.txtMark2.setText(subjectInfoArrayList.get(1).getMarks() + "%");


                holder.txtSubject3.setText(subjectInfoArrayList.get(2).getSubName());
                holder.txtMark3.setText(subjectInfoArrayList.get(2).getMarks() + "%");

                holder.txtSubject4.setText(subjectInfoArrayList.get(3).getSubName());
                holder.txtMark4.setText(subjectInfoArrayList.get(3).getMarks() + "%");

                holder.txtSubject5.setText(subjectInfoArrayList.get(4).getSubName());
                holder.txtMark5.setText(subjectInfoArrayList.get(4).getMarks() + "%");

                holder.txtSubject6.setText(subjectInfoArrayList.get(5).getSubName());
                holder.txtMark6.setText(subjectInfoArrayList.get(5).getMarks() + "%");

                holder.txtSubject7.setText(subjectInfoArrayList.get(6).getSubName());
                holder.txtMark7.setText(subjectInfoArrayList.get(6).getMarks() + "%");

                holder.txtSubject8.setText(subjectInfoArrayList.get(7).getSubName());
                holder.txtMark8.setText(subjectInfoArrayList.get(7).getMarks() + "%");


                holder.txtSubject9.setText(subjectInfoArrayList.get(8).getSubName());
                holder.txtMark9.setText(subjectInfoArrayList.get(8).getMarks() + "%");

                holder.txtSubject10.setText(subjectInfoArrayList.get(9).getSubName());
                holder.txtMark10.setText(subjectInfoArrayList.get(9).getMarks() + "%");

                holder.txtSubject11.setText(subjectInfoArrayList.get(10).getSubName());
                holder.txtMark11.setText(subjectInfoArrayList.get(10).getMarks() + "%");

                holder.txtSubject12.setText(subjectInfoArrayList.get(11).getSubName());
                holder.txtMark12.setText(subjectInfoArrayList.get(11).getMarks() + "%");


            }


            //Lay1
            holder.lay1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        int stdId = subjectInfoArrayList.get(0).getStdId();
                        int divId = subjectInfoArrayList.get(0).getDivId();
                        String stdName = subjectInfoArrayList.get(0).getStdName().toString();
                        String divName = subjectInfoArrayList.get(0).getDivName();
                        int subId = subjectInfoArrayList.get(0).getSubId();
                        String subName = subjectInfoArrayList.get(0).getSubName();

                        Intent intent = new Intent(context, MainPagerScreenActivity.class);
                        intent.putExtra("ayrYear", ayrYear);

                        intent.putExtra("stdId", stdId);
                        intent.putExtra("divId", divId);
                        intent.putExtra("stdName", stdName);
                        intent.putExtra("divName", divName);
                        intent.putExtra("subId", subId);
                        intent.putExtra("subName", subName);

                        context.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });
            //Lay2
            holder.lay2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(1).getStdId();
                    int divId = subjectInfoArrayList.get(1).getDivId();
                    String stdName = subjectInfoArrayList.get(1).getStdName().toString();
                    String divName = subjectInfoArrayList.get(1).getDivName();
                    int subId = subjectInfoArrayList.get(1).getSubId();
                    String subName = subjectInfoArrayList.get(1).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("divId", divId);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);

                    context.startActivity(intent);

                }
            });
            //Lay3
            holder.lay3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(2).getStdId();
                    int divId = subjectInfoArrayList.get(2).getDivId();
                    String stdName = subjectInfoArrayList.get(2).getStdName().toString();
                    String divName = subjectInfoArrayList.get(2).getDivName();
                    int subId = subjectInfoArrayList.get(2).getSubId();
                    String subName = subjectInfoArrayList.get(2).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("divId", divId);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);

                    context.startActivity(intent);

                }
            });
            //Lay4
            holder.lay4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(3).getStdId();
                    int divId = subjectInfoArrayList.get(3).getDivId();
                    String stdName = subjectInfoArrayList.get(3).getStdName().toString();
                    String divName = subjectInfoArrayList.get(3).getDivName();
                    int subId = subjectInfoArrayList.get(3).getSubId();
                    String subName = subjectInfoArrayList.get(3).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("divId", divId);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);

                    context.startActivity(intent);

                }
            });

            //Lay5
            holder.lay5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(4).getStdId();
                    int divId = subjectInfoArrayList.get(4).getDivId();
                    String stdName = subjectInfoArrayList.get(4).getStdName().toString();
                    String divName = subjectInfoArrayList.get(4).getDivName();
                    int subId = subjectInfoArrayList.get(4).getSubId();
                    String subName = subjectInfoArrayList.get(4).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("divId", divId);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);

                    context.startActivity(intent);

                }
            });
            //Lay6
            holder.lay6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(5).getStdId();
                    int divId = subjectInfoArrayList.get(5).getDivId();
                    String stdName = subjectInfoArrayList.get(5).getStdName().toString();
                    String divName = subjectInfoArrayList.get(5).getDivName();
                    int subId = subjectInfoArrayList.get(5).getSubId();
                    String subName = subjectInfoArrayList.get(5).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("divId", divId);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);

                    context.startActivity(intent);

                }
            });

            //Lay7
            holder.lay7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(6).getStdId();
                    int divId = subjectInfoArrayList.get(6).getDivId();
                    String stdName = subjectInfoArrayList.get(6).getStdName().toString();
                    String divName = subjectInfoArrayList.get(6).getDivName();
                    int subId = subjectInfoArrayList.get(6).getSubId();
                    String subName = subjectInfoArrayList.get(6).getSubName();


                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("divId", divId);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);

                    context.startActivity(intent);
                }
            });

            //Lay8
            holder.lay8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(7).getStdId();
                    int divId = subjectInfoArrayList.get(7).getDivId();
                    String stdName = subjectInfoArrayList.get(7).getStdName().toString();
                    String divName = subjectInfoArrayList.get(7).getDivName();
                    int subId = subjectInfoArrayList.get(7).getSubId();
                    String subName = subjectInfoArrayList.get(7).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("divId", divId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);
                    context.startActivity(intent);
                }
            });
            //Lay9
            holder.lay9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(8).getStdId();
                    int divId = subjectInfoArrayList.get(8).getDivId();
                    String stdName = subjectInfoArrayList.get(8).getStdName().toString();
                    String divName = subjectInfoArrayList.get(8).getDivName();
                    int subId = subjectInfoArrayList.get(8).getSubId();
                    String subName = subjectInfoArrayList.get(8).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("divId", divId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);
                    context.startActivity(intent);
                }
            });
            //Lay10
            holder.lay10.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(9).getStdId();
                    int divId = subjectInfoArrayList.get(9).getDivId();
                    String stdName = subjectInfoArrayList.get(9).getStdName().toString();
                    String divName = subjectInfoArrayList.get(9).getDivName();
                    int subId = subjectInfoArrayList.get(9).getSubId();
                    String subName = subjectInfoArrayList.get(9).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("divId", divId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);
                    context.startActivity(intent);
                }
            });
            //Lay11
            holder.lay11.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(10).getStdId();
                    int divId = subjectInfoArrayList.get(10).getDivId();
                    String stdName = subjectInfoArrayList.get(10).getStdName().toString();
                    String divName = subjectInfoArrayList.get(10).getDivName();
                    int subId = subjectInfoArrayList.get(10).getSubId();
                    String subName = subjectInfoArrayList.get(10).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("divId", divId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);
                    context.startActivity(intent);
                }
            });
            //Lay12
            holder.lay12.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId = subjectInfoArrayList.get(11).getStdId();
                    int divId = subjectInfoArrayList.get(11).getDivId();
                    String stdName = subjectInfoArrayList.get(11).getStdName().toString();
                    String divName = subjectInfoArrayList.get(11).getDivName();
                    int subId = subjectInfoArrayList.get(11).getSubId();
                    String subName = subjectInfoArrayList.get(11).getSubName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("divId", divId);
                    intent.putExtra("ayrYear", ayrYear);
                    intent.putExtra("stdName", stdName);
                    intent.putExtra("divName", divName);
                    intent.putExtra("subId", subId);
                    intent.putExtra("subName", subName);
                    context.startActivity(intent);
                }
            });


         /*   holder.txtStdDiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int stdId =   subjectInfoArrayList.get(position).getStdId();
                    int divId =   subjectInfoArrayList.get(position).getDivId();
                    String stdName =   subjectInfoArrayList.get(position).getStdName().toString();
                    String divName =  subjectInfoArrayList.get(position).getDivName();

                    Intent intent = new Intent(context, MainPagerScreenActivity.class);
                    intent.putExtra("stdId",stdId);
                    intent.putExtra("divId",divId);
                    intent.putExtra("stdName",stdName);
                    intent.putExtra("divName",divName);
                    intent.putExtra("subId",0);
                    intent.putExtra("subName","");
                    context.startActivity(intent);
                }
            });
*/




















         /*   for (int i = 0; i < subjectInfoArrayList.size(); i++) {



                LinearLayout parentLayout = new LinearLayout(context);
                parentLayout.setOrientation(LinearLayout.HORIZONTAL);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                parentLayout.setWeightSum(2);
                parentLayout.setLayoutParams(params);



                LinearLayout linearLayout1 = new LinearLayout(context);
                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                     ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params2.weight=1;
                linearLayout1.setLayoutParams(params2);
                linearLayout1.setBackgroundResource( (R.drawable.rect_shape_orange));
                linearLayout1.setOrientation(LinearLayout.VERTICAL);
                linearLayout1.setPadding(5,5,5,5);

                TextView subject = new TextView(context);
                subject.setTextSize(14);
                subject.setPadding(35,35,35,35);
                subject.setText(subjectInfoArrayList.get(i).getSubName());
                subject.setBackgroundResource( (R.drawable.rect_shape_orange));

                TextView mark = new TextView(context);
                mark.setTextSize(20);
                mark.setPadding(35,35,35,35);
                mark.setText(subjectInfoArrayList.get(i).getMarks()+"%");
                mark.setBackgroundResource( (R.drawable.rect_shape_orange));


                linearLayout1.addView(subject);
                linearLayout1.addView(mark);
                parentLayout.addView(linearLayout1);



                LinearLayout linearLayout2 = new LinearLayout(context);
                linearLayout2.setOrientation(LinearLayout.VERTICAL);
                linearLayout2.setPadding(5,5,5,5);
                LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(
                       ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params3.weight=1;
                linearLayout2.setLayoutParams(params3);
                linearLayout2.setBackgroundResource( (R.drawable.rect_shape_red));

                TextView subject2 = new TextView(context);
                subject2.setTextSize(14);
                subject2.setPadding(35,35,35,35);
                subject2.setText(subjectInfoArrayList.get(i+1).getSubName());
                subject2.setBackgroundResource( (R.drawable.rect_shape_red));

                TextView mark2 = new TextView(context);
                mark2.setTextSize(20);
                mark2.setPadding(35,35,35,35);
                mark2.setText(subjectInfoArrayList.get(i+1).getMarks()+"%");
                mark2.setBackgroundResource( (R.drawable.rect_shape_red));


                linearLayout2.addView(subject2);
                linearLayout2.addView(mark2);
                parentLayout.addView(linearLayout2);


               *//* holder.gridLay.addView(linearLayout1);
                holder.gridLay.addView(linearLayout2);
*//*
                holder.gridLay.addView(parentLayout);
                i++;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return acadmicDataArrayList.size();
    }
}

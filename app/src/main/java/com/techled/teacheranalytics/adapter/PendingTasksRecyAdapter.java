package com.techled.teacheranalytics.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.techled.teacheranalytics.ActivityList.TaskManagementModule.EditTaskActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.PendingTaskDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonal on 1/3/18.
 */

public class PendingTasksRecyAdapter extends RecyclerView.Adapter<PendingTasksRecyAdapter.PendingTasksHolder> {
    private Context context;
    private ArrayList<PendingTaskDetails> pendingTaskDetailsArrayList, scheduledTasksList;
    private String taskAssignedToMe;
    private int tskId = 0, tssId = 0, assignerId = 0;
    private int orgId, insId, dscId, stfId, evtId;
    private Util util;
    private ProgressDialog progDailog;
    private boolean isTsk = false;
    private String subject = "", description = "", creationDate = "", priority = "", tStatus = "", startDate = "", endDate = "", percentComplete = "", progressNote = "", asignee = "", dueDate = "";
    private Dialog dialog;
    private String lastActionDate, tskImg, tskImgName, tskImgContentType;

    public PendingTasksRecyAdapter(Context context, ArrayList<PendingTaskDetails> pendingTaskDetailsArrayList, String taskAssignedToMe, int orgId, int insId, int dscId, int stfId) {
        this.context = context;
        this.pendingTaskDetailsArrayList = pendingTaskDetailsArrayList;
        this.taskAssignedToMe = taskAssignedToMe;
        this.orgId = orgId;
        this.insId = insId;
        this.dscId = dscId;
        this.stfId = stfId;

        util = new Util();
    }

    public class PendingTasksHolder extends RecyclerView.ViewHolder {
        private LinearLayout taskAssignedByMeLayout, taskAssignedToMeLayout;
        private TextView txtTaskSubject, txtTaskAssigner, txtTaskDueDate, txtTaskStatus, txtTaskPercentComplete, statusLayout;
        private TextView txtTaskSubjectBy, txtTaskPriorityBy, txtTaskDueDateBy, txtTaskStartDateBy, txtTaskStatusBy, txtTaskPercentCompleteBy;
        private Button btnTaskDetails, btnTaskDetailsBy;

        public PendingTasksHolder(View itemView) {
            super(itemView);
            taskAssignedByMeLayout = itemView.findViewById(R.id.taskAssignedByMeLayout);
            taskAssignedToMeLayout = itemView.findViewById(R.id.taskAssignedToMeLayout);

            statusLayout = itemView.findViewById(R.id.statusLayout);

            txtTaskSubject = itemView.findViewById(R.id.txtTaskSubject);
            txtTaskAssigner = itemView.findViewById(R.id.txtTaskAssigner);
            txtTaskDueDate = itemView.findViewById(R.id.txtTaskDueDate);
            txtTaskStatus = itemView.findViewById(R.id.txtTaskStatus);
            txtTaskPercentComplete = itemView.findViewById(R.id.txtTaskPercentComplete);

            txtTaskSubjectBy = itemView.findViewById(R.id.txtTaskSubjectBy);
            txtTaskPriorityBy = itemView.findViewById(R.id.txtTaskPriorityBy);
            txtTaskStartDateBy = itemView.findViewById(R.id.txtTaskStartDateBy);
            txtTaskDueDateBy = itemView.findViewById(R.id.txtTaskDueDateBy);
            txtTaskStatusBy = itemView.findViewById(R.id.txtTaskStatusBy);
            txtTaskPercentCompleteBy = itemView.findViewById(R.id.txtTaskPercentCompleteBy);

            btnTaskDetails = itemView.findViewById(R.id.btnTaskDetails);
            btnTaskDetailsBy = itemView.findViewById(R.id.btnTaskDetailsBy);

            if(taskAssignedToMe.equals("To")){
                taskAssignedToMeLayout.setVisibility(View.VISIBLE);
                taskAssignedByMeLayout.setVisibility(View.GONE);
            }else {
                taskAssignedToMeLayout.setVisibility(View.GONE);
                taskAssignedByMeLayout.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public PendingTasksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_pending_tasks, parent, false);
        PendingTasksHolder holder = new PendingTasksHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final PendingTasksHolder holder, final int position) {

        final PendingTaskDetails pendingTaskDetails = pendingTaskDetailsArrayList.get(position);

        if(taskAssignedToMe.equals("To")){

            String status = "";

            if(pendingTaskDetails.getStatus().equals("A")){
                status = "Assigned";
            }else if(pendingTaskDetails.getStatus().equals("P")){
                status = "In Progress";
            }else if(pendingTaskDetails.getStatus().equals("R")){
                status = "Reassigned";
            }else if(pendingTaskDetails.getStatus().equals("C")){
                status = "Completed";
            }

            isTsk = false;
            holder.txtTaskSubject.setText(pendingTaskDetails.getSubject());
            holder.txtTaskAssigner.setText("Assigner : " + pendingTaskDetails.getAssigneeName());
            holder.txtTaskDueDate.setText("Due Date : " + pendingTaskDetails.getDueDate());
            holder.txtTaskStatus.setText("Status : " + status);
            holder.txtTaskPercentComplete.setText("Percent Complete : " + pendingTaskDetails.getPercentComplete());

            if(pendingTaskDetails.getColorCode().equals("Yellow")) {

                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.statusLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.yellow_shape));
                holder.statusLayout.setText("Assigned");
                holder.statusLayout.setTextColor(ContextCompat.getColor(context, R.color.gray));

            } else if(pendingTaskDetails.getColorCode().equals("Green")) {

                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.statusLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.green_shape));
                holder.statusLayout.setText("Completed");
                holder.statusLayout.setTextColor(ContextCompat.getColor(context, R.color.white));

            } else if(pendingTaskDetails.getColorCode().equals("Purple")) {

                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.statusLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.blue_shape));
                holder.statusLayout.setText("Reassigned");
                holder.statusLayout.setTextColor(ContextCompat.getColor(context, R.color.white));

            } else if(pendingTaskDetails.getColorCode().equals("Red")) {

                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.statusLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.red_shape));
                holder.statusLayout.setText("Suspended");
                holder.statusLayout.setTextColor(ContextCompat.getColor(context, R.color.white));

            } else if(pendingTaskDetails.getColorCode().equals("Orange")) {

                holder.statusLayout.setVisibility(View.VISIBLE);
                holder.statusLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.orange_shape));
                holder.statusLayout.setText("In Progress");
                holder.statusLayout.setTextColor(ContextCompat.getColor(context, R.color.gray));
            }

            holder.btnTaskDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tskId = pendingTaskDetails.getTskId();
                    tssId = pendingTaskDetails.getTssId();
                    assignerId = pendingTaskDetails.getAssignerId();
                    new LoadScheduledTaskDetails().execute();
                }
            });
        }else {

            String status = "", priority = "";

            if(pendingTaskDetails.getStatus().equals("A")){
                status = "Assigned";
            }else if(pendingTaskDetails.getStatus().equals("P")){
                status = "In Progress";
            }else if(pendingTaskDetails.getStatus().equals("R")){
                status = "Reassigned";
            }else if(pendingTaskDetails.getStatus().equals("C")){
                status = "Completed";
            }

            if(pendingTaskDetails.getPriority().equals("H")){
                priority = "High";
            }else if(pendingTaskDetails.getPriority().equals("M")){
                priority = "Medium";
            }else if(pendingTaskDetails.getPriority().equals("L")){
                priority = "Low";
            }

            holder.statusLayout.setVisibility(View.GONE);

            isTsk = true;
            holder.txtTaskSubjectBy.setText(pendingTaskDetails.getSubject());
            holder.txtTaskPriorityBy.setText("Priority : " + priority);
            holder.txtTaskStartDateBy.setText("Start Date : " + pendingTaskDetails.getStartDate());
            holder.txtTaskDueDateBy.setText("Due Date : " + pendingTaskDetails.getDueDate());
            holder.txtTaskStatusBy.setText("Status : " + status);
            holder.txtTaskPercentCompleteBy.setText("Percent Complete : " + pendingTaskDetails.getPercentComplete());

            holder.btnTaskDetailsBy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tskId = pendingTaskDetails.getTskId();
                    new LoadScheduledTaskDetails().execute();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return pendingTaskDetailsArrayList.size();
    }

    // method to access in activity after updating selection
    public List<PendingTaskDetails> getList() {
        return pendingTaskDetailsArrayList;
    }

    public class LoadScheduledTaskDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("success")) {
                        JSONObject dataObject = resArray.getJSONObject(1);
                        JSONObject dataObj = dataObject.getJSONObject("data");

                        tskId = dataObj.getInt("tskId");
                        evtId = dataObj.getInt("evtId");
                        subject = dataObj.getString("tskSubject");
                        description = dataObj.getString("tskDescription");
                        creationDate = dataObj.getString("tskCreationDate");
                        priority = dataObj.getString("tskPriority");
                        tStatus = dataObj.getString("tskStatus");
                        startDate = dataObj.getString("tskStartDate");
                        endDate = dataObj.getString("tskEndDate");
                        lastActionDate = dataObj.getString("tskLastActionDate");
                        percentComplete = dataObj.getString("tskPercentComplete");
                        progressNote = dataObj.getString("tskProgressNote");
                        tskImg = dataObj.getString("tskImg");
                        tskImgName = dataObj.getString("tskImgName");
                        tskImgContentType = dataObj.getString("tskImgContentType");

                        scheduledTasksList = new ArrayList<>();
                        JSONArray dataArray2 = dataObj.getJSONArray("taskSchedules");
                        for (int j = 0; j < dataArray2.length(); j++) {
                            PendingTaskDetails pendingTaskDetails = new PendingTaskDetails();
                            JSONObject object2 = dataArray2.getJSONObject(j);
                            int tskId = object2.getInt("tskId");
                            int tssId = object2.getInt("tssId");
                            asignee = object2.getString("assignee");
                            startDate = object2.getString("startDate");
                            dueDate = object2.getString("dueDate");
                            tStatus = object2.getString("tssStatus");
                            lastActionDate = object2.getString("tssLastActionDate");
                            percentComplete = object2.getString("tssPercentComplete");
                            progressNote = object2.getString("tssProgressNote");

                            pendingTaskDetails.setTskId(tskId);
                            pendingTaskDetails.setTssId(tssId);
                            pendingTaskDetails.setAssigneeName(asignee);
                            pendingTaskDetails.setStartDate(startDate);
                            pendingTaskDetails.setDueDate(dueDate);
                            pendingTaskDetails.setStatus(tStatus);
                            pendingTaskDetails.setPercentComplete(percentComplete);
                            pendingTaskDetails.setProgressNote(progressNote);

                            scheduledTasksList.add(pendingTaskDetails);
                        }

                        openTaskDetailPage();

                    } else {
                        Toast.makeText(context, "Task details not found!", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resMyLectures = util.getTaskDetails(orgId, insId, dscId, stfId, tskId, tssId, assignerId, isTsk);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void openTaskDetailPage() {

        Gson gson = new Gson();
        String json = gson.toJson(scheduledTasksList);

        Intent intent = new Intent(context, EditTaskActivity.class);
        intent.putExtra("subject", subject);
        intent.putExtra("description", description);
        intent.putExtra("creationDate", creationDate);
        intent.putExtra("priority", priority);
        intent.putExtra("status", tStatus);
        intent.putExtra("startDate", startDate);
        intent.putExtra("endDate", endDate);
        intent.putExtra("percentComplete", percentComplete);
        intent.putExtra("progressNote", progressNote);
        intent.putExtra("taskDetailList", json);
        intent.putExtra("taskAssignedToMe", taskAssignedToMe);
        intent.putExtra("assignerId", assignerId);
        intent.putExtra("lastActionDate", lastActionDate);
        intent.putExtra("evtId", evtId);
        EditTaskActivity.tskImg = tskImg;
        intent.putExtra("tskImgName", tskImgName);
        intent.putExtra("tskImgContentType", tskImgContentType);
        if(taskAssignedToMe.equals("To")){
            intent.putExtra("tskId", tskId);
            intent.putExtra("tssId", tssId);
        }else if(taskAssignedToMe.equals("By")){
            intent.putExtra("tskId", tskId);
        }
        context.startActivity(intent);

    }

}
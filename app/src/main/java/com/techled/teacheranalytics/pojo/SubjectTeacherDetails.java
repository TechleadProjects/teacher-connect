package com.techled.teacheranalytics.pojo;

/**
 * Created by sonal on 28/2/18.
 */

public class SubjectTeacherDetails {
    private String stfName,stfAddress,stfContactNo,stfEmail,stfImage;

    public String getStfName() {
        return stfName;
    }

    public void setStfName(String stfName) {
        this.stfName = stfName;
    }

    public String getStfAddress() {
        return stfAddress;
    }

    public void setStfAddress(String stfAddress) {
        this.stfAddress = stfAddress;
    }

    public String getStfContactNo() {
        return stfContactNo;
    }

    public void setStfContactNo(String stfContactNo) {
        this.stfContactNo = stfContactNo;
    }

    public String getStfEmail() {
        return stfEmail;
    }

    public void setStfEmail(String stfEmail) {
        this.stfEmail = stfEmail;
    }

    public String getStfImage() {
        return stfImage;
    }

    public void setStfImage(String stfImage) {
        this.stfImage = stfImage;
    }

    public int getStfId() {
        return stfId;
    }

    public void setStfId(int stfId) {
        this.stfId = stfId;
    }

    private int stfId;

}

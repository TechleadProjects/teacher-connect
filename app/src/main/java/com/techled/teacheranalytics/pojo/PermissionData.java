package com.techled.teacheranalytics.pojo;

/**
 * Created by ajinkya on 27/5/15.
 */
public class PermissionData {
    private String moduleName;
    private String applicableStdIds;
    private boolean view;
    private boolean add;
    private boolean edit;
    private boolean delete;

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getApplicableStdIds() {
        return applicableStdIds;
    }

    public void setApplicableStdIds(String applicableStdIds) {
        this.applicableStdIds = applicableStdIds;
    }

    public boolean isView() {
        return view;
    }

    public void setView(boolean view) {
        this.view = view;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}

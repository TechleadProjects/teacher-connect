package com.techled.teacheranalytics.pojo;

import java.util.List;

/**
 * Created by techlead on 5/2/18.
 */

public class AcadmicData {
    private String marks,color,subName,stdName,divName;
    private int subId,stdId,divId;

    private List<SubjectInfo> subjectInfoArrayList;

    public List<SubjectInfo> getSubjectInfoArrayList() {
        return subjectInfoArrayList;
    }

    public void setSubjectInfoArrayList(List<SubjectInfo> subjectInfoArrayList) {
        this.subjectInfoArrayList = subjectInfoArrayList;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getDivName() {
        return divName;
    }

    public void setDivName(String divName) {
        this.divName = divName;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }




}

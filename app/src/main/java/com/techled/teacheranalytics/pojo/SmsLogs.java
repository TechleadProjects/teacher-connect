package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 10/7/17.
 */

public class SmsLogs {
    public int getAseId() {
        return aseId;
    }

    public void setAseId(int aseId) {
        this.aseId = aseId;
    }

    public int getAssProcessId() {
        return assProcessId;
    }

    public void setAssProcessId(int assProcessId) {
        this.assProcessId = assProcessId;
    }

    public String  getAssContactNo() {
        return assContactNo;
    }

    public void setAssContactNo(String assContactNo) {
        this.assContactNo = assContactNo;
    }

    public String getAssMessage() {
        return assMessage;
    }

    public void setAssMessage(String assMessage) {
        this.assMessage = assMessage;
    }

    public String getAssTimestampRequested() {
        return assTimestampRequested;
    }

    public void setAssTimestampRequested(String assTimestampRequested) {
        this.assTimestampRequested = assTimestampRequested;
    }

    public String getAssTimestampProcessed() {
        return assTimestampProcessed;
    }

    public void setAssTimestampProcessed(String assTimestampProcessed) {
        this.assTimestampProcessed = assTimestampProcessed;
    }

    private int aseId,assProcessId;
    private String assMessage,assTimestampRequested,assTimestampProcessed,assContactNo;
}

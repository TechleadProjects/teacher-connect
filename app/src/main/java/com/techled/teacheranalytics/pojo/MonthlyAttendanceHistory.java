package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 8/5/18.
 */

public class MonthlyAttendanceHistory {
    private String workingCount,presentCount,monthName;

    public String getWorkingCount() {
        return workingCount;
    }

    public void setWorkingCount(String workingCount) {
        this.workingCount = workingCount;
    }

    public String getPresentCount() {
        return presentCount;
    }

    public void setPresentCount(String presentCount) {
        this.presentCount = presentCount;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }
}

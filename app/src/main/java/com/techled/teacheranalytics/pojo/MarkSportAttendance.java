package com.techled.teacheranalytics.pojo;

import java.util.ArrayList;


public class MarkSportAttendance {
    private int stuId;

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    private int rollNo;
    private String flagPAL;
    private String comment;
    private String stuStatusDaily;

    @Override
    public String toString() {

        if(flagPAL.equals("P")) {
            return "" + rollNo;
        }
        return "0";
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    private String stuName;
    private ArrayList<MarkSportAttendance> markAttendanceArrayList;

    public String getFlagPAL() {
        return flagPAL;
    }

    public void setFlagPAL(String flagPAL) {
        this.flagPAL = flagPAL;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<MarkSportAttendance> getMarkAttendanceArrayList() {
        return markAttendanceArrayList;
    }

    public void setMarkAttendanceArrayList(ArrayList<MarkSportAttendance> markAttendanceArrayList) {
        this.markAttendanceArrayList = markAttendanceArrayList;
    }

    public String getStuStatusDaily() {
        return stuStatusDaily;
    }

    public void setStuStatusDaily(String stuStatusDaily) {
        this.stuStatusDaily = stuStatusDaily;
    }

    public int getStdId() {

        return stuId;
    }

    public void setStdId(int stuId) {
        this.stuId = stuId;
    }
}

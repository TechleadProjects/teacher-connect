package com.techled.teacheranalytics.pojo;

import java.util.List;

/**
 * Created by techlead on 7/2/18.
 */

public class SubjectInfo {
    private String marks,color,subName,stdName,divName;
    private int subId;
    private List<StudentInfo> studentInfoArrayList;

    public List<StudentInfo> getStudentInfoArrayList() {
        return studentInfoArrayList;
    }

    public void setStudentInfoArrayList(List<StudentInfo> studentInfoArrayList) {
        this.studentInfoArrayList = studentInfoArrayList;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getDivName() {
        return divName;
    }

    public void setDivName(String divName) {
        this.divName = divName;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }

    private int stdId;
    private int divId;
}

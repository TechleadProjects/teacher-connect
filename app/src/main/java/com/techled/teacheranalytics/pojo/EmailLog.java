package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 7/7/17.
 */

public class EmailLog {
    public int getAscId() {
        return ascId;
    }

    public void setAscId(int ascId) {
        this.ascId = ascId;
    }

    public String getAscTo() {
        return ascTo;
    }

    public void setAscTo(String ascTo) {
        this.ascTo = ascTo;
    }

    public String getAscBcc() {
        return ascBcc;
    }

    public void setAscBcc(String ascBcc) {
        this.ascBcc = ascBcc;
    }

    public String getAscSubject() {
        return ascSubject;
    }

    public void setAscSubject(String ascSubject) {
        this.ascSubject = ascSubject;
    }

    public String getAscTimestampRequested() {
        return ascTimestampRequested;
    }

    public void setAscTimestampRequested(String ascTimestampRequested) {
        this.ascTimestampRequested = ascTimestampRequested;
    }

    public String getAscMessage() {
        return ascMessage;
    }

    public void setAscMessage(String ascMessage) {
        this.ascMessage = ascMessage;
    }

    public String getAscTimestampProcessed() {
        return ascTimestampProcessed;
    }

    public void setAscTimestampProcessed(String ascTimestampProcessed) {
        this.ascTimestampProcessed = ascTimestampProcessed;
    }

    private int ascId;
            private String ascTo;
    private String ascCc;

    public String getAscCc() {
        return ascCc;
    }

    public void setAscCc(String ascCc) {
        this.ascCc = ascCc;
    }

    private String ascBcc;
    private String ascSubject;
    private String ascTimestampRequested;
    private String ascMessage;
    private String ascTimestampProcessed;
}

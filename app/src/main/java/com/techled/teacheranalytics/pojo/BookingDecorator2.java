package com.techled.teacheranalytics.pojo;

import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import android.text.style.BackgroundColorSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.techled.teacheranalytics.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by techlead on 23/8/17.
 */

public class BookingDecorator2 implements DayViewDecorator {
    private int mColor;
    private Context context;
    private HashSet<Date> mCalendarDayCollection;
    private String type;

    public BookingDecorator2(Context mContext) {
       context = mContext;
    }

    @Override
    public boolean shouldDecorate(CalendarDay date) {
        try {

            String DATE = date.toString();
            String[] d = DATE.split("-");
            int dd = date.getDay();

            int mm = date.getMonth() + 1;
            int yyyy = date.getYear();

            DATE = yyyy + "-" + mm + "-" + dd;

            SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
            //   mcv.setSelected(false);
            Date d1 = simpledateformat.parse(DATE);
            return mCalendarDayCollection.contains(d1);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void decorate(DayViewFacade view) {

          //view.addSpan(new ForegroundColorSpan(mColor));
        view.addSpan(new BackgroundColorSpan(Color.RED));


    }
}
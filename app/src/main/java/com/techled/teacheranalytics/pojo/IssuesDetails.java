package com.techled.teacheranalytics.pojo;

public class IssuesDetails {
    private int sctId;
    private String sctTitle;
    private String sctDescription;
    private String sctReportingDate;
    private String sctExpDueDate;
    private String sctPriorityUhml;
    private String sctTypeBe;
    private String sctProgressStatusNircj;

    public int getSctId() {
        return sctId;
    }

    public void setSctId(int sctId) {
        this.sctId = sctId;
    }

    public String getSctProgressStatusNircj() {
        return sctProgressStatusNircj;
    }

    public void setSctProgressStatusNircj(String sctProgressStatusNircj) {
        this.sctProgressStatusNircj = sctProgressStatusNircj;
    }

    public String getSctTitle() {
        return sctTitle;
    }

    public void setSctTitle(String sctTitle) {
        this.sctTitle = sctTitle;
    }

    public String getSctDescription() {
        return sctDescription;
    }

    public void setSctDescription(String sctDescription) {
        this.sctDescription = sctDescription;
    }

    public String getSctReportingDate() {
        return sctReportingDate;
    }

    public void setSctReportingDate(String sctReportingDate) {
        this.sctReportingDate = sctReportingDate;
    }

    public String getSctExpDueDate() {
        return sctExpDueDate;
    }

    public void setSctExpDueDate(String sctExpDueDate) {
        this.sctExpDueDate = sctExpDueDate;
    }

    public String getSctPriorityUhml() {
        return sctPriorityUhml;
    }

    public void setSctPriorityUhml(String sctPriorityUhml) {
        this.sctPriorityUhml = sctPriorityUhml;
    }

    public String getSctTypeBe() {
        return sctTypeBe;
    }

    public void setSctTypeBe(String sctTypeBe) {
        this.sctTypeBe = sctTypeBe;
    }

    public String getMdlName() {
        return mdlName;
    }

    public void setMdlName(String mdlName) {
        this.mdlName = mdlName;
    }

    private String mdlName;
}

package com.techled.teacheranalytics.pojo;

import java.util.ArrayList;

public class StfGradesDetails {
    private String stfCd;
    private String stfGradeName;

    public String getSetSelected() {
        return setSelected;
    }

    public void setSetSelected(String setSelected) {
        this.setSelected = setSelected;
    }

    private String setSelected;
    private ArrayList<StfGradesDetails> stfGradesDetailsArrayList;

    public ArrayList<StfGradesDetails> getStfGradesDetailsArrayList() {
        return stfGradesDetailsArrayList;
    }

    public void setStfGradesDetailsArrayList(ArrayList<StfGradesDetails> stfGradesDetailsArrayList) {
        this.stfGradesDetailsArrayList = stfGradesDetailsArrayList;
    }

    @Override
    public String toString() {
        return stfCd;
    }

    public String getStfCd() {
        return stfCd;
    }

    public void setStfCd(String stfCd) {
        this.stfCd = stfCd;
    }

    public String getStfGradeName() {
        return stfGradeName;
    }

    public void setStfGradeName(String stfGradeName) {
        this.stfGradeName = stfGradeName;
    }
}

package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 27/6/18.
 */

public class StuExamsMarksDetails {
    private int rollNo,stuId;
    private String stuName;
    private String stuAttd;
    private String stuMarks;
    private String stuMaxMarks;

    public String getStuOldMark() {
        return stuOldMark;
    }

    public void setStuOldMark(String stuOldMark) {
        this.stuOldMark = stuOldMark;
    }

    private String stuOldMark;

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    public int getStuId() {
        return stuId;
    }

    @Override
    public String toString() {
        return "{" +
                "rollNo:" + rollNo +
                ", stuId:" + stuId +
                ", stuAttd:'" + stuAttd + '\'' +
                ", stuMarks:'" + stuMarks + '\'' +
                ", stuOldMark:'" + stuOldMark + '\'' +
                '}';
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getStuAttd() {
        return stuAttd;
    }

    public void setStuAttd(String stuAttd) {
        this.stuAttd = stuAttd;
    }

    public String getStuMarks() {
        return stuMarks;
    }

    public void setStuMarks(String stuMarks) {
        this.stuMarks = stuMarks;
    }

    public String getStuMaxMarks() {
        return stuMaxMarks;
    }

    public void setStuMaxMarks(String stuMaxMarks) {
        this.stuMaxMarks = stuMaxMarks;
    }
}

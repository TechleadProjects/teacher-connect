package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 1/6/18.
 */

public class TimelineDataDetails {
    private String stuTitle;
    private String tilDate;
    private String tilAnefhct;
    private String tilS3Url;
    private String assigneeName;

    public String getAssigneeName() {
        return assigneeName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    private String tilUploadedContentTypeVadi;
    private String tilDescription;

    public String getTilReadYn() {
        return tilReadYn;
    }

    public void setTilReadYn(String tilReadYn) {
        this.tilReadYn = tilReadYn;
    }

    private String tilReadYn;
    private int tilId;
    private int oldStfId;

    public int getYearMonthId() {
        return yearMonthId;
    }

    public void setYearMonthId(int yearMonthId) {
        this.yearMonthId = yearMonthId;
    }

    private int yearMonthId;

    public int getTilId() {
        return tilId;
    }

    public void setTilId(int tilId) {
        this.tilId = tilId;
    }

    public int getOldStfId() {
        return oldStfId;
    }

    public void setOldStfId(int oldStfId) {
        this.oldStfId = oldStfId;
    }

    public String getTilUploadedContentTypeVadi() {
        return tilUploadedContentTypeVadi;
    }

    public void setTilUploadedContentTypeVadi(String tilUploadedContentTypeVadi) {
        this.tilUploadedContentTypeVadi = tilUploadedContentTypeVadi;
    }

    public String getTilDescription() {
        return tilDescription;
    }

    public void setTilDescription(String tilDescription) {
        this.tilDescription = tilDescription;
    }

    public String getTilS3Url() {
        return tilS3Url;
    }

    public void setTilS3Url(String tilS3Url) {
        this.tilS3Url = tilS3Url;
    }

    public String getStuTitle() {
        return stuTitle;
    }

    public void setStuTitle(String stuTitle) {
        this.stuTitle = stuTitle;
    }

    public String getTilDate() {
        return tilDate;
    }

    public void setTilDate(String tilDate) {
        this.tilDate = tilDate;
    }

    public String getTilAnefhct() {
        return tilAnefhct;
    }

    public void setTilAnefhct(String tilAnefhct) {
        this.tilAnefhct = tilAnefhct;
    }

    public String getTilPriority() {
        return tilPriority;
    }

    public void setTilPriority(String tilPriority) {
        this.tilPriority = tilPriority;
    }

    private String tilPriority;

}

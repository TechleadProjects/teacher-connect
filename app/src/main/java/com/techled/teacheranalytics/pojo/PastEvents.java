package com.techled.teacheranalytics.pojo;

import java.util.List;

/**
 * Created by techlead on 11/7/17.
 */

public class PastEvents {
    public int getImgCount() {
        return imgCount;
    }

    public void setImgCount(int imgCount) {
        this.imgCount = imgCount;
    }

    private  int imgCount;
    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getInsId() {
        return insId;
    }

    public void setInsId(int insId) {
        this.insId = insId;
    }

    public int getDscId() {
        return dscId;
    }

    public void setDscId(int dscId) {
        this.dscId = dscId;
    }

    public int getAyr() {
        return ayr;
    }

    public void setAyr(int ayr) {
        this.ayr = ayr;
    }

    private int orgId,insId,dscId,ayr;
    public int getEvtId() {
        return evtId;
    }

    public void setEvtId(int evtId) {
        this.evtId = evtId;
    }

    public String getEvtDesc() {
        return evtDesc;
    }

    public void setEvtDesc(String evtDesc) {
        this.evtDesc = evtDesc;
    }

    public String getEvtOrganizedBy() {
        return evtOrganizedBy;
    }

    public void setEvtOrganizedBy(String evtOrganizedBy) {
        this.evtOrganizedBy = evtOrganizedBy;
    }

    public String getEvtTo() {
        return evtTo;
    }

    public void setEvtTo(String evtTo) {
        this.evtTo = evtTo;
    }

    public String getEvtFrom() {
        return evtFrom;
    }

    public void setEvtFrom(String evtFrom) {
        this.evtFrom = evtFrom;
    }

    public String getEvtTitle() {
        return evtTitle;
    }

    public void setEvtTitle(String evtTitle) {
        this.evtTitle = evtTitle;
    }

    private int evtId;
    private String evtDesc,evtOrganizedBy,evtTo,evtFrom,evtTitle;

    public static class StaffWiseTeachingLog {
        private String dayIndex;
        private String date;
        private String stfName;
        private String leaveAbsentCount;
        private String eventCount;
        private String holidayCount;
        private String onDutyCount;
        private String forgotCheckOut;
        private String rescheduledToMe;
        private String rescheduledToOthers;
        private String totalLecCount;

        // Teaching Log Fields
        private String telUniSun;
        private String telBokChp;
        private String telUniSunCompletionStatus;
        private String telBokChpCompletionStatus;
        private String telLectureConductedYn;
        private String telScheduledStfVisitingYn;
        private String telStfIdScheduled;
        private String telUsrIdScheduled;
        private String telSubIdScheduled;
        private String telScheduleTeaching;
        private String telScheduleTeachingDate;
        private String telActualTeaching;
        private String telActualLogDate;
        private String telScheduleSr;
        private String telPrdStartTime;
        private String telPrdEndTime;
        private String telRatingByStf;
        private String telRatingByStu;
        private String telAvgRatingByStf;
        private String telAvgRatingByStu;
        private String telComment;
        private String telAdminComment;
        private String telLectureEventBasedYn;
        private String telLectureNotConductedReason;
        private String telSpecialLogYn;
        private String telPlanApprovalApr;
        private String telLogCheckedYn;
        private String telActivitiesTaken;
        private String telWorksheetIds;
        private String telEvalEctSubectIds;
        private String telEvalCategoryData;
        private String trnInf;

        private List<StaffWiseTeachingLog> logList;

        public StaffWiseTeachingLog() {

        }

        public String getDayIndex() {
            return dayIndex;
        }

        public void setDayIndex(String dayIndex) {
            this.dayIndex = dayIndex;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getStfName() {
            return stfName;
        }

        public void setStfName(String stfName) {
            this.stfName = stfName;
        }

        public String getLeaveAbsentCount() {
            return leaveAbsentCount;
        }

        public void setLeaveAbsentCount(String leaveAbsentCount) {
            this.leaveAbsentCount = leaveAbsentCount;
        }

        public String getEventCount() {
            return eventCount;
        }

        public void setEventCount(String eventCount) {
            this.eventCount = eventCount;
        }

        public String getHolidayCount() {
            return holidayCount;
        }

        public void setHolidayCount(String holidayCount) {
            this.holidayCount = holidayCount;
        }

        public String getOnDutyCount() {
            return onDutyCount;
        }

        public void setOnDutyCount(String onDutyCount) {
            this.onDutyCount = onDutyCount;
        }

        public String getForgotCheckOut() {
            return forgotCheckOut;
        }

        public void setForgotCheckOut(String forgotCheckOut) {
            this.forgotCheckOut = forgotCheckOut;
        }

        public String getRescheduledToMe() {
            return rescheduledToMe;
        }

        public void setRescheduledToMe(String rescheduledToMe) {
            this.rescheduledToMe = rescheduledToMe;
        }

        public String getRescheduledToOthers() {
            return rescheduledToOthers;
        }

        public void setRescheduledToOthers(String rescheduledToOthers) {
            this.rescheduledToOthers = rescheduledToOthers;
        }

        public String getTotalLecCount() {
            return totalLecCount;
        }

        public void setTotalLecCount(String totalLecCount) {
            this.totalLecCount = totalLecCount;
        }

        public String getTelUniSun() {
            return telUniSun;
        }

        public void setTelUniSun(String telUniSun) {
            this.telUniSun = telUniSun;
        }

        public String getTelBokChp() {
            return telBokChp;
        }

        public void setTelBokChp(String telBokChp) {
            this.telBokChp = telBokChp;
        }

        public String getTelUniSunCompletionStatus() {
            return telUniSunCompletionStatus;
        }

        public void setTelUniSunCompletionStatus(String telUniSunCompletionStatus) {
            this.telUniSunCompletionStatus = telUniSunCompletionStatus;
        }

        public String getTelBokChpCompletionStatus() {
            return telBokChpCompletionStatus;
        }

        public void setTelBokChpCompletionStatus(String telBokChpCompletionStatus) {
            this.telBokChpCompletionStatus = telBokChpCompletionStatus;
        }

        public String getTelLectureConductedYn() {
            return telLectureConductedYn;
        }

        public void setTelLectureConductedYn(String telLectureConductedYn) {
            this.telLectureConductedYn = telLectureConductedYn;
        }

        public String getTelScheduledStfVisitingYn() {
            return telScheduledStfVisitingYn;
        }

        public void setTelScheduledStfVisitingYn(String telScheduledStfVisitingYn) {
            this.telScheduledStfVisitingYn = telScheduledStfVisitingYn;
        }

        public String getTelStfIdScheduled() {
            return telStfIdScheduled;
        }

        public void setTelStfIdScheduled(String telStfIdScheduled) {
            this.telStfIdScheduled = telStfIdScheduled;
        }

        public String getTelUsrIdScheduled() {
            return telUsrIdScheduled;
        }

        public void setTelUsrIdScheduled(String telUsrIdScheduled) {
            this.telUsrIdScheduled = telUsrIdScheduled;
        }

        public String getTelSubIdScheduled() {
            return telSubIdScheduled;
        }

        public void setTelSubIdScheduled(String telSubIdScheduled) {
            this.telSubIdScheduled = telSubIdScheduled;
        }

        public String getTelScheduleTeaching() {
            return telScheduleTeaching;
        }

        public void setTelScheduleTeaching(String telScheduleTeaching) {
            this.telScheduleTeaching = telScheduleTeaching;
        }

        public String getTelScheduleTeachingDate() {
            return telScheduleTeachingDate;
        }

        public void setTelScheduleTeachingDate(String telScheduleTeachingDate) {
            this.telScheduleTeachingDate = telScheduleTeachingDate;
        }

        public String getTelActualTeaching() {
            return telActualTeaching;
        }

        public void setTelActualTeaching(String telActualTeaching) {
            this.telActualTeaching = telActualTeaching;
        }

        public String getTelActualLogDate() {
            return telActualLogDate;
        }

        public void setTelActualLogDate(String telActualLogDate) {
            this.telActualLogDate = telActualLogDate;
        }

        public String getTelScheduleSr() {
            return telScheduleSr;
        }

        public void setTelScheduleSr(String telScheduleSr) {
            this.telScheduleSr = telScheduleSr;
        }

        public String getTelPrdStartTime() {
            return telPrdStartTime;
        }

        public void setTelPrdStartTime(String telPrdStartTime) {
            this.telPrdStartTime = telPrdStartTime;
        }

        public String getTelPrdEndTime() {
            return telPrdEndTime;
        }

        public void setTelPrdEndTime(String telPrdEndTime) {
            this.telPrdEndTime = telPrdEndTime;
        }

        public String getTelRatingByStf() {
            return telRatingByStf;
        }

        public void setTelRatingByStf(String telRatingByStf) {
            this.telRatingByStf = telRatingByStf;
        }

        public String getTelRatingByStu() {
            return telRatingByStu;
        }

        public void setTelRatingByStu(String telRatingByStu) {
            this.telRatingByStu = telRatingByStu;
        }

        public String getTelAvgRatingByStf() {
            return telAvgRatingByStf;
        }

        public void setTelAvgRatingByStf(String telAvgRatingByStf) {
            this.telAvgRatingByStf = telAvgRatingByStf;
        }

        public String getTelAvgRatingByStu() {
            return telAvgRatingByStu;
        }

        public void setTelAvgRatingByStu(String telAvgRatingByStu) {
            this.telAvgRatingByStu = telAvgRatingByStu;
        }

        public String getTelComment() {
            return telComment;
        }

        public void setTelComment(String telComment) {
            this.telComment = telComment;
        }

        public String getTelAdminComment() {
            return telAdminComment;
        }

        public void setTelAdminComment(String telAdminComment) {
            this.telAdminComment = telAdminComment;
        }

        public String getTelLectureEventBasedYn() {
            return telLectureEventBasedYn;
        }

        public void setTelLectureEventBasedYn(String telLectureEventBasedYn) {
            this.telLectureEventBasedYn = telLectureEventBasedYn;
        }

        public String getTelLectureNotConductedReason() {
            return telLectureNotConductedReason;
        }

        public void setTelLectureNotConductedReason(String telLectureNotConductedReason) {
            this.telLectureNotConductedReason = telLectureNotConductedReason;
        }

        public String getTelSpecialLogYn() {
            return telSpecialLogYn;
        }

        public void setTelSpecialLogYn(String telSpecialLogYn) {
            this.telSpecialLogYn = telSpecialLogYn;
        }

        public String getTelPlanApprovalApr() {
            return telPlanApprovalApr;
        }

        public void setTelPlanApprovalApr(String telPlanApprovalApr) {
            this.telPlanApprovalApr = telPlanApprovalApr;
        }

        public String getTelLogCheckedYn() {
            return telLogCheckedYn;
        }

        public void setTelLogCheckedYn(String telLogCheckedYn) {
            this.telLogCheckedYn = telLogCheckedYn;
        }

        public String getTelActivitiesTaken() {
            return telActivitiesTaken;
        }

        public void setTelActivitiesTaken(String telActivitiesTaken) {
            this.telActivitiesTaken = telActivitiesTaken;
        }

        public String getTelWorksheetIds() {
            return telWorksheetIds;
        }

        public void setTelWorksheetIds(String telWorksheetIds) {
            this.telWorksheetIds = telWorksheetIds;
        }

        public String getTelEvalEctSubectIds() {
            return telEvalEctSubectIds;
        }

        public void setTelEvalEctSubectIds(String telEvalEctSubectIds) {
            this.telEvalEctSubectIds = telEvalEctSubectIds;
        }

        public String getTelEvalCategoryData() {
            return telEvalCategoryData;
        }

        public void setTelEvalCategoryData(String telEvalCategoryData) {
            this.telEvalCategoryData = telEvalCategoryData;
        }

        public String getTrnInf() {
            return trnInf;
        }

        public void setTrnInf(String trnInf) {
            this.trnInf = trnInf;
        }

        public List<StaffWiseTeachingLog> getLogList() {
            return logList;
        }

        public void setLogList(List<StaffWiseTeachingLog> logList) {
            this.logList = logList;
        }
    }
}

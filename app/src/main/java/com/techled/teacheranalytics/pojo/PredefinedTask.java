package com.techled.teacheranalytics.pojo;

import androidx.annotation.NonNull;

public class PredefinedTask {
    private int ptkId;
    private String ptkSubject;

    @NonNull
    @Override
    public String toString() {
        return ptkSubject;
    }

    public int getPtkId() {
        return ptkId;
    }

    public void setPtkId(int ptkId) {
        this.ptkId = ptkId;
    }

    public String getPtkSubject() {
        return ptkSubject;
    }

    public void setPtkSubject(String ptkSubject) {
        this.ptkSubject = ptkSubject;
    }
}

package com.techled.teacheranalytics.pojo;

public class Standards {
    private String stdName;
    private String stdId;

    public String getStdName() {
        return stdName;
    }

    @Override
    public String toString() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }
}

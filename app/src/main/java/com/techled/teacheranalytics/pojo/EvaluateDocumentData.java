package com.techled.teacheranalytics.pojo;

import java.util.List;

/**
 * Created by techlead on 5/2/18.
 */

public class EvaluateDocumentData {

    private int exdId, sadRollNo;
    private String exdDocLink, exdStatus, exdStuComment, exdSource, exduploadedTime, exdEvaluatedDocLink, exdEvaluatedComment, exdEvaluatedTime, exdMarks, exdMaxMarks;

    public int getExdId() {
        return exdId;
    }

    public void setExdId(int exdId) {
        this.exdId = exdId;
    }

    public int getSadRollNo() {
        return sadRollNo;
    }

    public void setSadRollNo(int sadRollNo) {
        this.sadRollNo = sadRollNo;
    }

    public String getExdDocLink() {
        return exdDocLink;
    }

    public void setExdDocLink(String exdDocLink) {
        this.exdDocLink = exdDocLink;
    }

    public String getExdStatus() {
        return exdStatus;
    }

    public void setExdStatus(String exdStatus) {
        this.exdStatus = exdStatus;
    }

    public String getExdStuComment() {
        return exdStuComment;
    }

    public void setExdStuComment(String exdStuComment) {
        this.exdStuComment = exdStuComment;
    }

    public String getExdSource() {
        return exdSource;
    }

    public void setExdSource(String exdSource) {
        this.exdSource = exdSource;
    }

    public String getExduploadedTime() {
        return exduploadedTime;
    }

    public void setExduploadedTime(String exduploadedTime) {
        this.exduploadedTime = exduploadedTime;
    }

    public String getExdEvaluatedDocLink() {
        return exdEvaluatedDocLink;
    }

    public void setExdEvaluatedDocLink(String exdEvaluatedDocLink) {
        this.exdEvaluatedDocLink = exdEvaluatedDocLink;
    }

    public String getExdEvaluatedComment() {
        return exdEvaluatedComment;
    }

    public void setExdEvaluatedComment(String exdEvaluatedComment) {
        this.exdEvaluatedComment = exdEvaluatedComment;
    }

    public String getExdEvaluatedTime() {
        return exdEvaluatedTime;
    }

    public void setExdEvaluatedTime(String exdEvaluatedTime) {
        this.exdEvaluatedTime = exdEvaluatedTime;
    }

    public String getExdMarks() {
        return exdMarks;
    }

    public void setExdMarks(String exdMarks) {
        this.exdMarks = exdMarks;
    }

    public String getExdMaxMarks() {
        return exdMaxMarks;
    }

    public void setExdMaxMarks(String exdMaxMarks) {
        this.exdMaxMarks = exdMaxMarks;
    }
}

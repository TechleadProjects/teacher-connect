package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 27/6/18.
 */

public class StudentHomeworkData {

    private String stuName;
    private String hwkStatus;
    private String hwkDateSubmission;
    private String hwkDateCompletion;
    private String stuStd;
    private String stuDiv;
    private String subName;
    private String hwkDescription;
    private String hwkUrl;
    private String ashHwkCorrected;
    private String ashHwkRemark;

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getHwkStatus() {
        return hwkStatus;
    }

    public void setHwkStatus(String hwkStatus) {
        this.hwkStatus = hwkStatus;
    }

    public String getHwkDateSubmission() {
        return hwkDateSubmission;
    }

    public void setHwkDateSubmission(String hwkDateSubmission) {
        this.hwkDateSubmission = hwkDateSubmission;
    }

    public String getHwkDateCompletion() {
        return hwkDateCompletion;
    }

    public void setHwkDateCompletion(String hwkDateCompletion) {
        this.hwkDateCompletion = hwkDateCompletion;
    }

    public String getStuStd() {
        return stuStd;
    }

    public void setStuStd(String stuStd) {
        this.stuStd = stuStd;
    }

    public String getStuDiv() {
        return stuDiv;
    }

    public void setStuDiv(String stuDiv) {
        this.stuDiv = stuDiv;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getHwkDescription() {
        return hwkDescription;
    }

    public void setHwkDescription(String hwkDescription) {
        this.hwkDescription = hwkDescription;
    }

    public String getHwkUrl() {
        return hwkUrl;
    }

    public void setHwkUrl(String hwkUrl) {
        this.hwkUrl = hwkUrl;
    }

    public String getAshHwkCorrected() {
        return ashHwkCorrected;
    }

    public void setAshHwkCorrected(String ashHwkCorrected) {
        this.ashHwkCorrected = ashHwkCorrected;
    }

    public String getAshHwkRemark() {
        return ashHwkRemark;
    }

    public void setAshHwkRemark(String ashHwkRemark) {
        this.ashHwkRemark = ashHwkRemark;
    }
}

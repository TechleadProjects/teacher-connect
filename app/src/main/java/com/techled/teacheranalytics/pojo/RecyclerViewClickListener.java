package com.techled.teacheranalytics.pojo;

import android.view.View;

/**
 * Created by techlead on 15/6/18.
 */

public interface RecyclerViewClickListener {
    public void recyclerViewListClicked(View v, int position);
}

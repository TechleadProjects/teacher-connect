package com.techled.teacheranalytics.pojo;

public class AyrYearData {

    private String ayrLabel, ayrValue;

    @Override
    public String toString() {
        return ayrLabel;
    }

    public void setAyrLabel(String ayrLabel) {
        this.ayrLabel = ayrLabel;
    }

    public String getAyrValue() {
        return ayrValue;
    }

    public void setAyrValue(String ayrValue) {
        this.ayrValue = ayrValue;
    }
}

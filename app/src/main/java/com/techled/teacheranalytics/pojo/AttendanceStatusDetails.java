package com.techled.teacheranalytics.pojo;

public class AttendanceStatusDetails {
    private String uName,uContactNo;

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuContactNo() {
        return uContactNo;
    }

    public void setuContactNo(String uContactNo) {
        this.uContactNo = uContactNo;
    }
}

package com.techled.teacheranalytics.pojo;

import java.util.ArrayList;

public class ExamAnalysisData {

    private String className, examName;
    private int totalStudentCount, studentsAppearedCount;
    private ArrayList<String> studentsNotAppeared;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public int getTotalStudentCount() {
        return totalStudentCount;
    }

    public void setTotalStudentCount(int totalStudentCount) {
        this.totalStudentCount = totalStudentCount;
    }

    public int getStudentsAppearedCount() {
        return studentsAppearedCount;
    }

    public void setStudentsAppearedCount(int studentsAppearedCount) {
        this.studentsAppearedCount = studentsAppearedCount;
    }

    public ArrayList<String> getStudentsNotAppeared() {
        return studentsNotAppeared;
    }

    public void setStudentsNotAppeared(ArrayList<String> studentsNotAppeared) {
        this.studentsNotAppeared = studentsNotAppeared;
    }
}

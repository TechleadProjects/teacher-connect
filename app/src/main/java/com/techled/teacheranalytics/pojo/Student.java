package com.techled.teacheranalytics.pojo;

public class Student {

    private String stuName;
    private int stuId;
    private int stuRollNo;

    @Override
    public String toString() {
        return stuName;
    }


    public void setStuName(String stuName) {
        this.stuName = stuName;
    }


    public int getStuId() {
        return stuId;
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    public int getStuRollNo() {
        return stuRollNo;
    }

    public void setStuRollNo(int stuRollNo) {
        this.stuRollNo = stuRollNo;
    }
}

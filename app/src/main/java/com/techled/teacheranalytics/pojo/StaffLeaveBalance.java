package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 12/6/18.
 */

public  class StaffLeaveBalance {
    String name;
    String balance;
    String halfDayYn;
    String woffHDay;
    String creditEventCe;
    Float curCredit;
    Float debit;
    String leaveBalance;
    Integer lvrId;

    public String getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(String totalCredit) {
        this.totalCredit = totalCredit;
    }

    public double getCurrentDebit() {
        return currentDebit;
    }

    public void setCurrentDebit(double currentDebit) {
        this.currentDebit = currentDebit;
    }

    public float getApplied() {
        return applied;
    }

    public void setApplied(float applied) {
        this.applied = applied;
    }

    public float getEncash() {
        return encash;
    }

    public void setEncash(float encash) {
        this.encash = encash;
    }

    String totalCredit;
    float applied,encash;
    double currentDebit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getHalfDayYn() {
        return halfDayYn;
    }

    public void setHalfDayYn(String halfDayYn) {
        this.halfDayYn = halfDayYn;
    }

    public String getWoffHDay() {
        return woffHDay;
    }

    public void setWoffHDay(String woffHDay) {
        this.woffHDay = woffHDay;
    }

    public String getCreditEventCe() {
        return creditEventCe;
    }

    public void setCreditEventCe(String creditEventCe) {
        this.creditEventCe = creditEventCe;
    }

    public Float getCurCredit() {
        return curCredit;
    }

    public void setCurCredit(Float curCredit) {
        this.curCredit = curCredit;
    }

    public Float getDebit() {
        return debit;
    }

    public void setDebit(Float debit) {
        this.debit = debit;
    }

    public String getLeaveBalance() {
        return leaveBalance;
    }

    public void setLeaveBalance(String leaveBalance) {
        this.leaveBalance = leaveBalance;
    }

    public Integer getLvrId() {
        return lvrId;
    }

    public void setLvrId(Integer lvrId) {
        this.lvrId = lvrId;
    }
}
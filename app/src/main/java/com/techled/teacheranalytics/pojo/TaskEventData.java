package com.techled.teacheranalytics.pojo;

import androidx.annotation.NonNull;

public class TaskEventData {

	private Integer evtId;
	private String evtTitle;

	@NonNull
	@Override
	public String toString() {
		return evtTitle;
	}

	public Integer getEvtId() {
		return evtId;
	}

	public void setEvtId(Integer evtId) {
		this.evtId = evtId;
	}

	public String getEvtTitle() {
		return evtTitle;
	}

	public void setEvtTitle(String evtTitle) {
		this.evtTitle = evtTitle;
	}
}

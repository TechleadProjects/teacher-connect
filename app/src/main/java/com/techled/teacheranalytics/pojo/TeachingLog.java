package com.techled.teacheranalytics.pojo;

public class TeachingLog {
    private String std;

    public String getDiv() {
        return div;
    }

    public void setDiv(String div) {
        this.div = div;
    }

    private String div;

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    private String subName;

    public String getLectureStartTime() {
        return lectureStartTime;
    }

    public void setLectureStartTime(String lectureStartTime) {
        this.lectureStartTime = lectureStartTime;
    }

    private String lectureStartTime;

    public String getLectureEndTime() {
        return lectureEndTime;
    }

    public void setLectureEndTime(String lectureEndTime) {
        this.lectureEndTime = lectureEndTime;
    }

    private String lectureEndTime;

    public String getStd() {
        return std;
    }

    public void setStd(String std) {
        this.std = std;
    }

    private String logStatus;
    public  String getLogStatus() { return logStatus; }
    public void setLogStatus(String logStatus) { this.logStatus = logStatus; }

    private String scheduledTeachingDate;
    private String actualLogDate;
    private boolean syllabusBasted;
    private String uniSubUniName;
    private String getUniSubUniId;
    private boolean lectureTaken;
    private String scheduledTeachingPlan;
    private String teachingPlanStatus;
    private String actualTeaching;
    private boolean logChecked;
    private String comment;
    private String approverComment;
    private String higherAuthComment;
    private int prdId;
    private int prdIndex;
    private Boolean eventBased;
    private String lectureNotConductedReason;
    private Boolean specialLog;
    private Boolean filledLog;
    private int stdId;
    private int divId;
    private int subId;
    private Boolean isAbsent;
    private Boolean isHoliday;
    private Boolean isLectureOff;
    private Boolean isEvent;
    private String plannedUniSun;
    private String actualTakenUniSun;
    private String toolsMethods;
    private String plannedUniSubuniName;
    private String actualUniSubuniName;
    private String completedSubUnit;

    public String getScheduledTeachingDate() {
        return scheduledTeachingDate;
    }

    public void setScheduledTeachingDate(String scheduledTeachingDate) {
        this.scheduledTeachingDate = scheduledTeachingDate;
    }

    public String getActualLogDate() {
        return actualLogDate;
    }

    public void setActualLogDate(String actualLogDate) {
        this.actualLogDate = actualLogDate;
    }

    public boolean isSyllabusBasted() {
        return syllabusBasted;
    }

    public void setSyllabusBasted(boolean syllabusBasted) {
        this.syllabusBasted = syllabusBasted;
    }

    public String getUniSubUniName() {
        return uniSubUniName;
    }

    public void setUniSubUniName(String uniSubUniName) {
        this.uniSubUniName = uniSubUniName;
    }

    public String getGetUniSubUniId() {
        return getUniSubUniId;
    }

    public void setGetUniSubUniId(String getUniSubUniId) {
        this.getUniSubUniId = getUniSubUniId;
    }

    public boolean isLectureTaken() {
        return lectureTaken;
    }

    public void setLectureTaken(boolean lectureTaken) {
        this.lectureTaken = lectureTaken;
    }

    public String getScheduledTeachingPlan() {
        return scheduledTeachingPlan;
    }

    public void setScheduledTeachingPlan(String scheduledTeachingPlan) {
        this.scheduledTeachingPlan = scheduledTeachingPlan;
    }

    public String getTeachingPlanStatus() {
        return teachingPlanStatus;
    }

    public void setTeachingPlanStatus(String teachingPlanStatus) {
        this.teachingPlanStatus = teachingPlanStatus;
    }

    public String getActualTeaching() {
        return actualTeaching;
    }

    public void setActualTeaching(String actualTeaching) {
        this.actualTeaching = actualTeaching;
    }

    public boolean isLogChecked() {
        return logChecked;
    }

    public void setLogChecked(boolean logChecked) {
        this.logChecked = logChecked;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getApproverComment() {
        return approverComment;
    }

    public void setApproverComment(String approverComment) {
        this.approverComment = approverComment;
    }

    public String getHigherAuthComment() {
        return higherAuthComment;
    }

    public void setHigherAuthComment(String higherAuthComment) {
        this.higherAuthComment = higherAuthComment;
    }

    public int getPrdId() {
        return prdId;
    }

    public void setPrdId(int prdId) {
        this.prdId = prdId;
    }

    public int getPrdIndex() {
        return prdIndex;
    }

    public void setPrdIndex(int prdIndex) {
        this.prdIndex = prdIndex;
    }

    public Boolean getEventBased() {
        return eventBased;
    }

    public void setEventBased(Boolean eventBased) {
        this.eventBased = eventBased;
    }

    public String getLectureNotConductedReason() {
        return lectureNotConductedReason;
    }

    public void setLectureNotConductedReason(String lectureNotConductedReason) {
        this.lectureNotConductedReason = lectureNotConductedReason;
    }

    public Boolean getSpecialLog() {
        return specialLog;
    }

    public void setSpecialLog(Boolean specialLog) {
        this.specialLog = specialLog;
    }

    public Boolean getFilledLog() {
        return filledLog;
    }

    public void setFilledLog(Boolean filledLog) {
        this.filledLog = filledLog;
    }

    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public Boolean getAbsent() {
        return isAbsent;
    }

    public void setAbsent(Boolean absent) {
        isAbsent = absent;
    }

    public Boolean getHoliday() {
        return isHoliday;
    }

    public void setHoliday(Boolean holiday) {
        isHoliday = holiday;
    }

    public Boolean getLectureOff() {
        return isLectureOff;
    }

    public void setLectureOff(Boolean lectureOff) {
        isLectureOff = lectureOff;
    }

    public Boolean getEvent() {
        return isEvent;
    }

    public void setEvent(Boolean event) {
        isEvent = event;
    }

    public String getPlannedUniSun() {
        return plannedUniSun;
    }

    public void setPlannedUniSun(String plannedUniSun) {
        this.plannedUniSun = plannedUniSun;
    }

    public String getActualTakenUniSun() {
        return actualTakenUniSun;
    }

    public void setActualTakenUniSun(String actualTakenUniSun) {
        this.actualTakenUniSun = actualTakenUniSun;
    }

    public String getToolsMethods() {
        return toolsMethods;
    }

    public void setToolsMethods(String toolsMethods) {
        this.toolsMethods = toolsMethods;
    }

    public String getActualUniSubuniName() {
        return actualUniSubuniName;
    }

    public void setActualUniSubuniName(String actualUniSubuniName) {
        this.actualUniSubuniName = actualUniSubuniName;
    }

    public String getPlannedUniSubuniName() {
        return plannedUniSubuniName;
    }

    public void setPlannedUniSubuniName(String plannedUniSubuniName) {
        this.plannedUniSubuniName = plannedUniSubuniName;
    }

    public String getCompletedSubUnit() {
        return completedSubUnit;
    }

    public void setCompletedSubUnit(String completedSubUnit) {
        this.completedSubUnit = completedSubUnit;
    }
}

package com.techled.teacheranalytics.pojo;

public class ExamTypeData {

    private String etcId, extId, extName, extDisplayName, extLevelSubjectTypeYn, extPredictScoreExtIds, extSequence, extExamSelectionParam, extTerm, extAllowPromotionToNextLevel;

    @Override
    public String toString() {
        return extName;
    }


    public String getEtcId() {
        return etcId;
    }

    public void setEtcId(String etcId) {
        this.etcId = etcId;
    }

    public String getExtId() {
        return extId;
    }

    public void setExtId(String extId) {
        this.extId = extId;
    }

    public void setExtName(String extName) {
        this.extName = extName;
    }

    public String getExtDisplayName() {
        return extDisplayName;
    }

    public void setExtDisplayName(String extDisplayName) {
        this.extDisplayName = extDisplayName;
    }

    public String getExtLevelSubjectTypeYn() {
        return extLevelSubjectTypeYn;
    }

    public void setExtLevelSubjectTypeYn(String extLevelSubjectTypeYn) {
        this.extLevelSubjectTypeYn = extLevelSubjectTypeYn;
    }

    public String getExtPredictScoreExtIds() {
        return extPredictScoreExtIds;
    }

    public void setExtPredictScoreExtIds(String extPredictScoreExtIds) {
        this.extPredictScoreExtIds = extPredictScoreExtIds;
    }

    public String getExtSequence() {
        return extSequence;
    }

    public void setExtSequence(String extSequence) {
        this.extSequence = extSequence;
    }

    public String getExtExamSelectionParam() {
        return extExamSelectionParam;
    }

    public void setExtExamSelectionParam(String extExamSelectionParam) {
        this.extExamSelectionParam = extExamSelectionParam;
    }

    public String getExtTerm() {
        return extTerm;
    }

    public void setExtTerm(String extTerm) {
        this.extTerm = extTerm;
    }

    public String getExtAllowPromotionToNextLevel() {
        return extAllowPromotionToNextLevel;
    }

    public void setExtAllowPromotionToNextLevel(String extAllowPromotionToNextLevel) {
        this.extAllowPromotionToNextLevel = extAllowPromotionToNextLevel;
    }
}

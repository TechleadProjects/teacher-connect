package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 25/6/18.
 */

public class ELearningContentData {

    private String eclLink, eclDescription, eclDetails;

    public String getEclLink() {
        return eclLink;
    }

    public void setEclLink(String eclLink) {
        this.eclLink = eclLink;
    }

    public String getEclDescription() {
        return eclDescription;
    }

    public void setEclDescription(String eclDescription) {
        this.eclDescription = eclDescription;
    }

    public String getEclDetails() {
        return eclDetails;
    }

    public void setEclDetails(String eclDetails) {
        this.eclDetails = eclDetails;
    }
}

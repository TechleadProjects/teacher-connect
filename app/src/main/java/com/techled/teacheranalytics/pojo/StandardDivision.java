package com.techled.teacheranalytics.pojo;

/**
 * Created by sonal on 14/6/18.
 */

public class StandardDivision {
    private String stdName;

    public String getDivName() {
        return divName;
    }

    public void setDivName(String divName) {
        this.divName = divName;
    }

    private String divName;
    private int stdId;

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }

    private int divId;
/*
    @Override
    public String toString() {
        return stdName+" "+divName;
    }*/

    @Override
    public String toString() {
        return stdName+" - "+divName;
    }


    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }
}

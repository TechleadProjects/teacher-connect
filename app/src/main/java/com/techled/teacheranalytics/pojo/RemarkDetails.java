package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 12/6/18.
 */

public class RemarkDetails {
    private int disId;

    @Override
    public String toString() {
        return disName;
    }

    private String disName;

    public int getDisId() {
        return disId;
    }

    public void setDisId(int disId) {
        this.disId = disId;
    }

    public String getDisName() {
        return disName;
    }

    public void setDisName(String disName) {
        this.disName = disName;
    }
}

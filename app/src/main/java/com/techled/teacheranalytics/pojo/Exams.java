package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 27/6/18.
 */

public class Exams {
    private int examId;
    private String examName;

    @Override
    public String toString() {
        return examName;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }
}

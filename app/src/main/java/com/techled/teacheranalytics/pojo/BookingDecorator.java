package com.techled.teacheranalytics.pojo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import com.techled.teacheranalytics.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by techlead on 23/8/17.
 */

public class BookingDecorator implements DayViewDecorator {
    private int mColor;
    private Context context;
    private HashSet<Date> mCalendarDayCollection;
    private String type;

    public BookingDecorator(int color, HashSet<Date> calendarDayCollection, Context context, String type) {
        mColor = color;
        mCalendarDayCollection = calendarDayCollection;
        this.context = context;
        this.type = type;
    }

    @Override
    public boolean shouldDecorate(CalendarDay date) {
        try {

            String DATE = date.toString();
            String[] d = DATE.split("-");
            int dd = date.getDay();

            int mm = date.getMonth() + 1;
            int yyyy = date.getYear();

            DATE = yyyy + "-" + mm + "-" + dd;

            SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
            //   mcv.setSelected(false);
            Date d1 = simpledateformat.parse(DATE);
            return mCalendarDayCollection.contains(d1);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void decorate(DayViewFacade view) {
        if (type.equals("P")) {

            //  view.addSpan(new ForegroundColorSpan(mColor));
            //  view.addSpan(new BackgroundColorSpan(Color.GREEN));
            // for TextColor
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.white)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_green));
        } else if (type.equals("A")) {
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_red));
            // view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.red)));
        } else if (type.equals("W")) {
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_blue));
            // view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.blue)));
        } else if (type.equals("O")) {
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_orange));
            //  view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.orange)));
        } else if (type.equals("V")) {
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_purple));
            /// view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.black)));
        }
        //second half
        else if (type.equals("S")) {
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_pink));
            //view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.pink)));
        }
        //first half
        else if (type.equals("F")) {
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_light_blue));
            // view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.lightb)));
        } else if (type.equals("X")) {
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_gray));
            // view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.gray)));
        } else {
            //view.addSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)));
            view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_red));
            //view.addSpan(new BackgroundColorSpan(context.getResources().getColor(R.color.red)));
        }


    }
}
package com.techled.teacheranalytics.pojo;

public class SyllabusDetails {
    private int terId, uniId;
    private String showActiveYn;
    private String showContentYn;
    private String count;
    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }



    public String getShowActiveYn() {
        return showActiveYn;
    }

    public void setShowActiveYn(String showActiveYn) {
        this.showActiveYn = showActiveYn;
    }

    public String getShowContentYn() {
        return showContentYn;
    }

    public void setShowContentYn(String showContentYn) {
        this.showContentYn = showContentYn;
    }

    public int getTerId() {
        return terId;
    }

    public void setTerId(int terId) {
        this.terId = terId;
    }

    public int getUniId() {
        return uniId;
    }

    public void setUniId(int uniId) {
        this.uniId = uniId;
    }

    public String getUniNumber() {
        return uniNumber;
    }

    public void setUniNumber(String uniNumber) {
        this.uniNumber = uniNumber;
    }

    private String uniNumber;
    private String uniDescription;
    private String fromDate;

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    private String toDate;

    public String getUniDescription() {
        return uniDescription;
    }

    public void setUniDescription(String uniDescription) {
        this.uniDescription = uniDescription;
    }

    public String toString() { return uniDescription; }

}

package com.techled.teacheranalytics.pojo;

import androidx.annotation.NonNull;

/**
 * Created by sonal on 14/2/18.
 */

public class StuMarkData implements Comparable<StuMarkData> {
    private String stuName;
    private int stuId;

    public int getStuId() {
        return stuId;
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    private float stuMark;
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public float getStuMark() {
        return stuMark;
    }

    public void setStuMark(float stuMark) {
        this.stuMark = stuMark;
    }


    @Override
    public int compareTo(@NonNull StuMarkData markData) {
        int compareage=(int)((StuMarkData)markData).getStuMark();
        /* For Ascending order*/
        return (int) (this.getStuMark()-compareage);

    }
}

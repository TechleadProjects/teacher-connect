package com.techled.teacheranalytics.pojo;

public class Tool {
    private int ectId;
    private int ecsId;
    private String ectName;
    private String ecsHeader;

    public int getEctId() {
        return ectId;
    }

    public void setEctId(int ectId) {
        this.ectId = ectId;
    }

    public int getEcsId() {
        return ecsId;
    }

    public void setEcsId(int ecsId) {
        this.ecsId = ecsId;
    }

    public String getEctName() {
        return ectName;
    }

    public void setEctName(String ectName) {
        this.ectName = ectName;
    }

    public String getEcsHeader() {
        return ecsHeader;
    }

    public void setEcsHeader(String ecsHeader) {
        this.ecsHeader = ecsHeader;
    }
}

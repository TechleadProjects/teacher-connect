package com.techled.teacheranalytics.pojo;

public class LeaveSummary {

    private String leaveType, leavePeriod, leaveApplicationDate, leaveAccountedDays, leaveStatus, leaveReason;

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getLeavePeriod() {
        return leavePeriod;
    }

    public void setLeavePeriod(String leavePeriod) {
        this.leavePeriod = leavePeriod;
    }

    public String getLeaveApplicationDate() {
        return leaveApplicationDate;
    }

    public void setLeaveApplicationDate(String leaveApplicationDate) {
        this.leaveApplicationDate = leaveApplicationDate;
    }

    public String getLeaveAccountedDays() {
        return leaveAccountedDays;
    }

    public void setLeaveAccountedDays(String leaveAccountedDays) {
        this.leaveAccountedDays = leaveAccountedDays;
    }

    public String getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(String leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }
}

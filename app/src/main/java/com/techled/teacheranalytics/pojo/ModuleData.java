package com.techled.teacheranalytics.pojo;

/**
 * Created by sonal on 19/2/18.
 */

public class ModuleData {
    @Override
    public String toString() {
        return mdlName;
    }

    private String mdlName;
    private int mdlId;

    public String getMdlName() {
        return mdlName;
    }

    public void setMdlName(String mdlName) {
        this.mdlName = mdlName;
    }

    public int getMdlId() {
        return mdlId;
    }

    public void setMdlId(int mdlId) {
        this.mdlId = mdlId;
    }
}

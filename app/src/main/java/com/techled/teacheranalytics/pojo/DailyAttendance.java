package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 31/5/18.
 */

public class DailyAttendance {
    private String date,desc,status;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

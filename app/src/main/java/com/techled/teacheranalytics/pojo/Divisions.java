package com.techled.teacheranalytics.pojo;

public class Divisions {
    private int divId;
    private String divName;

    @Override
    public String toString() {
        return divName;
    }

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }

    public String getDivName() {
        return divName;
    }

    public void setDivName(String divName) {
        this.divName = divName;
    }
}

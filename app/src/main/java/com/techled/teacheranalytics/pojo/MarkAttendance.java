package com.techled.teacheranalytics.pojo;

import java.util.ArrayList;

/**
 * Created by sonal on 12/6/18.
 */

public class MarkAttendance {
    private int stuId;

    public int getRollNo() {
        return rollNo;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }

    private int rollNo;
    private String flagPAL;
    private String comment;

    @Override
    public String toString() {
        return "{" +
                "stuId:" + stuId +
                ",stuStatus:'" + flagPAL + '\'' +
                ",comment:'" + comment + '\'' +


                '}';
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    private String stuName;
    private ArrayList<MarkAttendance> markAttendanceArrayList;

    public String getFlagPAL() {
        return flagPAL;
    }

    public void setFlagPAL(String flagPAL) {
        this.flagPAL = flagPAL;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ArrayList<MarkAttendance> getMarkAttendanceArrayList() {
        return markAttendanceArrayList;
    }

    public void setMarkAttendanceArrayList(ArrayList<MarkAttendance> markAttendanceArrayList) {
        this.markAttendanceArrayList = markAttendanceArrayList;
    }

    public int getStdId() {

        return stuId;
    }

    public void setStdId(int stuId) {
        this.stuId = stuId;
    }
}

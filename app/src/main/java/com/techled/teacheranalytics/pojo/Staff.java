package com.techled.teacheranalytics.pojo;

public class Staff {

    private String stfName;
    private int stfId;

    @Override
    public String toString() {
        return stfName;
    }

    public String getStfName() {
        return stfName;
    }

    public void setStfName(String stfName) {
        this.stfName = stfName;
    }

    public int getStfId() {
        return stfId;
    }

    public void setStfId(int stfId) {
        this.stfId = stfId;
    }
}

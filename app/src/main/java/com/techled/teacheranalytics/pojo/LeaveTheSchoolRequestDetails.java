package com.techled.teacheranalytics.pojo;

public class LeaveTheSchoolRequestDetails {

    private String stuName,stuLcReason,stuAppliedLcDate,stuTentLcDate;

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getStuLcReason() {
        return stuLcReason;
    }

    public void setStuLcReason(String stuLcReason) {
        this.stuLcReason = stuLcReason;
    }

    public String getStuAppliedLcDate() {
        return stuAppliedLcDate;
    }

    public void setStuAppliedLcDate(String stuAppliedLcDate) {
        this.stuAppliedLcDate = stuAppliedLcDate;
    }

    public String getStuTentLcDate() {
        return stuTentLcDate;
    }

    public void setStuTentLcDate(String stuTentLcDate) {
        this.stuTentLcDate = stuTentLcDate;
    }
}

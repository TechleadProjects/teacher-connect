package com.techled.teacheranalytics.pojo;

public class MyLectureDetails {
    public String getStd() { return std;}

    public void setStd(String std) {this.std = std;}

    public String getDiv() {return div;}

    public void setDiv(String div) {this.div = div;}

    public String getSubName() {  return subName;  }

    public void setSubName(String subName) {this.subName = subName;}

    public String getLectureStartTime() {return lectureStartTime;}

    public void setLectureStartTime(String lectureStartTime) {this.lectureStartTime = lectureStartTime;}

    public String getLectureEndTime() {return lectureEndTime; }

    public void setLectureEndTime(String lectureEndTime) {this.lectureEndTime = lectureEndTime;    }

    public String getScheduledDate() {return scheduledDate;}

    public void setScheduledDate(String scheduledDate) {this.scheduledDate = scheduledDate;}

    public String getDayName() {return dayName; }

    public void setDayName(String dayName) {this.dayName = dayName;    }

    private String std;
    private String div, subName, lectureStartTime, lectureEndTime, scheduledDate, dayName, telOnlineMeetingLink, telSlbBokSb, onlineLectureYn;
    private int prdId, subId, stdId, divId, tepPrdIndex;
    private boolean bRescheduled;

    public int getPrdId() {
        return prdId;
    }

    public void setPrdId(int prdId) {
        this.prdId = prdId;
    }

    public String getTelOnlineMeetingLink() {
        return telOnlineMeetingLink;
    }

    public void setTelOnlineMeetingLink(String telOnlineMeetingLink) {
        this.telOnlineMeetingLink = telOnlineMeetingLink;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }

    public int getTepPrdIndex() {
        return tepPrdIndex;
    }

    public void setTepPrdIndex(int tepPrdIndex) {
        this.tepPrdIndex = tepPrdIndex;
    }

    public boolean isbRescheduled() {
        return bRescheduled;
    }

    public void setbRescheduled(boolean bRescheduled) {
        this.bRescheduled = bRescheduled;
    }

    public String getTelSlbBokSb() {
        return telSlbBokSb;
    }

    public void setTelSlbBokSb(String telSlbBokSb) {
        this.telSlbBokSb = telSlbBokSb;
    }

    public String getOnlineLectureYn() {
        return onlineLectureYn;
    }

    public void setOnlineLectureYn(String onlineLectureYn) {
        this.onlineLectureYn = onlineLectureYn;
    }
}

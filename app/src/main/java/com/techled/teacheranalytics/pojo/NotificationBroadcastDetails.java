package com.techled.teacheranalytics.pojo;


public class NotificationBroadcastDetails {

    private String nntTitle;

    public String getNntType() {
        return nntType;
    }

    public void setNntType(String nntType) {
        this.nntType = nntType;
    }

    private String nntType;
    private String nntDescription;
    private String nntDateTime;
    private String nntSendTo;
    private String nntUrl;

    public String getNnIndentifier() {
        return nnIndentifier;
    }

    public void setNnIndentifier(String nnIndentifier) {
        this.nnIndentifier = nnIndentifier;
    }

    private String nnIndentifier;

    public String getNntTitle() {
        return nntTitle;
    }

    public void setNntTitle(String nntTitle) {
        this.nntTitle = nntTitle;
    }

    public String getNntDescription() {
        return nntDescription;
    }

    public void setNntDescription(String nntDescription) {
        this.nntDescription = nntDescription;
    }

    public String getNntDateTime() {
        return nntDateTime;
    }

    public void setNntDateTime(String nntDateTime) {
        this.nntDateTime = nntDateTime;
    }

    public String getNntSendTo() {
        return nntSendTo;
    }

    public void setNntSendTo(String nntSendTo) {
        this.nntSendTo = nntSendTo;
    }

    public String getNntUrl() {
        return nntUrl;
    }

    public void setNntUrl(String nntUrl) {
        this.nntUrl = nntUrl;
    }
}

package com.techled.teacheranalytics.pojo;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by techlead on 6/12/17.
 */

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
//fSv8LxCH8K8:APA91bGuc_dZ6AEZbqWDt5T8bnEHW6JDC1sAUfvFggCeIOcKTk5_SesSC2cc7pCdQjo15IgevvBY1GzAES0TQk4mxP6ZDpTMuC-o1xuiSxd67Vlv5NDjgejDS9u5Wg4wuBP05iKbN8Tc
    }

    private void sendRegistrationToServer(String token) {

        // Add custom implementation, as needed.

        SharedPreferences settings = getSharedPreferences("REG_TOKEN", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("token",token );
        editor.commit();

      // fmsLMCcc1Ys:APA91bF8znZwWVPLUzjXCQHLNDijsYqCB9OEM0Sk3TWYhBoSFwVOqnX8W3QIuW89GO-CO61VTliW_a53LLM9SyM0Id93fhMvM3FS5wGuLX6syub3qP-JIDSJRSjvTMoX0y28Ikq2Nygt

    }
}



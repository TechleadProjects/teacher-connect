package com.techled.teacheranalytics.pojo;

import java.util.List;

/**
 * Created by sonal on 8/2/18.
 */

public class StudentInfo {

    private int stdId;
    private List<SubjectInfo> subjectInfoArrayList;
    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }

    public List<SubjectInfo> getSubjectInfoArrayList() {
        return subjectInfoArrayList;
    }

    public void setSubjectInfoArrayList(List<SubjectInfo> subjectInfoArrayList) {
        this.subjectInfoArrayList = subjectInfoArrayList;
    }

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }

    public int getStuId() {
        return stuId;
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getDivName() {
        return divName;
    }

    public void setDivName(String divName) {
        this.divName = divName;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    private int divId;
    private int stuId;
    private int subId;
    private String stdName,divName,stuName,subName,marks,color;


}

package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 25/6/18.
 */

public class SubjectDetails {

    private int subId;
    private String subName;

    @Override
    public String toString() {
        return subName;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }
}

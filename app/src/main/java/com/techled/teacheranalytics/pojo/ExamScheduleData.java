package com.techled.teacheranalytics.pojo;

public class ExamScheduleData {

    private String exmLabel, exmValue;

    @Override
    public String toString() {
        return exmLabel;
    }

    public void setExmLabel(String exmLabel) {
        this.exmLabel = exmLabel;
    }

    public String getExmValue() {
        return exmValue;
    }

    public void setExmValue(String exmValue) {
        this.exmValue = exmValue;
    }
}

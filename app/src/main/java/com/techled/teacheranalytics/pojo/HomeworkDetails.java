package com.techled.teacheranalytics.pojo;

/**
 * Created by techlead on 31/7/18.
 */

public class HomeworkDetails {
    private String stdName;
    private String hwkTitle;

    public String getAddToTimeline() {
        return addToTimeline;
    }

    public void setAddToTimeline(String addToTimeline) {
        this.addToTimeline = addToTimeline;
    }

    private String addToTimeline;

    private String hwkSubject;
    private String hwkDueDate;
    private String hwkAssDate;
    private String tilIndentifier;

    public String getTilIndentifier() {
        return tilIndentifier;
    }

    public void setTilIndentifier(String tilIndentifier) {
        this.tilIndentifier = tilIndentifier;
    }

    private int hwkId;
    private int stdId;

    public int getStdId() {
        return stdId;
    }

    public void setStdId(int stdId) {
        this.stdId = stdId;
    }

    public int getDivId() {
        return divId;
    }

    public void setDivId(int divId) {
        this.divId = divId;
    }

    private int divId;

    public int getHwkId() {
        return hwkId;
    }

    public void setHwkId(int hwkId) {
        this.hwkId = hwkId;
    }

    public String getHwkAssDate() {
        return hwkAssDate;
    }

    public void setHwkAssDate(String hwkAssDate) {
        this.hwkAssDate = hwkAssDate;
    }


    private String hwkURL;

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    private String subType;

    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getHwkTitle() {
        return hwkTitle;
    }

    public void setHwkTitle(String hwkTitle) {
        this.hwkTitle = hwkTitle;
    }


    public String getHwkSubject() {
        return hwkSubject;
    }

    public void setHwkSubject(String hwkSubject) {
        this.hwkSubject = hwkSubject;
    }

    public String getHwkDueDate() {
        return hwkDueDate;
    }

    public void setHwkDueDate(String hwkDueDate) {
        this.hwkDueDate = hwkDueDate;
    }

    public String getHwkURL() {
        return hwkURL;
    }

    public void setHwkURL(String hwkURL) {
        this.hwkURL = hwkURL;
    }
}

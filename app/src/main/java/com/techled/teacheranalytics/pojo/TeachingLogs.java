package com.techled.teacheranalytics.pojo;

public class TeachingLogs {

    private String stdDiv,subject,time,event;
    private int logId;

    public String getStdDiv() {
        return stdDiv;
    }

    public void setStdDiv(String stdDiv) {
        this.stdDiv = stdDiv;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public int getLogId() {
        return logId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }
}

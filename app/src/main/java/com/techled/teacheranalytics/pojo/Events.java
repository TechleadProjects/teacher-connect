package com.techled.teacheranalytics.pojo;

public class Events {

	private Integer orgId;
	private Integer insId;
	private Integer dscId;
	private Integer ayrYear;
	private Integer evtId;
	private String organizedBy;
	private String title;
	private String from;
	private String to;
    private String description;
    private String venue;
    private byte[] image;

	public Events() {

	}

	public String getOrganizedBy() {
		return organizedBy;
	}

	public void setOrganizedBy(String organizedBy) {
		this.organizedBy = organizedBy;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getInsId() {
		return insId;
	}

	public void setInsId(Integer insId) {
		this.insId = insId;
	}

	public Integer getDscId() {
		return dscId;
	}

	public void setDscId(Integer dscId) {
		this.dscId = dscId;
	}

	public Integer getAyrYear() {
		return ayrYear;
	}

	public void setAyrYear(Integer ayrYear) {
		this.ayrYear = ayrYear;
	}

	public Integer getEvtId() {
		return evtId;
	}

	public void setEvtId(Integer evtId) {
		this.evtId = evtId;
	}
}

package com.techled.teacheranalytics.pojo;

public class SubUnitsSyllabusDetails
{
    private String sunDescription;
    private String sunContents;
    private String sunActivities;
    private String from;
    private String showActiveYn;
    private String showContentYn;
    private String count;

    public String getShowActiveYn() {
        return showActiveYn;
    }

    public void setShowActiveYn(String showActiveYn) {
        this.showActiveYn = showActiveYn;
    }

    public String getShowContentYn() {
        return showContentYn;
    }

    public void setShowContentYn(String showContentYn) {
        this.showContentYn = showContentYn;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {

        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    private String to, sunElcId;
    private int uniId, sunId, terId;

    public String getSunDescription() {
        return sunDescription;
    }

    public void setSunDescription(String sunDescription) {
        this.sunDescription = sunDescription;
    }

    public String getSunContents() {
        return sunContents;
    }

    public void setSunContents(String sunContents) {
        this.sunContents = sunContents;
    }

    public int getUniId() {
        return uniId;
    }

    public void setUniId(int uniId) {
        this.uniId = uniId;
    }

    public int getSunId() {
        return sunId;
    }

    public void setSunId(int sunId) {
        this.sunId = sunId;
    }

    public int getTerId() {
        return terId;
    }

    public void setTerId(int terId) {
        this.terId = terId;
    }

    public String toString() { return sunDescription; }

    public String getSunActivities() {
        return sunActivities;
    }

    public void setSunActivities(String sunActivities) {
        this.sunActivities = sunActivities;
    }

    public String getSunElcId() {
        return sunElcId;
    }

    public void setSunElcId(String sunElcId) {
        this.sunElcId = sunElcId;
    }
}

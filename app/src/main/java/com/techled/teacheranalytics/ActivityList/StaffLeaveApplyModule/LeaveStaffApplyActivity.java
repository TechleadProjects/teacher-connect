package com.techled.teacheranalytics.ActivityList.StaffLeaveApplyModule;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.StaffLeaveApplyRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.StaffLeaveBalance;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class LeaveStaffApplyActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private ArrayList<StaffLeaveBalance> leaveList = new ArrayList<StaffLeaveBalance>();
    private RecyclerView stfLeaveApplyRecyView;
    private int orgId, insId, dscId, ugpId, usrId, ayr,usrAssetId;
    private String params,resMyInfo;
    private LinearLayout layParent;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // No session
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_staff_apply);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Apply Leave");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        try {
            util = new Util();
            context = this;

            //login details
            try {
                SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);
                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);

                    orgId = object.getInt("orgId");
                    insId = object.getInt("insId");
                    ayr = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");
                    ugpId = object.getInt("ugpId");
                    usrId = object.getInt("usrId");
                    usrAssetId = object.getInt("assetId1");

                }

                SharedPreferences prefsForMyInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
                if (prefsForMyInfo != null) {
                    resMyInfo = prefsForMyInfo.getString("response", null);
                }else {
                    resMyInfo = util.getMyInfo(usrId, ayr);
                }

                if (resMyInfo != null) {
                    JSONArray array = new JSONArray(resMyInfo);
                    JSONObject jsonObject1 = array.getJSONObject(0);
                    String Status = jsonObject1.getString("status");
                    if (Status.equals("SUCCESS")) {
                        JSONObject jsonObject2 = array.getJSONObject(1);
                        JSONObject jsonObject = jsonObject2.getJSONObject("data");


                        if (jsonObject.getInt("payrollInsId") != 0) {
                            insId = jsonObject.getInt("payrollInsId");
                        }
                        if (jsonObject.getInt("payrollStfId") != 0) {
                            usrAssetId = jsonObject.getInt("payrollStfId");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            layParent = (LinearLayout)findViewById(R.id.layParent);
            stfLeaveApplyRecyView = (RecyclerView) findViewById(R.id.stfLeaveApplyRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            stfLeaveApplyRecyView.setLayoutManager(layoutManager);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            if (usrId == 0) {
//                Toast.makeText(context,
//                        "Staff is not associated with user!",
//                        Toast.LENGTH_LONG).show();

                Snackbar snackbar = Snackbar
                        .make(layParent, "Staff is not associated with user!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }


            try {
                String data = loadData(false);
                jsonParser(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public String loadData(boolean fetchFromServer) {
        String data = util.getMyLeaveBalance(orgId, insId, dscId, ayr, usrAssetId);
        return data;
    }


    public void jsonParser(String data) {
        try {
            JSONArray mainArray = new JSONArray(data);

            JSONObject statusObj = mainArray.getJSONObject(0);
            String statusMsg = statusObj.getString("status");

            if (statusMsg.equals("FAILURE")) {
//                Toast.makeText(context, "Leaves related information not present!", Toast.LENGTH_LONG)
//                        .show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Leaves related information not present!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            /*********** Process each JSON Node ************/
            JSONArray array = mainArray.getJSONObject(1).getJSONArray("data");
            int lengthJsonArr = array.length();

            for (int index = 0; index < lengthJsonArr; index++) {
                /****** Get Object for each JSON node. ***********/
                JSONObject jsonChildNode = array.getJSONObject(index);
                StaffLeaveBalance staffLeaveBalance = new StaffLeaveBalance();
                staffLeaveBalance.setBalance((jsonChildNode.getString("balance")));
                staffLeaveBalance.setApplied(jsonChildNode.getInt("applied"));
                staffLeaveBalance.setCurrentDebit(jsonChildNode.getDouble("currentDebit"));
                staffLeaveBalance.setEncash(jsonChildNode.getInt("encash"));
                staffLeaveBalance.setCreditEventCe(jsonChildNode.getString("creditEventCe"));
                staffLeaveBalance.setCurCredit(Float.parseFloat(jsonChildNode.getString("currentCredit")));
                staffLeaveBalance.setHalfDayYn(jsonChildNode.getString("halfDayLeave"));
                staffLeaveBalance.setLeaveBalance(jsonChildNode.getString("leaveBalance"));
                staffLeaveBalance.setName(jsonChildNode.getString("lvrName"));
                staffLeaveBalance.setWoffHDay(jsonChildNode.getString("woffHday"));
                staffLeaveBalance.setLvrId(jsonChildNode.getInt("lvrId"));
                leaveList.add(staffLeaveBalance);
            }

            //set adapter

            StaffLeaveApplyRecyAdapter adapter = new StaffLeaveApplyRecyAdapter(leaveList, context);
            stfLeaveApplyRecyView.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
            util.SaveSystemLog(orgId,insId,"Error occur while loading details of leave balance","Teacher Connect",usrId,"Leave application");
        }
    }
}

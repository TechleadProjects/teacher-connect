package com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.ViewAttendanceStatusActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.AttendanceManagementRecyAdapter2;
import com.techled.teacheranalytics.adapter.AttendanceStatusRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.MarkAttendance;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.StudentAttendanceStatus;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class AttendanceActivity extends AppCompatActivity {

    private RecyclerView recyViewAbsent;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId;
    private AttendanceManagementRecyAdapter2 adapter;
    private ArrayList<MarkAttendance> studentDetailsArrayList;
    public int stdId;
    public int divId;
    private int mYear, mMonth, mDay;
    private Button btnUpdate, btnViewNotifyStatus;
    private LinearLayout layDataNotFound, layParent;
    private Util util;
    private Context context;
    private String params;
    private int reqYear, reqMonth, reqDay;
    private String reqDate;
    private TextView txtDate;
    private Dialog dialog;
    private String notifyParents;
    private ProgressDialog progDailog, progDailog2;
    private String response, date;
    private JSONArray classTeacherOfJson;
    private ArrayList<StandardDivision> standardDivisionArrayList;
    private String resUserInfo, insName;
    private String resMarkAttd, markAttdDate;
    private List<MarkAttendance> stList;
    private String selectedDate;
    private String stdName, divName;
    private ArrayList<StudentAttendanceStatus> studentAttendanceStatusArrayList;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Student Daily Attendance");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference

            context = this;
            util = new Util();
            btnUpdate = (Button) findViewById(R.id.btnUpdate);
            recyViewAbsent = (RecyclerView) findViewById(R.id.recyViewAbsent);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            layoutManager.scrollToPositionWithOffset(0, 20);
            recyViewAbsent.setLayoutManager(layoutManager);

            layParent = (LinearLayout) findViewById(R.id.layParent);
            layDataNotFound = (LinearLayout) findViewById(R.id.layDataNotFound);
            txtDate = (TextView) findViewById(R.id.txtDate);


            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //set current date
            Calendar c1 = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String curDate = df.format(c1.getTime());

            Intent intent = getIntent();
            if (intent != null) {
                selectedDate = intent.getStringExtra("selectedDate");
            }

            if (selectedDate.equals("") || selectedDate == null) {
                txtDate.setText("" + curDate);
            } else {
                txtDate.setText("" + selectedDate);
            }


            mYear = c1.get(Calendar.YEAR);
            mMonth = c1.get(Calendar.MONTH);
            mDay = c1.get(Calendar.DAY_OF_MONTH);

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                // ayr = 2016;
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");

            }


            //get data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("classTeacherOf");
                    insName = jsonObject.getString("insName");
                   /* if (jsonObject.getInt("payrollInsId") != 0) {
                        insId = jsonObject.getInt("payrollInsId");
                    }*/
                   /* if (jsonObject.getInt("payrollStfId") != 0) {
                        usrAssetId = jsonObject.getInt("payrollStfId");
                    }*/
                }
            }


            if (classTeacherOfJson == null || classTeacherOfJson.equals("") || classTeacherOfJson.length() == 0) {
//                Toast.makeText(this,
//                        "Error to load standard/division!",
//                        Toast.LENGTH_LONG).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Error to load standard/division!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                layParent.setVisibility(View.GONE);
                layDataNotFound.setVisibility(View.VISIBLE);
                TextView txtToast = (TextView) findViewById(R.id.txtToast);
                txtToast.setText("Error to load standard/division");
                return;
            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                String stdDiv = object.getString("value");
                StandardDivision standardDivision = new StandardDivision();
                standardDivision.setStdName(object.getString("label"));
                String stdDivName = object.getString("label");
                if (stdDivName.contains("-")) {
                    String splStr[] = stdDivName.split("-");
                    standardDivision.setStdName(splStr[0]);
                    Log.d("Length", "" + splStr.length);
                    if (splStr.length == 2) {
                        standardDivision.setDivName(splStr[1]);
                    } else {
                        standardDivision.setDivName("-");
                    }
                } else {
                    standardDivision.setStdName(stdDivName);
                    standardDivision.setDivName(" ");
                }

                String stdDivIds = object.getString("value");
                String[] splStrs = stdDivIds.split("-");
                standardDivision.setStdId(Integer.parseInt(splStrs[0]));
                standardDivision.setDivId(Integer.parseInt(splStrs[1]));
                standardDivisionArrayList.add(standardDivision);

            }


            dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_load_std_div_for_markattendance);
            dialog.show();
            Window window = dialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            Spinner spinnStdDiv = (Spinner) dialog.findViewById(R.id.spinnStdDiv);
            ArrayAdapter<StandardDivision> stdDivAdaptor = new ArrayAdapter<StandardDivision>(AttendanceActivity.this,
                    android.R.layout.simple_spinner_item, standardDivisionArrayList);
            stdDivAdaptor
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnStdDiv.setAdapter(stdDivAdaptor);

            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    stdId = standardDivisionArrayList.get(position).getStdId();
                    divId = standardDivisionArrayList.get(position).getDivId();
                    stdName = standardDivisionArrayList.get(position).getStdName();
                    divName = standardDivisionArrayList.get(position).getDivName();
                    standardDivisionArrayList.get(position).getStdName();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (stdId != 0 || divId != 0) {
                        try {
                            dialog.dismiss();
                            AttendanceActivity.LoadAttendance attendance = new AttendanceActivity.LoadAttendance();
                            attendance.execute(null, null);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        // Toast.makeText(context, "Please select standard/division!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Please select standard/division!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                }
            });

            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    onBackPressed();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


        //update attendance
        Button btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_confirm_mark_attendance);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                dialog.setCanceledOnTouchOutside(false);
                notifyParents = "N";
                markAttdDate = txtDate.getText().toString().trim();

                TextView txtConfirmDate = (TextView) dialog.findViewById(R.id.txtConfirmDate);
                txtConfirmDate.setText("" + markAttdDate);


                RadioGroup rgNotifyParents = (RadioGroup) dialog.findViewById(R.id.rgNotifyParents);
                rgNotifyParents.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == R.id.rbYes) {
                            notifyParents = "Y";

                        } else if (checkedId == R.id.rbNo) {
                            notifyParents = "N";
                        }
                    }
                });

                Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            dialog.dismiss();

                            stList = ((AttendanceManagementRecyAdapter2) adapter)
                                    .getList();
                            Log.d("LIST", "" + stList);

                            if (stList.size() != 0) {

                                //mark attendance
                                AttendanceActivity.markAttendance attendance = new AttendanceActivity.markAttendance();
                                attendance.execute(null, null);
                            } else {
                                //  Toast.makeText(AttendanceActivity.this, "Failed to mark attendance!", Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(layParent, "Failed to mark attendance!", Snackbar.LENGTH_LONG);
                                snackbar.setDuration(3000);
                                snackbar.show();
                                return;
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


            }

        });
    }

    public class markAttendance extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog2 = new ProgressDialog(context);
            progDailog2.setIndeterminate(false);
            progDailog2.setCancelable(false);
            progDailog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog2.setMessage("Loading...");
            progDailog2.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (progDailog2 != null) {
                if (progDailog2.isShowing()) {
                    progDailog2.dismiss();
                }
            }
            try {
                if (resMarkAttd != null) {
                    JSONArray jsonArray = new JSONArray(resMarkAttd);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String Status = object1.getString("status");
                    if (Status.equals("SUCCESS")) {
                        // Toast.makeText(context, "Attendance successfully marked!", Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(AttendanceActivity.this, MarkAttendanceStatusActivity.class);
//                        startActivity(intent);
//                        finish();

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Attendance successfully marked!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();

                        studentAttendanceStatusArrayList = new ArrayList<>();
                        JSONArray array = object1.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {
                            StudentAttendanceStatus studentAttendanceStatus = new StudentAttendanceStatus();
                            JSONObject object = array.getJSONObject(i);
                            studentAttendanceStatus.setName(object.getString("studentName"));
                            studentAttendanceStatus.setContactNo(object.getString("stuRegMobileNo"));
                            studentAttendanceStatusArrayList.add(studentAttendanceStatus);
                        }

                        if (studentAttendanceStatusArrayList.size() != 0) {
                            dialog = new Dialog(context);
                            dialog.setContentView(R.layout.dialog_attendance_notify_status);
                            dialog.show();
                            Window window = dialog.getWindow();
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            dialog.setCanceledOnTouchOutside(false);

                            TextView txtAttdDate = (TextView) dialog.findViewById(R.id.txtAttdDate);

                            txtAttdDate.setText("Attendance marked successfully for standard " + stdName + "-" + divName + " on " + date);

                            RecyclerView attdRecyView = (RecyclerView) dialog.findViewById(R.id.attdRecyView);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                            attdRecyView.setLayoutManager(linearLayoutManager);

                            //adapter
                            AttendanceStatusRecyAdapter adapter = new AttendanceStatusRecyAdapter(studentAttendanceStatusArrayList, context);
                            attdRecyView.setAdapter(adapter);

                            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(AttendanceActivity.this, MarkAttendanceStatusActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });

                        } else {
                            //  Toast.makeText(context, "Failed to notify parents!", Toast.LENGTH_LONG).show();
                            //Snackbar.make(layParent, "Failed to notify parents!", Snackbar.LENGTH_LONG)
                            //        .setDuration(3000)
                            //        .show();
                            //util.SaveSystemLog(orgId,insId,"Failed to notify parents","Teacher Connect",usrId,"Mark Attendance");
                            return;
                        }


                    } else {
                        //  Toast.makeText(context, "Failed to mark attendance!", Toast.LENGTH_LONG).show();
                        Snackbar.make(layParent, "Failed to mark attendance!", Snackbar.LENGTH_LONG)
                                .setDuration(3000)
                                .show();
                        util.SaveSystemLog(orgId,insId,"Failed to mark attendance","Teacher Connect",usrId,"Mark Attendance");
                        return;
                    }


                } else {
                    //Toast.makeText(context, "Failed to mark attendance!", Toast.LENGTH_LONG).show();
                    Snackbar.make(layParent, "Failed to mark attendance!", Snackbar.LENGTH_LONG)
                            .setDuration(3000)
                            .show();
                    util.SaveSystemLog(orgId,insId,"Failed to mark attendance","Teacher Connect",usrId,"Mark Attendance");
                    return;
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while marked attendance","Teacher Connect",usrId,"Mark Attendance");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resMarkAttd = util.markStudentAttd(orgId, insId, dscId, ayr, stdId, divId, stList.toString(), date, notifyParents, insName, usrAssetId);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while marked attendance","Teacher Connect",usrId,"Mark Attendance");

            }
            return null;
        }
    }

    public class LoadAttendance extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();
            date = txtDate.getText().toString().trim();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    layParent.setVisibility(View.GONE);
                    layDataNotFound.setVisibility(View.VISIBLE);
                    //  Toast.makeText(AttendanceActivity.this, "Unable to load attendance history!", Toast.LENGTH_SHORT).show();

                    Snackbar.make(layParent, "Unable to load attendance history!", Snackbar.LENGTH_LONG)
                            .setDuration(3000)
                            .show();
                    util.SaveSystemLog(orgId,insId,"Unable to load attendance history","Teacher Connect",usrId,"Mark Attendance");
                    return;
                } else {
                    JSONArray responseArray = new JSONArray(response);
                    JSONObject object1 = responseArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {

                        //   JSONObject object2 = responseArray.getJSONObject(1);
                        studentDetailsArrayList = new ArrayList<>();
                        JSONArray array = object1.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {
                            MarkAttendance markAttendance = new MarkAttendance();
                            JSONObject object = array.getJSONObject(i);
                            markAttendance.setStdId(object.getInt("stuId"));
                            markAttendance.setComment(object.getString("comment"));
                            markAttendance.setFlagPAL(object.getString("stuStatus"));
                            markAttendance.setStuName(object.getString("stuName"));
                            markAttendance.setRollNo(object.getInt("rollNo"));
                            studentDetailsArrayList.add(markAttendance);
                        }
                        if (studentDetailsArrayList.size() == 0) {
                            layParent.setVisibility(View.GONE);
                            layDataNotFound.setVisibility(View.VISIBLE);
                            //    Toast.makeText(AttendanceActivity.this, "Unable to load attendance history!", Toast.LENGTH_SHORT).show();
                            Snackbar.make(layParent, "Unable to load attendance history!", Snackbar.LENGTH_LONG)
                                    .setDuration(3000)
                                    .show();
                            util.SaveSystemLog(orgId,insId,"Unable to load attendance history","Teacher Connect",usrId,"Mark Attendance");
                            return;
                        }

                        //set adapter
                        layParent.setVisibility(View.VISIBLE);
                        layDataNotFound.setVisibility(View.GONE);
                        adapter = new AttendanceManagementRecyAdapter2(context, studentDetailsArrayList, txtDate.getText().toString());
                        recyViewAbsent.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

                    } else {

                        layParent.setVisibility(View.GONE);
                        layDataNotFound.setVisibility(View.VISIBLE);
                        //   Toast.makeText(AttendanceActivity.this, "Unable to load attendance history!", Toast.LENGTH_SHORT).show();
                        Snackbar.make(layParent, "Unable to load attendance history!", Snackbar.LENGTH_LONG)
                                .setDuration(3000)
                                .show();
                        util.SaveSystemLog(orgId,insId,"Unable to load attendance history","Teacher Connect",usrId,"Mark Attendance");
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading attendance history","Teacher Connect",usrId,"Mark Attendance");
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                response = util.fetchMarkedAttendanceV1(orgId, insId, dscId, ayr, stdId, divId, date);

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading attendance history","Teacher Connect",usrId,"Mark Attendance");
            }
            return null;
        }
    }


    public void setDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.timeDatePicker,
                fromDateSetListener, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private DatePickerDialog.OnDateSetListener fromDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                reqDate = String.valueOf(mYear) + "-" + String.valueOf(mMonth + 1)
                        + "-" + String.valueOf(mDay);

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                cal.set(Calendar.DAY_OF_MONTH, mDay);
                cal.set(Calendar.MONTH, mMonth);
                cal.set(Calendar.YEAR, mYear);
                cal.set(Calendar.HOUR, 00);
                cal.set(Calendar.MINUTE, 00);
                cal.set(Calendar.SECOND, 00);
                cal.set(Calendar.MILLISECOND, 00);
                Date dateFrom = cal.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                reqDate = sdf.format(dateFrom);

                txtDate.setText("" + reqDate);

                dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_load_std_div_for_markattendance);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                dialog.setCanceledOnTouchOutside(false);

                Spinner spinnStdDiv = (Spinner) dialog.findViewById(R.id.spinnStdDiv);
                ArrayAdapter<StandardDivision> ayrAdaptor = new ArrayAdapter<StandardDivision>(AttendanceActivity.this,
                        android.R.layout.simple_spinner_item, standardDivisionArrayList);
                ayrAdaptor
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnStdDiv.setAdapter(ayrAdaptor);

                spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        stdId = standardDivisionArrayList.get(position).getStdId();
                        divId = standardDivisionArrayList.get(position).getDivId();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (stdId != 0 || divId != 0) {
                            try {
                                dialog.dismiss();
                                AttendanceActivity.LoadAttendance attendance = new AttendanceActivity.LoadAttendance();
                                attendance.execute(null, null);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //  Toast.makeText(context, "Please select standard/division!", Toast.LENGTH_SHORT).show();
                            Snackbar.make(layParent, "Please select standard/division!", Snackbar.LENGTH_LONG)
                                    .setDuration(3000)
                                    .show();
                            return;
                        }
                    }
                });

                Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        onBackPressed();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

}

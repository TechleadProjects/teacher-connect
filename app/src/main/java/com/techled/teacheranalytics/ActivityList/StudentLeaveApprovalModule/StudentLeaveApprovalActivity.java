package com.techled.teacheranalytics.ActivityList.StudentLeaveApprovalModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.EnterExamMarksModule.EnterExamMarksActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.PendingStuLeavesRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.PendingStuLeave;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.SubjectDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class StudentLeaveApprovalActivity extends AppCompatActivity {
    private Context context;
    private Util util;
    private String params, resUserInfo, insName, resSubject;
    private JSONArray classTeacherOfJson;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId, stdId, divId, subId, examId;
    private ArrayList<StandardDivision> standardDivisionArrayList;
    private Spinner spinnStdDiv;
    private String stdName, divName;
    private ProgressDialog progDailog;
    private LinearLayout layParent, layNoRecord;
    private RecyclerView stuLeaveRecyView;
    private String response;
    private ArrayList<PendingStuLeave> leavesApprovalArrayList;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_leave_approval);

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Student Leave Approval");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();

            layParent = (LinearLayout) findViewById(R.id.layParent);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            spinnStdDiv = (Spinner)findViewById(R.id.spinnStdDiv);
            stuLeaveRecyView = (RecyclerView)findViewById(R.id.stuLeaveRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            stuLeaveRecyView.setLayoutManager(layoutManager);


            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");

            }


            //get data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("classTeacherOf");
                    insName = jsonObject.getString("insName");
                   /* if (jsonObject.getInt("payrollInsId") != 0) {
                        insId = jsonObject.getInt("payrollInsId");
                    }
                    if (jsonObject.getInt("payrollStfId") != 0) {
                        usrAssetId = jsonObject.getInt("payrollStfId");
                    }*/
                }
            }

            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
               /* Toast.makeText(this,
                        "Error to load standard/division!",
                        Toast.LENGTH_LONG).show();
*/
                Snackbar snackbar = Snackbar
                        .make(layParent, "Error to load standard/division!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();

                return;
            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                String stdDiv = object.getString("value");
                StandardDivision standardDivision = new StandardDivision();
                standardDivision.setStdName(object.getString("label"));
                String stdDivName = object.getString("label");
                if (stdDivName.contains("-")) {
                    String splStr[] = stdDivName.split("-");
                    standardDivision.setStdName(splStr[0]);
                    standardDivision.setDivName(splStr[1]);
                } else {
                    standardDivision.setStdName(stdDivName);
                    standardDivision.setDivName(" ");
                }

                String stdDivIds = object.getString("value");
                String[] splStrs = stdDivIds.split("-");
                standardDivision.setStdId(Integer.parseInt(splStrs[0]));
                standardDivision.setDivId(Integer.parseInt(splStrs[1]));
                standardDivisionArrayList.add(standardDivision);

            }

            if (standardDivisionArrayList.size() != 0) {
                ArrayAdapter<StandardDivision> stdDivAdaptor = new ArrayAdapter<StandardDivision>(StudentLeaveApprovalActivity.this,
                        android.R.layout.simple_spinner_item, standardDivisionArrayList);
                stdDivAdaptor
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnStdDiv.setAdapter(stdDivAdaptor);
            }
            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        stdId = standardDivisionArrayList.get(position).getStdId();
                        divId = standardDivisionArrayList.get(position).getDivId();
                        stdName = standardDivisionArrayList.get(position).getStdName();
                        divName = standardDivisionArrayList.get(position).getDivName();
                        if (stdId != 0 || divId != 0) {
                            StudentLeaveApprovalActivity.LoadPendingStuLeaves loadPendingStuLeaves = new StudentLeaveApprovalActivity.LoadPendingStuLeaves();
                            loadPendingStuLeaves.execute(null, null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class LoadPendingStuLeaves extends AsyncTask<String, String, String>

    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading");
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response != null) {
                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("FAILURE")) {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                       // Toast.makeText(context, "No pending request found for student leave approval!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No pending request found for student leave approval!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                    JSONObject object2 = array.getJSONObject(1);
                    JSONArray jsonArray = object2.getJSONArray("stuLeaves");
                    leavesApprovalArrayList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        PendingStuLeave pendingStuLeave = new PendingStuLeave();
                        JSONObject object = jsonArray.getJSONObject(i);
                        pendingStuLeave.setLeaveId(object.getInt("leaveNo"));
                        pendingStuLeave.setLeaveReason(object.getString("leaveReason"));
                        pendingStuLeave.setLeaveDuration(object.getString("leaveDuration"));
                        pendingStuLeave.setStuName(object.getString("stuName"));
                        pendingStuLeave.setMobileNo(object.getString("mobile"));
                        pendingStuLeave.setOrgId(orgId);
                        pendingStuLeave.setDscId(dscId);
                        pendingStuLeave.setInsId(insId);
                        if(object.has("stuId"))
                        {
                            pendingStuLeave.setStuId(object.getInt("stuId"));
                        }
                        leavesApprovalArrayList.add(pendingStuLeave);


                    }

                    //set adapter
                    layParent.setVisibility(View.VISIBLE);
                    layNoRecord.setVisibility(View.GONE);
                    PendingStuLeavesRecyAdapter adapter = new PendingStuLeavesRecyAdapter(leavesApprovalArrayList, context,usrAssetId);
                    stuLeaveRecyView.setAdapter(adapter);

                } else {

                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                   // Toast.makeText(context, "No pending request found for student leave approval!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No pending request found for student leave approval!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while getting pending approval","Teacher Connect",usrId,"Student Leave Approval");
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                response = util.getPendingStuLeaves(orgId, insId, dscId, ayr, usrAssetId, stdId, divId);
                //response =  util.getPendingStuLeaves(orgId,insId,dscId,2012,7,4,2);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while getting pending approval","Teacher Connect",usrId,"Student Leave Approval");
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(progDailog != null)
        {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(progDailog != null)
        {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(progDailog != null)
        {
            progDailog.dismiss();
        }
    }
}

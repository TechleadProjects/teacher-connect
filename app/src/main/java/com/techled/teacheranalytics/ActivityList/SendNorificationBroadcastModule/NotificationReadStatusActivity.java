package com.techled.teacheranalytics.ActivityList.SendNorificationBroadcastModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationReadStatusActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private String identifier;
    private Util util;
    private Context context;
    private ProgressDialog progDailog;
    private String params, resTimReadStatus, tilIdentifier, date;
    private int orgId, insId;
    private PieChart chart;
    private LinearLayout layNoRecord, layParent;
    private TextView txtReadCount,txtUnreadCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_read_status);
        try {




            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Read Status");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();

            chart = (PieChart) findViewById(R.id.chart);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            txtReadCount = (TextView) findViewById(R.id.txtReadCount);
            txtUnreadCount = (TextView) findViewById(R.id.txtUnreadCount);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
            }

            Intent intent = getIntent();
            if (intent != null) {

                resTimReadStatus = intent.getStringExtra("ReadStatus");
            }



            if (resTimReadStatus != null) {


                layParent.setVisibility(View.VISIBLE);
                layNoRecord.setVisibility(View.GONE);
                JSONArray resTimReadStatusArray = new JSONArray(resTimReadStatus);
                JSONObject resObject1 = resTimReadStatusArray.getJSONObject(0);
                String resStatus = resObject1.getString("status");
                if (resStatus.equals("SUCCESS")) {
                    JSONObject resObject2 = resTimReadStatusArray.getJSONObject(1);
                    JSONArray array = resObject2.getJSONArray("data");


                    ArrayList<Entry> yValue = new ArrayList<>();
                    ArrayList<String> xValue = new ArrayList<>();
                    if (array.length() == 2) {

                        JSONObject object = array.getJSONObject(0);
                        object.getString("readCount");

                        yValue.add(new Entry((float) array.getJSONObject(0).getInt("readCount"), 0));
                        yValue.add(new Entry((float) array.getJSONObject(1).getInt("readCount"), 1));

                        txtUnreadCount.setText("Unread Count : "+array.getJSONObject(0).getInt("readCount"));
                        txtReadCount.setText("Read Count : "+array.getJSONObject(1).getInt("readCount"));
                    } else {

                        if (array.getJSONObject(0).getString("readStatus").equals("N") ||array.getJSONObject(0).getString("readStatus").equals("") ) {


                            yValue.add(new Entry((float) array.getJSONObject(0).getInt("readCount"), 0));
                            yValue.add(new Entry((float) Integer.parseInt("0"), 1));

                            txtUnreadCount.setText("Unread Count : " + array.getJSONObject(0).getInt("readCount"));
                            txtReadCount.setText("Read Count : 0" );


                        }else {

                            yValue.add(new Entry((float) Integer.parseInt("0"), 1));
                            yValue.add(new Entry((float) array.getJSONObject(0).getInt("readCount"), 0));

                            txtUnreadCount.setText("Unread Count : 0" );
                            txtReadCount.setText("Read Count : "+array.getJSONObject(0).getString("readStatus") );
                        }
                    }

                    xValue.add("");
                    xValue.add("");
                    PieDataSet dataSet = new PieDataSet(yValue, "");
                    dataSet.setColors(new int[]{ColorTemplate.rgb("#F6546A"), ColorTemplate.rgb("#7ABF60")});
                    PieData data = new PieData(xValue, dataSet);
                    //  data.setValueFormatter(new PercentFormatter());
                    chart.setData(data);
                    chart.setDrawHoleEnabled(true);
                    chart.setTransparentCircleRadius(30f);
                    chart.setHoleRadius(30f);
                    data.setValueTextSize(13f);
                    chart.setDescription("");
                    data.setValueTextColor(Color.DKGRAY);
                    chart.getLegend().setEnabled(false);
                    chart.animateXY(1400, 1400);
                    //   chart.setOnChartValueSelectedListener((OnChartValueSelectedListener) getActivity());

                } else {
                   // Toast.makeText(context, "Failed to read notification status!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Failed to read notification status!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    return;
                }

            } else {
              //  Toast.makeText(context, "Failed to read timeline status!", Toast.LENGTH_SHORT).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Failed to read notification status!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                layNoRecord.setVisibility(View.VISIBLE);
                layParent.setVisibility(View.GONE);
                return;
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

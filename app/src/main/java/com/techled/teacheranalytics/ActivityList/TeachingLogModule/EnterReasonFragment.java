package com.techled.teacheranalytics.ActivityList.TeachingLogModule;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.techled.teacheranalytics.R;


public class EnterReasonFragment extends Fragment {

    public interface onGetReasontListener {
        public void getReason(String s);
    }

    private onGetReasontListener onGetReasontListener;
    private EditText edtReason;
    private Button btnOk;
    private Context context;
    private String reason;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enter_reason, container, false);

        context = getActivity();
        btnOk = (Button) view.findViewById(R.id.btnOk);
        edtReason = (EditText) view.findViewById(R.id.edtReason);


        Intent intent = getActivity().getIntent();
        if(intent != null)
        {
            reason = intent.getStringExtra("reason");

            if(reason == null || reason.equals(""))
            {

            }else {
                edtReason.setText(reason);
            }
        }
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reason = edtReason.getText().toString().trim();

                Intent intent = new Intent(context, FillTeachingLogsActivity.class);
                intent.putExtra("reason",reason);
                startActivity(intent);
                ((Activity)context).finish();

            }
        });
        return view;
    }


}

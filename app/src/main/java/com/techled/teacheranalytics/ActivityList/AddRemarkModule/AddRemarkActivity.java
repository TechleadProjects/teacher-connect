package com.techled.teacheranalytics.ActivityList.AddRemarkModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.AttendanceManagementDetails;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.RemarkDetails;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AddRemarkActivity extends AppCompatActivity {
    private Util util;
    private Context context;
    private String params, resDisType;
    private int orgId, insId, usrId, dscId, ayr, ugpId, vioTypeId, stuId;
    private Spinner spinnVioType;
    private String selectRbStuStf = "S";
    private EditText edtMessage;
    private ArrayList<RemarkDetails> violationList;
    private String response;
    private Spinner spinnStdDiv, spinnStudent;
    private ArrayList<AttendanceManagementDetails> studentDetailsArrayList = null;
    public int stdId;
    public int divId;
    private ArrayList<StandardDivision> standardDivisionArrayList = null;
    private JSONArray classTeacherOfJson;
    private String resUserInfo, resStudentList;
    private ProgressDialog progDailog, progDailog2, progDailog3;
    private String disRuleCheckRb = "P";
    private String remark, curDate;
    private LinearLayout layParent;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_remark);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Student Remarks");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference
            context = this;
            util = new Util();

            layParent = (LinearLayout) findViewById(R.id.layParent);
            spinnVioType = (Spinner) findViewById(R.id.spinnVioType);
            spinnStdDiv = (Spinner) findViewById(R.id.spinnStdDiv);
            spinnStudent = (Spinner) findViewById(R.id.spinnStudent);
            edtMessage = (EditText) findViewById(R.id.edtMessage);


            //get login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");

            }


            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            //check for student/staff
            RadioGroup rgForStuStf = (RadioGroup) findViewById(R.id.rgForStuStf);
            rgForStuStf.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbStu) {
                        selectRbStuStf = "S";
                    } else {
                        selectRbStuStf = "E";

                    }
                }
            });


            //defualt load positive disciplinary rule
            AddRemarkActivity.FetchDisciplinaryRule disciplinaryRule = new AddRemarkActivity.FetchDisciplinaryRule();
            disciplinaryRule.execute(null, null);


            //change on selection of radio buttons
            RadioGroup rgGoodBad = (RadioGroup) findViewById(R.id.rgGoodBad);
            rgGoodBad.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbGood) {

                        disRuleCheckRb = "P";

                        AddRemarkActivity.FetchDisciplinaryRule disciplinaryRule = new AddRemarkActivity.FetchDisciplinaryRule();
                        disciplinaryRule.execute(null, null);
                    } else if (checkedId == R.id.rbBad) {

                        disRuleCheckRb = "N";

                        AddRemarkActivity.FetchDisciplinaryRule disciplinaryRule = new AddRemarkActivity.FetchDisciplinaryRule();
                        disciplinaryRule.execute(null, null);

                    }
                }
            });


            //get selected disiplinary rule
            spinnVioType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0) {
                        vioTypeId = violationList.get(position).getDisId();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


        //load std/div
        loadStdDiv();

        //load students
        if (stdId != 0 || divId != 0) {
            AddRemarkActivity.fetchStudent fetchStudentList = new AddRemarkActivity.fetchStudent();
            fetchStudentList.execute(null, null);
        }

        Button btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                try {

                    Calendar c1 = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    curDate = df.format(c1.getTime());

                    //remark category
                    if (vioTypeId == 0) {
                       /* Toast.makeText(context, "Please select remark category!", Toast.LENGTH_SHORT).show();
                        return;*/

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Please select remark category!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;


                    }

                    //remark message
                    remark = edtMessage.getText().toString().trim();
                    if (remark.equals("") || remark == null) {
                        /*Toast.makeText(context, "Please enter remark message!", Toast.LENGTH_SHORT).show();
                        return;*/
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Please enter remark message!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;

                    }

                    //save remark
                    AddRemarkActivity.SendRemark sendRemark = new AddRemarkActivity.SendRemark();
                    sendRemark.execute(null, null);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public class SendRemark extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog3 = new ProgressDialog(context);
            progDailog3.setIndeterminate(true);
            progDailog3.setCancelable(false);
            progDailog3.setMessage("Loading...");
            progDailog3.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog3.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog3.dismiss();
                if (response != null) {

                    JSONObject object = new JSONObject(response);
                    String status = object.getString("status");
                    if (status.equals("SUCCESS")) {
                        //   Toast.makeText(AddRemarkActivity.this, "Remark sent successfully!", Toast.LENGTH_SHORT).show();

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Remark sent successfully!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(4000);
                        snackbar.show();
                        onBackPressed();
                        return;


                    } else {

//                        Toast.makeText(AddRemarkActivity.this, "Failed to send remark!", Toast.LENGTH_SHORT).show();
//                        return;

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Failed to send remark!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(4000);
                        snackbar.show();

                        //enter in system log
                        util.SaveSystemLog(orgId,insId,"Failed to send remark","Teacher Connect",usrId,"Add Remark");

                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while sending remark","Teacher Connect",usrId,"Add Remark");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = util.sendRemark(orgId, insId, ayr, stuId, vioTypeId, selectRbStuStf, remark, curDate, usrId);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while sending remark","Teacher Connect",usrId,"Add Remark");

            }
            return null;
        }
    }

    public class FetchDisciplinaryRule extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resDisType.equals("") || resDisType == null) {
                   /* Toast.makeText(context, "Remark category not found!", Toast.LENGTH_SHORT).show();
                    return;*/
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Remark category not found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(4000);
                    snackbar.show();
                    return;
                } else {
                    JSONArray array = new JSONArray(resDisType);
                    JSONObject object11 = array.getJSONObject(0);
                    String stat = object11.getString("status");
                    if (stat.equals("SUCCESS")) {
                        violationList = new ArrayList<>();
                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray1 = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray1.length(); i++) {

                            JSONObject obj = jsonArray1.getJSONObject(i);
                            RemarkDetails remarkDetails = new RemarkDetails();
                            remarkDetails.setDisId(obj.getInt("disId"));
                            remarkDetails.setDisName(obj.getString("disRule"));
                            violationList.add(remarkDetails);

                        }

                        //set adapter
                        if (violationList != null) {
                            ArrayAdapter<RemarkDetails> ayrAdaptor = new ArrayAdapter<RemarkDetails>(AddRemarkActivity.this,
                                    android.R.layout.simple_spinner_item, violationList);
                            ayrAdaptor
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnVioType.setAdapter(ayrAdaptor);

                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resDisType = util.getDisciplinaryRulesV1(orgId, insId, disRuleCheckRb);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    public void loadStdDiv() {
        try {
            //get data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("subTeacherOf");

                }
            }


            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
                return;
            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                String stdDiv = object.getString("value");
                StandardDivision standardDivision = new StandardDivision();
                standardDivision.setStdName(object.getString("label"));
                String stdDivName = object.getString("label");
                if (stdDivName.contains("-")) {
                    String splStr[] = stdDivName.split("-");
                    standardDivision.setStdName(splStr[0]);
                    standardDivision.setDivName(splStr[1]);
                } else {
                    standardDivision.setStdName(stdDivName);
                    standardDivision.setDivName(" ");
                }

                String stdDivIds = object.getString("value");
                String[] splStrs = stdDivIds.split("-");
                standardDivision.setStdId(Integer.parseInt(splStrs[0]));
                standardDivision.setDivId(Integer.parseInt(splStrs[1]));
                standardDivisionArrayList.add(standardDivision);

            }
            //set adapter to standard and division
            ArrayAdapter<StandardDivision> ayrAdaptor = new ArrayAdapter<StandardDivision>(AddRemarkActivity.this,
                    android.R.layout.simple_spinner_item, standardDivisionArrayList);
            ayrAdaptor
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnStdDiv.setAdapter(ayrAdaptor);


            // Load ClassRooms here
            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    stdId = standardDivisionArrayList.get(position).getStdId();
                    divId = standardDivisionArrayList.get(position).getDivId();
                    if (stdId != 0 || divId != 0) {
                        AddRemarkActivity.fetchStudent fetchStudentList = new AddRemarkActivity.fetchStudent();
                        fetchStudentList.execute(null, null);
                    } else {
                        if (studentDetailsArrayList != null) {
                            studentDetailsArrayList.clear();
                            ArrayAdapter<AttendanceManagementDetails> ayrAdaptor = new ArrayAdapter<AttendanceManagementDetails>(AddRemarkActivity.this,
                                    android.R.layout.simple_spinner_item, studentDetailsArrayList);
                            ayrAdaptor
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnStudent.setAdapter(ayrAdaptor);

                            return;
                        }
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class fetchStudent extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog2 = new ProgressDialog(context);
            progDailog2.setIndeterminate(true);
            progDailog2.setCancelable(false);
            progDailog2.setMessage("Loading...");
            progDailog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog2.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog2.dismiss();
            try {
                if (resStudentList.equals("") || resStudentList == null) {

                } else {
                    JSONArray array = new JSONArray(resStudentList);
                    JSONObject objStatus = array.getJSONObject(0);
                    JSONObject objData = array.getJSONObject(1);

                    studentDetailsArrayList = new ArrayList<>();
                    if (objStatus.getString("status").equals("FAILURE")) {
                        studentDetailsArrayList.clear();
                        ArrayAdapter<AttendanceManagementDetails> ayrAdaptor = new ArrayAdapter<AttendanceManagementDetails>(AddRemarkActivity.this,
                                android.R.layout.simple_spinner_item, studentDetailsArrayList);
                        ayrAdaptor
                                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnStudent.setAdapter(ayrAdaptor);

                        return;
                    }

                    JSONArray arrayData = objData.getJSONArray("data");

                    for (int nIndex = 0; nIndex < arrayData.length(); nIndex++) {
                        AttendanceManagementDetails managementDetails = new AttendanceManagementDetails();
                        managementDetails.setStuId(arrayData.getJSONObject(nIndex).getInt("stuId"));
                        managementDetails.setStuName(arrayData.getJSONObject(nIndex).getString("stuName"));
                        studentDetailsArrayList.add(managementDetails);

                    }

                    if (studentDetailsArrayList.size() != 0) {


                        //set adapter
                        ArrayAdapter<AttendanceManagementDetails> ayrAdaptor = new ArrayAdapter<AttendanceManagementDetails>(AddRemarkActivity.this,
                                android.R.layout.simple_spinner_item, studentDetailsArrayList);
                        ayrAdaptor
                                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnStudent.setAdapter(ayrAdaptor);

                        spinnStudent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                stuId = studentDetailsArrayList.get(position).getStuId();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resStudentList = util.getAttdStudentsString(orgId, insId, dscId, ayr, stdId, divId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

}
package com.techled.teacheranalytics.ActivityList.TaskManagementModule;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techled.teacheranalytics.ActivityList.SyllabusModule.SyllabusHomeActivity;
import com.techled.teacheranalytics.ActivityList.TimetableModule.ViewTimeTableActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadLevelSubjectHomeworkActivity;
import com.techled.teacheranalytics.BuildConfig;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.ScheduledTasksRecyAdapter;
import com.techled.teacheranalytics.pojo.PendingTaskDetails;
import com.techled.teacheranalytics.pojo.StdDivDetails;
import com.techled.teacheranalytics.pojo.SubjectDetails;
import com.techled.teacheranalytics.pojo.TaskEventData;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditTaskActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private String subject = "", description = "", creationDate = "", priority = "", tStatus = "", startDate = "", endDate = "", percentComplete = "", progressNote = "", taskDetailList = "";
    private ArrayList<PendingTaskDetails> pendingTaskDetailsArrayList;
    private EditText subjectEt, descriptionEt, progressNoteEt;
    private Button startDateB, endDateB, lastActionDateB;
    private TextView subjectTv, descriptionTv, progressNoteTv, changeDisabledTv;
    private TextView startDateTv, endDateTv;
    private TextView priorityTv, progressStatusTv;
    private RadioGroup priorityRadioGroup, progressStatusRadioGroup;
    private RadioButton highP, mediumP, lowP;
    private RadioButton assignedProgress, inProgressProgress, reAssignedProgress, completedProgress;
    private TextView creationDateTv, percentCompleteText, filename;
    private SeekBar percentCompleteSeekBar;
    private final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private String whichDateSelected = "", taskAssignedToMe = "", response;
    private Dialog dialog;
    private ScheduledTasksRecyAdapter scheduledTasksRecyAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private int orgId, insId, dscId, stfId, ayrYear, assignerId;
    private ProgressDialog progDailog;
    private String lastActionDate;
    private LinearLayout lastActionDateLayout, eventLayout, documentLayout;
    private int tskId, tssId, evtId;
    private Button seeScheduledTasksB, btnSubmit;
    private boolean isTsk;
    private Spinner eventSpinner;
    private ArrayList<TaskEventData> taskEventDataArrayList;
    private String tskImgName, tskImgContentType;
    public static String tskImg;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Edit task");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        context = this;
        util = new Util();

        subject = getIntent().getStringExtra("subject");
        description = getIntent().getStringExtra("description");
        creationDate = getIntent().getStringExtra("creationDate");
        priority = getIntent().getStringExtra("priority");
        tStatus = getIntent().getStringExtra("status");
        startDate = getIntent().getStringExtra("startDate");
        endDate = getIntent().getStringExtra("endDate");
        percentComplete = getIntent().getStringExtra("percentComplete");
        progressNote = getIntent().getStringExtra("progressNote");
        taskDetailList = getIntent().getStringExtra("taskDetailList");
        taskAssignedToMe = getIntent().getStringExtra("taskAssignedToMe");
        assignerId = getIntent().getIntExtra("assignerId", 0);
        lastActionDate = getIntent().getStringExtra("lastActionDate");
        evtId = getIntent().getIntExtra("evtId", 0);
        tskImgName = getIntent().getStringExtra("tskImgName");
        tskImgContentType = getIntent().getStringExtra("tskImgContentType");

        if(taskAssignedToMe.equals("To")){
            isTsk = false;
            tskId = getIntent().getIntExtra("tskId", 0);
            tssId = getIntent().getIntExtra("tssId", 0);
        }else if(taskAssignedToMe.equals("By")){
            isTsk = true;
            tskId = getIntent().getIntExtra("tskId", 0);
            tssId = 0;
        }

        Type type = new TypeToken<List<PendingTaskDetails>>(){}.getType();
        Gson gson = new Gson();
        pendingTaskDetailsArrayList = gson.fromJson(taskDetailList, type);

        //login details
        SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
        String params = loginDetails.getString("params", null);

        try {
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                stfId = object.getInt("assetId1");
                ayrYear = object.getInt("ayrYear");

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        subjectEt = findViewById(R.id.subject);
        subjectTv = findViewById(R.id.subjectTv);
        descriptionEt = findViewById(R.id.description);
        descriptionTv = findViewById(R.id.descriptionTv);
        progressNoteEt = findViewById(R.id.progressNote);

        startDateB = findViewById(R.id.startDate);
        startDateTv = findViewById(R.id.startDateTv);
        endDateB = findViewById(R.id.endDate);
        endDateTv = findViewById(R.id.endDateTv);
        lastActionDateB = findViewById(R.id.lastActionDate);
        priorityRadioGroup = findViewById(R.id.priorityRadioGroup);
        priorityTv = findViewById(R.id.priorityTv);
        creationDateTv = findViewById(R.id.creationDate);
        highP = findViewById(R.id.high);
        mediumP = findViewById(R.id.medium);
        lowP = findViewById(R.id.low);
        assignedProgress = findViewById(R.id.assignedProgress);
        inProgressProgress = findViewById(R.id.inProgressProgress);
        reAssignedProgress = findViewById(R.id.reAssignedProgress);
        completedProgress = findViewById(R.id.completedProgress);
        percentCompleteText = findViewById(R.id.percentCompleteText);
        percentCompleteSeekBar = findViewById(R.id.percentCompleteSeekBar);
        progressStatusRadioGroup = findViewById(R.id.progressStatusRadioGroup);
        progressStatusTv = findViewById(R.id.progressStatusTv);
        lastActionDateLayout = findViewById(R.id.lastActionDateLayout);
        seeScheduledTasksB = findViewById(R.id.seeScheduledTasksB);
        btnSubmit = findViewById(R.id.btnSubmit);
        changeDisabledTv = findViewById(R.id.changeDisabledTv);
        eventSpinner = findViewById(R.id.eventSpinner);
        eventLayout = findViewById(R.id.eventLayout);
        documentLayout = findViewById(R.id.documentLayout);
        filename = findViewById(R.id.fileName);

        if ((tskImg != null && !tskImg.equals("") && !tskImg.equals("null") && !tskImg.equals("noimage"))
                && (tskImgName != null && !tskImgName.equals("") && !tskImgName.equals("null"))
                && (tskImgContentType != null && !tskImgContentType.equals("") && !tskImgContentType.equals("null"))) {
            documentLayout.setVisibility(View.VISIBLE);

            filename.setText(tskImgName);
        } else {
            documentLayout.setVisibility(View.GONE);
        }

        subjectEt.setText(subject);
        subjectTv.setText(subject);
        descriptionEt.setText(description);
        descriptionTv.setText(description);
        creationDateTv.setText(creationDate);
        if(progressNote.equals("null")){
            progressNoteEt.setText("");
        } else {
            progressNoteEt.setText(progressNote);
        }

        startDateB.setText(startDate);
        startDateTv.setText(startDate);
        endDateB.setText(endDate);
        endDateTv.setText(endDate);
        percentCompleteText.setText(percentComplete.concat("% Completed."));
        percentCompleteSeekBar.setProgress(Integer.parseInt(percentComplete));
        lastActionDateB.setText(lastActionDate);

        if(priority.equals("H")){
            priorityTv.setText("High");
            highP.setChecked(true);
            mediumP.setChecked(false);
            lowP.setChecked(false);
        }else if(priority.equals("M")){
            priorityTv.setText("Medium");
            highP.setChecked(false);
            mediumP.setChecked(true);
            lowP.setChecked(false);
        }else if(priority.equals("L")){
            priorityTv.setText("Low");
            highP.setChecked(false);
            mediumP.setChecked(false);
            lowP.setChecked(true);
        }

        if(tStatus.equals("A")){
            progressStatusTv.setText("Assigned");
            assignedProgress.setChecked(true);
            inProgressProgress.setChecked(false);
            reAssignedProgress.setChecked(false);
            completedProgress.setChecked(false);
        }else if(tStatus.equals("P")){
            progressStatusTv.setText("In Progress");
            assignedProgress.setChecked(false);
            inProgressProgress.setChecked(true);
            reAssignedProgress.setChecked(false);
            completedProgress.setChecked(false);
        }else if(tStatus.equals("R")){
            progressStatusTv.setText("Reassigned");
            assignedProgress.setChecked(false);
            inProgressProgress.setChecked(false);
            reAssignedProgress.setChecked(true);
            completedProgress.setChecked(false);
        }else if(tStatus.equals("C")){
            progressStatusTv.setText("Completed");
            assignedProgress.setChecked(false);
            inProgressProgress.setChecked(false);
            reAssignedProgress.setChecked(false);
            completedProgress.setChecked(true);

            if (taskAssignedToMe.equals("To")) {
                changeDisabledTv.setVisibility(View.VISIBLE);
            } else {
                changeDisabledTv.setVisibility(View.GONE);
            }
        }

        if(percentComplete.equals("100")){
            if(taskAssignedToMe.equals("To")){
                percentCompleteSeekBar.setEnabled(false);
                progressNoteEt.setEnabled(false);
                lastActionDateB.setEnabled(false);
                btnSubmit.setVisibility(View.GONE);
            }else if(taskAssignedToMe.equals("By")){
                subjectEt.setEnabled(false);
                descriptionEt.setEnabled(false);
                startDateB.setEnabled(false);
                endDateB.setEnabled(false);
                highP.setEnabled(false);
                mediumP.setEnabled(false);
                lowP.setEnabled(false);
                assignedProgress.setEnabled(false);
                reAssignedProgress.setEnabled(false);
                inProgressProgress.setEnabled(false);
                completedProgress.setEnabled(false);
                percentCompleteSeekBar.setEnabled(false);
                progressNoteEt.setEnabled(false);
                btnSubmit.setVisibility(View.GONE);
            }
        } else {

        }

        if(taskAssignedToMe.equals("To")){
            subjectEt.setVisibility(View.GONE);
            descriptionEt.setVisibility(View.GONE);
            startDateB.setVisibility(View.GONE);
            endDateB.setVisibility(View.GONE);

            //disable radio buttons priority
            priorityRadioGroup.setVisibility(View.GONE);

            //disable radio buttons status
            progressStatusRadioGroup.setVisibility(View.GONE);
            seeScheduledTasksB.setVisibility(View.GONE);

        }else if(taskAssignedToMe.equals("By")){
            percentCompleteSeekBar.setEnabled(false);
            lastActionDateLayout.setVisibility(View.GONE);

            subjectTv.setVisibility(View.GONE);
            descriptionTv.setVisibility(View.GONE);
            startDateTv.setVisibility(View.GONE);
            endDateTv.setVisibility(View.GONE);

            //disable radio buttons priority
            priorityTv.setVisibility(View.GONE);

            //disable radio buttons status
            progressStatusTv.setVisibility(View.GONE);
        }

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        if(lastActionDate == null || lastActionDate.equals("")){
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            lastActionDate = dateFormat.format(date);
            lastActionDateB.setText(lastActionDate);
        }

        percentCompleteSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    percentCompleteText.setText(String.valueOf(progress).concat("% Completed."));
                    percentComplete = String.valueOf(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        priorityRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.high){
                    priority = "H";
                }else if(checkedId == R.id.medium){
                    priority = "M";
                }else if(checkedId == R.id.low){
                    priority = "L";
                }
            }
        });

        progressStatusRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.assignedProgress){
                    tStatus = "A";
                }else if(checkedId == R.id.inProgressProgress){
                    tStatus = "P";
                }else if(checkedId == R.id.reAssignedProgress){
                    tStatus = "R";
                }else if(checkedId == R.id.completedProgress){
                    tStatus = "C";
                }
            }
        });

        if(taskAssignedToMe.equals("By")){

            eventLayout.setVisibility(View.VISIBLE);
            new LoadEvents().execute();

        } else {

            eventLayout.setVisibility(View.GONE);
        }
    }

    public void editTaskDetails(View view) {

        if(taskAssignedToMe.equals("To")){

            progressNote = progressNoteEt.getText().toString();
            lastActionDate = lastActionDateB.getText().toString();
            new UpdateTaskToMeDetails().execute();

        }else if(taskAssignedToMe.equals("By")){
            subject = subjectEt.getText().toString();
            description = descriptionEt.getText().toString();
            creationDate = creationDateTv.getText().toString();
            startDate = startDateB.getText().toString();
            endDate = endDateB.getText().toString();
            progressNote = progressNoteEt.getText().toString();

            new UpdateTaskByMeDetails().execute();
        }
    }

    public void selectEndDate(View view) {
        whichDateSelected = "end";
        new DatePickerDialog(EditTaskActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void selectStartDate(View view) {
        whichDateSelected = "start";
        new DatePickerDialog(EditTaskActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void selectLastActionDate(View view) {

        whichDateSelected = "lastAction";
        new DatePickerDialog(EditTaskActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if(whichDateSelected.equals("start")){
            startDate = sdf.format(myCalendar.getTime());
            startDateB.setText(startDate);
        }else if(whichDateSelected.equals("end")){
            endDate = sdf.format(myCalendar.getTime());
            endDateB.setText(endDate);
        }else if(whichDateSelected.equals("lastAction")){
            lastActionDate = sdf.format(myCalendar.getTime());
            lastActionDateB.setText(lastActionDate);
        }
    }

    public void seeScheduledTasks(View view) {

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_scheduled_tasks);
        dialog.show();
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);

        Button closeButton = dialog.findViewById(R.id.closeButton);
        RecyclerView scheduledTasksRecyclerView = dialog.findViewById(R.id.scheduledTasksRecyclerView);

        layoutManager = new LinearLayoutManager(context);
        scheduledTasksRecyclerView.setLayoutManager(layoutManager);
        scheduledTasksRecyclerView.setItemAnimator(new DefaultItemAnimator());

        scheduledTasksRecyAdapter = new ScheduledTasksRecyAdapter(context, pendingTaskDetailsArrayList);
        scheduledTasksRecyclerView.setAdapter(scheduledTasksRecyAdapter);
        scheduledTasksRecyAdapter.notifyDataSetChanged();

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void viewUploadedDocument(View view) {

        convertByteStringToFile(tskImg, tskImgName, tskImgContentType);
    }

    public class UpdateTaskByMeDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        Toast.makeText(context, "Task details updated successfully!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(context, "Edit task failed!", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resMyLectures = util.updateTaskByMeDetails(orgId, insId, dscId, stfId, tskId, subject, description, startDate, endDate, progressNote, priority, tStatus, evtId);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class UpdateTaskToMeDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        Toast.makeText(context, "Task details updated successfully!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(context, "Edit task failed!", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resMyLectures = util.updateTaskToMeDetails(orgId, insId, dscId, stfId, tskId, tssId, progressNote, percentComplete, lastActionDate, assignerId, isTsk, evtId);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class LoadEvents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {

                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject statusObject = jsonArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {

                        taskEventDataArrayList = new ArrayList<>();

                        TaskEventData taskEvent = new TaskEventData();

                        taskEvent.setEvtId(0);
                        taskEvent.setEvtTitle("Choose Event");
                        taskEventDataArrayList.add(taskEvent);

                        JSONObject dataObject = jsonArray.getJSONObject(1);
                        JSONArray array = dataObject.getJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);

                            TaskEventData taskEventData = new TaskEventData();

                            taskEventData.setEvtId(jsonObject.getInt("evtId"));
                            taskEventData.setEvtTitle(jsonObject.getString("evtTitle"));

                            taskEventDataArrayList.add(taskEventData);
                        }

                    } else {

                        Toast.makeText(context, "Unable to load events, try Again later!", Toast.LENGTH_SHORT).show();
                    }

                    //set adapter
                    ArrayAdapter<TaskEventData> orgAdaptor = new ArrayAdapter<>(EditTaskActivity.this,
                            android.R.layout.simple_spinner_item, taskEventDataArrayList);
                    orgAdaptor
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    eventSpinner.setAdapter(orgAdaptor);
                    eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            evtId = taskEventDataArrayList.get(position).getEvtId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    if (evtId != 0) {

                        int position = 0;

                        for (int i=0; i<taskEventDataArrayList.size(); i++) {
                            if (taskEventDataArrayList.get(i).getEvtId() == evtId) {
                                position = i;
                            }
                        }

                        eventSpinner.setSelection(position);
                    }
                } else {
                    Toast.makeText(context, "Unable to load events, try Again later!", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = util.findEventsForTask(orgId, insId, dscId, ayrYear);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void convertByteStringToFile(String byteString, String fileName, String contentType){

        String[] id = byteString.split(",");
        byte[] byteData = new byte[id.length];
        for (int i = 0; i < id.length; i++) {
            byteData[i] = Byte.parseByte(id[i]);
        }

        System.out.println(byteData.length);
        // FileOutputStream out = openFileOutput("out.pdf", Context.MODE_PRIVATE);

        try {

            OutputStream out = new FileOutputStream(Environment.getExternalStorageDirectory() + "/" + fileName);
            out.write(byteData);
            out.close();

            File examPaperFile = new File(Environment.getExternalStorageDirectory() + "/" + fileName);  // -> filename = maven.pdf
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", examPaperFile);
            intent.setDataAndType(uri, contentType);
            context.startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();

            new AlertDialog.Builder(context)
                    .setTitle("Teacher Connect")
                    .setCancelable(false)
                    .setMessage("Couldn't open the document. Please make sure you have an app which shows pdf document.\nEg. Adobe Reader")
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }
    }
}

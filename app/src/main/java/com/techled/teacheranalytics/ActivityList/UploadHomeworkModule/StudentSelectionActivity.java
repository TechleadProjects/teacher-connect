package com.techled.teacheranalytics.ActivityList.UploadHomeworkModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.HomeworkDetailsRecyclerAdapter;
import com.techled.teacheranalytics.adapter.StudentSelectionRecyclerAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.HomeworkDetails;
import com.techled.teacheranalytics.pojo.StudentHomeworkData;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class StudentSelectionActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private RecyclerView studentsRecyclerView;
    private FloatingActionButton btnUploadHwk;
    private ProgressDialog progDailog;
    private int orgId, dscId, insId, ayr, stfId;
    private String params, resViewHwk;
    private LinearLayout layParent, layNoRecord;
    private ArrayList<StudentHomeworkData> studentHomeworkDataArrayList;
    private int stdId, divId;
    private HomeworkDetails homeworkDetails;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_selection);

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Students");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            context = this;
            util = new Util();
            studentHomeworkDataArrayList = new ArrayList<>();

            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            studentsRecyclerView = findViewById(R.id.studentsRecyclerView);

            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            studentsRecyclerView.setLayoutManager(layoutManager);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            //login details
            try {
                SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);
                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);

                    orgId = object.getInt("orgId");
                    insId = object.getInt("insId");
                    ayr = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");
                    stfId = object.getInt("assetId1");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            String intentData = getIntent().getStringExtra("homeworkObject");

            if (intentData.equals("")) {

                Type type = new TypeToken<HomeworkDetails>(){}.getType();
                Gson gson = new Gson();
                homeworkDetails = gson.fromJson(intentData, type);
            }

            StudentHomeworkData studentHomeworkData1 = new StudentHomeworkData();
            studentHomeworkData1.setStuName("A");
            studentHomeworkData1.setHwkDateSubmission("12-01-20");
            studentHomeworkData1.setHwkDateCompletion("21-02-20");

            StudentHomeworkData studentHomeworkData2 = new StudentHomeworkData();
            studentHomeworkData2.setStuName("A");
            studentHomeworkData2.setHwkDateSubmission("12-01-20");
            studentHomeworkData2.setHwkDateCompletion("21-02-20");

            StudentHomeworkData studentHomeworkData3 = new StudentHomeworkData();
            studentHomeworkData3.setStuName("A");
            studentHomeworkData3.setHwkDateSubmission("12-01-20");
            studentHomeworkData3.setHwkDateCompletion("21-02-20");

            StudentHomeworkData studentHomeworkData4 = new StudentHomeworkData();
            studentHomeworkData4.setStuName("A");
            studentHomeworkData4.setHwkDateSubmission("12-01-20");
            studentHomeworkData4.setHwkDateCompletion("21-02-20");

            studentHomeworkDataArrayList.add(studentHomeworkData1);
            studentHomeworkDataArrayList.add(studentHomeworkData2);
            studentHomeworkDataArrayList.add(studentHomeworkData3);
            studentHomeworkDataArrayList.add(studentHomeworkData4);

            //set adapter
            layParent.setVisibility(View.VISIBLE);
            layNoRecord.setVisibility(View.GONE);
            StudentSelectionRecyclerAdapter adapter = new StudentSelectionRecyclerAdapter(context, homeworkDetails, studentHomeworkDataArrayList);
            studentsRecyclerView.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class LoadStudentsTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resViewHwk != null) {

                    JSONArray jsonArray = new JSONArray(resViewHwk);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    studentHomeworkDataArrayList = new ArrayList<>();
                    if (status.equals("SUCCESS")) {

                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);
                            StudentHomeworkData studentHomeworkData = new StudentHomeworkData();
                            studentHomeworkData.setStuName(object.getString("stuName"));
                            studentHomeworkData.setHwkStatus(object.getString("hwkStatus"));
                            studentHomeworkData.setHwkDateSubmission(object.getString("hwkDateSubmission"));
                            studentHomeworkData.setHwkDateCompletion(object.getString("hwkDateCompletion"));
                            studentHomeworkData.setStuStd(object.getString("stuStd"));
                            studentHomeworkData.setStuDiv(object.getString("stuDiv"));
                            studentHomeworkData.setSubName(object.getString("subName"));
                            studentHomeworkData.setHwkDescription(object.getString("stdId"));
                            studentHomeworkData.setHwkUrl(object.getString("hwkUrl"));
                            studentHomeworkData.setAshHwkCorrected(object.getString("ashHwkCorrected"));
                            studentHomeworkData.setAshHwkRemark(object.getString("ashHwkRemark"));

                            studentHomeworkDataArrayList.add(studentHomeworkData);
                        }

                        if(studentHomeworkDataArrayList.size() != 0)
                        {
                            //set adapter
                            layParent.setVisibility(View.VISIBLE);
                            layNoRecord.setVisibility(View.GONE);
                            StudentSelectionRecyclerAdapter adapter = new StudentSelectionRecyclerAdapter(context, homeworkDetails, studentHomeworkDataArrayList);
                            studentsRecyclerView.setAdapter(adapter);

                        } else {

                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Error occurred while fetching student details!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            layNoRecord.setVisibility(View.VISIBLE);
                            layParent.setVisibility(View.GONE);
                        }

                    } else {

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Error occurred while fetching student details!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                    }

                } else {

                    Snackbar snackbar = Snackbar
                            .make(layParent, "Error occurred while fetching student details!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                }

            } catch (Exception e) {
                e.printStackTrace();

                Snackbar snackbar = Snackbar
                        .make(layParent, "Error occurred while fetching student details!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                layNoRecord.setVisibility(View.VISIBLE);
                layParent.setVisibility(View.GONE);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resViewHwk = util.getHomeworkList(orgId, insId, dscId, ayr, stfId);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading homework details","Teacher Connect",stfId,"Homework");

            }
            return null;
        }
    }
}

package com.techled.teacheranalytics.ActivityList.PerformanceModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.StudentInfo;
import com.techled.teacheranalytics.pojo.SubjectInfo;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class SubjectAnalyticsGraphFragment extends Fragment {
    private BarChart barChart;
    private ProgressDialog progDailog;
    private Context context;
    private Util util;
    private int subId=0;
    private String response;
    private String examTypes = "";
    private int orgId=0, insId=0, dscId=0, ayrYear=0;
    private String params;
    private RecyclerView subjectAnalyticsRecyView;
    private ArrayList<SubjectInfo> subjectInfoArrayList;
    private String subName;
    private int stdId,divId;
    private String stdName,divName;
    private int countGreen=0,countOrange=0,countRed=0;
    private List<StudentInfo> studentInfoArrayList;
    private LinearLayout layParent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            View view = inflater.inflate(R.layout.fragment_sub_analytics_graph, container, false);
            barChart = (BarChart) view.findViewById(R.id.barChart);
            context = getActivity();
            util = new Util();
            layParent = (LinearLayout)view.findViewById(R.id.layParent);



            try {
                SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);

                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);


                    //org shared pref
                    SharedPreferences orgPref = context.getSharedPreferences("Organization", 0);
                    if (orgPref != null) {
                        if (!orgPref.getString("Org_Id", "0").equals("")) {
                            orgId = Integer.parseInt(orgPref.getString("Org_Id", "0"));

                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        } else {
                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        }

                    }
                    //ins shared pref
                    SharedPreferences insPref = context.getSharedPreferences("Institute", 0);
                    if (insPref != null) {
                        if (!insPref.getString("Ins_Id", "0").equals("")) {
                            insId = Integer.parseInt(insPref.getString("Ins_Id", "0"));
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        } else {
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        }
                    }
                    ayrYear = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");

                }


            } catch (Exception e) {
                e.printStackTrace();
            }


            Intent intent = getActivity().getIntent();
            if (intent != null) {
                stdId = intent.getIntExtra("stdId", 0);
                divId = intent.getIntExtra("divId", 0);
                stdName = intent.getStringExtra("stdName");
                divName = intent.getStringExtra("divName");
                subId = intent.getIntExtra("subId", 0);
                subName = intent.getStringExtra("subName");
                ayrYear = intent.getIntExtra("ayrYear",0);
            }

            try {
                loadSubjectAnalyticsData();
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Inflate the layout for this fragment
            return view;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  null;
    }


    public void loadSubjectAnalyticsData() {
        new SubjectAnalyticsGraphFragment.loadSubjectAnalyticsData().execute();
    }

    private  class loadSubjectAnalyticsData extends AsyncTask<String,String,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try{
                if(response != null)
                {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    subjectInfoArrayList = new ArrayList<>();
                    if(status.equals("SUCCESS"))
                    {
                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray dataArray = object2.getJSONArray("data");

                        for(int i=0;i<dataArray.length();i++)
                        {
                            SubjectInfo subjectInfo = new SubjectInfo();
                            JSONObject object = dataArray.getJSONObject(i);
                            subjectInfo.setMarks(object.getString("marks"));
                            subjectInfo.setColor(object.getString("color"));
                            subjectInfo.setSubId(object.getInt("subId"));
                            subjectInfo.setSubName(object.getString("subName"));
                            subjectInfo.setStdId(object.getInt("stdId"));
                            subjectInfo.setDivId(object.getInt("divId"));
                            subjectInfo.setStdName(object.getString("stdName"));
                            subjectInfo.setDivName(object.getString("divName"));


                            JSONArray array1 = object.getJSONArray("subjectInfo");
                            studentInfoArrayList = new ArrayList<>();
                            for (int j = 0; j < array1.length(); j++) {
                                JSONObject object3 = array1.getJSONObject(j);
                                StudentInfo studentInfo = new StudentInfo();
                                studentInfo.setSubName(object3.getString("subName"));
                                studentInfo.setMarks(object3.getString("marks"));
                                studentInfo.setColor(object3.getString("color"));
                                studentInfo.setSubId(object3.getInt("subId"));
                                studentInfo.setStdName(object3.getString("stdName"));
                                studentInfo.setStdId(object3.getInt("stdId"));
                                studentInfo.setDivName(object3.getString("divName"));
                                studentInfo.setDivId(object3.getInt("divId"));
                                studentInfo.setStuName(object3.getString("stuName"));
                                studentInfo.setStuId(object3.getInt("stuId"));
                                studentInfoArrayList.add(studentInfo);
                                subjectInfo.setStudentInfoArrayList(studentInfoArrayList);



                            }


                            //store data
                            SharedPreferences.Editor editor =getActivity(). getSharedPreferences("subAnalyticsData", context.MODE_PRIVATE).edit();
                            editor.putString("response", response);
                            editor.apply();

                            if(subName.equals(object.getString("subName"))) {

                                subjectInfoArrayList.add(subjectInfo);


                                for (int m = 0; m < studentInfoArrayList.size(); m++) {

                                    if (studentInfoArrayList.get(m).getColor().equals("#43A047")) {
                                        countGreen++;
                                    } else if (studentInfoArrayList.get(m).getColor().equals("#FB8C00")) {
                                        countOrange++;
                                    } else {
                                        countRed++;
                                    }
                                }

                                ArrayList<Integer> countList =  new ArrayList<>();
                                countList.add(countGreen);
                                countList.add(countOrange);
                                countList.add(countRed);

                                ArrayList<String> arrayList = new ArrayList<>();
                                arrayList.add("Above Avg.");
                                arrayList.add("Avg.");
                                arrayList.add("Below Avg.");


                                ArrayList<BarEntry> valueSet1 = new ArrayList<>();

                                ;for (int j = 0; j < countList.size(); j++) {
                                    valueSet1.add(new BarEntry((float) countList.get(j), j));
                                }

                                BarDataSet set = new BarDataSet(valueSet1, "Acadmic Performance");
                                BarData barData = new BarData(arrayList,set);

                                set.setColors(new int[] {getResources().getColor(R.color.green),getResources().getColor(R.color.orange),getResources().getColor(R.color.red)});
                                barChart.setData(barData);
                                barChart.invalidate();

                            }
                        }

                    }else {
                      //  Toast.makeText(context, "No data available for academic performace!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No data available for academic performace!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }


                }else {
                  //  Toast.makeText(context, "No data available for academic performance!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No data available for academic performace!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try{
                response = util.getClassAnalytics(orgId, insId, dscId, ayrYear, stdId, divId, subId, examTypes);

            }catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static Fragment newInstance(int i, String s) {
        SubjectAnalyticsGraphFragment fragmentFirst = new SubjectAnalyticsGraphFragment();
        return fragmentFirst;
    }
}

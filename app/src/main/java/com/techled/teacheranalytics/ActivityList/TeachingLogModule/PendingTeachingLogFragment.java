package com.techled.teacheranalytics.ActivityList.TeachingLogModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.PendingTeachingLogsRecyAdapter;
import com.techled.teacheranalytics.pojo.TeachingLogs;

import java.util.ArrayList;

public class PendingTeachingLogFragment extends Fragment {

    private ProgressDialog progDailog;
    private Context context;
    private RecyclerView teachingLogrecyView;
    private ArrayList<TeachingLogs> teachingLogsArrayList;
    private String selectedDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_pending_teaching_log, container, false);

        context = getActivity();
        teachingLogrecyView = (RecyclerView)view.findViewById(R.id.teachingLogrecyView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        teachingLogrecyView.setLayoutManager(layoutManager);


        //load teaching logs

        /*LoadPendingTeachingLogs loadPendingTeachingLogs = new LoadPendingTeachingLogs();
        loadPendingTeachingLogs.execute(null,null);*/

        teachingLogsArrayList = new ArrayList<>();
        TeachingLogs logs = new TeachingLogs();
        logs.setEvent("Lecture 123");
        logs.setLogId(1);
        logs.setStdDiv("5- Girls");
        logs.setSubject("Marathi");
        logs.setTime("8:30");
        teachingLogsArrayList.add(logs);
        PendingTeachingLogsRecyAdapter adapter = new PendingTeachingLogsRecyAdapter(teachingLogsArrayList,context);
        teachingLogrecyView.setAdapter(adapter);

        return view;
    }

    public class LoadPendingTeachingLogs extends AsyncTask<String,String,String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try
            {
                progDailog.dismiss();

                teachingLogsArrayList = new ArrayList<>();
                TeachingLogs logs = new TeachingLogs();
                logs.setEvent("Lecture 123");
                logs.setLogId(1);
                logs.setStdDiv("5- Girls");
                logs.setSubject("Marathi");
                logs.setTime("8:30");
                teachingLogsArrayList.add(logs);
                PendingTeachingLogsRecyAdapter adapter = new PendingTeachingLogsRecyAdapter(teachingLogsArrayList,context);
                teachingLogrecyView.setAdapter(adapter);

            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try
            {


            }catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }
    }

}

package com.techled.teacheranalytics.ActivityList.TimetableModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadHomeworkActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadLevelSubjectHomeworkActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.ViewHomeworkActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.StdDivDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ViewTimeTableActivity extends AppCompatActivity {
    private ImageView btnRefresh;
    private Context context;
    private Util util;
    private JSONArray classTeacherOfJson;
    private int orgId, insId, ayr, dscId, usrId, ugpId, usrAssetId;
    private String params, resUserInfo;
    private ArrayList<StdDivDetails> standardDivisionDataArrayList;
    private ArrayList<String> monthsDataArrayList, weeksArrayList, timeTableArrayList;
    private Spinner spinnStdDiv, spinnMonth, spinnWeek, spinnTimetable;
    private int stdId, divId;
    private ProgressDialog progDailog;
    private String response;
    private TextView txtUrl;
    private Button btnView, btnViewStaff;
    private String resTimetableUrl, url, selectedWeek;
    private LinearLayout layParent;
    private LinearLayout classWiseTimeTableLayout, myTimeTableLayout;
    private int currentYear;
    private String currentDate;

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_time_table);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Timetable");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            //
            spinnStdDiv = (Spinner) findViewById(R.id.spinnStdDiv);
            spinnMonth = findViewById(R.id.spinnMonth);
            spinnWeek = findViewById(R.id.spinnWeek);
            txtUrl = (TextView) findViewById(R.id.txtUrl);
            btnView = (Button) findViewById(R.id.btnView);
            btnViewStaff = (Button) findViewById(R.id.btnViewStaff);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            spinnTimetable = findViewById(R.id.spinnTimetable);
            classWiseTimeTableLayout = findViewById(R.id.classWiseTimeTableLayout);
            myTimeTableLayout = findViewById(R.id.myTimeTableLayout);

            currentYear = Calendar.getInstance().get(Calendar.YEAR);
            currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");

                usrAssetId = object.getInt("assetId1");

            }

            monthsDataArrayList = new ArrayList<>();
            weeksArrayList = new ArrayList<>();
            monthsDataArrayList.add("January");
            monthsDataArrayList.add("February");
            monthsDataArrayList.add("March");
            monthsDataArrayList.add("April");
            monthsDataArrayList.add("May");
            monthsDataArrayList.add("June");
            monthsDataArrayList.add("July");
            monthsDataArrayList.add("August");
            monthsDataArrayList.add("September");
            monthsDataArrayList.add("October");
            monthsDataArrayList.add("November");
            monthsDataArrayList.add("December");

            //set adapter to the month and week
            final ArrayAdapter<String> weekAdaptor = new ArrayAdapter<>(ViewTimeTableActivity.this,
                    android.R.layout.simple_spinner_item, weeksArrayList);
            weekAdaptor
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnWeek.setAdapter(weekAdaptor);
            spinnWeek.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedWeek = weeksArrayList.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            ArrayAdapter<String> orgAdaptor = new ArrayAdapter<>(ViewTimeTableActivity.this,
                    android.R.layout.simple_spinner_item, monthsDataArrayList);
            orgAdaptor
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnMonth.setAdapter(orgAdaptor);
            spinnMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    List<List<String>> weeks = getNumberOfWeeks(currentYear, position);
                    weeksArrayList.clear();
                    for(int i=0; i<weeks.size(); i++){
                        weeksArrayList.add(weeks.get(i).get(0) + " to " + weeks.get(i).get(1));
                    }
                    weekAdaptor.notifyDataSetChanged();

                    Calendar calendar = Calendar.getInstance();
                    calendar.setFirstDayOfWeek(Calendar.MONDAY);
                    if(calendar.get(Calendar.MONTH) == position) {
                        spinnWeek.setSelection(calendar.get(Calendar.WEEK_OF_MONTH) - 1);
                    } else {
                        spinnWeek.setSelection(0);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            //time table spinner
            timeTableArrayList = new ArrayList<>();
            timeTableArrayList.add("Class based");
            timeTableArrayList.add("My Lectures");

            spinnMonth.setSelection(Calendar.getInstance().get(Calendar.MONTH));

            ArrayAdapter<String> timeTableAdaptor = new ArrayAdapter<>(ViewTimeTableActivity.this,
                    android.R.layout.simple_spinner_item, timeTableArrayList);
            timeTableAdaptor
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnTimetable.setAdapter(timeTableAdaptor);
            spinnTimetable.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if(position == 0){
                        classWiseTimeTableLayout.setVisibility(View.VISIBLE);
                        myTimeTableLayout.setVisibility(View.GONE);
                    }else {
                        classWiseTimeTableLayout.setVisibility(View.GONE);
                        myTimeTableLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

        //load stadard division
            ViewTimeTableActivity.LoadStandardDivision loadStandardDivision = new ViewTimeTableActivity.LoadStandardDivision();
            loadStandardDivision.execute(null, null);

            //view the timetable
            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (stdId != 0 || divId != 0) {

                        //open attachment
                        if (url == null || url.equals("") || url.equals("null")) {
                            // Toast.makeText(context, "Timetable not assigned!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Timetable not assigned!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            context.startActivity(intent);
                        }
                    } else {
                        // Toast.makeText(context, "Please select standard division to view timetable!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Please select standard division to view timetable!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }

                }
            });

            //view the timetable
            btnViewStaff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!selectedWeek.equals("")) {

                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.dialog_view_edit_lecture);
                        dialog.setCancelable(true);
                        dialog.show();
                        Window window = dialog.getWindow();
                        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                        window.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_background_3));

                        ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
                        TextView view = dialog.findViewById(R.id.view);
                        TextView edit = dialog.findViewById(R.id.edit);

                        closeDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String[] weekStartEnd = selectedWeek.split(" to ");
                                String weekStart = weekStartEnd[0];
                                String weekEnd = weekStartEnd[1];

                                Intent intent = new Intent(ViewTimeTableActivity.this, ViewStaffTimeTableActivity.class);
                                intent.putExtra("weekStart",weekStart);
                                intent.putExtra("weekEnd",weekEnd);
                                startActivity(intent);
                            }
                        });

                        edit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String[] weekStartEnd = selectedWeek.split(" to ");
                                String weekStart = weekStartEnd[0];
                                String weekEnd = weekStartEnd[1];

                                dialog.dismiss();
                                Intent intent = new Intent(ViewTimeTableActivity.this, MeetingForLecturesActivity.class);
                                intent.putExtra("weekStart",weekStart);
                                intent.putExtra("weekEnd",weekEnd);
                                startActivity(intent);
                            }
                        });

                    } else {
                        // Toast.makeText(context, "Please select standard division to view timetable!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Please select the appropriate week!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private class LoadStandardDivision extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {

                    JSONObject object = new JSONObject(response);
                    JSONArray array = object.getJSONArray("stdDivList");
                    standardDivisionDataArrayList = new ArrayList<>();

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        StdDivDetails data = new StdDivDetails();

                        data.setStdId(jsonObject.getInt("stdId"));
                        data.setDivId(jsonObject.getInt("divId"));
                        data.setStdName(jsonObject.getString("stdName"));
                        data.setDivName(jsonObject.getString("divName"));

                        standardDivisionDataArrayList.add(data);
                    }

                    //set adapter
                    ArrayAdapter<StdDivDetails> orgAdaptor = new ArrayAdapter<StdDivDetails>(ViewTimeTableActivity.this,
                            android.R.layout.simple_spinner_item, standardDivisionDataArrayList);
                    orgAdaptor
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnStdDiv.setAdapter(orgAdaptor);
                    spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            stdId = standardDivisionDataArrayList.get(position).getStdId();
                            divId = standardDivisionDataArrayList.get(position).getDivId();

                            if (stdId != 0 || divId != 0) {
                                ViewTimeTableActivity.LoadTimetableUrl loadTimetableUrl = new ViewTimeTableActivity.LoadTimetableUrl();
                                loadTimetableUrl.execute(null, null);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else {
                    //  Toast.makeText(context, "Unable to load Standard-Division, try Again later!", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(layParent, "Unable to load Standard-Division, try Again later!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;

                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading std div","Teacher Connect",usrId,"Timetable");
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = util.getStandardDivisionJSON(orgId, insId, dscId, ayr, 0);


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading std div","Teacher Connect",usrId,"Timetable");
            }
            return null;
        }
    }

    public class LoadTimetableUrl extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (resTimetableUrl != null) {

                    JSONArray jsonArray = new JSONArray(resTimetableUrl);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");
                        JSONObject object = array.getJSONObject(0);
                        url = object.getString("url");
                        if (url == null || url.equals("null") || url.equals("")) {
                            //  Toast.makeText(context, "Timetable not assigned!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Timetable not assigned!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            txtUrl.setText("Timetable  not assigned!");
                            btnView.setVisibility(View.GONE);
                            return;
                        } else {
                            btnView.setVisibility(View.VISIBLE);
                            txtUrl.setVisibility(View.GONE);

                        }

                    } else {
                        //  Toast.makeText(context, "Timetable not assigned!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Timetable not assigned!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }

                } else {
                    // Toast.makeText(context, "Timetable not assigned!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Timetable not assigned!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading timetable","Teacher Connect",usrId,"Timetable");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resTimetableUrl = util.getTimetableUrl(orgId, insId, dscId, ayr, stdId, divId);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading timetable","Teacher Connect",usrId,"Timetable");
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (progDailog != null) {
                progDailog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (progDailog != null) {
                progDailog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (progDailog != null) {
                progDailog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    List<List<String>> getNumberOfWeeks(int year, int month) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        List<List<String>> weekdates = new ArrayList<List<String>>();
        List<String> dates;
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, 1);
        int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        while (c.get(Calendar.MONTH) == month) {
            dates = new ArrayList<String>();
            //new method
//            dates.add(format.format(c.getTime()));
//            c.add(Calendar.DAY_OF_MONTH, Math.min(lastDay - c.get(Calendar.DAY_OF_MONTH), 6));
//            dates.add(format.format(c.getTime()));
//            weekdates.add(dates);
//            c.add(Calendar.DAY_OF_MONTH, 1);

            //old method
            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
                c.add(Calendar.DAY_OF_MONTH, -1);
            }
            dates.add(format.format(c.getTime()));
            c.add(Calendar.DAY_OF_MONTH, 6);
            dates.add(format.format(c.getTime()));
            weekdates.add(dates);
            c.add(Calendar.DAY_OF_MONTH, 1);
        }
        System.out.println(weekdates);
        return weekdates;
    }

//    List<List<String>> getNumberOfWeeks(int year, int month) {
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        List<List<String>> weekdates = new ArrayList<List<String>>();
//        List<String> dates;
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.YEAR, year);
//        c.set(Calendar.MONTH, month);
//        c.set(Calendar.DAY_OF_MONTH, 1);
//        int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
//        while (c.get(Calendar.MONTH) == month) {
//            dates = new ArrayList<String>();
////            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
////                c.add(Calendar.DAY_OF_MONTH, -1);
////            }
//
//            dates.add(format.format(c.getTime()));
//
//            int currentDay = c.get(Calendar.DAY_OF_WEEK);
//            int daysToBeAddForWeekend = Calendar.SATURDAY - currentDay + 1;
//            int daysRemainingForMonthEnd = lastDay - c.get(Calendar.DAY_OF_MONTH);
//
//            if (c.get(Calendar.DAY_OF_MONTH) + daysToBeAddForWeekend <= lastDay) {
//
//                c.add(Calendar.DAY_OF_MONTH, daysToBeAddForWeekend);
//            } else {
//
//                c.add(Calendar.DAY_OF_MONTH, daysRemainingForMonthEnd);
//            }
//
////            if (lastDay - c.get(Calendar.DAY_OF_MONTH) >= 6) {
////                c.add(Calendar.DAY_OF_MONTH, 6);
////            } else {
////                c.add(Calendar.DAY_OF_MONTH, lastDay - c.get(Calendar.DAY_OF_MONTH));
////            }
//            dates.add(format.format(c.getTime()));
//            weekdates.add(dates);
//            c.add(Calendar.DAY_OF_MONTH, 1);
//        }
//        System.out.println(weekdates);
//        return weekdates;
//    }
}

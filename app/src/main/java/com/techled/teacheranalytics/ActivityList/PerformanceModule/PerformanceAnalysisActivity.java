package com.techled.teacheranalytics.ActivityList.PerformanceModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule.AttendanceActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.AcadmicPerformanceRecyAdapter;
import com.techled.teacheranalytics.pojo.AcadmicData;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.SubjectInfo;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PerformanceAnalysisActivity extends AppCompatActivity {

    private RecyclerView acdPerRecyView;

    private Util util;
    private Context context;
    private JSONArray classTeacherOfJson;
    private String params, uName, notType, resUserInfo;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId, stdId, divId;
    private ArrayList<StandardDivision> standardDivisionArrayList;
    private Dialog dialog;
    private String response;
    private ProgressDialog progDailog;
    private LinearLayout layParent,layNoRecord;
    private ArrayList<AcadmicData> acadmicDataArrayList;
    private int subId=0;
    private String examTypes = "";
    private TextView txtColMark1,txtColMark2,txtColMark3;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_analysis);

        try {

            androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Performance Analysis");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            }

            context = this;
            util = new Util();

            acdPerRecyView = (RecyclerView) findViewById(R.id.acdPerRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            acdPerRecyView.setLayoutManager(layoutManager);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            //set layout
            txtColMark1 = (TextView) findViewById(R.id.txtColMark1);
            txtColMark2 = (TextView) findViewById(R.id.txtColMark2);
            txtColMark3 = (TextView) findViewById(R.id.txtColMark3);

            txtColMark1.setText(">=80%");
            txtColMark2.setText(">60%");
            txtColMark3.setText("<=60%");

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                uName = object.getString("usrName");
                usrAssetId = object.getInt("assetId1");

            }

            //get data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("subTeacherOf");

                }
            }

            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
//                Toast.makeText(this,
//                        "Unable to load standard/division!",
//                        Toast.LENGTH_LONG).show();

                Snackbar snackbar = Snackbar
                        .make(layParent, "Unable to load standard/division!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();


            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                String stdDiv = object.getString("value");
                StandardDivision standardDivision = new StandardDivision();
                standardDivision.setStdName(object.getString("label"));
                String stdDivName = object.getString("label");
                if (stdDivName.contains("-")) {
                    String splStr[] = stdDivName.split("-");
                    standardDivision.setStdName(splStr[0]);
                    standardDivision.setDivName(splStr[1]);
                } else {
                    standardDivision.setStdName(stdDivName);
                    standardDivision.setDivName(" ");
                }

                String stdDivIds = object.getString("value");
                String[] splStrs = stdDivIds.split("-");
                standardDivision.setStdId(Integer.parseInt(splStrs[0]));
                standardDivision.setDivId(Integer.parseInt(splStrs[1]));
                standardDivisionArrayList.add(standardDivision);

            }


            dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_load_std_div_for_markattendance);
            dialog.show();
            Window window = dialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);

            Spinner spinnStdDiv = (Spinner) dialog.findViewById(R.id.spinnStdDiv);
            ArrayAdapter<StandardDivision> stdDivAdaptor = new ArrayAdapter<StandardDivision>(PerformanceAnalysisActivity.this,
                    android.R.layout.simple_spinner_item, standardDivisionArrayList);
            stdDivAdaptor
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnStdDiv.setAdapter(stdDivAdaptor);

            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    stdId = standardDivisionArrayList.get(position).getStdId();
                    divId = standardDivisionArrayList.get(position).getDivId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (stdId != 0 || divId != 0) {
                        try {
                            dialog.dismiss();

                            PerformanceAnalysisActivity.LoadAcadmicPerfData loadAcadmicPerfData = new PerformanceAnalysisActivity.LoadAcadmicPerfData();
                            loadAcadmicPerfData.execute(null,null);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                       // Toast.makeText(context, "Please select standard/division!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Please select standard/division!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                }
            });

            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    onBackPressed();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    private class LoadAcadmicPerfData extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getAcadmicPerformance(orgId, insId, dscId, ayr, stdId, divId, subId, examTypes);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (response != null) {

                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    JSONObject object2 = jsonArray.getJSONObject(1);

                    String status = object1.getString("status");
                    acadmicDataArrayList = new ArrayList<>();

                    if (status.equals("SUCCESS")) {
                        JSONArray array = object2.getJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            AcadmicData acadmicData = new AcadmicData();
                            acadmicData.setMarks(object.getString("marks"));
                            acadmicData.setColor(object.getString("color"));
                            acadmicData.setSubId(object.getInt("subId"));
                            acadmicData.setSubName(object.getString("subName"));
                            acadmicData.setStdId(object.getInt("stdId"));
                            acadmicData.setDivId(object.getInt("divId"));
                            acadmicData.setStdName(object.getString("stdName"));
                            acadmicData.setDivName(object.getString("divName"));

                            JSONArray array1 = object.getJSONArray("subjectInfo");
                            List<SubjectInfo> subjectInfoArrayList = new ArrayList<>();
                            for (int j = 0; j < array1.length(); j++) {
                                JSONObject object3 = array1.getJSONObject(j);
                                SubjectInfo subjectInfo = new SubjectInfo();
                                subjectInfo.setSubName(object3.getString("subName"));
                                subjectInfo.setMarks(object3.getString("marks"));
                                subjectInfo.setColor(object3.getString("color"));
                                subjectInfo.setSubId(object3.getInt("subId"));
                                subjectInfo.setStdName(object3.getString("stdName"));
                                subjectInfo.setStdId(object3.getInt("stdId"));
                                subjectInfo.setDivName(object3.getString("divName"));
                                subjectInfo.setDivId(object3.getInt("divId"));
                                if (object3.getString("marks") == null || object3.getString("marks").equals("0.0")) {

                                } else {
                                    subjectInfoArrayList.add(subjectInfo);
                                    acadmicData.setSubjectInfoArrayList(subjectInfoArrayList);
                                }
                            }

                            acadmicDataArrayList.add(acadmicData);

                        }

                        //store data
                        SharedPreferences.Editor editor = getSharedPreferences("acadmicPerformance", MODE_PRIVATE).edit();
                        editor.putString("response", response);
                        editor.apply();

                        //set adapter
                        layParent.setVisibility(View.VISIBLE);
                        layNoRecord.setVisibility(View.GONE);

                        AcadmicPerformanceRecyAdapter adapter = new AcadmicPerformanceRecyAdapter(context,ayr,acadmicDataArrayList);
                        acdPerRecyView.setAdapter(adapter);




                    } else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        //Toast.makeText(context, "No data available for academic performance!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No data available for academic performance!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }

                } else {

                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                 //   Toast.makeText(context, "No data available for academic performance!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No data available for academic performance!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }
            } catch (Exception e) {

                layParent.setVisibility(View.GONE);
                layNoRecord.setVisibility(View.VISIBLE);
              //  Toast.makeText(context, "Unable to load academic performance data!", Toast.LENGTH_SHORT).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "No data available for academic performance!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                e.printStackTrace();
            }
        }
    }
}

package com.techled.teacheranalytics.ActivityList.PerformanceModule;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;

import me.relex.circleindicator.CircleIndicator;

public class MainPagerScreenActivity extends AppCompatActivity {
    FragmentPagerAdapter adapterViewPager;
    private ViewPager vpPager;
    private ImageView btnRefresh;
    private Context context;
    private LinearLayout layParent;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_pager_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Academic Performance");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        //get reference
        context = this;
        layParent = (LinearLayout)findViewById(R.id.layParent);
        vpPager = (ViewPager) findViewById(R.id.vpPager);
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setCurrentItem(0);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(vpPager);

        //check internet connection
        CheckInternet checkInternet = new CheckInternet();
        boolean checkConnection = checkInternet.checkConnection(context);
        if (checkConnection) {

        } else {

            Snackbar snackbar = Snackbar
                    .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
            return;
        }

        //check poor internet connection
        CheckInternet checkPoorInternet = new CheckInternet();
        boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
        if (checkPoorConnection) {
            Snackbar snackbar = Snackbar
                    .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
        }



    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0: // Fragment # 0 - This will show FirstFragment different title
                    return SubjectAnalyticsFragment.newInstance(0, "Subject Analytics");
                case 1: // Fragment # 1 - This will show SecondFragment
                    return SubjectAnalyticsGraphFragment.newInstance(1, "Subject Analytics Graph");
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment different title
                    return "Subject Analytics";
                case 1: // Fragment # 1 - This will show SecondFragment
                    return "Subject Analytics Graph";
            }
            return null;
        }

    }

}

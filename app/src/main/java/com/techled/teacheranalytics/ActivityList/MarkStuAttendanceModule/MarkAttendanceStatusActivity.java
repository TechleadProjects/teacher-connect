package com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.BookingDecorator;
import com.techled.teacheranalytics.pojo.BookingDecorator2;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

public class MarkAttendanceStatusActivity extends AppCompatActivity {
    private Util util;
    private Context context;
    private String params, resUserInfo, resMarkAttd;
    private MaterialCalendarView calView;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId, stdId, divId;
    private ProgressDialog progDailog;
    private ArrayList<Date> dates;
    private HashSet<Date> workingDaysSet, datesSetForNotMarkedAttd, attdMarkedSet;
    private HashMap<Date, String> hashMap;
    private Date stringDate, compareDate;
    private int lastDay, mMonth, mYear;
    private Date gDate;
    private LinearLayout layNoRecord, layParent;
    private Button btnMarkAttd;
    private String selectedDate = "";


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_attendance_status);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Attendance Marked Status");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference

            context = this;
            util = new Util();


            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");

            }

            //get data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    JSONArray classTeacherOfJson = jsonObject.getJSONArray("classTeacherOf");


                    if (classTeacherOfJson == null || classTeacherOfJson.equals("") || classTeacherOfJson.length() == 0) {

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Error to load standard/division!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }

                    JSONObject object = classTeacherOfJson.getJSONObject(0);
                    String stdDivIds = object.getString("value");
                    String[] splStrs = stdDivIds.split("-");

                    stdId = Integer.parseInt(splStrs[0]);
                    divId = Integer.parseInt(splStrs[1]);


                }
            }

            dates = new ArrayList<Date>();
            hashMap = new HashMap();
            calView = (MaterialCalendarView) findViewById(R.id.calView);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //load mark attendance
            MarkAttendanceStatusActivity.LoadMarkedAttdDetails loadMarkedAttdDetails = new MarkAttendanceStatusActivity.LoadMarkedAttdDetails();
            loadMarkedAttdDetails.execute(null, null);

            btnMarkAttd = (Button) findViewById(R.id.btnMarkAttd);
            btnMarkAttd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (compareDate == null) {
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select date for attendance marking!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }
                      /*  if (!datesSetForMarkedAttd.contains(compareDate) && !attdMarkedSet.contains(compareDate)) {

                            Calendar c = Calendar.getInstance();   // this takes current date
                            c.set(Calendar.DAY_OF_MONTH, 1);
                            System.out.println(c.getTime());
                            Date firstDateOfMonth = c.getTime();
                            if (compareDate.before(firstDateOfMonth)) {
                                Intent intent = new Intent(MarkAttendanceStatusActivity.this, AttendanceActivity.class);
                                intent.putExtra("selectedDate", selectedDate);
                                startActivity(intent);
                                finish();
                                return;
                            } else {
                                //  Toast.makeText(context, selectedDate + " is not working day or its future date!", Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(layParent, selectedDate + " is not working day or its future date!", Snackbar.LENGTH_LONG);
                                snackbar.setDuration(3000);
                                snackbar.show();
                                return;
                            }
                        }*/
                       /* SimpleDateFormat spf = new SimpleDateFormat("dd-MMM-yyyy");
                        Date newDate = spf.parse(selectedDate);
                        spf = new SimpleDateFormat("dd-MM-yyyy");
                        String date = spf.format(newDate);
                        if (!workingDaysSet.contains(date)) {
                            Snackbar snackbar = Snackbar
                                    .make(layParent, selectedDate + " is not working day or its future date!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }*/


                        Calendar c = Calendar.getInstance();   // this takes current date
                        Date currentDate = c.getTime();
                        if (compareDate.before(currentDate)) {
                            Intent intent = new Intent(MarkAttendanceStatusActivity.this, AttendanceActivity.class);
                            intent.putExtra("selectedDate", selectedDate);
                            startActivity(intent);
                            finish();
                            return;
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(layParent, selectedDate + " is not working day or its future date!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class LoadMarkedAttdDetails extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {

                if (resMarkAttd == null || resMarkAttd.equals("")) {

                    // Toast.makeText(context, "Attendance status not found!", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(layParent, "Attendance status not found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    util.SaveSystemLog(orgId, insId, "Attendance status not found", "Teacher Connect", usrId, "Mark Attendance");
                    return;


                } else {
                    JSONArray jsonArray = new JSONArray(resMarkAttd);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {

                        JSONArray array = object1.getJSONArray("workingDays");

                      /*  for (int j = 1; j <= lastDay; j++) {
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                final String date1 = object.getString("workingDay");
                                SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
                                stringDate = simpledateformat.parse(date1);

                                //  datesSetForMarkedAttd.add(stringDate);
                                dates.add(stringDate);
                                hashMap.put(stringDate, "");

                                String genDate = j + "-" + mMonth + "-" + mYear;
                                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                gDate = format.parse(genDate);

                                if (stringDate.compareTo(gDate) == 0) {
                                    datesSetForMarkedAttd.add(stringDate);
                                    Log.d("Mark", "" + stringDate + "-" + gDate);
                                } else {
                                    datesSetForNotMarkedAttd.add(gDate);
                                    Log.d("NotMark", "" + stringDate + "-" + gDate);
                                }


                            }


                        }
*/
                        JSONArray array1 = object1.getJSONArray("data");
                        attdMarkedSet = new HashSet<>();
                        datesSetForNotMarkedAttd = new HashSet<>();
                        workingDaysSet = new HashSet<>();
                        for (int m = 0; m < array1.length(); m++) {
                            JSONObject object = array1.getJSONObject(m);
                            String markDate = object.getString("date");
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                            Date d1 = format.parse(markDate);

                            attdMarkedSet.add(d1);
                        }

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            final String date1 = object.getString("workingDay");
                            SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
                            stringDate = simpledateformat.parse(date1);
                            workingDaysSet.add(stringDate);

                            if (attdMarkedSet.contains(stringDate)) {
                                // datesSetForMarkedAttd.add(stringDate);
                            } else {
                                datesSetForNotMarkedAttd.add(stringDate);
                            }


                        }


                    } else {

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Attendance status not found!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);

                        util.SaveSystemLog(orgId, insId, "Attendance status not found", "Teacher Connect", usrId, "Mark Attendance");
                        return;
                    }


                    calView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, datesSetForNotMarkedAttd, context, "A"));
                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_green, attdMarkedSet, context, "P"));
                    Log.d("List", "" + datesSetForNotMarkedAttd.size() + datesSetForNotMarkedAttd.toString());
                    calView.state().edit()
                            .setCalendarDisplayMode(CalendarMode.MONTHS)
                            .commit();


                }


                calView.setOnDateChangedListener(new OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                        try {


                            selectedDate = date.toString();
                            int dd = date.getDay();
                            int mm = date.getMonth() + 1;
                            int yyyy = date.getYear();

                            selectedDate = dd + "-" + mm + "-" + yyyy;

                            DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                            DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy");
                            compareDate = originalFormat.parse(selectedDate);
                            selectedDate = targetFormat.format(compareDate);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


            } catch (Exception e) {

                e.printStackTrace();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Attendance status not found!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                layNoRecord.setVisibility(View.VISIBLE);
                layParent.setVisibility(View.GONE);
                util.SaveSystemLog(orgId, insId, "Error occur while loading attendance history in graph view", "Teacher Connect", usrId, "Mark Attendance");
                return;
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resMarkAttd = util.fetchAttdMarkedDaysV1(orgId, insId, dscId, stdId, divId, ayr);

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading attendance history in graph view", "Teacher Connect", usrId, "Mark Attendance");
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

}

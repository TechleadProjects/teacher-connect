package com.techled.teacheranalytics.ActivityList.UploadHomeworkModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.HomeworkDetailsRecyclerAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.HomeworkDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewHomeworkActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private RecyclerView hwkRecyView;
    private FloatingActionButton btnUploadHwk;
    private ProgressDialog progDailog;
    private int orgId, dscId, insId, ayr, stfId;
    private String params, resViewHwk;
    private LinearLayout layParent, layNoRecord;
    private ArrayList<HomeworkDetails> homeworkDetailsArrayList;
    private int stdId,divId;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_homework);

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("View Homework");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            hwkRecyView = (RecyclerView) findViewById(R.id.hwkRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            hwkRecyView.setLayoutManager(layoutManager);
            btnUploadHwk = (FloatingActionButton) findViewById(R.id.btnUploadHwk);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //login details
            try {
                SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);
                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);

                    orgId = object.getInt("orgId");
                    insId = object.getInt("insId");
                    ayr = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");
                    stfId = object.getInt("assetId1");

                }


                btnUploadHwk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            final Dialog dialog = new Dialog(context);
                            dialog.setContentView(R.layout.dialog_choose_homework_upload);
                            dialog.setCancelable(true);
                            dialog.show();
                            Window window = dialog.getWindow();
                            window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            window.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button_background_3));

                            ImageView closeDialog = dialog.findViewById(R.id.closeDialog);
                            TextView level = dialog.findViewById(R.id.level);
                            TextView stdDiv = dialog.findViewById(R.id.stdDiv);

                            closeDialog.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });

                            level.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    dialog.dismiss();
                                    Intent intent = new Intent(ViewHomeworkActivity.this, UploadLevelSubjectHomeworkActivity.class);
                                    startActivity(intent);
                                }
                            });

                            stdDiv.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    dialog.dismiss();
                                    Intent intent = new Intent(ViewHomeworkActivity.this, UploadHomeworkActivity.class);
                                    startActivity(intent);
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                //load homework
                ViewHomeworkActivity.LoadViewHomework loadViewHomework = new ViewHomeworkActivity.LoadViewHomework();
                loadViewHomework.execute(null,null);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class LoadViewHomework extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
//            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resViewHwk != null) {

                    JSONArray jsonArray = new JSONArray(resViewHwk);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    homeworkDetailsArrayList = new ArrayList<>();
                    if (status.equals("SUCCESS")) {

                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);
                            HomeworkDetails homeworkDetails = new HomeworkDetails();
                            homeworkDetails.setStdName(object.getString("stdName"));
                            homeworkDetails.setHwkTitle(object.getString("hwkTitle"));
                            homeworkDetails.setHwkDueDate(object.getString("hwkDueDate"));
                            homeworkDetails.setHwkSubject(object.getString("hwkSubject"));
                            homeworkDetails.setHwkURL(object.getString("hwkURL"));
                            homeworkDetails.setSubType(object.getString("hwkSubType"));
                            homeworkDetails.setHwkAssDate(object.getString("hwkAssignedDate"));

                            //new
                            homeworkDetails.setStdId(object.getInt("stdId"));
                            homeworkDetails.setDivId(object.getInt("divId"));
                            homeworkDetails.setHwkId(object.getInt("hwkId"));
                            homeworkDetails.setAddToTimeline(object.getString("addedToTimeLineYn"));
                            homeworkDetails.setTilIndentifier(object.getString("tilIdentifier"));
                            homeworkDetailsArrayList.add(homeworkDetails);
                        }

                        if(homeworkDetailsArrayList.size() != 0)
                        {
                            //set adapter
                            layParent.setVisibility(View.VISIBLE);
                            layNoRecord.setVisibility(View.GONE);
                            HomeworkDetailsRecyclerAdapter adapter = new HomeworkDetailsRecyclerAdapter(homeworkDetailsArrayList,context,orgId,insId,dscId,stdId,divId,ayr);
                            hwkRecyView.setAdapter(adapter);

                        }

                    } else {
                      //  Toast.makeText(context, "No Homework Assigned!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No Homework Assigned!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                        return;
                    }

                } else {
                   // Toast.makeText(context, "No Homework Assigned!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No Homework Assigned!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);


                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading homework details","Teacher Connect",stfId,"Homework");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resViewHwk = util.getHomeworkList(orgId, insId, dscId, ayr, stfId);
                } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading homework details","Teacher Connect",stfId,"Homework");

            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(progDailog != null)
        {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(progDailog != null)
        {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(progDailog != null)
        {
            progDailog.dismiss();
        }
    }
}

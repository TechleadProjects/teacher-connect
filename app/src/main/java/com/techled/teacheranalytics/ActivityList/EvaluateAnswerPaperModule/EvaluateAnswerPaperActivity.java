package com.techled.teacheranalytics.ActivityList.EvaluateAnswerPaperModule;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.techled.teacheranalytics.R;

import java.io.File;

import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ImageEditorIntentBuilder;

public class EvaluateAnswerPaperActivity extends AppCompatActivity {

    private Context context;
    private ProgressDialog progDailog;

    private ImageView imageView;

    private final int PHOTO_EDITOR_REQUEST_CODE = 231;
    private String imagePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluate_answer_paper);

        context = this;

        imageView = findViewById(R.id.editedImage);

        try {

            Thread t1 = new Thread(() -> {

                runOnUiThread(() -> {
                    progDailog = new ProgressDialog(context);
                    progDailog.setMessage("Fetching file from server...");
                    progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progDailog.setCancelable(false);
                    progDailog.setIndeterminate(false);
                    progDailog.show();
                });

                try {

                    File file = Glide.with(context).asFile().load("https://farmai.s3.ap-south-1.amazonaws.com/Prasad2/Stuffs_PrasadPics_2020.06.26-10%3A15%3A03%3A260_0003.jpg").submit().get();
                    imagePath = file.getPath();

                    runOnUiThread(() -> {

                        if (progDailog != null) {
                            progDailog.dismiss();
                        }

                        try {

//                                    Intent intent = new ImageEditorIntentBuilder(context, imagePath, imagePath)
//                                            .withAddText() // Add the features you need
//                                            .withPaintFeature()
//                                            .withFilterFeature()
//                                            .withRotateFeature()
//                                            .withCropFeature()
//                                            .withBrightnessFeature()
//                                            .withSaturationFeature()
//                                            .withBeautyFeature()
//                                            .withStickerFeature()
//                                            .forcePortrait(true)  // Add this to force portrait mode (It's set to false by default)
//                                            .build();

                            Intent intent = new ImageEditorIntentBuilder(context, imagePath, imagePath)
                                    .withAddText() // Add the features you need
                                    .withPaintFeature()
                                    .withAddText()
                                    .withRotateFeature()
                                    .forcePortrait(true)  // Add this to force portrait mode (It's set to false by default)
                                    .build();

                            EditImageActivity.start(EvaluateAnswerPaperActivity.this, intent, PHOTO_EDITOR_REQUEST_CODE);

                        }catch (Exception e) {

                            e.printStackTrace();
                            Thread t2 = new Thread(() -> Glide.get(context).clearDiskCache());
                            t2.start();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Thread t2 = new Thread(() -> Glide.get(context).clearDiskCache());
                    t2.start();
                }
            });
            t1.start();

        } catch (Exception e) {
            Log.e("Demo App", e.getMessage());
            Thread t2 = new Thread(() -> Glide.get(context).clearDiskCache());
            t2.start();// This could throw if either `sourcePath` or `outputPath` is blank or Null
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PHOTO_EDITOR_REQUEST_CODE) { // same code you used while starting

            Uri uri = Uri.fromFile(new File(imagePath));
            imageView.setImageURI(uri);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread t1 = new Thread(() -> Glide.get(context).clearDiskCache());
        t1.start();
    }
}

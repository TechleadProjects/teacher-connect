package com.techled.teacheranalytics.ActivityList.MyInfoModule;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class PasswordChangeActivity extends AppCompatActivity {

    private TextView viewMyName;
    private EditText newPwd, confirmPwd, oldPwd;
    private Button btnSubmit;
    private CircleImageView viewMyImage;
    private Util util;
    private int usrId;
    private String imageString, uName;
    private Context context;
    private ImageView shOldPwd, shNewPwd, shCnfPwd;
    private ImageView btnRefresh;
    private LinearLayout layParent;

    private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Change Password");
            toolbar.setTitleTextColor(Color.WHITE);

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            util = new Util();
            context = this;

            //get intent data
            Intent intent = getIntent();
            if (intent != null) {
                usrId = intent.getIntExtra("usrId", 0);
                uName = intent.getStringExtra("name");
                imageString = intent.getStringExtra("image");
            }

            //get reference
            viewMyImage = (CircleImageView) findViewById(R.id.viewMyImage);
            newPwd = (EditText) findViewById(R.id.newPwd);
            confirmPwd = (EditText) findViewById(R.id.confirmPwd);
            btnSubmit = (Button) findViewById(R.id.btnSubmit);
            viewMyName = (TextView) findViewById(R.id.viewMyName);
            oldPwd = (EditText) findViewById(R.id.oldPwd);
            layParent = (LinearLayout) findViewById(R.id.layParent);


            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //user name
            viewMyName.setText("" + uName);

            //image
            byte[] byteData = null;
            try {
                if (imageString != null) {
                    String[] id = imageString.split(",");
                    byteData = new byte[id.length];
                    for (int i = 0; i < id.length; i++) {

                        byteData[i] = Byte.valueOf(id[i]);
                    }
                }
            } catch (Exception e) {
                byteData = null;
            }
            if (byteData != null) {
                ByteArrayInputStream bais = new ByteArrayInputStream(
                        byteData);
                Bitmap btMap = BitmapFactory.decodeStream(bais);

                Bitmap circleBitmap = Bitmap.createBitmap(btMap.getWidth(), btMap.getHeight(), Bitmap.Config.ARGB_8888);

                BitmapShader shader = new BitmapShader(btMap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                Paint paint = new Paint();
                paint.setShader(shader);

                Canvas c = new Canvas(circleBitmap);
                c.drawCircle(btMap.getWidth() / 2, btMap.getHeight() / 2, btMap.getWidth() / 2, paint);
                viewMyImage.setImageBitmap(btMap);


            } else {
                Bitmap noImage = BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.defaultuser);

                viewMyImage.setImageBitmap(noImage);

            }

            //change password
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        boolean flag = false;
                        String NewPwd = newPwd.getText().toString();
                        String ConfirmPwd = confirmPwd.getText().toString();
                        String OldPwd = oldPwd.getText().toString();

                        oldPwd.setError(null);
                        newPwd.setError(null);
                        confirmPwd.setError(null);

                        if (OldPwd.equals("")) {
                            setErrorToEditText(oldPwd, "Please enter old password feild ");
                            return;

                        }
                        if (NewPwd.equals("")) {
                            setErrorToEditText(newPwd, "Please enter new password feild ");
                            return;
                        }
                        if (ConfirmPwd.equals("")) {
                            setErrorToEditText(confirmPwd, "Please enter confirm password feild");
                        }
                        if (OldPwd.equals("") && ConfirmPwd.equals("") && NewPwd.equals("")) {
                            setErrorToEditText(oldPwd, "Please enter old password feild ");
                            setErrorToEditText(newPwd, "Please enter new password feild ");
                            setErrorToEditText(confirmPwd, "Please enter confirm password feild");
                            return;
                        }
                        if (!NewPwd.equals(ConfirmPwd)) {
                          //  Toast.makeText(context, "Password does not match the confirm password!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Password does not match the confirm password!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }
                        if (!flag) {

                            OldPwd = strToHex(OldPwd.getBytes());
                            NewPwd = strToHex(NewPwd.getBytes());
                            ConfirmPwd = strToHex(ConfirmPwd.getBytes());

                            //change pwd
                            String res = util.changePwd(usrId, OldPwd, NewPwd, ConfirmPwd);
                            try {
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String success = jsonObject.getString("status");
                                if (success.equals("SUCCESS")) {
                                  //  Toast.makeText(context, "Your password has been changed successfully!", Toast.LENGTH_SHORT).show();
                                    Snackbar snackbar = Snackbar
                                            .make(layParent, "Your password has been changed successfully!", Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(3000);
                                    snackbar.show();

                                    onBackPressed();
                                } else {
                                 //   Toast.makeText(context, "We're sorry. We were not able to change your password!", Toast.LENGTH_SHORT).show();
                                    Snackbar snackbar = Snackbar
                                            .make(layParent, "We're sorry. We were not able to change your password!", Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(3000);
                                    snackbar.show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Method to convert string to hexadecimal
    private String strToHex(byte[] buf) {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i) {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }

    protected void setErrorToEditText(EditText editText, String errorMesssage) {
        editText.setError(errorMesssage);
        editText.requestFocus();
    }
}

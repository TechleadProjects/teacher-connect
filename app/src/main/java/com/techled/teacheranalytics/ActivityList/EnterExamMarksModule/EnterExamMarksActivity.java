package com.techled.teacheranalytics.ActivityList.EnterExamMarksModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.AddRemarkModule.AddRemarkActivity;
import com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule.AttendanceActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadHomeworkActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.ExamMarksDeatilsRecyAdapter;
import com.techled.teacheranalytics.pojo.AttendanceManagementDetails;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.Exams;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.StuExamsMarksDetails;
import com.techled.teacheranalytics.pojo.SubjectDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class EnterExamMarksActivity extends AppCompatActivity {
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            if (scrollView.getVisibility() == View.VISIBLE) {

                finish();
            } else {

                scrollView.setVisibility(View.VISIBLE);
                layParent.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
            }
        }
        return false;
    }

    private Spinner spinnStdDiv, spinnSubject, spinnExam, spinnStudent;
    private Context context;
    private Util util;
    private String params, resUserInfo, insName, resSubject;
    private JSONArray classTeacherOfJson;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId, stdId, divId, subId, examId;
    private ArrayList<StandardDivision> standardDivisionArrayList;
    private Button btnOk, btnSave, btnEdit;
    private ProgressDialog progDailog;
    private ArrayList<SubjectDetails> subjectDetailsArrayList;
    private String resExams, resExamMarksDetails;
    private ArrayList<Exams> examsArrayList;
    private ArrayList<StuExamsMarksDetails> stuExamsMarksDetailsArrayList;
    private RecyclerView exmRecyView;
    private LinearLayout layParent;
    private ScrollView scrollView;
    private boolean editable = false;
    private ExamMarksDeatilsRecyAdapter deatilsRecyAdapter;
    private ArrayList<StuExamsMarksDetails> arrayList;
    private String resSaveExamMarks, stdName, divName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_exam_marks);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Enter Exam Marks");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference

            context = this;
            util = new Util();

            //get reference
            spinnStdDiv = (Spinner) findViewById(R.id.spinnStdDiv);
            spinnSubject = (Spinner) findViewById(R.id.spinnSubject);
            spinnExam = (Spinner) findViewById(R.id.spinnExam);

            btnOk = (Button) findViewById(R.id.btnOk);
            btnSave = (Button) findViewById(R.id.btnSave);
            btnEdit = (Button) findViewById(R.id.btnEdit);
            exmRecyView = (RecyclerView) findViewById(R.id.exmRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            exmRecyView.setLayoutManager(layoutManager);
            scrollView = (ScrollView) findViewById(R.id.scrollView);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");

            }

            //get user data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("subTeacherOf");
                    insName = jsonObject.getString("insName");
                  /*  if (jsonObject.getInt("payrollInsId") != 0) {
                        insId = jsonObject.getInt("payrollInsId");
                    }
                    if (jsonObject.getInt("payrollStfId") != 0) {
                        usrAssetId = jsonObject.getInt("payrollStfId");
                    }*/
                }
            }

            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
                Toast.makeText(this,
                        "Error to load standard/division!",
                        Toast.LENGTH_LONG).show();


                return;
            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                String stdDiv = object.getString("value");
                StandardDivision standardDivision = new StandardDivision();
                standardDivision.setStdName(object.getString("label"));
                String stdDivName = object.getString("label");
                if (stdDivName.contains("-")) {
                    String splStr[] = stdDivName.split("-");
                    if(orgId != 22){
                        standardDivision.setStdName(splStr[0]);
                        standardDivision.setDivName(splStr[1]);
                    }else {
                        standardDivision.setStdName(stdDivName);
                        standardDivision.setDivName(" ");
                    }
                }else {
                    standardDivision.setStdName(stdDivName);
                    standardDivision.setDivName(" ");
                }

                String stdDivIds = object.getString("value");
                String[] splStrs = stdDivIds.split("-");
                standardDivision.setStdId(Integer.parseInt(splStrs[0]));
                standardDivision.setDivId(Integer.parseInt(splStrs[1]));
                standardDivisionArrayList.add(standardDivision);

            }

            if (standardDivisionArrayList.size() != 0) {
                ArrayAdapter<StandardDivision> stdDivAdaptor = new ArrayAdapter<StandardDivision>(EnterExamMarksActivity.this,
                        android.R.layout.simple_spinner_item, standardDivisionArrayList);
                stdDivAdaptor
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnStdDiv.setAdapter(stdDivAdaptor);
            }
            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        stdId = standardDivisionArrayList.get(position).getStdId();
                        divId = standardDivisionArrayList.get(position).getDivId();
                        stdName = standardDivisionArrayList.get(position).getStdName();
                        divName = standardDivisionArrayList.get(position).getDivName();
                        if (stdId != 0 || divId != 0) {
                            EnterExamMarksActivity.LoadSubjects loadSubjects = new EnterExamMarksActivity.LoadSubjects();
                            loadSubjects.execute(null, null);
                        } else {


                            if (subjectDetailsArrayList != null) {
                                subjectDetailsArrayList.clear();
                                ArrayAdapter<SubjectDetails> ayrAdaptor = new ArrayAdapter<SubjectDetails>(EnterExamMarksActivity.this,
                                        android.R.layout.simple_spinner_item, subjectDetailsArrayList);
                                ayrAdaptor
                                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinnSubject.setAdapter(ayrAdaptor);
                                return;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                   /* EnterExamMarksActivity.loadSubjects loadSubjects = new EnterExamMarksActivity.loadSubjects();
                    loadSubjects.execute(null, null);*/
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            //btnOk
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {


                        if (stdId == 0 || divId == 0) {
                            Toast.makeText(context, "Please select standard/division!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (subId == 0) {
                            Toast.makeText(context, "Please select subject!", Toast.LENGTH_SHORT).show();
                            return;

                        }
                        if (examId == 0) {
                            Toast.makeText(context, "Please select exam!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //load exam details

                        editable = false;
                        btnEdit.setVisibility(View.VISIBLE);
                        scrollView.setVisibility(View.GONE);
                        layParent.setVisibility(View.VISIBLE);
                        btnSave.setVisibility(View.GONE);

                        EnterExamMarksActivity.LoadExamDetails loadExamDetails = new EnterExamMarksActivity.LoadExamDetails();
                        loadExamDetails.execute(null, null);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        btnEdit.setVisibility(View.GONE);
                        btnSave.setVisibility(View.VISIBLE);
                        editable = true;
                        EnterExamMarksActivity.LoadExamDetails loadExamDetails = new EnterExamMarksActivity.LoadExamDetails();
                        loadExamDetails.execute(null, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            //btn save
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        arrayList = new ArrayList<>();
                        arrayList = deatilsRecyAdapter.getList();
                        boolean isMarksEnteredOk = true;

                        for(int i=0; i<arrayList.size(); i++){
                            if(!arrayList.get(i).getStuMarks().equals("")){
                                Double marks = new Double(arrayList.get(i).getStuMarks());
                                Double maxMarks = new Double(arrayList.get(i).getStuMaxMarks());
                                if(marks > maxMarks){
                                    isMarksEnteredOk = false;
                                    Toast.makeText(context, "Entered marks cannot be greater than " + maxMarks + " for student " + arrayList.get(i).getStuName(), Toast.LENGTH_LONG).show();
                                    break;
                                }
                            }
                        }

                        //update
                        if(isMarksEnteredOk){
                            EnterExamMarksActivity.UpdateMarks updateMarks = new EnterExamMarksActivity.UpdateMarks();
                            updateMarks.execute(null, null);
                        }
                    } catch (Exception e)

                    {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class UpdateMarks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resSaveExamMarks.equals("") || resSaveExamMarks == null) {
                    Toast.makeText(context, "Failed to save exam marks!", Toast.LENGTH_SHORT).show();
                    util.SaveSystemLog(orgId,insId,"Failed to save exam marks","Teacher Connect",usrId,"Enter Exam Marks");
                    return;
                } else {
                    JSONObject object = new JSONObject(resSaveExamMarks);
                    String status = object.getString("status");
                    if (status.equals("") || status == null) {
                        Toast.makeText(context, "Failed to save exam marks!", Toast.LENGTH_SHORT).show();
                        util.SaveSystemLog(orgId,insId,"Failed to save exam marks","Teacher Connect",usrId,"Enter Exam Marks");

                        return;
                    }
                    if (status.equals("SUCCESS")) {
                        Toast.makeText(context, "Exam marks save successfully!", Toast.LENGTH_SHORT).show();
                        EnterExamMarksActivity.LoadExamDetails loadExamDetails = new EnterExamMarksActivity.LoadExamDetails();
                        loadExamDetails.execute(null, null);
                    } else if (status.equals("FAILURE")) {
                        Toast.makeText(context, "Failed to save exam marks!", Toast.LENGTH_SHORT).show();
                        util.SaveSystemLog(orgId,insId,"Failed to save exam marks","Teacher Connect",usrId,"Enter Exam Marks");

                    }
                }

            } catch (Exception e) {
                util.SaveSystemLog(orgId,insId,"Error occur while entering exam marks","Teacher Connect",usrId,"Enter Exam Marks");
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resSaveExamMarks = util.saveEnteredExamMarksV1(orgId, insId, dscId, stdId, divId, ayr, examId, subId, arrayList.toString(), usrId, ugpId, stdName, divName);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while entering exam marks","Teacher Connect",usrId,"Enter Exam Marks");

            }
            return null;
        }
    }

    public class LoadSubjects extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resSubject == null || resSubject.equals("")) {
                    Toast.makeText(context, "Unable to load subject details!", Toast.LENGTH_SHORT).show();
                    subjectDetailsArrayList.clear();
                    ArrayAdapter<SubjectDetails> ayrAdaptor = new ArrayAdapter<SubjectDetails>(EnterExamMarksActivity.this,
                            android.R.layout.simple_spinner_item, subjectDetailsArrayList);
                    ayrAdaptor
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnSubject.setAdapter(ayrAdaptor);
                    return;
                } else {

                    JSONObject jsonObject = new JSONObject(resSubject);
                    JSONArray subArray = jsonObject.getJSONArray("subject");
                    if (subArray.length() != 0) {
                        subjectDetailsArrayList = new ArrayList<>();

                        for (int i = 0; i < subArray.length(); i++) {
                            SubjectDetails subjectDetails = new SubjectDetails();
                            JSONObject object = subArray.getJSONObject(i);
                            subjectDetails.setSubId(object.getInt("subId"));
                            subjectDetails.setSubName(object.getString("subName"));
                            subjectDetailsArrayList.add(subjectDetails);


                        }

                        //set adapter
                        if (subjectDetailsArrayList.size() != 0) {
                            ArrayAdapter<SubjectDetails> ayrAdaptor = new ArrayAdapter<SubjectDetails>(EnterExamMarksActivity.this,
                                    android.R.layout.simple_spinner_item, subjectDetailsArrayList);
                            ayrAdaptor
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnSubject.setAdapter(ayrAdaptor);
                        } else {
                            Toast.makeText(context, "Unable to load subject details!", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(context, "Unable to load subject details!", Toast.LENGTH_SHORT).show();
                        subjectDetailsArrayList.clear();
                        ArrayAdapter<SubjectDetails> ayrAdaptor = new ArrayAdapter<SubjectDetails>(EnterExamMarksActivity.this,
                                android.R.layout.simple_spinner_item, subjectDetailsArrayList);
                        ayrAdaptor
                                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnSubject.setAdapter(ayrAdaptor);
                        return;
                    }

                    spinnSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position != 0) {
                                subId = subjectDetailsArrayList.get(position).getSubId();

                                if (subId != 0) {
                                    EnterExamMarksActivity.LoadExams loadExams = new EnterExamMarksActivity.LoadExams();
                                    loadExams.execute(null, null);

                                } else {
                                    if (examsArrayList != null) {
                                        examsArrayList.clear();
                                        ArrayAdapter<Exams> examAdapter = new ArrayAdapter<Exams>(EnterExamMarksActivity.this,
                                                android.R.layout.simple_spinner_item, examsArrayList);
                                        examAdapter
                                                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinnExam.setAdapter(examAdapter);
                                    }
                                }
                                /*EnterExamMarksActivity.loadExams loadExams = new EnterExamMarksActivity.loadExams();
                                loadExams.execute(null, null);*/
                            } else {
                                if (examsArrayList != null) {
                                    examsArrayList.clear();
                                    ArrayAdapter<Exams> examAdapter = new ArrayAdapter<Exams>(EnterExamMarksActivity.this,
                                            android.R.layout.simple_spinner_item, examsArrayList);
                                    examAdapter
                                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinnExam.setAdapter(examAdapter);
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resSubject = util.getStaffClassSubjects(orgId, insId, dscId, ayr, usrAssetId, stdId, divId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadExams extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resExams == null || resExams.equals("")) {
                    if (examsArrayList != null) {
                        examsArrayList.clear();
                        ArrayAdapter<Exams> examAdapter = new ArrayAdapter<Exams>(EnterExamMarksActivity.this,
                                android.R.layout.simple_spinner_item, examsArrayList);
                        examAdapter
                                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnExam.setAdapter(examAdapter);
                    }
                    return;
                } else {

                    JSONObject jsonObject = new JSONObject(resExams);
                    JSONArray array = jsonObject.getJSONArray("exam");

                    if (array.length() == 0) {
                        Toast.makeText(context, "Unable to load exams!", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        examsArrayList = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            Exams exams = new Exams();
                            exams.setExamId(object.getInt("exmId"));
                            exams.setExamName(object.getString("exmName"));
                            examsArrayList.add(exams);

                        }
                        if (examsArrayList.size() != 0) {
                            ArrayAdapter<Exams> examAdapter = new ArrayAdapter<Exams>(EnterExamMarksActivity.this,
                                    android.R.layout.simple_spinner_item, examsArrayList);
                            examAdapter
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnExam.setAdapter(examAdapter);
                        }

                        spinnExam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                examId = examsArrayList.get(position).getExamId();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resExams = util.getStfExamsScheduleV1(orgId, insId, dscId, stdId, divId, ayr, usrAssetId, subId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadExamDetails extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resExamMarksDetails == null || resExamMarksDetails.equals("")) {
                    Toast.makeText(context, "Unable to load exams details!", Toast.LENGTH_SHORT).show();
                    return;
                } else {

                    JSONObject object2 = new JSONObject(resExamMarksDetails);
                    JSONArray array = object2.getJSONArray("marks");
                    if (array.length() == 0) {
                        Toast.makeText(context, "Unable to load exams details!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    stuExamsMarksDetailsArrayList = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        StuExamsMarksDetails marksDetails = new StuExamsMarksDetails();
                        marksDetails.setRollNo(object.getInt("rollNo"));
                        marksDetails.setStuId(object.getInt("stuId"));
                        marksDetails.setStuAttd(object.getString("stuAttd"));
                        marksDetails.setStuMarks(object.getString("stuMarks"));
                        marksDetails.setStuName(object.getString("stuName"));
                        marksDetails.setStuMaxMarks(object.getString("maxMarks"));
                        stuExamsMarksDetailsArrayList.add(marksDetails);
                    }

                    //set adapter
                    if (stuExamsMarksDetailsArrayList.size() != 0) {

                        deatilsRecyAdapter = new ExamMarksDeatilsRecyAdapter(context, stuExamsMarksDetailsArrayList, editable);
                        exmRecyView.setAdapter(deatilsRecyAdapter);

                    } else {
                        Toast.makeText(context, "Unable to load exams details!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resExamMarksDetails = util.getExamMarksV2(orgId, insId, dscId, stdId, divId, ayr, examId, subId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }
}

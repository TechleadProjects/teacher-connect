package com.techled.teacheranalytics.ActivityList.EvaluateAnswerPaperModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.style.AlignmentSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.techled.teacheranalytics.ActivityList.TaskManagementModule.AddNewTaskActivity;
import com.techled.teacheranalytics.ActivityList.TimetableModule.ViewTimeTableActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.StudentSelectionForEvaluationRecyclerAdapter;
import com.techled.teacheranalytics.adapter.UploadedDocsForEvaluationRecyclerAdapter;
import com.techled.teacheranalytics.pojo.AyrYearData;
import com.techled.teacheranalytics.pojo.Divisions;
import com.techled.teacheranalytics.pojo.EvaluateAnsPaperStudentData;
import com.techled.teacheranalytics.pojo.ExamScheduleData;
import com.techled.teacheranalytics.pojo.ExamTypeData;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.StandardDivisionDetails;
import com.techled.teacheranalytics.pojo.Standards;
import com.techled.teacheranalytics.pojo.StdDivDetails;
import com.techled.teacheranalytics.pojo.SubjectDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class StandardDivisionWiseEvaluatePaperActivity extends AppCompatActivity {

    private Context context;
    private ArrayList<EvaluateAnsPaperStudentData> studentsList;
    private Util util;

    private int orgId, insId, ayrYear, dscId, usrId, stfId;
    private String params, response;

    private ArrayList<AyrYearData> ayrYearList;
    private ArrayList<StandardDivisionDetails> standardDivisionList;
    private ArrayList<Divisions> divisionList;
    private ArrayList<SubjectDetails> subjectList;
    private ArrayList<ExamTypeData> examTypeList;
    private ArrayList<ExamScheduleData> examScheduleList;

    private Spinner ayrYearSpinner, standardSpinner, divisionSpinner, subjectSpinner, examTypeSpinner, examScheduleSpinner;

    private ProgressDialog progDailog;

    private String selectedAyrYear, selectedStandard, selectedDivision, selectedSubject, selectedExamType, selectedExamSchedule, selectedExamName;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standard_division_wise_evaluate_paper);

        try {

            context = this;
            util = new Util();

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Evaluate Answer Paper");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            ayrYearList = new ArrayList<>();
            standardDivisionList = new ArrayList<>();
            divisionList = new ArrayList<>();
            subjectList = new ArrayList<>();
            examTypeList = new ArrayList<>();
            examScheduleList = new ArrayList<>();

            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {

                    Glide.get(context).clearDiskCache();
                }
            });
            t1.start();

            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayrYear = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");

                stfId = object.getInt("assetId1");
            }

            studentsList = new ArrayList<>();

            ayrYearSpinner = findViewById(R.id.ayrYearSpinner);
            standardSpinner = findViewById(R.id.standardSpinner);
            divisionSpinner = findViewById(R.id.divisionSpinner);
            subjectSpinner = findViewById(R.id.subjectSpinner);
            examTypeSpinner = findViewById(R.id.examTypeSpinner);
            examScheduleSpinner = findViewById(R.id.examScheduleSpinner);

            Button btnOk = findViewById(R.id.btnOk);
            Button btnCancel = findViewById(R.id.btnCancel);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (selectedAyrYear == null || selectedAyrYear.equals("") || selectedAyrYear.equals("null") || selectedAyrYear.equals("0")) {

                        Toast.makeText(context, "Please select Ayr year.", Toast.LENGTH_SHORT).show();
                    } else if ((selectedStandard == null || selectedStandard.equals("") || selectedStandard.equals("null") || selectedDivision.equals("0")) && (selectedDivision == null || selectedDivision.equals("") || selectedDivision.equals("null") || selectedDivision.equals("0"))) {

                        Toast.makeText(context, "Please select Standard - Division.", Toast.LENGTH_SHORT).show();
                    } else if (selectedSubject == null || selectedSubject.equals("") || selectedSubject.equals("null") || selectedSubject.equals("0")) {

                        Toast.makeText(context, "Please select Subject.", Toast.LENGTH_SHORT).show();
                    }
//                    else if (selectedExamType == null || selectedExamType.equals("") || selectedExamType.equals("null")) {
//
//                        Toast.makeText(context, "Please select Exam type.", Toast.LENGTH_SHORT).show();
//                    }
                    else if (selectedExamSchedule == null || selectedExamSchedule.equals("") || selectedExamSchedule.equals("null") || selectedExamSchedule.equals("0")) {

                        Toast.makeText(context, "Please select Exam.", Toast.LENGTH_SHORT).show();
                    } else if (studentsList == null || studentsList.size() == 0) {

                        Toast.makeText(context, "Students not found for current selection.", Toast.LENGTH_SHORT).show();
                    } else {

                        Gson gson = new Gson();
                        String json = gson.toJson(studentsList);

                        Intent intent = new Intent(context, StudentSelectionForEvaluateAnsPaper.class);
                        intent.putExtra("studentsList", json);
                        intent.putExtra("ayrYear", selectedAyrYear);
                        intent.putExtra("standard", selectedStandard);
                        intent.putExtra("division", selectedDivision);
                        intent.putExtra("subject", selectedSubject);
                        intent.putExtra("examType", selectedExamType);
                        intent.putExtra("examSchedule", selectedExamSchedule);
                        intent.putExtra("examName", selectedExamName);
                        startActivity(intent);
                    }
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            new LoadAyrYearsTask().execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class LoadAyrYearsTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Academic Years...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {

                    ayrYearList.clear();
                    selectedAyrYear = "";

                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {

                        ayrYearList.clear();
                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");

                        int currentIndex = -1;
                        for (int i=0; i<array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);

                            AyrYearData ayrYearData = new AyrYearData();
                            ayrYearData.setAyrLabel(object.getString("ayrLabel"));
                            ayrYearData.setAyrValue(object.getString("ayrValue"));

                            if (String.valueOf(ayrYear).equals(object.getString("ayrValue"))) {
                                currentIndex = i;
                                selectedAyrYear = object.getString("ayrValue");
                            }

                            ayrYearList.add(ayrYearData);
                        }

                        if (ayrYearList.size() > 0) {

                            //set value to ayr year spinner
                            ArrayAdapter<AyrYearData> arrayAdapter1 = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, ayrYearList);
                            arrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            ayrYearSpinner.setAdapter(arrayAdapter1);

                            if (currentIndex != -1) {
                                ayrYearSpinner.setSelection(currentIndex, true);

                            } else {

                                ayrYearSpinner.setSelection(ayrYearList.size() - 1, true);
                                selectedAyrYear = ayrYearList.get(ayrYearList.size() - 1).getAyrValue();
                            }

                            if (!selectedAyrYear.equals("0")) {

                                new LoadStandardDivisions().execute();
                            }

                            ayrYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    selectedAyrYear = ayrYearList.get(position).getAyrValue();

                                    if (!selectedAyrYear.equals("0")) {

                                        new LoadStandardDivisions().execute();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else {

                            Toast.makeText(context, "Could not fetch Ayr Years. Please try again later.", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    } else {

                        Toast.makeText(context, "Could not fetch Ayr Years. Please try again later.", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } else {

                    Toast.makeText(context, "Could not fetch Ayr Years. Please try again later.", Toast.LENGTH_SHORT).show();
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, "Could not fetch Ayr Years. Please try again later.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getAyrYears(orgId, insId, dscId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadStandardDivisions extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Standard - Divisions...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if (response == null || response.equals("")) {
                    Toast.makeText(context, "Failed to get Standard - Divisions.", Toast.LENGTH_SHORT).show();
                    StandardDivisionDetails standards = new StandardDivisionDetails();
                    standards.setStdId("0");
                    standards.setStdName("Standard");
                    standards.setDivId("0");
                    standards.setDivName("Divisions not found");
                    standardDivisionList.clear();
                    standardDivisionList.add(standards);
                    assignStandard();
                } else {

                    standardDivisionList = new ArrayList<>();
                    selectedStandard = "";
                    selectedDivision = "";

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        standardDivisionList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            StandardDivisionDetails standardDivisionDetails = new StandardDivisionDetails();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            standardDivisionDetails.setStdId(jsonObject1.getString("stdId"));
                            standardDivisionDetails.setStdName(jsonObject1.getString("stdName"));
                            standardDivisionDetails.setDivId(jsonObject1.getString("divId"));
                            standardDivisionDetails.setDivName(jsonObject1.getString("divName"));

                            standardDivisionList.add(standardDivisionDetails);
                        }

                        if(standardDivisionList.size() > 0){

                            assignStandard();

                        }else {
                            Toast.makeText(context, "Failed to get Standard - Divisions.", Toast.LENGTH_SHORT).show();
                            StandardDivisionDetails standards = new StandardDivisionDetails();
                            standards.setStdId("0");
                            standards.setStdName("Standard");
                            standards.setDivId("0");
                            standards.setDivName("-Divisions not found");
                            standardDivisionList.clear();
                            standardDivisionList.add(standards);
                            assignStandard();
                        }

                    } else {

                        Toast.makeText(context, "Failed to get Standard - Divisions.", Toast.LENGTH_SHORT).show();
                        StandardDivisionDetails standards = new StandardDivisionDetails();
                        standards.setStdId("0");
                        standards.setStdName("Standard");
                        standards.setDivId("0");
                        standards.setDivName("-Divisions not found");
                        standardDivisionList.clear();
                        standardDivisionList.add(standards);
                        assignStandard();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getStandardDivisionForMarksEntry(orgId, insId, dscId, selectedAyrYear, String.valueOf(stfId));

                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void assignStandard(){

        ArrayAdapter<StandardDivisionDetails> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, standardDivisionList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        standardSpinner.setAdapter(adapter);

        standardSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedStandard = standardDivisionList.get(position).getStdId();
                selectedDivision = standardDivisionList.get(position).getDivId();
                if(!selectedStandard.equals("0") && !selectedDivision.equals("0")){
                    new LoadSubjects().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public class LoadDivisions extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Divisions...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if ((response == null || response.equals(""))) {
                    Toast.makeText(context, "Failed to get divisions.", Toast.LENGTH_SHORT).show();
                } else {

                    divisionList = new ArrayList<>();
                    selectedDivision = "";

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        divisionList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Divisions divisions = new Divisions();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            divisions.setDivId(jsonObject1.getInt("divId"));
                            divisions.setDivName(jsonObject1.getString("divName"));

                            divisionList.add(divisions);
                        }

                        if(divisionList.size() > 0){
                            assignDivision();
                        }else {
                            Toast.makeText(context, "Failed to get divisions.", Toast.LENGTH_SHORT).show();
                            Divisions divisions = new Divisions();
                            divisions.setDivId(0);
                            divisions.setDivName("Divisions not found");
                            divisionList.clear();
                            divisionList.add(divisions);
                            assignDivision();
                        }

                    } else {
                        Toast.makeText(context, "Failed to get divisions.", Toast.LENGTH_SHORT).show();
                        Divisions divisions = new Divisions();
                        divisions.setDivId(0);
                        divisions.setDivName("Divisions not found");
                        divisionList.clear();
                        divisionList.add(divisions);
                        assignDivision();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getDivisionByStandardId(orgId, insId, dscId, Integer.parseInt(selectedAyrYear), Integer.parseInt(selectedStandard));
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void assignDivision(){

        ArrayAdapter<Divisions> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, divisionList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        divisionSpinner.setAdapter(adapter);

        divisionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedDivision = String.valueOf(divisionList.get(position).getDivId());

                if(!selectedDivision.equals("0")){

                    new LoadSubjects().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public class LoadSubjects extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Subjects...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if (response == null || response.equals("")) {
                    Toast.makeText(context, "Failed to get Subjects.", Toast.LENGTH_SHORT).show();
                    SubjectDetails subjectDetails = new SubjectDetails();
                    subjectDetails.setSubId(0);
                    subjectDetails.setSubName("Subjects not found");
                    subjectList.clear();
                    subjectList.add(subjectDetails);
                    assignSubjects();
                } else {

                    subjectList = new ArrayList<>();
                    selectedSubject = "";

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        subjectList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            SubjectDetails subjectDetails = new SubjectDetails();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            subjectDetails.setSubId(jsonObject1.getInt("subId"));
                            subjectDetails.setSubName(jsonObject1.getString("subName"));

                            subjectList.add(subjectDetails);
                        }

                        if(subjectList.size() > 0){
                            assignSubjects();

                        }else {
                            Toast.makeText(context, "Failed to get subjects.", Toast.LENGTH_SHORT).show();
                            SubjectDetails subjectDetails = new SubjectDetails();
                            subjectDetails.setSubId(0);
                            subjectDetails.setSubName("Subjects not found");
                            subjectList.clear();
                            subjectList.add(subjectDetails);
                            assignSubjects();
                        }

                    } else {
                        Toast.makeText(context, "Failed to get subjects.", Toast.LENGTH_SHORT).show();
                        SubjectDetails subjectDetails = new SubjectDetails();
                        subjectDetails.setSubId(0);
                        subjectDetails.setSubName("Subjects not found");
                        subjectList.clear();
                        subjectList.add(subjectDetails);
                        assignSubjects();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getSubjectsForStaff(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), selectedDivision, selectedStandard, selectedAyrYear, String.valueOf(stfId));
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void assignSubjects(){

        ArrayAdapter<SubjectDetails> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, subjectList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        subjectSpinner.setAdapter(adapter);

        subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedSubject = String.valueOf(subjectList.get(position).getSubId());

                if(!selectedSubject.equals("0")){

                    //new LoadExamTypes().execute();
                    selectedExamType = "0";
                    new LoadExamSchedule().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public class LoadExamTypes extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Exam types...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if ((response == null || response.equals(""))) {
                    Toast.makeText(context, "Failed to get exam types.", Toast.LENGTH_SHORT).show();
                    ExamTypeData examTypeData = new ExamTypeData();
                    examTypeData.setEtcId("0");
                    examTypeData.setExtId("0");
                    examTypeData.setExtTerm("0");
                    examTypeData.setExtName("Exam types not found");
                    examTypeList.clear();
                    examTypeList.add(examTypeData);
                    assignExamTypes();
                } else {

                    examTypeList = new ArrayList<>();

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        examTypeList = new ArrayList<>();
                        selectedExamType = "";

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            ExamTypeData examTypeData = new ExamTypeData();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            //examTypeData.setEtcId(jsonObject1.getString("etcId"));
                            examTypeData.setExtId(jsonObject1.getString("extId"));
                            examTypeData.setExtName(jsonObject1.getString("extName"));
                            examTypeData.setExtDisplayName(jsonObject1.getString("extDisplayName"));
//                            examTypeData.setExtLevelSubjectTypeYn(jsonObject1.getString("extLevelSubjectTypeYn"));
//                            examTypeData.setExtPredictScoreExtIds(jsonObject1.getString("extPredictScoreExtIds"));
//                            examTypeData.setExtSequence(jsonObject1.getString("extSequence"));
//                            examTypeData.setExtExamSelectionParam(jsonObject1.getString("extExamSelectionParam"));
//                            examTypeData.setExtTerm(jsonObject1.getString("extTerm"));
//                            examTypeData.setExtAllowPromotionToNextLevel(jsonObject1.getString("extAllowPromotionToNextLevel"));

                            examTypeList.add(examTypeData);
                        }

                        if(examTypeList.size() > 0){
                            assignExamTypes();

                        }else {
                            Toast.makeText(context, "Failed to get exam types.", Toast.LENGTH_SHORT).show();
                            ExamTypeData examTypeData = new ExamTypeData();
                            examTypeData.setEtcId("0");
                            examTypeData.setExtId("0");
                            examTypeData.setExtTerm("0");
                            examTypeData.setExtName("Exam types not found");
                            examTypeList.clear();
                            examTypeList.add(examTypeData);
                            assignExamTypes();
                        }

                    } else {
                        Toast.makeText(context, "Failed to get exam types.", Toast.LENGTH_SHORT).show();
                        ExamTypeData examTypeData = new ExamTypeData();
                        examTypeData.setEtcId("0");
                        examTypeData.setExtId("0");
                        examTypeData.setExtTerm("0");
                        examTypeData.setExtName("Exam types not found");
                        examTypeList.clear();
                        examTypeList.add(examTypeData);
                        assignExamTypes();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getExamTypes(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), selectedDivision, selectedStandard, selectedAyrYear, String.valueOf(selectedSubject));
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void assignExamTypes(){

        ArrayAdapter<ExamTypeData> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, examTypeList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        examTypeSpinner.setAdapter(adapter);

        examTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedExamType = String.valueOf(examTypeList.get(position).getExtId());

                new LoadExamSchedule().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public class LoadExamSchedule extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Exams...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if ((response == null || response.equals(""))) {
                    Toast.makeText(context, "Failed to get exams.", Toast.LENGTH_SHORT).show();
                    ExamScheduleData examScheduleData = new ExamScheduleData();
                    examScheduleData.setExmValue("0");
                    examScheduleData.setExmLabel("Exams not found");
                    examScheduleList.clear();
                    examScheduleList.add(examScheduleData);
                    assignExams();
                } else {

                    examScheduleList = new ArrayList<>();
                    selectedExamSchedule = "";
                    selectedExamName = "";

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        examScheduleList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            ExamScheduleData examScheduleData = new ExamScheduleData();
                            examScheduleData.setExmLabel(jsonObject1.getString("exmName"));
                            examScheduleData.setExmValue(jsonObject1.getString("exmId"));

                            examScheduleList.add(examScheduleData);
                        }

                        if(examScheduleList.size() > 0){
                            assignExams();

                        }else {

                            Toast.makeText(context, "Failed to get exams.", Toast.LENGTH_SHORT).show();
                            ExamScheduleData examScheduleData = new ExamScheduleData();
                            examScheduleData.setExmValue("0");
                            examScheduleData.setExmLabel("Exams not found");
                            examScheduleList.clear();
                            examScheduleList.add(examScheduleData);
                            assignExams();
                        }

                    } else {

                        Toast.makeText(context, "Failed to get exams.", Toast.LENGTH_SHORT).show();
                        ExamScheduleData examScheduleData = new ExamScheduleData();
                        examScheduleData.setExmValue("0");
                        examScheduleData.setExmLabel("Exams not found");
                        examScheduleList.clear();
                        examScheduleList.add(examScheduleData);
                        assignExams();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getStfExamScheduleV2(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), selectedDivision, selectedStandard, selectedAyrYear, String.valueOf(selectedSubject), String.valueOf(stfId), String.valueOf(0), selectedExamType);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void assignExams(){

        ArrayAdapter<ExamScheduleData> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, examScheduleList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        examScheduleSpinner.setAdapter(adapter);

        examScheduleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedExamSchedule = String.valueOf(examScheduleList.get(position).getExmValue());
                selectedExamName = examScheduleList.get(position).toString();

                if(!selectedExamSchedule.equals("0")){

                    new LoadStudents().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public class LoadStudents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Students...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if ((response == null || response.equals(""))) {
                    Toast.makeText(context, "Failed to load students.", Toast.LENGTH_SHORT).show();

                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    studentsList = new ArrayList<>();

                    if (status.equals("SUCCESS")) {

                        studentsList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            EvaluateAnsPaperStudentData evaluateAnsPaperStudentData = new EvaluateAnsPaperStudentData();
                            evaluateAnsPaperStudentData.setStuId(jsonObject1.getString("stuId"));
                            evaluateAnsPaperStudentData.setRollNo(jsonObject1.getString("rollNo"));
                            evaluateAnsPaperStudentData.setStuName(jsonObject1.getString("stuName"));
                            evaluateAnsPaperStudentData.setUploadStatus(jsonObject1.getString("uploadStatus"));
                            evaluateAnsPaperStudentData.setMarks(jsonObject1.getString("marks"));
                            evaluateAnsPaperStudentData.setEvaluationStatus(jsonObject1.getString("evaluationStatus"));

                            studentsList.add(evaluateAnsPaperStudentData);
                        }

                        if(studentsList.size() > 0){

                        }else {

                            Toast.makeText(context, "Students not found for current selection.", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Toast.makeText(context, "Students not found for current selection.", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

                Toast.makeText(context, "Failed to load students.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getStudentExmDocs(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), selectedDivision, selectedStandard, selectedAyrYear, String.valueOf(selectedSubject), String.valueOf(stfId), selectedExamSchedule);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        try {

            new LoadStudents().execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

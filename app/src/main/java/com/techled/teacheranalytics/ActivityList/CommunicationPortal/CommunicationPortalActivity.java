package com.techled.teacheranalytics.ActivityList.CommunicationPortal;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.CommunicationPortalPagerAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;

public class CommunicationPortalActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    private Dialog dialog;
    private LinearLayout layParent;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication_portal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Communication Portal");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //get reference
        context = this;
        layParent = (LinearLayout) findViewById(R.id.layParent);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Email"));
        tabLayout.addTab(tabLayout.newTab().setText("SMS"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);
        //Creating our pager adapter
        CommunicationPortalPagerAdapter adapter = new CommunicationPortalPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(CommunicationPortalActivity.this);

        //check internet connection
        CheckInternet checkInternet = new CheckInternet();
        boolean checkConnection = checkInternet.checkConnection(context);
        if (checkConnection) {

        } else {

            Snackbar snackbar = Snackbar
                    .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
            return;
        }

        //check poor internet connection
        CheckInternet checkPoorInternet = new CheckInternet();
        boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
        if (checkPoorConnection) {
            Snackbar snackbar = Snackbar
                    .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
        }


        //refresh the data from server
        ImageView imgRefresh = (ImageView) findViewById(R.id.imgRefresh);
        imgRefresh.setVisibility(View.VISIBLE);
        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_confirm_refresh);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    dialog.setCanceledOnTouchOutside(false);


                    Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
                    Button btnNo = (Button) dialog.findViewById(R.id.btnNo);

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();

                            //clear shared pref for email/sms
                            SharedPreferences.Editor emailEditor = getSharedPreferences("email", MODE_PRIVATE).edit();
                            emailEditor.putString("response", null);
                            emailEditor.apply();

                            SharedPreferences.Editor smsEditor = getSharedPreferences("sms", MODE_PRIVATE).edit();
                            smsEditor.putString("response", null);
                            smsEditor.apply();

                            Intent intent = new Intent(CommunicationPortalActivity.this, CommunicationPortalActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}

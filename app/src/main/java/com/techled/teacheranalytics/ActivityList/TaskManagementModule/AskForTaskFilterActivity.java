package com.techled.teacheranalytics.ActivityList.TaskManagementModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.PendingTaskDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AskForTaskFilterActivity extends AppCompatActivity {

    private Context context;
    private ArrayList<String> filterArrayList;
    private Spinner filterSpinner;
    private String filterValue;
    private ProgressDialog progDailog;
    private ArrayList<PendingTaskDetails> pendingTaskDetailsArrayList;
    private Util util;
    private int orgId, insId, dscId, stfId;

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_for_task_filter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Task Management");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        context = this;
        util = new Util();
        pendingTaskDetailsArrayList = new ArrayList<>();

        //login details
        SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
        String params = loginDetails.getString("params", null);

        try {
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);


                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                stfId = object.getInt("assetId1");

            }
        }catch (Exception e){
            e.printStackTrace();
        }


        filterSpinner = findViewById(R.id.filterSpinner);

        filterArrayList = new ArrayList<>();
        filterArrayList.add("Task Assigned To Me");
        filterArrayList.add("Task Assigned By Me");

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, filterArrayList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        filterSpinner.setAdapter(adapter);

        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    filterValue = "To";
                }else {
                    filterValue = "By";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getPendingTasks(View view) {

        //new LoadMyPendingTaskDetails().execute();

        Intent intent = new Intent(AskForTaskFilterActivity.this, MyTasksActivity.class);
        intent.putExtra("byOrTo", filterValue);
        startActivity(intent);
    }

    public class LoadMyPendingTaskDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("success")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        //JSONObject dataObject = dataObj.getJSONObject("data");
                        JSONArray dataArray1 = dataObj.getJSONArray("data");
                        if(dataArray1.length() == 0)
                        {
                            Toast.makeText(context, "Pending tasks not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        pendingTaskDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray1.length(); i++) {
                            PendingTaskDetails pendingTaskDetails = new PendingTaskDetails();
                            JSONObject object = dataArray1.getJSONObject(i);

                            if(filterValue.contains("By")){
                                pendingTaskDetails.setSubject(object.getString("tskSubject"));
                                pendingTaskDetails.setPriority(object.getString("tskPriority"));
                                pendingTaskDetails.setPercentComplete(object.getString("tskPercentComplete"));
                                pendingTaskDetails.setStatus(object.getString("tskStatus"));
                                pendingTaskDetails.setStartDate(object.getString("tskStartDate"));
                                pendingTaskDetails.setDueDate(object.getString("tskEndDate"));
                            }else if(filterValue.contains("To")) {
                                pendingTaskDetails.setAssignerId(object.getInt("assignerId"));
                                pendingTaskDetails.setSubject(object.getString("tssSubject"));
                                pendingTaskDetails.setAssigneeName(object.getString("assigner"));
                                pendingTaskDetails.setPercentComplete(object.getString("tssPercentComplete"));
                                pendingTaskDetails.setStatus(object.getString("tssStatus"));
                                pendingTaskDetails.setDueDate(object.getString("dueDate"));
                                pendingTaskDetails.setColorCode(object.getString("colourCode"));
                            }

                            pendingTaskDetailsArrayList.add(pendingTaskDetails);

                        }

                        //set adapter
                        if (pendingTaskDetailsArrayList.size() > 0) {

                            Intent intent = new Intent(AskForTaskFilterActivity.this, MyTasksActivity.class);
                            Gson gson = new Gson();
                            String json = gson.toJson(pendingTaskDetailsArrayList);
                            intent.putExtra("taskList", json);
                            intent.putExtra("byOrTo", filterValue);
                            intent.putExtra("dataNotFound", false);
                            startActivity(intent);

                        } else {
                            Intent intent = new Intent(AskForTaskFilterActivity.this, MyTasksActivity.class);
                            intent.putExtra("byOrTo", filterValue);
                            intent.putExtra("dataNotFound", true);
                            startActivity(intent);
                        }

                    } else {
                        Intent intent = new Intent(AskForTaskFilterActivity.this, MyTasksActivity.class);
                        intent.putExtra("byOrTo", filterValue);
                        intent.putExtra("dataNotFound", true);
                        startActivity(intent);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                String isTaskToMe;

                if(filterValue.equals("To")){
                    isTaskToMe = "Y";
                }else {
                    isTaskToMe = "N";
                }

                resMyLectures = util.findPendingTasks(orgId, insId, dscId, stfId, isTaskToMe);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

package com.techled.teacheranalytics.ActivityList.LeaveTheSchoolRequestsModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.LeaveTheSchoolRequestRecyAdapter;
import com.techled.teacheranalytics.pojo.LeaveTheSchoolRequestDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LeaveTheSchoolRequestsActivity extends AppCompatActivity {

    private RecyclerView leaveTheSchoolRecyView;
    private LinearLayout layNoRecord, layParent;
    private Context context;
    private Util util;
    private ProgressDialog progDailog;
    private int orgId, insId;
    private ArrayList<LeaveTheSchoolRequestDetails> leaveTheSchoolRequestDetailsArrayList;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_the_school_requests);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Applications To Leave The School");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }


            //get reference
            context = this;
            util = new Util();
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            leaveTheSchoolRecyView = (RecyclerView) findViewById(R.id.leaveTheSchoolRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            leaveTheSchoolRecyView.setLayoutManager(layoutManager);


            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");


            }

            //load the requests
            LoadLeaveTheSchoolRequest loadLeaveTheSchoolRequest = new LoadLeaveTheSchoolRequest();
            loadLeaveTheSchoolRequest.execute(null, null);
        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    public class LoadLeaveTheSchoolRequest extends AsyncTask<String, String, String> {
        private String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (response != null) {


                    JSONArray resArray = new JSONArray(response);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject dataObject = resArray.getJSONObject(1);
                        JSONArray dataArray = dataObject.getJSONArray("data");
                        leaveTheSchoolRequestDetailsArrayList = new ArrayList<>();

                        for (int i = 0; i < dataArray.length(); i++) {

                            LeaveTheSchoolRequestDetails leaveTheSchoolRequestDetails = new LeaveTheSchoolRequestDetails();
                            JSONObject object = dataArray.getJSONObject(i);
                            leaveTheSchoolRequestDetails.setStuName(object.getString("stuName"));
                            leaveTheSchoolRequestDetails.setStuLcReason(object.getString("stuLcReason"));
                            leaveTheSchoolRequestDetails.setStuAppliedLcDate(object.getString("stuAppliedLcDate"));
                            leaveTheSchoolRequestDetailsArrayList.add(leaveTheSchoolRequestDetails);

                        }

                        //set adapter
                        if (leaveTheSchoolRequestDetailsArrayList.size() > 0) {
                            LeaveTheSchoolRequestRecyAdapter adapter = new LeaveTheSchoolRequestRecyAdapter(leaveTheSchoolRequestDetailsArrayList, context);
                            leaveTheSchoolRecyView.setAdapter(adapter);
                        } else {
                            layNoRecord.setVisibility(View.VISIBLE);
                            layParent.setVisibility(View.GONE);
                            Toast.makeText(context, "No request Found!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    } else {
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                        Toast.makeText(context, "No request Found!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                } else {
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    Toast.makeText(context, "No request Found!", Toast.LENGTH_SHORT).show();
                    return;

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                response = util.GetLeaveTheSchoolRequest(orgId, insId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }
}

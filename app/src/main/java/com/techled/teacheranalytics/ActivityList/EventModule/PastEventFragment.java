package com.techled.teacheranalytics.ActivityList.EventModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.PastEventsRecyclerAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.PastEvents;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class PastEventFragment extends Fragment {

    private Util util;
    private ArrayList<PastEvents> pastEventsArrayList;
    private RecyclerView recycleView;
    private int orgId = 0, insId = 0, dscId = 0, ayr = 0, ugpId = 0, usrId = 0;
    private Context context;
    private String params;
    private LinearLayout layNoRecord, layParent;
    private ProgressDialog progDailog;
    private String response, resMyInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            View view = inflater.inflate(R.layout.fragment_past_event, container, false);

            //get reference
            context = getActivity().getApplicationContext();
            util = new Util();
            recycleView = (RecyclerView) view.findViewById(R.id.recycleView);
            recycleView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(context);
            recycleView.setLayoutManager(layoutManager2);
            layParent = (LinearLayout) view.findViewById(R.id.layParent);
            layNoRecord = (LinearLayout) view.findViewById(R.id.layNoRecord);


            //login details
            try {
                SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);
                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);

                    orgId = object.getInt("orgId");
                    insId = object.getInt("insId");
                    ayr = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");
                    usrId = object.getInt("usrId");
                }


                //load past events
                loadEvents();


            } catch (Exception e) {
                e.printStackTrace();
            }


            return view;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void loadEvents() {
        new LoadPastEvents().execute();
    }

    public class LoadPastEvents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(getActivity());
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response != null) {
                    if (response.equals("Failure")) {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                      //  Toast.makeText(context, "No past events!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No past events!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                    pastEventsArrayList = new ArrayList<PastEvents>();

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("event");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        PastEvents pastEvents = new PastEvents();
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        pastEvents.setOrgId(orgId);
                        pastEvents.setInsId(insId);
                        pastEvents.setDscId(dscId);
                        pastEvents.setAyr(ayr);
                        pastEvents.setEvtId(jsonObject1.getInt("evtId"));
                        pastEvents.setEvtDesc(jsonObject1.getString("evtDesc"));
                        pastEvents.setEvtFrom(jsonObject1.getString("evtFrom"));
                        pastEvents.setEvtTo(jsonObject1.getString("evtTo"));
                        pastEvents.setEvtTitle(jsonObject1.getString("evtTitle"));
                        pastEvents.setImgCount(jsonObject1.getInt("imgcnt"));
                        if (jsonObject1.getString("evtOrganizedBy") == null) {
                            pastEvents.setEvtOrganizedBy("-");
                        } else {
                            pastEvents.setEvtOrganizedBy(jsonObject1.getString("evtOrganizedBy"));
                        }
                        pastEventsArrayList.add(pastEvents);
                    }

                    if (pastEventsArrayList.size() != 0) {

                        //set adapter
                        layParent.setVisibility(View.VISIBLE);
                        layNoRecord.setVisibility(View.GONE);
                        PastEventsRecyclerAdapter pastEventsRecAdapter = new PastEventsRecyclerAdapter(context, pastEventsArrayList);
                        recycleView.setAdapter(pastEventsRecAdapter);

                    } else {

                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                       // Toast.makeText(context, "No past events!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No past events!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                } else {

                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                   // Toast.makeText(context, "No past events!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No past events!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }


            } catch (Exception e) {
                layParent.setVisibility(View.GONE);
                layNoRecord.setVisibility(View.VISIBLE);
               // Toast.makeText(context, "No past events!", Toast.LENGTH_SHORT).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "No past events!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                response = util.getPastEvents(orgId, insId,
                        dscId, ayr, "E", usrId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}

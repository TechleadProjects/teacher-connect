package com.techled.teacheranalytics.ActivityList.TeachingLogModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.techled.teacheranalytics.ActivityList.MyAttendanceModule.MyAttendanceFragment;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadHomeworkActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.BookingDecorator;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.DailyAttendance;
import com.techled.teacheranalytics.pojo.Standards;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

public class TeachingLogCalendarSummaryActivity extends AppCompatActivity implements OnChartValueSelectedListener {

    private LinearLayout layNoRecord, layParent, layPieChartView;
    private MaterialCalendarView calView;
    private Button btnProceed;
    private Context context;
    private Util util;
    private String selectedDate,selectedDateCompare;
    private int orgId, insId, dscId, ayr, ugpId, usrId, usrAssetId, stdId;
    private ProgressDialog progDailog;
    private JSONArray classTeacherOfJson;
    private ArrayList<Standards> standardDivisionArrayList;
    private String resUserInfo, pastResDailyAttd;
    private Spinner spinnStdDiv;
    private Dialog dialog;
    private String response, ayrDate;
    private int stfId, divId;
    private PieChart chartTeachingLog;
    private String filledLog, approvedLog, pendingLog,absent, resDailyAttd;
    private ArrayList<DailyAttendance> dailyAttendanceArrayList;
    private ArrayList<Date> dates;
    private HashSet<Date> datesSetForO, datesSetForW, datesSetForV, datesSetForP, datesSetForA, datesSetForF, datesSetForS, datesSetForX;
    private HashMap<Date, String> hashMap;
    private Date stringDate, startDate, endDate;
    private Boolean presentStatus;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teaching_log_calendar_summary);


        try {


            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Teaching Logs ");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
            //get reference
            layParent = (LinearLayout) findViewById(R.id.layParent);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            btnProceed = (Button) findViewById(R.id.btnProceed);
            calView = (MaterialCalendarView) findViewById(R.id.calView);
            chartTeachingLog = (PieChart) findViewById(R.id.chartTeachingLog);
            layPieChartView = (LinearLayout) findViewById(R.id.layPieChartView);

            context = this;
            util = new Util();
            presentStatus = false;
            progDailog = new ProgressDialog(context);

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
            }

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //load subject teachers std div
            SharedPreferences prefUserInfo = getSharedPreferences("MyInfo", MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }


            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    usrId = jsonObject.getInt("usrAssetId");
                    stdId = jsonObject.getInt("stdId");
                    divId = jsonObject.getInt("divId");
                    if (jsonObject.getInt("payrollInsId") != 0) {
                        insId = jsonObject.getInt("payrollInsId");
                    }
                    if (jsonObject.getInt("payrollStfId") != 0) {
                        usrAssetId = jsonObject.getInt("payrollStfId");
                    }
                    classTeacherOfJson = jsonObject.getJSONArray("applicableStds");

                }
            }


            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Error to load standard/division!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                Standards standards = new Standards();
                standards.setStdId(object.getString("value"));
                standards.setStdName(object.getString("label"));

                standardDivisionArrayList.add(standards);

            }

            //defualt calView
            try {
                calView.state().edit()
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        .setCalendarDisplayMode(CalendarMode.MONTHS)
                        .commit();
                calView.setShowOtherDates(0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //load Daily attendance from pref
            SharedPreferences dailyAttdprefs = context.getSharedPreferences("dailyAttd", context.MODE_PRIVATE);
            if (dailyAttdprefs != null) {
                pastResDailyAttd = dailyAttdprefs.getString("response", null);
            }



//            dialog = new Dialog(context);
//            dialog.setContentView(R.layout.dialog_select_std_div);
//            dialog.show();
//            Window window = dialog.getWindow();
//            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
//
//            dialog.setCanceledOnTouchOutside(false);
//            //set adapter to standard spinner
//            spinnStdDiv = (Spinner) dialog.findViewById(R.id.spinnStdDiv);
//            ArrayAdapter<Standards> stdDivAdapter = new ArrayAdapter<Standards>(TeachingLogCalendarSummaryActivity.this,
//                    android.R.layout.simple_spinner_item, standardDivisionArrayList);
//            stdDivAdapter
//                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            spinnStdDiv.setAdapter(stdDivAdapter);

//            //get standard id
//            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    stdId = Integer.parseInt(standardDivisionArrayList.get(position).getStdId());
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });

//            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
//            btnOk.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (stdId == 0) {
//                        Toast.makeText(context, "Please select standard!", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
//                    dialog.dismiss();
//                    //load teaching log summary
//                    LoadTeachingLogSummary loadTeachingLogSummary = new LoadTeachingLogSummary();
//                    loadTeachingLogSummary.execute(null, null);
//
//                }
//            });

            //Piechart View
            filledLog = "70";
            pendingLog = "20";
            approvedLog = "10";

            chartTeachingLog.setUsePercentValues(false);
            ArrayList<String> xValue = new ArrayList<>();
            xValue.add("");
            xValue.add("");
            xValue.add("");
            ArrayList<Entry> logDetails = new ArrayList<>();
            logDetails.add(new Entry((float) 70, 0));
            logDetails.add(new Entry((float) 20, 1));
            logDetails.add(new Entry((float) 10, 2));
                        PieDataSet dataSet = new PieDataSet(logDetails, "");
            dataSet.setColors(new int[]{ColorTemplate.rgb("#7ABF60"), ColorTemplate.rgb("#fa0f0f"),ColorTemplate.rgb("#8dc4bf")});
            PieData data = new PieData(xValue, dataSet);
            chartTeachingLog.setData(data);
            chartTeachingLog.setDrawHoleEnabled(true);
            chartTeachingLog.setTransparentCircleRadius(30f);
            chartTeachingLog.setHoleRadius(30f);
            data.setValueTextSize(13f);
            chartTeachingLog.setDescription("");
            data.setValueTextColor(Color.DKGRAY);
            chartTeachingLog.getLegend().setEnabled(false);
            chartTeachingLog.animateXY(1400, 1400);
            chartTeachingLog.setOnChartValueSelectedListener(TeachingLogCalendarSummaryActivity.this);

            //Calendar View
//            LoadTeachingLogSummary loadTeachingLogSummary = new LoadTeachingLogSummary();
//            loadTeachingLogSummary.execute(null, null);

            new LoadDayWiseAttd().execute();


//            calView.setShowOtherDates(0);
//            calView.setOnDateChangedListener(new OnDateSelectedListener() {
//                @Override
//                public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
//                    try {
//                        selectedDate = date.toString();
//                        int dd = date.getDay();
//                        int mm = date.getMonth() + 1;
//                        int yyyy = date.getYear();
//
//                        selectedDate = yyyy + "-" + mm + "-" + dd;
//
////                        startDate = yyyy + "-"+ mm + "-" + dd;
////                        endDate = yyyy + "-"+ mm + "-" + dd;
////
////                        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
////                        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
////                        Date compareDate = originalFormat.parse(selectedDate);
////                        selectedDate = targetFormat.format(compareDate);
//
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });


            //proceed
            btnProceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedDate == null) {
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Please select date!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                    else if(!presentStatus){
//                        Snackbar snackbar = Snackbar
//                                .make(layParent, "As you were not present for this day, you can't fill teaching log for the same!", Snackbar.LENGTH_LONG);
//                        snackbar.setDuration(3000);
//                        snackbar.show();
                        Toast.makeText(context, "As you were not present for this day, you can't fill teaching log for the same!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Intent intent = new Intent(TeachingLogCalendarSummaryActivity.this, TeachingLogDetails.class);
                    intent.putExtra("selectedDate", selectedDate);
                    intent.putExtra("selectedDateCompare",selectedDateCompare);
                    startActivity(intent);
                }
            });


        } catch (Exception e) {

            e.printStackTrace();
            Snackbar snackbar = Snackbar
                    .make(layParent, "Teaching log status not found!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
            layNoRecord.setVisibility(View.VISIBLE);
            layParent.setVisibility(View.GONE);
            util.SaveSystemLog(orgId, insId, "Error occur while loading teaching log history!", "Teacher Connect", usrId, "Mark Attendance");
            return;
        }


    }


    public class LoadTeachingLogSummary extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {



            try {
              // response = util.findTeachingLogForDay(orgId, insId, dscId,usrAssetId,ayr, startDate, endDate);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadDayWiseAttd extends AsyncTask<String, String, String> {
        public LoadDayWiseAttd() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();

            try {
                if (resDailyAttd != null) {

                    JSONObject jsonObject = new JSONObject(resDailyAttd);
                    JSONArray array = jsonObject.getJSONArray("staffAttd");

                    if (array.length() == 0) {
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                        //    Toast.makeText(context, "Daily attendance not marked!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Daily attendance not marked!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        util.SaveSystemLog(orgId,insId,"Daily attendance not marked","Teacher Connect",usrId,"My Attendance");
                        return;
                    }
                    dailyAttendanceArrayList = new ArrayList<>();
                    dates = new ArrayList<Date>();
                    datesSetForA = new HashSet<Date>();
                    datesSetForO = new HashSet<Date>();
                    datesSetForP = new HashSet<Date>();
                    datesSetForV = new HashSet<Date>();
                    datesSetForW = new HashSet<Date>();
                    datesSetForS = new HashSet<Date>();
                    datesSetForF = new HashSet<Date>();
                    datesSetForX = new HashSet<Date>();
                    hashMap = new HashMap();

                    JSONObject object1 = array.getJSONObject(0);
                    JSONObject object2 = array.getJSONObject(array.length()-1);
                    String ayrStartDate = object1.getString("date");
                    String ayrEndDate = object2.getString("date");

                    for (int i = 0; i < array.length(); i++) {

                        DailyAttendance dailyAttendance = new DailyAttendance();
                        JSONObject object = array.getJSONObject(i);

                        dailyAttendance.setDate(object.getString("date"));
                        dailyAttendance.setDesc(object.getString("desc"));
                        dailyAttendance.setStatus(object.getString("status"));
                        dailyAttendanceArrayList.add(dailyAttendance);
                        final String date1 = object.getString("date");

                        if (date1 == null) {

                        } else {

                            SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
                            stringDate = simpledateformat.parse(date1);
                            calView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
                            calView.setSelectionColor(Color.TRANSPARENT);
                            startDate =  simpledateformat.parse(ayrStartDate);
                            endDate = simpledateformat.parse(ayrEndDate);
                            calView.state().edit().setMaximumDate(endDate).commit();
                            calView.state().edit().setMinimumDate(startDate).commit();

                            // datesSet.add(stringDate);
                            if (object.getString("status").equals("O")) {
                                datesSetForO.add(stringDate);
                            } else if (object.getString("status").equals("A")) {
                                datesSetForA.add(stringDate);
                            } else if (object.getString("status").equals("P")) {
                                datesSetForP.add(stringDate);
                            } else if (object.getString("status").equals("V")) {
                                datesSetForV.add(stringDate);
                            } else if (object.getString("status").equals("W")) {
                                datesSetForW.add(stringDate);
                            } else if (object.getString("status").equals("S")) {
                                datesSetForS.add(stringDate);
                            } else if (object.getString("status").equals("F")) {
                                datesSetForF.add(stringDate);
                            } else if (object.getString("status").equals("X")) {
                                datesSetForX.add(stringDate);
                            }
                            dates.add(stringDate);
                            hashMap.put(stringDate, object.getString("desc"));
                        }

                    }


                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, datesSetForV, context, "V"));

                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, datesSetForA, context, "A"));
                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, datesSetForP, context, "P"));


                    calView.addDecorator(new BookingDecorator(R.drawable.circle_orange, datesSetForW, context, "W"));
                    calView.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForO, context, "O"));
                    calView.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForS, context, "S"));
                    calView.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForF, context, "F"));
                    calView.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForX, context, "X"));

                    calView.setOnDateChangedListener(new OnDateSelectedListener() {
                        @Override
                        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                            Log.d("SIZE", "" + dates.size());
                            try {

                                String DATE = date.toString();
                                String[] d = DATE.split("-");
                                int dd = date.getDay();

                                int mm = date.getMonth() + 1;
                                int yyyy = date.getYear();

                                DATE = yyyy + "-" + mm + "-" + dd;
                                selectedDate = DATE;
                                selectedDateCompare = dd+"-"+mm+"-"+yyyy;

                                String ketDate = yyyy + "-" + date.getMonth() + "-" + dd;

                                SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
                                //   calView.setSelected(false);
                                Date d1 = simpledateformat.parse(DATE);


                                if (dates.contains(d1)) {
                                    //Toast.makeText(application, "Right Value Holiday Present"+DATE, Toast.LENGTH_SHORT).show();

                                    if(datesSetForP.contains(d1)||datesSetForX.contains(d1)){
                                        presentStatus = true;
                                    }

                                } else {
                                    Snackbar snackbar = Snackbar
                                            .make(layParent, "Sorry! No information is available for this " + DATE, Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(3000);
                                    snackbar.show();
                                    snackbar.show();



                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance history","Teacher Connect",usrId,"My Attendance");
                            }
                        }
                    });

                } else {
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    //     Toast.makeText(context, "Daily attendance not marked!", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(layParent, "Daily attendance not marked!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();

                    util.SaveSystemLog(orgId,insId,"Daily attendance not marked","Teacher Connect",usrId,"My Attendance");
                    return;

                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance history","Teacher Connect",usrId,"My Attendance");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                // if (pastResDailyAttd == null || pastResDailyAttd.equals("")) {
                resDailyAttd = util.getDayWiseMonthInfo(orgId, insId, dscId, ayr, usrAssetId);

                SharedPreferences.Editor editor = context.getSharedPreferences("dailyAttd", context.MODE_PRIVATE).edit();
                editor.putString("response", resDailyAttd);
                editor.apply();
//                } else {
//                    resDailyAttd = pastResDailyAttd;
//                }

            } catch (Exception e) {
                e.printStackTrace();

                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance history","Teacher Connect",usrId,"My Attendance");
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    public void onValueSelected(Entry e, int i, Highlight highlight) {

    }

    @Override
    public void onNothingSelected() {

    }
}

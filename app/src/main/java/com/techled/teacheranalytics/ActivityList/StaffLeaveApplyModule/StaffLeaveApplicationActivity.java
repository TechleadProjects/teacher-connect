package com.techled.teacheranalytics.ActivityList.StaffLeaveApplyModule;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class
StaffLeaveApplicationActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private String whichHalfLeave;
    private String halfDayLeaveYn;
    private String woffHDayYn;
    private String applicationDate;
    private String fromDate;
    private String toDate;
    private String balance;
    private String leaveReason;
    private String leaveAddress;
    private int fromYear, fromMonth, fromDay;
    private int toYear, toMonth, toDay;
    private Integer lvrId;
    Button btnApply;
    Button btnFrom;
    Button btnTo;
    private Date dateFrom;
    private Date dateTo;
    private int orgId, insId, dscId, ayr, ugpId, usrId, usrAssetId;
    RadioGroup radioGroup;
    CheckBox chkNoSession;
    CheckBox chkFDLH;
    CheckBox chkLDFH;
    String bothHalfLeave = "";
    TextView lblSession;
    private String params;
    private String df, dt;
    private ProgressDialog progDailog;
    private String resApplyLeave, resMyInfo;
    private ImageView imgWOHL, imgHDL;
    private Dialog dialog;
    private String sendEmail = "N", sendSms = "N";
    private LinearLayout layParent;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // No session
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_application);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Apply Leave");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            try {

                context = this;
                util = new Util();

                Intent intent = getIntent();
                Bundle b = intent.getExtras();
                TextView txtLeaveFor = (TextView) findViewById(R.id.txtLeaveFor);
                txtLeaveFor.setText(b.getString("lvrName"));

                TextView txtAppDate = (TextView) findViewById(R.id.txtAppDate);
                Date d = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
                applicationDate = sdf.format(d);
                txtAppDate.setText(applicationDate);

                layParent = (LinearLayout)findViewById(R.id.layParent);

                //check internet connection
                CheckInternet checkInternet = new CheckInternet();
                boolean checkConnection = checkInternet.checkConnection(context);
                if (checkConnection) {

                } else {

                    Snackbar snackbar = Snackbar
                            .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

                //check poor internet connection
                CheckInternet checkPoorInternet = new CheckInternet();
                boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
                if (checkPoorConnection) {
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                }
                imgHDL = (ImageView) findViewById(R.id.imgHDL);
                imgWOHL = (ImageView) findViewById(R.id.imgWOHL);

                radioGroup = (RadioGroup) findViewById(R.id.radHalfLeave);
                radioGroup.setVisibility(RadioGroup.GONE);

                lblSession = (TextView) findViewById(R.id.lblHalfDay);
                lblSession.setVisibility(TextView.GONE);

                chkNoSession = (CheckBox) findViewById(R.id.chkNoSession);
                chkNoSession.setVisibility(CheckBox.GONE);
                chkNoSession.setChecked(true);

                chkFDLH = (CheckBox) findViewById(R.id.chkFDLH);
                chkFDLH.setVisibility(CheckBox.GONE);
                chkFDLH.setEnabled(false);
                chkLDFH = (CheckBox) findViewById(R.id.chkLDFH);
                chkLDFH.setVisibility(CheckBox.GONE);
                chkLDFH.setEnabled(false);

                halfDayLeaveYn = b.getString("halfDay");
                TextView txtHalfDay = (TextView) findViewById(R.id.txtHalfDay);
                if (halfDayLeaveYn.equals("Y")) {
                    txtHalfDay.setText("Yes");
                    imgHDL.setImageResource(R.drawable.ic_if_sign_check_299110);
                    //radioGroup.setVisibility(RadioGroup.VISIBLE);
                } else {
                    txtHalfDay.setText("No");
                    imgHDL.setImageResource(R.drawable.ic_if_close_309090);
                    //radioGroup.setVisibility(RadioGroup.GONE);
                }

                woffHDayYn = b.getString("woffHDay");
                TextView txtWoffHDay = (TextView) findViewById(R.id.txtWoffHDay);
                if (woffHDayYn.equals("Y")) {
                    txtWoffHDay.setText("Yes");
                    imgWOHL.setImageResource(R.drawable.ic_if_sign_check_299110);
                } else {
                    txtWoffHDay.setText("No");
                    imgWOHL.setImageResource(R.drawable.ic_if_close_309090);
                }

                balance = b.getString("leaveBalance");
                lvrId = b.getInt("lvrId");

                //login details
                try {
                    SharedPreferences loginDetails = getSharedPreferences("user", 0);
                    params = loginDetails.getString("params", null);
                    JSONArray jsonArray = new JSONArray(params);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {

                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");
                        JSONObject object = array.getJSONObject(0);

                        orgId = object.getInt("orgId");
                        insId = object.getInt("insId");
                        ayr = object.getInt("ayrYear");
                        dscId = object.getInt("dscId");
                        ugpId = object.getInt("ugpId");
                        usrId = object.getInt("usrId");
                        usrAssetId = object.getInt("assetId1");


                    }

                    SharedPreferences prefsForMyInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
                    if (prefsForMyInfo != null) {
                        resMyInfo = prefsForMyInfo.getString("response", null);
                    } else {
                        resMyInfo = util.getMyInfo(usrId, ayr);
                    }


                    if (resMyInfo != null) {
                        JSONArray array = new JSONArray(resMyInfo);
                        JSONObject jsonObject1 = array.getJSONObject(0);
                        String Status = jsonObject1.getString("status");
                        if (Status.equals("SUCCESS")) {
                            JSONObject jsonObject2 = array.getJSONObject(1);
                            JSONObject jsonObject = jsonObject2.getJSONObject("data");


                            if (jsonObject.getInt("payrollInsId") != 0) {
                                insId = jsonObject.getInt("payrollInsId");
                            }
                            if (jsonObject.getInt("payrollStfId") != 0) {
                                usrAssetId = jsonObject.getInt("payrollStfId");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (usrId == 0) {
                  /*  Toast.makeText(this,
                            "Staff is not associated with user!",
                            Toast.LENGTH_LONG).show();*/

                    Snackbar snackbar = Snackbar
                            .make(layParent, "Staff is not associated with user!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    onBackPressed();
                    return;
                }


                btnApply = (Button) findViewById(R.id.btnApplyLeave);
                btnApply.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        applyLeave();
                    }
                });

                final Calendar c = Calendar.getInstance();
                fromYear = c.get(Calendar.YEAR);
                fromMonth = c.get(Calendar.MONTH);
                fromDay = c.get(Calendar.DAY_OF_MONTH);

                toYear = c.get(Calendar.YEAR);
                toMonth = c.get(Calendar.MONTH);
                toDay = c.get(Calendar.DAY_OF_MONTH);

                btnFrom = (Button) findViewById(R.id.btnFromDate);
                btnFrom.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                                fromDateSetListener, fromYear, fromMonth, fromDay);
                        datePickerDialog.show();
                    }
                });

                btnTo = (Button) findViewById(R.id.btnToDate);
                btnTo.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                                toDateSetListener, toYear, toMonth, toDay);
                        datePickerDialog.show();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onRadioButtonClicked(View v) {
        switch (v.getId()) {
            case R.id.radFDFH:
                whichHalfLeave = "FF";
                break;
            case R.id.radFDLH:
                whichHalfLeave = "FL";
                break;
            case R.id.noSession:
                whichHalfLeave = "";
                break;
        }
    }

    public void onCheckBoxClicked(View v) {
        boolean checked = ((CheckBox) v).isChecked();
        switch (v.getId()) {
            case R.id.chkNoSession:
                if (checked) {
                    whichHalfLeave = "";
                    chkFDLH.setEnabled(false);
                    chkLDFH.setEnabled(false);
                    chkFDLH.setChecked(false);
                    chkLDFH.setChecked(false);
                } else {
                    whichHalfLeave = "";
                    chkFDLH.setEnabled(true);
                    chkLDFH.setEnabled(true);
                }
                break;
            case R.id.chkFDLH:
                if (checked) {
                    if (whichHalfLeave == null || whichHalfLeave.isEmpty()) {
                        whichHalfLeave = "FL";
                    } else {
                        whichHalfLeave += "-" + "FL";
                    }
                } else {
                    whichHalfLeave = "";
                }
                break;
            case R.id.chkLDFH:
                if (checked) {
                    if (whichHalfLeave == null || whichHalfLeave.isEmpty()) {
                        whichHalfLeave = "LF";
                    } else {
                        whichHalfLeave += "-" + "LF";
                    }
                } else {
                    whichHalfLeave = "";
                }
                break;

        }
    }

    public void applyLeave() {
        try {

            if (dateFrom == null) {
              /*  Toast.makeText(this, "Leave From-Date cannot be blank!", Toast.LENGTH_LONG)
                        .show();*/
                Snackbar snackbar = Snackbar
                        .make(layParent, "Leave From-Date cannot be blank!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            } else if (dateTo == null) {
               /* Toast.makeText(this, "Leave To-Date cannot be blank!", Toast.LENGTH_LONG)
                        .show();*/
                Snackbar snackbar = Snackbar
                        .make(layParent, "Leave To-Date cannot be blank!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;


            }
            EditText txtReason = (EditText) findViewById(R.id.txtReason);
            leaveReason = txtReason.getText().toString();

            if (leaveReason == null || leaveReason.isEmpty()) {
//                Toast.makeText(this, "Leave reason cannot be blank!", Toast.LENGTH_LONG)
//                        .show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Leave reason cannot be blank!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            EditText txtAddress = (EditText) findViewById(R.id.txtAddress);
            leaveAddress = txtAddress.getText().toString();

            if (leaveAddress == null || leaveAddress.isEmpty()) {
               /* Toast.makeText(this, "Address cannot be blank!", Toast.LENGTH_LONG)
                        .show();*/
                Snackbar snackbar = Snackbar
                        .make(layParent, "Address cannot be blank!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            df = sdf.format(dateFrom);
            dt = sdf.format(dateTo);


            dialog = new Dialog(context);
            dialog.setContentView(R.layout.confirm_send_sms_email);
            Window window = dialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

            dialog.show();

            final CheckBox chkSms = (CheckBox) dialog.findViewById(R.id.chkSms);
            final CheckBox chkEmail = (CheckBox) dialog.findViewById(R.id.chkEmail);

            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    //email

                    if (chkEmail.isChecked() == true) {
                        sendEmail = "Y";
                    } else {
                        sendEmail = "N";
                    }

                    //sms
                    if (chkSms.isChecked() == true) {
                        sendSms = "Y";
                    } else {
                        sendSms = "N";
                    }


                    //apply leave
                    StaffLeaveApplicationActivity.ApplyStaffLeave staffLeaveApply = new StaffLeaveApplicationActivity.ApplyStaffLeave();
                    staffLeaveApply.execute(null, null);

                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ApplyStaffLeave extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resApplyLeave.equals("") || resApplyLeave == null) {
                  //  Toast.makeText(context, "Failed to apply leave!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Failed to apply leave!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    util.SaveSystemLog(orgId,insId,"Failed to apply leave","Teacher Connect",usrId,"Leave application");
                    return;
                } else {
                    JSONObject status = new JSONObject(resApplyLeave);
                    String msg = status.getString("status");
                    if (msg.equals("LEAVE_SUCCESSFULLY_APPLIED")) {
//                        Toast.makeText(context, "Leave applied successfully!", Toast.LENGTH_LONG)
//                                .show();

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Leave applied successfully!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        Intent intent = new Intent(StaffLeaveApplicationActivity.this, LeaveStaffApplyActivity.class);
                        startActivity(intent);
                        finish();
                        // onBackPressed();
                    } else {
                     //   Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();

                        Snackbar snackbar = Snackbar
                                .make(layParent, ""+msg, Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while apply staff leave","Teacher Connect",usrId,"Leave application");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resApplyLeave = util.applyStaffLeave(orgId,
                        insId, dscId, ayr, usrAssetId, lvrId,
                        df, dt, balance, halfDayLeaveYn, whichHalfLeave, woffHDayYn,
                        leaveReason, leaveAddress, sendSms, sendEmail, "N");

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while apply staff leave","Teacher Connect",usrId,"Leave application");
            }
            return null;
        }
    }

    private DatePickerDialog.OnDateSetListener fromDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                fromYear = year;
                fromMonth = monthOfYear;
                fromDay = dayOfMonth;

                fromDate = String.valueOf(fromYear) + "-" + String.valueOf(fromMonth + 1)
                        + "-" + String.valueOf(fromDay);

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                cal.set(Calendar.DAY_OF_MONTH, fromDay);
                cal.set(Calendar.MONTH, fromMonth);
                cal.set(Calendar.YEAR, fromYear);
                cal.set(Calendar.HOUR, 00);
                cal.set(Calendar.MINUTE, 00);
                cal.set(Calendar.SECOND, 00);
                cal.set(Calendar.MILLISECOND, 00);
                dateFrom = cal.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                fromDate = sdf.format(dateFrom);

                TextView txtFromDate = (TextView) findViewById(R.id.txtFromDate);
                txtFromDate.setText(fromDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private DatePickerDialog.OnDateSetListener toDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                toYear = year;
                toMonth = monthOfYear;
                toDay = dayOfMonth;

                toDate = String.valueOf(toYear) + "-" + String.valueOf(toMonth + 1)
                        + "-" + String.valueOf(toDay);

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                cal.set(Calendar.DAY_OF_MONTH, toDay);
                cal.set(Calendar.MONTH, toMonth);
                cal.set(Calendar.YEAR, toYear);
                cal.set(Calendar.HOUR, 00);
                cal.set(Calendar.MINUTE, 00);
                cal.set(Calendar.SECOND, 00);
                cal.set(Calendar.MILLISECOND, 00);
                dateTo = cal.getTime();

                if (dateFrom.getTime() > dateTo.getTime()) {
                  /*  Toast.makeText(getApplicationContext(), "To Date Should be greater than From Date", Toast.LENGTH_LONG)
                            .show();*/

                    Snackbar snackbar = Snackbar
                            .make(layParent, "To Date Should be greater than From Date!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                toDate = sdf.format(dateTo);

                TextView txtToDate = (TextView) findViewById(R.id.txtToDate);
                txtToDate.setText(toDate);

                // Render RadioButton dependsOn date selection
                // If dates are equal then render radiobutton for half day leave selection
                if (halfDayLeaveYn.equals("Y")) {
                    lblSession.setVisibility(TextView.VISIBLE);
                    if (dateFrom.getTime() == dateTo.getTime()) {
                        radioGroup.setVisibility(RadioGroup.VISIBLE);
                        chkNoSession.setVisibility(CheckBox.GONE);
                        chkFDLH.setVisibility(CheckBox.GONE);
                        chkLDFH.setVisibility(CheckBox.GONE);
                    } else {
                        // render checkboxes for which half selections
                        radioGroup.setVisibility(RadioGroup.GONE);
                        chkNoSession.setVisibility(CheckBox.VISIBLE);
                        chkFDLH.setVisibility(CheckBox.VISIBLE);
                        chkLDFH.setVisibility(CheckBox.VISIBLE);
                    }
                } else {
                    whichHalfLeave = "";
                    lblSession.setVisibility(TextView.GONE);
                    radioGroup.setVisibility(RadioGroup.GONE);
                    chkNoSession.setVisibility(CheckBox.GONE);
                    chkFDLH.setVisibility(CheckBox.GONE);
                    chkLDFH.setVisibility(CheckBox.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_staff_leave_application, menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

}

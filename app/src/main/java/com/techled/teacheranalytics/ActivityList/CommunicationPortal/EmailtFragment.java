package com.techled.teacheranalytics.ActivityList.CommunicationPortal;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.EmailLogRecyclerAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.EmailLog;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class EmailtFragment extends Fragment {

    private Util util;
    private int orgId = 0, insId = 0, dscId = 0, ayr = 0, usrId = 0;
    private Context context;
    private String params, status;
    private LinearLayout layNoRecord, layParent;
    private ProgressDialog progDailog;
    private String resEmail, pastResEmail;
    private RecyclerView recycleView;
    private ArrayList<EmailLog> emailLogArrayList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_email_log, container, false);

        //get reference
        context = this.getActivity();
        util = new Util();
        recycleView = (RecyclerView) view.findViewById(R.id.recycleView);
        recycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(context);
        recycleView.setLayoutManager(layoutManager2);
        layParent = (LinearLayout) view.findViewById(R.id.layParent);
        layNoRecord = (LinearLayout) view.findViewById(R.id.layNoRecord);


        //login details
        try {
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
            }

            //get stored email data
            SharedPreferences emailPref = context.getSharedPreferences("email", context.MODE_PRIVATE);
            if (emailPref != null) {
                pastResEmail = emailPref.getString("response", null);
            }

            //load emails
            new LoadEmailLog().execute(null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    public class LoadEmailLog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(getActivity());
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();

        }

        @Override
        protected String doInBackground(String... aurl) {

            try {

                if (pastResEmail == null || pastResEmail.equals("")) {

                    resEmail = util.getEmailLog(orgId, insId, dscId, usrId, "E");

                    SharedPreferences.Editor editor = context.getSharedPreferences("email", context.MODE_PRIVATE).edit();
                    editor.putString("response", resEmail);
                    editor.apply();

                } else {
                    resEmail = pastResEmail;
                }

                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String unused) {
            super.onPostExecute(unused);
            progDailog.dismiss();
            try {

                emailLogArrayList = new ArrayList<EmailLog>();
                if (resEmail.equals("")) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    // Toast.makeText(context, "No Emails found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No Email found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;

                }
                if (resEmail != null) {

                    JSONArray jsonArray = new JSONArray(resEmail);

                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    status = jsonObject.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject jsonObject12 = jsonArray.getJSONObject(1);
                        JSONArray jsonArray1 = jsonObject12.getJSONArray("data");


                        for (int i = 0; i < jsonArray1.length(); i++) {
                            EmailLog emailLog = new EmailLog();
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            emailLog.setAscId(jsonObject1.getInt("aseId"));
                            emailLog.setAscTo(jsonObject1.getString("aseTo"));
                            emailLog.setAscCc(jsonObject1.getString("aseCc"));
                            emailLog.setAscBcc(jsonObject1.getString("aseBcc"));
                            emailLog.setAscSubject(jsonObject1.getString("aseSubject"));
                            emailLog.setAscMessage(jsonObject1.getString("aseMessage"));
                            emailLog.setAscTimestampRequested(jsonObject1.getString("aseTimestampRequested"));
                            emailLog.setAscTimestampProcessed(jsonObject1.getString("aseTimestampProcessed"));
                            emailLogArrayList.add(emailLog);

                        }
                        if (emailLogArrayList.size() == 0) {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            // Toast.makeText(context, "No Emails found!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "No Email found!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }


                    } else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        //  Toast.makeText(context, "No Emails found!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No Email found!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                } else {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    //   Toast.makeText(context, "No Emails found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No Email found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

                if (status.equals("FAILURE")) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    //  Toast.makeText(context, "No Emails found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No Email found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }
                if (emailLogArrayList != null) {

                    //set adapter
                    layParent.setVisibility(View.VISIBLE);
                    layNoRecord.setVisibility(View.GONE);

                    EmailLogRecyclerAdapter adapter = new EmailLogRecyclerAdapter(emailLogArrayList, context);
                    recycleView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    //  Toast.makeText(context, "No Emails found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No Email found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}

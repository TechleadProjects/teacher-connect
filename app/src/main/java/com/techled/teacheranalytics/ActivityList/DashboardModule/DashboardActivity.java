package com.techled.teacheranalytics.ActivityList.DashboardModule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.techled.teacheranalytics.ActivityList.AboutUsModule.AboutUsActivity;
import com.techled.teacheranalytics.ActivityList.AddRemarkModule.AddRemarkActivity;
import com.techled.teacheranalytics.ActivityList.CommunicationPortal.CommunicationPortalActivity;
import com.techled.teacheranalytics.ActivityList.EnterExamMarksModule.EnterExamMarksActivity;
import com.techled.teacheranalytics.ActivityList.EvaluateAnswerPaperModule.StandardDivisionWiseEvaluatePaperActivity;
import com.techled.teacheranalytics.ActivityList.EvaluationParameterModule.GetCategorySubCategoryActivity;
import com.techled.teacheranalytics.ActivityList.EventModule.EventsActivity;
import com.techled.teacheranalytics.ActivityList.ExamAnalysisModule.ExamAnalysisActivity;
import com.techled.teacheranalytics.ActivityList.GetOxymeterReadingModule.GetOxymeterReadingActivity;
import com.techled.teacheranalytics.ActivityList.LeaveTheSchoolRequestsModule.LeaveTheSchoolRequestsActivity;
import com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule.MarkAttendanceStatusActivity;
import com.techled.teacheranalytics.ActivityList.MyAttendanceModule.MainPagerScreenActivity;
import com.techled.teacheranalytics.ActivityList.MyInfoModule.MyInfoActivity;
import com.techled.teacheranalytics.ActivityList.MyLectures.MyLectures;
import com.techled.teacheranalytics.ActivityList.PerformanceModule.PerformanceAnalysisActivity;
import com.techled.teacheranalytics.ActivityList.ReportIssueModule.ViewReportIssueActivity;
import com.techled.teacheranalytics.ActivityList.SendNorificationBroadcastModule.NotificationBroadcastActivity;
import com.techled.teacheranalytics.ActivityList.SportAttendanceModule.SportAttendanceStatusHistoryActivity;
import com.techled.teacheranalytics.ActivityList.StaffLeaveApplyModule.StaffLeaveActivity;
import com.techled.teacheranalytics.ActivityList.StudentLeaveApprovalModule.StudentLeaveApprovalActivity;
import com.techled.teacheranalytics.ActivityList.SyllabusModule.SyllabusHomeActivity;
import com.techled.teacheranalytics.ActivityList.TaskManagementModule.AskForTaskFilterActivity;
import com.techled.teacheranalytics.ActivityList.TeachingLogModule.TeachingLogCalendarSummaryActivity;
import com.techled.teacheranalytics.ActivityList.TimetableModule.ViewTimeTableActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.ViewHomeworkActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.SplashScreenActivity;
import com.techled.teacheranalytics.TimelineHomeActivity;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Context context;
    private Util util;
    private ProgressDialog progressDialog;
    private String response;
    private boolean doubleBackToExitPressedOnce;
    public static final String PARAMS_FILE = "ParamsFile";
    private CircleImageView uImage;
    private TextView txtEmail, txtUContactNo;
    private int orgId, insId, ayr, usrId;
    private TextView txtName;
    private String isClassTeacher;
    private LinearLayout dashboardTimeline, dashboardMyAttendance, dashboardStudentAttendance, dashboardHomework, dashboardTimetable;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;
        util = new Util();

        try {

            SharedPreferences s3CredentialsPref = getSharedPreferences("user", 0);
            boolean s3Credentials = s3CredentialsPref.getBoolean("s3Credentials", false);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                //And finally ask for the permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                //And finally ask for the permission
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);
            navigationView.setItemTextAppearance(Typeface.BOLD);
            navigationView.setItemTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));

            //get ids
            uImage = navigationView.getHeaderView(0).findViewById(R.id.imageView);
            txtName = navigationView.getHeaderView(0).findViewById(R.id.txtName);
            txtUContactNo = navigationView.getHeaderView(0).findViewById(R.id.txtUContactNo);
            txtEmail = navigationView.getHeaderView(0).findViewById(R.id.txtUemail);
            TextView versionText = navigationView.getHeaderView(0).findViewById(R.id.versionText);

            try {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
                versionText.setText("v".concat(pInfo.versionName));

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                usrId = object.getInt("usrId");
            }

            hideItem();

            dashboardTimeline = findViewById(R.id.dashboardTimeline);
            dashboardMyAttendance = findViewById(R.id.dashboardMyAttendance);
            dashboardStudentAttendance = findViewById(R.id.dashboardStudentAttendance);
            dashboardHomework = findViewById(R.id.dashboardHomework);
            dashboardTimetable = findViewById(R.id.dashboardTimetable);

            dashboardTimeline.setOnClickListener(v -> {
                Intent intent = new Intent(DashboardActivity.this, TimelineHomeActivity.class);
                startActivity(intent);
            });

            dashboardMyAttendance.setOnClickListener(v -> {
                Intent intent = new Intent(DashboardActivity.this, MainPagerScreenActivity.class);
                startActivity(intent);
            });

            dashboardStudentAttendance.setOnClickListener(v -> {
                if (isClassTeacher.equals("Y")) {
                    Intent intent = new Intent(DashboardActivity.this, MarkAttendanceStatusActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "Only class teachers or administrators can mark attendance!", Toast.LENGTH_SHORT).show();

                }
            });

            dashboardHomework.setOnClickListener(v -> {
                Intent intent = new Intent(DashboardActivity.this, ViewHomeworkActivity.class);
                startActivity(intent);
            });

            dashboardTimetable.setOnClickListener(v -> {
                Intent intent = new Intent(DashboardActivity.this, ViewTimeTableActivity.class);
                startActivity(intent);
            });

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

                if (!s3Credentials) {
                    new GetS3CredentialsTask().execute();
                } else {
                    getLoginParams();
                }

            } else {
                Toast.makeText(context,
                        "Please check your internet connection or try again later!", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void hideItem() {
        //provided permission to only rkvm
        if (orgId == 27 || orgId == 22) {
            NavigationView navigationView = findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.examMarksEntry).setVisible(true);
        } else {
            NavigationView navigationView = findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.examMarksEntry).setVisible(false);
        }
    }

    public void getLoginParams() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        SharedPreferences loginDetails = getSharedPreferences("user", 0);

        boolean isDataAvailableForToday = loginDetails.getBoolean(formatter.format(new Date()).concat("@loginParams"), false);

        if (!isDataAvailableForToday) {

            new LoadCurrentDataForUser().execute();
        } else {
            loadUserDetails();
        }
    }

    public class LoadCurrentDataForUser extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (response != null) {
                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        SharedPreferences loginDetails = getSharedPreferences("user", 0);
                        loginDetails.edit().putString("params", response).commit();

                        new LoadCurrentUserData().execute();
                    }

                } else {

                    Toast.makeText(context, "No data found.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getLatestParamsVersion2(usrId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadCurrentUserData extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading User Details..");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (response != null) {
                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        SharedPreferences userDetails = getSharedPreferences("MyInfo", 0);
                        userDetails.edit().putString("response", response).commit();

                        SharedPreferences loginDetails = getSharedPreferences("user", 0);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        loginDetails.edit().putBoolean(formatter.format(new Date()).concat("@loginParams"), true).commit();

                        loadUserDetails();
                    }

                } else {
                    Toast.makeText(context, "No data found.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getMyInfo(usrId, ayr);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class GetS3CredentialsTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (response != null) {
                    JSONObject object1 = new JSONObject(response);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        String accessKey = object1.getString("accessKey");
                        String secretKey = object1.getString("secretKey");

                        final SharedPreferences sharedPreferences = getSharedPreferences("user", 0);
                        sharedPreferences.edit().putBoolean("s3Credentials", true).commit();
                        sharedPreferences.edit().putString("secretKey", secretKey).commit();
                        sharedPreferences.edit().putString("accessKey", accessKey).commit();

                        getLoginParams();

                    }

                } else {
                    Toast.makeText(context, "No data found.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getAwsS3Credentials();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void loadUserDetails() {
        try {

            SharedPreferences loginDetails = getSharedPreferences("MyInfo", 0);
            String resUserInfo = loginDetails.getString("response", null);

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");

                if (Status.equals("SUCCESS")) {

                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");

                    txtName.setText(jsonObject.getString("usrName"));
                    String uName = jsonObject.getString("usrName");
                    String uEmail = jsonObject.getString("usrEmail");
                    String uContactNo = jsonObject.getString("usrContact");

                    isClassTeacher = jsonObject.getString("isClassTeacher");
                    String classTeacherOf = jsonObject.getString("classTeacherOf");
                    String subTeacherOf = jsonObject.getString("subTeacherOf");
                    jsonObject.getString("applicableStds");

                    //email address
                    if (uEmail.equals("")) {
                        txtEmail.setText("Email : -");

                    } else {
                        txtEmail.setText("" + uEmail);
                    }

                    //contact number
                    if (uContactNo.equals("") || uContactNo == null) {
                        txtUContactNo.setText("Contact Number : -");
                    } else {
                        txtUContactNo.setText("" + uContactNo);
                    }

                    //image
                    byte[] byteData = null;
                    try {
                        String imageString = jsonObject.getString("usrImg");
                        if (imageString.equals("noimage")) {
                            Bitmap noImage = BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.defaultuser);

                            uImage.setImageBitmap(noImage);
                        } else {
                            // Image Setting
                            String[] id = jsonObject.getString("usrImg").split(",");
                            byteData = new byte[id.length];
                            for (int i = 0; i < id.length; i++) {

                                byteData[i] = Byte.valueOf(id[i]);
                            }

                            if (byteData != null) {
                                ByteArrayInputStream bais = new ByteArrayInputStream(
                                        byteData);
                                Bitmap btMap = BitmapFactory.decodeStream(bais);

                                Bitmap circleBitmap = Bitmap.createBitmap(btMap.getWidth(), btMap.getHeight(), Bitmap.Config.ARGB_8888);

                                BitmapShader shader = new BitmapShader(btMap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                                Paint paint = new Paint();
                                paint.setShader(shader);

                                Canvas c = new Canvas(circleBitmap);
                                c.drawCircle(btMap.getWidth() / 2, btMap.getHeight() / 2, btMap.getWidth() / 2, paint);
                                uImage.setImageBitmap(btMap);

                            } else {
                                Bitmap noImage = BitmapFactory.decodeResource(context.getResources(),
                                        R.drawable.defaultuser);

                                uImage.setImageBitmap(noImage);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        byteData = null;
                    }

                    boolean isTimeline = getIntent().getBooleanExtra("timeline", false);
                    if (isTimeline) {
                        Intent intent = new Intent(DashboardActivity.this, TimelineHomeActivity.class);
                        startActivity(intent);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        //teaching logs
        if(id == R.id.btnTeachingLog)
        {
            Intent intent = new Intent(DashboardActivity.this, TeachingLogCalendarSummaryActivity.class);
            startActivity(intent);
        }

        //live lecture
        if(id == R.id.taskManagement)
        {
            Intent intent = new Intent(DashboardActivity.this, AskForTaskFilterActivity.class);
            startActivity(intent);

        }

        if(id == R.id.sportAttendance)
        {
            Intent intent = new Intent(DashboardActivity.this, SportAttendanceStatusHistoryActivity.class);
            startActivity(intent);
        }

        if(id == R.id.evaluateAnsPapers)
        {

//            itemSelected = 0;
//            String[] singleChoiceItems = new String[]{"Standard Division wise evaluate papers", "Level wise evaluate papers"};
//            new androidx.appcompat.app.AlertDialog.Builder(context)
//                    .setTitle("Evaluate Answer Papers")
//                    .setSingleChoiceItems(singleChoiceItems, itemSelected, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//
//                            itemSelected = i;
//                        }
//                    })
//                    .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogs, int which) {
//                            if (itemSelected == 0) {
//
//                                dialogs.dismiss();
//
//                                Intent intent = new Intent(DashboardActivity.this, StandardDivisionWiseEvaluatePaperActivity.class);
//                                startActivity(intent);
//
//                            } else if (itemSelected == 1) {
//
//                                dialogs.dismiss();
//
//                                Intent intent = new Intent(DashboardActivity.this, LevelWiseEvaluatePaperActivity.class);
//                                startActivity(intent);
//                            }
//                        }
//                    })
//                    .setNegativeButton("Cancel", null)
//                    .show();

            Intent intent = new Intent(DashboardActivity.this, StandardDivisionWiseEvaluatePaperActivity.class);
            startActivity(intent);
        }

        //school leave requests
        if(id == R.id.btnStuLeaveSchoolRequest)
        {
            Intent intent = new Intent(DashboardActivity.this, LeaveTheSchoolRequestsActivity.class);
            startActivity(intent);
        }
        //report issue
        if(id == R.id.btnReportIssue)
        {
            Intent intent = new Intent(DashboardActivity.this, ViewReportIssueActivity.class);
            startActivity(intent);
        }

        //live lecture
        if(id == R.id.timetable)
        {
            Intent intent = new Intent(DashboardActivity.this, ViewTimeTableActivity.class);
            startActivity(intent);

        }

        //performance academic
        if (id == R.id.acdPerf) {
            Intent intent = new Intent(DashboardActivity.this, PerformanceAnalysisActivity.class);
            startActivity(intent);
        }

        //mark attendance
        if (id == R.id.attendance) {
            if (isClassTeacher.equals("Y")) {
                Intent intent = new Intent(DashboardActivity.this, MarkAttendanceStatusActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(context, "Only class teachers or administrators can mark attendance!", Toast.LENGTH_SHORT).show();

            }
        }
        //student leave approval
        if (id == R.id.leaveApprove) {
            Intent intent = new Intent(DashboardActivity.this, StudentLeaveApprovalActivity.class);
            startActivity(intent);
        }
        //comm portal
        if (id == R.id.commPortal) {
            Intent intent = new Intent(DashboardActivity.this, CommunicationPortalActivity.class);
            startActivity(intent);

        } else if (id == R.id.examMarksEntry) {

            //only for rkvm institute
            if (orgId == 27 || orgId == 22) {

          /*  boolean perm = false;
            for (PermissionData pd : permissions) {
                if (pd.getModuleName().equals("Enter Exam Marks")) {
                    perm = pd.isView();
                    break;
                }
            }

            if (!perm) {
                Toast.makeText(getBaseContext(), "Sorry!! You are yet to be given permission to access this facility by your school!!", Toast.LENGTH_LONG).show();
                return false;
            }*/
                Intent intent = new Intent(DashboardActivity.this, EnterExamMarksActivity.class);
                startActivity(intent);

            } else {
                Toast.makeText(context, "You don't have a permission to enter exam marks!", Toast.LENGTH_SHORT).show();
            }

        }
        //my attendance
        else if (id == R.id.myAttendance) {
            Intent intent = new Intent(DashboardActivity.this, MainPagerScreenActivity.class);
            startActivity(intent);

        }
        //homework
        else if (id == R.id.homework) {
            Intent intent = new Intent(DashboardActivity.this, ViewHomeworkActivity.class);
            startActivity(intent);
        }
        //about
        else if (id == R.id.about) {
            try {

                Intent intent = new Intent(DashboardActivity.this, AboutUsActivity.class);
                startActivity(intent);

            } catch (Exception e) {
                e.printStackTrace();

            }

        }

        //oxymeter reading
        else if (id == R.id.oxymeterReading) {
            Intent intent = new Intent(DashboardActivity.this, GetOxymeterReadingActivity.class);
            startActivity(intent);
        }

        //evaluation parameter
        else if (id == R.id.evaluationParameter) {
            Intent intent = new Intent(DashboardActivity.this, GetCategorySubCategoryActivity.class);
            startActivity(intent);
        }

        //my lectures
        else if (id == R.id.btnMyLectures) {
            Intent intent = new Intent(DashboardActivity.this, MyLectures.class);
            startActivity(intent);
        }

        //my account
        else if (id == R.id.myAccount) {
            Intent intent = new Intent(DashboardActivity.this, MyInfoActivity.class);
            startActivity(intent);
        }

        //notification
        else if (id == R.id.notification) {
            Intent intent = new Intent(DashboardActivity.this, NotificationBroadcastActivity.class);
            startActivity(intent);
        }

        //marked attd
        else if (id == R.id.markedAttdStatus) {
            Intent intent = new Intent(DashboardActivity.this, MarkAttendanceStatusActivity.class);
            startActivity(intent);
        }

        //leave apply
        else if (id == R.id.leaveApply) {
            Intent intent = new Intent(DashboardActivity.this, StaffLeaveActivity.class);
            startActivity(intent);
            // finish();
        }
        //remark
        else if (id == R.id.remark) {
            Intent intent = new Intent(DashboardActivity.this, AddRemarkActivity.class);
            startActivity(intent);
        }
        //exam Analysis
        else if (id == R.id.examAnalysis) {
            Intent intent = new Intent(DashboardActivity.this, ExamAnalysisActivity.class);
            startActivity(intent);
        }

        else if(id == R.id.events)
        {
            Intent intent = new Intent(DashboardActivity.this, EventsActivity.class);
            startActivity(intent);

        }
        else if(id == R.id.btnSyllabus)
        {
            Intent intent = new Intent(DashboardActivity.this, SyllabusHomeActivity.class);
            startActivity(intent);
        }

        //logout
        else if (id == R.id.logout) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(DashboardActivity.this);

            // Setting Dialog Title
            alertDialog.setTitle("Logout...");

            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want to logout?");

            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.ic_logout);

            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        dialog.dismiss();
                        //login info
                        SharedPreferences loginDetails = getSharedPreferences("user", 0);
                        loginDetails.edit().clear().commit();

                        //myInfo
                        SharedPreferences myInfo = getSharedPreferences("MyInfo", MODE_PRIVATE);
                        myInfo.edit().clear().commit();

                        //daily attendance
                        SharedPreferences dailyAttd = getSharedPreferences("dailyAttd", MODE_PRIVATE);
                        dailyAttd.edit().putString("response", "").commit();

                        //monthly attendance
                        SharedPreferences monthlyAttd = getSharedPreferences("monthlyAttd", MODE_PRIVATE);
                        monthlyAttd.edit().putString("response", "").commit();

                        //sms
                        SharedPreferences smsPref = getSharedPreferences("sms", MODE_PRIVATE);
                        smsPref.edit().putString("response", "").commit();

                        //email
                        SharedPreferences emailPref = getSharedPreferences("email", MODE_PRIVATE);
                        emailPref.edit().putString("response", "").commit();

                        SharedPreferences settings = getSharedPreferences(PARAMS_FILE, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("SERVER_URL", "-");

                        //save user log

                        //regImei = regImei + " - " + currentVersion;
                        //String resUserLog = util.saveUserEventLog(orgId, insId, dscId, ugpId, usrId, uName, regImei, "", "LOGOUT", "N");

                        //fcm unregister
                        String response = util.setAndroidAnalyticsRegId(usrId, "-", orgId, insId);

                        //continue from splash
                        Intent intent = new Intent(DashboardActivity.this, SplashScreenActivity.class);
                        startActivity(intent);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    dialog.cancel();
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

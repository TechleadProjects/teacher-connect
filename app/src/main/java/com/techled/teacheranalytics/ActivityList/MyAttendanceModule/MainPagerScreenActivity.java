package com.techled.teacheranalytics.ActivityList.MyAttendanceModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;

import me.relex.circleindicator.CircleIndicator;

public class MainPagerScreenActivity extends AppCompatActivity {
    FragmentPagerAdapter adapterViewPager;
    private ViewPager vpPager;
    private ImageView btnRefresh;
    private Context context;
    private Dialog dialog;
    private LinearLayout layParent;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_pager_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Attendance");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        context = this;
        layParent = (LinearLayout)findViewById(R.id.layParent);

        //check internet connection
        CheckInternet checkInternet = new CheckInternet();
        boolean checkConnection = checkInternet.checkConnection(context);
        if (checkConnection) {

        } else {

            Snackbar snackbar = Snackbar
                    .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
            return;
        }

        //check poor internet connection
        CheckInternet checkPoorInternet = new CheckInternet();
        boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
        if (checkPoorConnection) {
            Snackbar snackbar = Snackbar
                    .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
        }

        //get reference
        vpPager = (ViewPager) findViewById(R.id.vpPager);
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setCurrentItem(0);
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(vpPager);

        ImageView imgRefresh = (ImageView) findViewById(R.id.imgRefresh);
        imgRefresh.setVisibility(View.VISIBLE);
        imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_confirm_refresh);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    dialog.setCanceledOnTouchOutside(false);


                    Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
                    Button btnNo = (Button) dialog.findViewById(R.id.btnNo);

                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            SharedPreferences.Editor editor = getSharedPreferences("dailyAttd", MODE_PRIVATE).edit();
                            editor.putString("response", null);
                            editor.apply();
                            SharedPreferences.Editor editor2 = getSharedPreferences("monthlyAttd", MODE_PRIVATE).edit();
                            editor2.putString("response", null);
                            editor2.apply();
                            Intent intent = new Intent(MainPagerScreenActivity.this, MainPagerScreenActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 2;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {

                case 0: // Fragment # 0 - This will show FirstFragment different title
                    return MyAttendanceFragment.newInstance(0, "Monthly Attendance");
                case 1: // Fragment # 1 - This will show SecondFragment
                    return MyAttendanceGraphFragment.newInstance(1, "Total Percentage");
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment different title
                    return "Monthly Attendance";
                case 1: // Fragment # 1 - This will show SecondFragment
                    return "Total Percentage";
            }
            return null;
        }

    }

}

package com.techled.teacheranalytics.ActivityList.EventModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.UpcomingEventsRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.EventsData;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class UpcommingEventFragment extends Fragment {


    private Util util;
    private RecyclerView eventListView;
    private Context context;
    public static Integer UGP_ID;
    private LinearLayout layout2;
    private String params;
    private int orgId = 0, insId = 0, dscId = 0, ayrYear = 0, usrId = 0, ugpId = 0;
    private LinearLayout layNoRecord, layParent;
    private ProgressDialog progDailog;
    private String response;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_upcomming_event, container, false);


        //get reference
        context = getActivity().getApplicationContext();
        util = new Util();

        eventListView = (RecyclerView) view.findViewById(R.id.eventsListView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        eventListView.setLayoutManager(layoutManager);
        layParent = (LinearLayout) view.findViewById(R.id.layParent);
        layNoRecord = (LinearLayout) view.findViewById(R.id.layNoRecord);
        String events = null;

        //login details
        try {
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayrYear = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");

            }


            //get upcoming events
            loadEventsData();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    public void loadEventsData() {
        new LoadUpcommingEvents().execute();

    }

    public class LoadUpcommingEvents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(getActivity());
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response.isEmpty()) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                 /*   Toast.makeText(context, "No upcoming events!", Toast.LENGTH_SHORT)
                            .show();*/
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No upcoming events!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }
                if (response.equals("Failure")) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    //  Toast.makeText(context, "No upcoming events!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No upcoming events!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }
                JSONArray mArray = new JSONArray(response);

                JSONObject jsonStatus = mArray.getJSONObject(0);
                JSONObject dataObj = mArray.getJSONObject(1);
                String status = jsonStatus.getString("status");
                if (status.equalsIgnoreCase("failure")) {
                    String msg = dataObj.getString("data");
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                  /*  Toast.makeText(context, "No upcoming events!", Toast.LENGTH_LONG)
                            .show();
*/
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No upcoming events!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

                JSONArray array = dataObj.getJSONArray("data");

                if (array == null) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
//                    Toast.makeText(context, "No upcoming events!", Toast.LENGTH_LONG)
//                            .show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No upcoming events!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

                ArrayList<EventsData> eventList = new ArrayList<EventsData>();

                /*********** Process each JSON Node ************/

                int lengthJsonArr = array.length();

                for (int index = 0; index < lengthJsonArr; index++) {
                    /****** Get Object for each JSON node. ***********/
                    JSONObject jsonChildNode = array.getJSONObject(index);

                    EventsData event = new EventsData();
                    event.setOrgId(jsonChildNode.getInt("orgId"));
                    event.setInsId(jsonChildNode.getInt("insId"));
                    event.setDscId(jsonChildNode.getInt("dscId"));
                    event.setAyrYear(jsonChildNode.getInt("ayrYear"));
                    event.setEvtId(jsonChildNode.getInt("evtId"));
                    event.setTitle(jsonChildNode.getString("evtTitle"));
                    event.setOrganizedBy(jsonChildNode.getString("evtOrgnizer"));
                    event.setFrom(jsonChildNode.getString("evtFrom"));
                    event.setTo(jsonChildNode.getString("evtTo"));
                    event.setDescription(jsonChildNode.getString("evtDesc"));
                    event.setVenue(jsonChildNode.getString("evtVenue"));
                    event.setEvtDate(jsonChildNode.getString("evtDate"));
                    /* get image data */
                    if (jsonChildNode.getString("evtImg").equalsIgnoreCase(
                            "noimage")) {
                        event.setImage(null);

                    } else {
                        String[] id = jsonChildNode.getString("evtImg").split(",");
                        byte[] byteData = new byte[id.length];
                        for (int i = 0; i < id.length; i++) {
                            byteData[i] = Byte.valueOf(id[i]);
                        }
                        event.setImage(byteData);
                    }
                    eventList.add(event);

                }
                if (eventList.size() == 0) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
//                    Toast.makeText(context, "No upcoming events!", Toast.LENGTH_LONG)
//                            .show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No upcoming events!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

                //set adapter
                layParent.setVisibility(View.VISIBLE);
                layNoRecord.setVisibility(View.GONE);
                UpcomingEventsRecyAdapter adapter = new UpcomingEventsRecyAdapter(eventList);
                eventListView.setAdapter(adapter);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                response = util.getUpcomingEvents(orgId, insId,
                        dscId, ayrYear, "E", usrId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}

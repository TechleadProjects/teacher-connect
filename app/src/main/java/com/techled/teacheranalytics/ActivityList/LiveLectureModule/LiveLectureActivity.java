package com.techled.teacheranalytics.ActivityList.LiveLectureModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.LiveLectureRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.LiveLecture;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class LiveLectureActivity extends AppCompatActivity {

    private LinearLayout layNoRecord, layParent;
    private Context context;
    private Util util;
    private ProgressDialog progDailog = null;
    private RecyclerView liveLectureRecyView;
    private String params;
    private int orgId, insId, dscId;
    private String resLiveLecture;
    private ArrayList<LiveLecture> liveLectureArrayList;

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_lecture);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Live Lecture");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //get reference
        context = this;
        util = new Util();
        liveLectureRecyView = (RecyclerView) findViewById(R.id.liveLectureRecyView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        liveLectureRecyView.setLayoutManager(linearLayoutManager);
        layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
        layParent = (LinearLayout) findViewById(R.id.layParent);


        //check internet connection
        CheckInternet checkInternet = new CheckInternet();
        boolean checkConnection = checkInternet.checkConnection(context);
        if (checkConnection) {

        } else {

            Snackbar snackbar = Snackbar
                    .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
            return;
        }

        //check poor internet connection
        CheckInternet checkPoorInternet = new CheckInternet();
        boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
        if (checkPoorConnection) {
            Snackbar snackbar = Snackbar
                    .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
        }


        //login details
        try {
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");


            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        //load lecture
        LoadLiveLecture loadLiveLecture = new LoadLiveLecture();
        loadLiveLecture.execute(null, null);

    }

    public class LoadLiveLecture extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setCancelable(false);
            progDailog.setCanceledOnTouchOutside(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (resLiveLecture != null) {
                    JSONArray jsonArray = new JSONArray(resLiveLecture);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");
                        liveLectureArrayList = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);
                            LiveLecture liveLecture = new LiveLecture();
                            liveLecture.setSubName(object.getString("subName"));
                            liveLecture.setStdName(object.getString("stdName"));
                            liveLecture.setDivName(object.getString("divName"));
                            liveLecture.setStfName(object.getString("stfName"));
                            liveLecture.setStartTime(object.getString("startTime"));
                            liveLecture.setEndTime(object.getString("endTime"));
                            liveLectureArrayList.add(liveLecture);
                        }
                        if (liveLectureArrayList.size() == 0) {
                            //   Toast.makeText(context, "No Live lecture!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "No Live lecture!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            layNoRecord.setVisibility(View.VISIBLE);
                            layParent.setVisibility(View.GONE);
                            return;

                        }
                        //set adapter
                        layNoRecord.setVisibility(View.GONE);
                        layParent.setVisibility(View.VISIBLE);
                        LiveLectureRecyAdapter adapter = new LiveLectureRecyAdapter(liveLectureArrayList, context);
                        liveLectureRecyView.setAdapter(adapter);

                    } else {
                        //    Toast.makeText(context, "No Live lecture!", Toast.LENGTH_SHORT).show();

                        Snackbar snackbar = Snackbar
                                .make(layParent, "No Live lecture!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                        return;
                    }

                } else {
                    //Toast.makeText(context, "No Live lecture!", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(layParent, "No Live lecture!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                // Toast.makeText(context, "No Live lecture!", Toast.LENGTH_SHORT).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "No Live lecture!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                layNoRecord.setVisibility(View.VISIBLE);
                layParent.setVisibility(View.GONE);
                util.SaveSystemLog(orgId,insId,"Error occur while fetching live lecture details","Teacher Connect",0,"Live Lecture");
                return;
            }
        }

        @Override
        protected String doInBackground(String... strings) {


            try {
                resLiveLecture = util.getLiveLecture(orgId, insId, dscId);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while fetching live lecture details","Teacher Connect",0,"Live Lecture");
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (progDailog != null) {
                progDailog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (progDailog != null) {
                progDailog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (progDailog != null) {
                progDailog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.techled.teacheranalytics.ActivityList.ForgotPasswordModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgotPasswordActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private EditText schoolCodeEditText, userNameEditText, emailIdEditText;
    private String schoolCodeText, userNameText, emailIdText;
    String response;
    private ProgressDialog progDailog;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Forgot Password");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //get reference
        context = this;
        util = new Util();

        schoolCodeEditText = findViewById(R.id.schoolCode);
        userNameEditText = findViewById(R.id.userName);
        emailIdEditText = findViewById(R.id.emailId);
    }

    public void submitForgotPassword(View view) {

        schoolCodeText = schoolCodeEditText.getText().toString().trim();
        userNameText = userNameEditText.getText().toString().trim();
        emailIdText = emailIdEditText.getText().toString().trim();

        if(schoolCodeText.equals("") || userNameText.equals("") || emailIdText.equals("")){
            Toast.makeText(context, "Please don't leave any field blank..", Toast.LENGTH_SHORT).show();
        }else if(!validateEmailAddress(emailIdText)) {
            Toast.makeText(context, "Please enter valid email address..", Toast.LENGTH_SHORT).show();
        }else {
            new ForgotPasswordTask().execute();
        }
    }

    public boolean validateEmailAddress(String email){
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public class ForgotPasswordTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (response != null) {
                    JSONArray resArray = new JSONArray(response);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        Toast.makeText(context, "Password reset successfully! New password will be send to your email address.", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(context, "Invalid Details. Please make sure the entered details are valid and try again.", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(context, "Something went wrong! Please try again later..", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = util.forgotPassword(emailIdText, userNameText, schoolCodeText);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
package com.techled.teacheranalytics.ActivityList.EvaluateAnswerPaperModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.StudentSelectionActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.StudentSelectionForEvaluationRecyclerAdapter;
import com.techled.teacheranalytics.adapter.UploadedDocsForEvaluationRecyclerAdapter;
import com.techled.teacheranalytics.pojo.EvaluateAnsPaperStudentData;
import com.techled.teacheranalytics.pojo.Student;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class StudentSelectionForEvaluateAnsPaper extends AppCompatActivity {

    private Context context;
    private ArrayList<EvaluateAnsPaperStudentData> studentsList;
    private Util util;

    private RecyclerView recyclerView;
    private StudentSelectionForEvaluationRecyclerAdapter adapter;

    private int orgId, insId, ayrYear, dscId, usrId, stfId;
    private ProgressDialog progDailog;
    private String response;

    private String selectedAyrYear, selectedStandard, selectedDivision, selectedSubject, selectedExamType, selectedExamSchedule, stuId, examName, stuName;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_selection_for_evaluate_ans_paper);

        try {

            context = this;
            util = new Util();

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Student Selection");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            studentsList = new ArrayList<>();

            recyclerView = findViewById(R.id.recyclerView);

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            String studentData = getIntent().getStringExtra("studentsList");

            selectedAyrYear = getIntent().getStringExtra("ayrYear");
            selectedStandard = getIntent().getStringExtra("standard");
            selectedDivision = getIntent().getStringExtra("division");
            selectedSubject = getIntent().getStringExtra("subject");
            selectedExamType = getIntent().getStringExtra("examType");
            selectedExamSchedule = getIntent().getStringExtra("examSchedule");
            examName = getIntent().getStringExtra("examName");

            if (studentData != null && !studentData.equals("")) {

                Type type = new TypeToken<List<EvaluateAnsPaperStudentData>>(){}.getType();
                Gson gson = new Gson();
                studentsList = gson.fromJson(studentData, type);

                adapter = new StudentSelectionForEvaluationRecyclerAdapter(context, studentsList, selectedAyrYear, selectedStandard, selectedDivision, selectedSubject, selectedExamType, selectedExamSchedule, examName);
                recyclerView.setAdapter(adapter);

            } else {

                Toast.makeText(context, "Students list not found. Please try again later.", Toast.LENGTH_SHORT).show();
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        try {

            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayrYear = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");

                stfId = object.getInt("assetId1");

            }

            new LoadStudents().execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class LoadStudents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Students...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if ((response == null || response.equals(""))) {
                    Toast.makeText(context, "Failed to load students.", Toast.LENGTH_SHORT).show();

                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    studentsList = new ArrayList<>();

                    if (status.equals("SUCCESS")) {

                        studentsList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            EvaluateAnsPaperStudentData evaluateAnsPaperStudentData = new EvaluateAnsPaperStudentData();
                            evaluateAnsPaperStudentData.setStuId(jsonObject1.getString("stuId"));
                            evaluateAnsPaperStudentData.setRollNo(jsonObject1.getString("rollNo"));
                            evaluateAnsPaperStudentData.setStuName(jsonObject1.getString("stuName"));
                            evaluateAnsPaperStudentData.setUploadStatus(jsonObject1.getString("uploadStatus"));
                            evaluateAnsPaperStudentData.setMarks(jsonObject1.getString("marks"));
                            evaluateAnsPaperStudentData.setEvaluationStatus(jsonObject1.getString("evaluationStatus"));

                            studentsList.add(evaluateAnsPaperStudentData);
                        }

                        if(studentsList.size() > 0){

                            Gson gson = new Gson();
                            String json = gson.toJson(studentsList);

                            Intent intent = new Intent(context, StudentSelectionForEvaluateAnsPaper.class);
                            intent.putExtra("studentsList", json);
                            intent.putExtra("ayrYear", selectedAyrYear);
                            intent.putExtra("standard", selectedStandard);
                            intent.putExtra("division", selectedDivision);
                            intent.putExtra("subject", selectedSubject);
                            intent.putExtra("examType", selectedExamType);
                            intent.putExtra("examSchedule", selectedExamSchedule);
                            intent.putExtra("examName", examName);
                            startActivity(intent);
                            finish();

                        }else {

                            Toast.makeText(context, "Students not found for current selection.", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    } else {

                        Toast.makeText(context, "Students not found for current selection.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

                Toast.makeText(context, "Failed to load students.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getStudentExmDocs(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), selectedDivision, selectedStandard, selectedAyrYear, String.valueOf(selectedSubject), String.valueOf(stfId), selectedExamSchedule);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
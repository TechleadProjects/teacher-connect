package com.techled.teacheranalytics.ActivityList.SyllabusModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.DashboardModule.DashboardActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadHomeworkActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.SyllabusDetailsRecyAdapter;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.Standards;
import com.techled.teacheranalytics.pojo.SubjectDetails;
import com.techled.teacheranalytics.pojo.SyllabusDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SyllabusHomeActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private Dialog dialog;
    private ProgressDialog progDailog;
    private Spinner ayrYearSpinner, stdSpinner, subjectSpinner;
    private String resAcdemicYear, pastResAcdemicYear;
    private int orgId, insId, dscId, ayr, usrId, usrAssetId, subId, stdId;
    private String[] academicYears;
    private int[] ayrs;
    private JSONArray classTeacherOfJson;
    private String resUserInfo, resSubject;
    private ArrayList<SubjectDetails> subjectDetailsArrayList;
    private ArrayList<Standards> standardDivisionArrayList;
    private ArrayList<SyllabusDetails> syllabusDetailsArrayList;
    private RecyclerView syllabusRecyView;
    private LinearLayout layParent, layNoRecord;
    private Button btnAddNewSyllabus;
    private Button btnCancel;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus_home);

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Syllabus");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            syllabusRecyView = (RecyclerView) findViewById(R.id.syllabusRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            syllabusRecyView.setLayoutManager(layoutManager);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
            }

            dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_selection_details);
            dialog.show();
            Window window = dialog.getWindow();
            dialog.setCancelable(false);
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            dialog.setCanceledOnTouchOutside(false);

            ayrYearSpinner = (Spinner) dialog.findViewById(R.id.ayrYearSpinner);
            stdSpinner = (Spinner) dialog.findViewById(R.id.stdSpinner);
            subjectSpinner = (Spinner) dialog.findViewById(R.id.subjectSpinner);

            //load academic years
            LoadAcdYears loadAcdYears = new LoadAcdYears();
            loadAcdYears.execute(null, null);

            //load standard/division
            //get data
            SharedPreferences prefUserInfo = getSharedPreferences("MyInfo", MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("applicableStds");

                }
            }

            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
                Toast.makeText(this,
                        "Error to load standard/division!",
                        Toast.LENGTH_LONG).show();
                return;
            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                Standards standards = new Standards();
                standards.setStdId(object.getString("value"));
                standards.setStdName(object.getString("label"));

                standardDivisionArrayList.add(standards);
            }


            //button ok
            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (ayr == 0) {
                        Toast.makeText(context, "Please select academic year!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (stdId == 0) {
                        Toast.makeText(context, "Please select standard!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (subId == 0) {
                        Toast.makeText(context, "Please select subject!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    dialog.dismiss();


                    //load syllabus details
                    layParent.setVisibility(View.VISIBLE);
                    layNoRecord.setVisibility(View.GONE);
                    LoadSyllabusDetails loadSyllabusDetails = new LoadSyllabusDetails();
                    loadSyllabusDetails.execute(null, null);

                }
            });

            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(SyllabusHomeActivity.this, DashboardActivity.class);
                    startActivity(intent);
                    finish();

                }
            });


            FloatingActionButton btnAddNewSyllabus = (FloatingActionButton) findViewById(R.id.btnAddNewSyllabus);
            btnAddNewSyllabus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(SyllabusHomeActivity.this, AddNewUnitActivity.class);
                    intent.putExtra("ayr", ayr);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("subId", subId);
                    startActivity(intent);

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadStd() {
        //set adapter to standard spinner

        ArrayAdapter<Standards> stdDivAdapter = new ArrayAdapter<Standards>(SyllabusHomeActivity.this,
                android.R.layout.simple_spinner_item, standardDivisionArrayList);
        stdDivAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stdSpinner.setAdapter(stdDivAdapter);

        //get standard id
        stdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                stdId = Integer.parseInt(standardDivisionArrayList.get(position).getStdId());

                //load subjects
                if (stdId != 0) {
                    LoadSubjects loadSubjects = new LoadSubjects();
                    loadSubjects.execute(null, null);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public class LoadSyllabusDetails extends AsyncTask<String, String, String> {
        private String resSyllabus;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resSyllabus != null) {
                    JSONArray resArray = new JSONArray(resSyllabus);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        JSONArray dataArray = dataObj.getJSONArray("data");

                        syllabusDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            SyllabusDetails syllabusDetails = new SyllabusDetails();
                            JSONObject object = dataArray.getJSONObject(i);

                            syllabusDetails.setUniNumber(object.getString("uniNumber"));
                            syllabusDetails.setUniDescription(object.getString("uniDescription"));
                            //  syllabusDetails.setTerId(object.getInt("terId"));
                            syllabusDetails.setUniId(object.getInt("uniId"));
                            String uniCompletionDate = object.getString("uniCompletionDate");

                            //set from - to date
                            if (uniCompletionDate == null || uniCompletionDate.equals(";") || uniCompletionDate.equals("")
                                    ) {
                                syllabusDetails.setToDate("");
                                syllabusDetails.setFromDate("");
                            } else {
                                String completionDate[] = uniCompletionDate.split(";");

                                if (completionDate[0].equals("")) {

                                    syllabusDetails.setFromDate("");
                                } else {

                                    syllabusDetails.setFromDate(completionDate[0]);
                                }

                                if(completionDate.length > 1) {
                                    if (completionDate[1].equals("")) {
                                        syllabusDetails.setToDate("");
                                    } else {
                                        syllabusDetails.setToDate(completionDate[1]);
                                    }
                                }
                            }

                            //show activeYn
                            syllabusDetails.setShowActiveYn("");
                            syllabusDetails.setShowContentYn("");
                            syllabusDetails.setCount("");
                            syllabusDetailsArrayList.add(syllabusDetails);

                        }

                        //set adapter
                        if (syllabusDetailsArrayList.size() > 0) {
                            layNoRecord.setVisibility(View.GONE);
                            layParent.setVisibility(View.VISIBLE);
                            SyllabusDetailsRecyAdapter adapter = new SyllabusDetailsRecyAdapter(orgId, insId, dscId, ayr, stdId, subId, syllabusDetailsArrayList, context);
                            syllabusRecyView.setAdapter(adapter);
                        } else {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Syllabus not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Syllabus not found!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resSyllabus = util.getSyallabusDetails(orgId, insId, dscId, ayr, stdId, subId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadSubjects extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (resSubject == null || resSubject.equals("")) {
                    Toast.makeText(context, "Unable to load subject details!", Toast.LENGTH_SHORT).show();

                } else {

                    JSONObject jsonObject = new JSONObject(resSubject);
                    JSONArray subArray = jsonObject.getJSONArray("subject");
                    if (subArray.length() != 0) {
                        subjectDetailsArrayList = new ArrayList<>();

                        for (int i = 0; i < subArray.length(); i++) {
                            SubjectDetails subjectDetails = new SubjectDetails();
                            JSONObject object = subArray.getJSONObject(i);
                            subjectDetails.setSubId(object.getInt("subId"));
                            subjectDetails.setSubName(object.getString("subName"));
                            subjectDetailsArrayList.add(subjectDetails);


                        }

                        //set adapter
                        if (subjectDetailsArrayList.size() != 0) {
                            ArrayAdapter<SubjectDetails> ayrAdaptor = new ArrayAdapter<SubjectDetails>(SyllabusHomeActivity.this,
                                    android.R.layout.simple_spinner_item, subjectDetailsArrayList);
                            ayrAdaptor
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            subjectSpinner.setAdapter(ayrAdaptor);
                        } else {
                            Toast.makeText(context, "Unable to load subject details!", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(context, "Unable to load subject details!", Toast.LENGTH_SHORT).show();

                    }

                    subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position != 0) {
                                subId = subjectDetailsArrayList.get(position).getSubId();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resSubject = util.getStaffClassSubjects(orgId, insId, dscId, ayr, usrAssetId, stdId, 0);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class LoadAcdYears extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                if (resAcdemicYear.equals("") || resAcdemicYear == null) {
                    Toast.makeText(context, "Academic year not found!", Toast.LENGTH_SHORT).show();
                } else {

                    String[] splitIns = resAcdemicYear.split(";");

                    academicYears = new String[splitIns.length];
                    ayrs = new int[splitIns.length];

                    /* fill organization, orgs list */
                    for (int iterator = 0; iterator < splitIns.length; iterator++) {
                        String[] record = splitIns[iterator].split(":");

                        academicYears[iterator] = record[0];
                        ayrs[iterator] = Integer.valueOf(record[1]);
                    }

                    /* create adaptor for dsc spinner */
                    ArrayAdapter<String> ayrAdaptor = new ArrayAdapter<String>(SyllabusHomeActivity.this,
                            android.R.layout.simple_spinner_item, academicYears);
                    ayrAdaptor
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ayrYearSpinner.setAdapter(ayrAdaptor);

                    ayrYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ayr = ayrs[position];

                            //load standards
                            if (ayr != 0) {
                                loadStd();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                //
                // if (pastResAcdemicYear == null || pastResAcdemicYear.equals("")) {

                resAcdemicYear = util.getAcademicYears(orgId, insId, dscId);

                    /*SharedPreferences.Editor syllabusPref = getSharedPreferences("AYR_YEAR", MODE_PRIVATE).edit();
                    syllabusPref.putString("Response", resAcdemicYear);
                    syllabusPref.commit();

                } else {

                    resAcdemicYear = pastResAcdemicYear;
                }
*/

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}

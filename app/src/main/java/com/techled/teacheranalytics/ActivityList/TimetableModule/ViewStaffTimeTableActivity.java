package com.techled.teacheranalytics.ActivityList.TimetableModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.StaffTimetableRecyAdapter;
import com.techled.teacheranalytics.pojo.MyLectureDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewStaffTimeTableActivity extends AppCompatActivity {

    private List<MyLectureDetails> timetableList = new ArrayList<>();
    private List<MyLectureDetails> detailTimetableList = new ArrayList<>();
    private RecyclerView staffTimeatbleRecyView;
    private StaffTimetableRecyAdapter mStaffTimeTableAdapter;
    private ProgressDialog progDailog;
    private Context context;
    private Util util;
    private int orgId, insId, dscId, ayr, usrAssetId;
    private String startDate, endDate;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_staff_time_table);

        context = this;
        util = new Util();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Time table");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        try {
            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrAssetId = object.getInt("assetId1");
                ayr = object.getInt("ayrYear");

            }

            startDate = getIntent().getStringExtra("weekStart");
            endDate = getIntent().getStringExtra("weekEnd");

            staffTimeatbleRecyView = findViewById(R.id.staffTimetableRecyView);

            timetableList = new ArrayList<>();
            detailTimetableList = new ArrayList<>();
            mStaffTimeTableAdapter = new StaffTimetableRecyAdapter(detailTimetableList, timetableList);
            staffTimeatbleRecyView.setLayoutManager(new LinearLayoutManager(this));
            staffTimeatbleRecyView.setItemAnimator(new DefaultItemAnimator());
            staffTimeatbleRecyView.setAdapter(mStaffTimeTableAdapter);

            new LoadMyLecturesDetails().execute();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class LoadMyLecturesDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        //JSONObject dataObject = dataObj.getJSONObject("data");
                        JSONArray dataArray1 = dataObj.getJSONArray("data");
                        if(dataArray1.length() == 0)
                        {
                            Toast.makeText(context, "Timetable not found!", Toast.LENGTH_SHORT).show();
                            finish();
                            return;
                        }

                        for (int p=0; p<dataArray1.length(); p++){
                            JSONObject dataObj1 = dataArray1.getJSONObject(p);
                            JSONArray dataArray = dataObj1.getJSONArray("logList");

                            MyLectureDetails myLectureDetails = new MyLectureDetails();
                            myLectureDetails.setDayName(dataObj1.getString("day"));

                            detailTimetableList.add(myLectureDetails);

                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject object = dataArray.getJSONObject(i);

                                MyLectureDetails myLectureDetails2 = new MyLectureDetails();
                                myLectureDetails2.setDiv(object.getString("divName"));
                                myLectureDetails2.setLectureEndTime(object.getString("lectureEndTime"));
                                myLectureDetails2.setLectureStartTime(object.getString("lectureStartTime"));
                                myLectureDetails2.setSubName(object.getString("subName"));
                                myLectureDetails2.setStd(object.getString("stdName"));
                                myLectureDetails2.setDayName(dataObj1.getString("day"));
                                myLectureDetails.setScheduledDate(dataObj1.getString("scheduledDate"));

                                timetableList.add(myLectureDetails2);
                            }
                        }

                        if (timetableList.size() > 0){
                            mStaffTimeTableAdapter.notifyDataSetChanged();
                        }

                    } else {
                        Toast.makeText(context, "Timetable not found!", Toast.LENGTH_SHORT).show();
                        finish();
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resMyLectures = util.findTeachingLogForDay(orgId, insId, dscId, usrAssetId, ayr, startDate, endDate);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

package com.techled.teacheranalytics.ActivityList.ExamAnalysisModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.ExamAnalysisRecyAdapter;
import com.techled.teacheranalytics.pojo.ExamAnalysisData;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class ExamAnalysisActivity extends AppCompatActivity {

    private Context context;
    private Util util;

    private SimpleDateFormat formatter;

    private int orgId, insId, dscId, ayrYear;
    private String response, selectedDate;
    private static RecyclerView.Adapter adapter;
    private static RecyclerView recyclerView;
    private static ArrayList<ExamAnalysisData> examAnalysisDataArrayList;
    private ProgressDialog progressDialog;
    private LinearLayout layParent;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_analysis);

        try {

            context = this;
            util = new Util();

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Exam Analysis");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            formatter = new SimpleDateFormat("yyyy-MM-dd");

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayrYear = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
            }

            layParent = findViewById(R.id.layParent);
            recyclerView = findViewById(R.id.recycler_view);

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            showDatePicker();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class GetExamAnalysisData extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (response != null) {
                    JSONArray array = new JSONArray(response);

                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {

                        examAnalysisDataArrayList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray arrayJSONObject = object2.getJSONArray("data");
                        if(arrayJSONObject.length() > 0) {
                            for (int i=0; i<arrayJSONObject.length(); i++) {

                                JSONObject jsonObject = arrayJSONObject.getJSONObject(i);

                                ExamAnalysisData examAnalysisData = new ExamAnalysisData();
                                examAnalysisData.setClassName(jsonObject.getString("Class"));
                                examAnalysisData.setExamName(jsonObject.getString("Exam Name"));
                                examAnalysisData.setTotalStudentCount(jsonObject.getInt("TotalStudentCount"));
                                examAnalysisData.setStudentsAppearedCount(jsonObject.getInt("studentsAppearedCount"));

                                String studentsNotAppeared = jsonObject.getString("studentsNotAppeared");

                                if (!studentsNotAppeared.equals("")) {
                                    String[] studentsList = studentsNotAppeared.split(",");
                                    ArrayList<String> studentsNotAppearedList = new ArrayList<>(Arrays.asList(studentsList));
                                    examAnalysisData.setStudentsNotAppeared(studentsNotAppearedList);
                                }

                                examAnalysisDataArrayList.add(examAnalysisData);
                            }

                            if(examAnalysisDataArrayList.size() > 0) {

                                layParent.setVisibility(View.VISIBLE);
                                adapter = new ExamAnalysisRecyAdapter(context, examAnalysisDataArrayList);
                                recyclerView.setAdapter(adapter);
                            } else {

                                layParent.setVisibility(View.GONE);
                                Toast.makeText(context, "Data not found!", Toast.LENGTH_SHORT).show();
                                showDatePicker();
                            }
                        } else {

                            layParent.setVisibility(View.GONE);
                            showDatePicker();
                        }
                    } else {
                        Toast.makeText(context, "Data not found!", Toast.LENGTH_SHORT).show();
                        showDatePicker();
                    }

                } else {
                    Toast.makeText(context, "Couldn't reach servers at the moment. Please try again later.", Toast.LENGTH_SHORT).show();
                    finish();
                }

            } catch (Exception e) {
                e.printStackTrace();

                layParent.setVisibility(View.GONE);
                Toast.makeText(context, "Data not found!", Toast.LENGTH_SHORT).show();
                showDatePicker();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getExamStudentAnalysis(orgId, insId, dscId, ayrYear, selectedDate);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        finish();
        startActivity(getIntent());
    }

    public void showDatePicker() {

        final Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            selectedDate = formatter.format(myCalendar.getTime());

            new GetExamAnalysisData().execute();

//            ExamAnalysisData examAnalysisData1 = new ExamAnalysisData();
//            examAnalysisData1.setClassName("Class A");
//            examAnalysisData1.setExamName("Exam A");
//            examAnalysisData1.setStudentsAppearedCount(1);
//            examAnalysisData1.setTotalStudentCount(30);
//
//            String studentsNotAppeared = "I-A-NISHANT OJHA,I-A-OM PANDEY,I-A-POORVI SHARMA,I-A-PREKSHA JAIN,I-A-PRIYANSHU GURJAR,I-A-RAJVEER SINGH,I-A-RAM PALIYA,I-A-ROHIT SOLANKI,I-A-RONIT UCHARIYA,I-A-RUDRA PRATAP PARIHAR,I-A-SANJANA DANDOTIYA,I-A-SANJANA SINGH,I-A-SHOMYA SINGH";
//            String[] studentsList = studentsNotAppeared.split(",");
//            ArrayList<String> studentsNotAppearedList = new ArrayList<>(Arrays.asList(studentsList));
//            examAnalysisData1.setStudentsNotAppeared(studentsNotAppearedList);
//
//            ExamAnalysisData examAnalysisData2 = new ExamAnalysisData();
//            examAnalysisData2.setClassName("Class B");
//            examAnalysisData2.setExamName("Exam B");
//            examAnalysisData2.setStudentsAppearedCount(5);
//            examAnalysisData2.setTotalStudentCount(30);
//
//            String studentsNotAppeared2 = "I-A-ROHIT SOLANKI,I-A-RONIT UCHARIYA,I-A-RUDRA PRATAP PARIHAR,I-A-SANJANA DANDOTIYA,I-A-SANJANA SINGH,I-A-SHOMYA SINGH,I-A-SOURYA PATHAK,I-A-SUMIT SEJWAR,I-A-TANISHK PANDEY,I-A-TANVI PAL,I-A-TANVI SHARMA";
//            String[] studentsList2 = studentsNotAppeared2.split(",");
//            ArrayList<String> studentsNotAppearedList2 = new ArrayList<>(Arrays.asList(studentsList2));
//            examAnalysisData2.setStudentsNotAppeared(studentsNotAppearedList2);
//
//            examAnalysisDataArrayList = new ArrayList<>();
//            examAnalysisDataArrayList.add(examAnalysisData1);
//            examAnalysisDataArrayList.add(examAnalysisData2);
//
//            layParent.setVisibility(View.VISIBLE);
//            adapter = new ExamAnalysisRecyAdapter(context, examAnalysisDataArrayList);
//            recyclerView.setAdapter(adapter);

        };

        // TODO Auto-generated method stub
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setCanceledOnTouchOutside(false);
        datePickerDialog.setCancelable(false);
        datePickerDialog.show();

        datePickerDialog.setOnCancelListener(dialogInterface -> finish());
    }
}
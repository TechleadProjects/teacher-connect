package com.techled.teacheranalytics.ActivityList.PerformanceModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.SubjectTeacherRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.SubjectTeacherDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SubjectTeacherInfoActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private int stuId;
    private String params;
    private ImageView btnRefresh;
    private int orgId = 0, insId = 0, dscId = 0, ayrYear = 0, stdId = 0, divId = 0, subId = 0;
    private ProgressDialog progDailog;
    private String stdName, divName, subName;
    private String response;
    private RecyclerView subjectTeacherRecyView;
    private TextView txtStdDiv, txtSubject;
    private ArrayList<SubjectTeacherDetails> teacherDetailsArrayList;
    private LinearLayout layParent;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_teacher);
        try {

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Subject Teacher Info");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            layParent = (LinearLayout) findViewById(R.id.layParent);


            //login details
            try {
                SharedPreferences loginDetails = getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);

                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);


                    //org shared pref
                    SharedPreferences orgPref = getSharedPreferences("Organization", 0);
                    if (orgPref != null) {
                        if (!orgPref.getString("Org_Id", "0").equals("")) {
                            orgId = Integer.parseInt(orgPref.getString("Org_Id", "0"));

                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        } else {
                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        }

                    }
                    //ins shared pref
                    SharedPreferences insPref = getSharedPreferences("Institute", 0);
                    if (insPref != null) {
                        if (!insPref.getString("Ins_Id", "0").equals("")) {
                            insId = Integer.parseInt(insPref.getString("Ins_Id", "0"));
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        } else {
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        }
                    }
                    ayrYear = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            //get intent data
            Intent intent = getIntent();
            if (intent != null) {
                stdId = intent.getIntExtra("stdId", 0);
                divId = intent.getIntExtra("divId", 0);
                subId = intent.getIntExtra("subId", 0);
                stdName = intent.getStringExtra("stdName");
                divName = intent.getStringExtra("divName");
                subName = intent.getStringExtra("subName");
                ayrYear = intent.getIntExtra("ayrYear", 0);

            }

            //get reference
            txtStdDiv = (TextView) findViewById(R.id.txtStdDiv);
            txtSubject = (TextView) findViewById(R.id.txtSubjectInfo);
            subjectTeacherRecyView = (RecyclerView) findViewById(R.id.subjectTeacherRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            subjectTeacherRecyView.setLayoutManager(layoutManager);
            txtStdDiv.setText(stdName + " " + divName);
            txtSubject.setText(subName);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //load data
            SubjectTeacherInfoActivity.loadDetails details = new SubjectTeacherInfoActivity.loadDetails();
            details.execute(null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class loadDetails extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading");
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response != null) {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {
                        teacherDetailsArrayList = new ArrayList<>();
                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            SubjectTeacherDetails teacherDetails = new SubjectTeacherDetails();
                            teacherDetails.setStfName(object.getString("usrFullName"));
                            teacherDetails.setStfContactNo(object.getString("usrContactNo"));
                            teacherDetails.setStfAddress(object.getString("usrAddressLine1") + " " + object.getString("usrCity"));
                            teacherDetails.setStfEmail(object.getString("usrEmailId"));
                            teacherDetails.setStfImage(object.getString("stuImage"));
                            teacherDetailsArrayList.add(teacherDetails);

                        }
                        //set adapter
                        SubjectTeacherRecyAdapter adapter = new SubjectTeacherRecyAdapter(context, teacherDetailsArrayList);
                        subjectTeacherRecyView.setAdapter(adapter);


                    } else {
                        // Toast.makeText(context, "Unable to load subject teacher information!", Toast.LENGTH_SHORT).show();

                        Snackbar snackbar = Snackbar
                                .make(layParent, "Unable to load subject teacher information!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                } else {
                    //  Toast.makeText(context, "Unable to load subject teacher information!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Unable to load subject teacher information!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                response = util.getTeacherBySubStdDiv(orgId, insId, dscId, ayrYear, stdId, divId, subId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}

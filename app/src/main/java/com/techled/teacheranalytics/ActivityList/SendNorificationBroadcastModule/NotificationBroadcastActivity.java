package com.techled.teacheranalytics.ActivityList.SendNorificationBroadcastModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.NotificationBroadcastRecyAdapater;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.NotificationBroadcastDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationBroadcastActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private Util util;
    private Context context;
    private LinearLayout layParent, layNoRecord;
    private ProgressDialog progDailog;
    private RecyclerView notificationRecyView;
    private int usrId,orgId,insId,ayr,dscId;
    private String resNotifications;
    private ArrayList<NotificationBroadcastDetails> notificationBroadcastDetailsArrayList;
    private FloatingActionButton btnAddNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_broadcast);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Notifications");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        //get reference
        context = this;
        util = new Util();

        layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
        layParent = (LinearLayout) findViewById(R.id.layParent);
        btnAddNotification = (FloatingActionButton) findViewById(R.id.btnAddNotification);
        notificationRecyView = (RecyclerView) findViewById(R.id.notificationRecyView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        notificationRecyView.setLayoutManager(layoutManager);

        //check internet connection
        CheckInternet checkInternet = new CheckInternet();
        boolean checkConnection = checkInternet.checkConnection(context);
        if (checkConnection) {

        } else {

            Snackbar snackbar = Snackbar
                    .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
            return;
        }

        //check poor internet connection
        CheckInternet checkPoorInternet = new CheckInternet();
        boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
        if (checkPoorConnection) {
            Snackbar snackbar = Snackbar
                    .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
        }



        //login details
        try {
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");

            }

            //load notifications details

            LoadNotificationSentDetails notificationSentDetails = new LoadNotificationSentDetails();
            notificationSentDetails.execute(null, null);

            //add new notification
            btnAddNotification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(NotificationBroadcastActivity.this, AddNotificationToActivity.class);
                    startActivity(intent);
                  //  finish();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public class LoadNotificationSentDetails extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resNotifications != null) {
                    JSONArray resArray = new JSONArray(resNotifications);

                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject datatObject = resArray.getJSONObject(1);
                        JSONArray dataArray = datatObject.getJSONArray("data");

                        notificationBroadcastDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {

                            JSONObject dataObject = dataArray.getJSONObject(i);
                            NotificationBroadcastDetails notificationBroadcastDetails = new NotificationBroadcastDetails();
                            notificationBroadcastDetails.setNntDateTime(dataObject.getString("nntDateTime"));
                            notificationBroadcastDetails.setNntDescription(dataObject.getString("nntDescription"));
                            notificationBroadcastDetails.setNntTitle(dataObject.getString("nntTitle"));
                            notificationBroadcastDetails.setNntSendTo(dataObject.getString("nntSendTo"));
                            notificationBroadcastDetails.setNntUrl(dataObject.getString("nntUrl"));
                            notificationBroadcastDetails.setNnIndentifier(dataObject.getString("tilIdentifier"));
                            notificationBroadcastDetails.setNntType(dataObject.getString("nntType"));
                            notificationBroadcastDetailsArrayList.add(notificationBroadcastDetails);

                        }

                        if (notificationBroadcastDetailsArrayList.size() != 0) {
                            //set adapter
                            layParent.setVisibility(View.VISIBLE);
                            layNoRecord.setVisibility(View.GONE);
                            NotificationBroadcastRecyAdapater adapater = new NotificationBroadcastRecyAdapater(notificationBroadcastDetailsArrayList, context,orgId,insId,ayr);
                            notificationRecyView.setAdapter(adapater);
                        } else {
                           // Toast.makeText(context, "No notification found!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "No notification found!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            util.SaveSystemLog(orgId,insId,"No notification found","Teacher Connect",usrId,"Notification Broadcast");
                            return;
                        }
                    } else {
                        //Toast.makeText(context, "No notification found!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No notification found!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        util.SaveSystemLog(orgId,insId,"No notification found","Teacher Connect",usrId,"Notification Broadcast");
                        return;
                    }
                } else {
                   // Toast.makeText(context, "No notification found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No notification found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    util.SaveSystemLog(orgId,insId,"No notification found","Teacher Connect",usrId,"Notification Broadcast");
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading notification history","Teacher Connect",usrId,"Notification Broadcast");
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resNotifications = util.getNotificationBroadcast(usrId);

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading notification history","Teacher Connect",usrId,"Notification Broadcast");
            }
            return null;
        }
    }
}

package com.techled.teacheranalytics.ActivityList.MyInfoModule;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyInfoActivity extends AppCompatActivity {
    private Util util;
    private Context context;
    private String params;
    private int usrId, ayr;
    private CircleImageView usrImg;
    private String uName, imageString;
    private String response;
    private LinearLayout layParent;


    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_info);
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("My Account");
            toolbar.setTitleTextColor(Color.WHITE);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            util = new Util();
            context = this;


            TextView changePwd = (TextView) findViewById(R.id.changePwd);
            changePwd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(MyInfoActivity.this, PasswordChangeActivity.class);
                    intent.putExtra("usrId", usrId);
                    intent.putExtra("name", uName);
                    intent.putExtra("image", imageString);
                    startActivity(intent);

                }
            });
            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);
                ayr = object.getInt("ayrYear");
                usrId = object.getInt("usrId");
            }

            //get reference
            usrImg = (CircleImageView) findViewById(R.id.usrImg);
            TextView myName = (TextView) findViewById(R.id.viewMyName);
            TextView myAddress = (TextView) findViewById(R.id.viewMyAddress);
            TextView myEmail = (TextView) findViewById(R.id.viewMyEmail);
            TextView myContact = (TextView) findViewById(R.id.viewMyContact);
            TextView myDob = (TextView) findViewById(R.id.viewMyDob);
            TextView myDoj = (TextView) findViewById(R.id.viewMyDoj);
            TextView myRegNo = (TextView) findViewById(R.id.viewMyRegNo);
            TextView insName = (TextView) findViewById(R.id.insName);
            TextView orgName = (TextView) findViewById(R.id.orgName);
            TextView stdDivName = (TextView) findViewById(R.id.stdDivName);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            //get data
            SharedPreferences prefsForStdDiv = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefsForStdDiv != null) {
                response = prefsForStdDiv.getString("response", null);
            } else {
                response = util.getMyInfo(usrId, ayr);
            }


            if (response != null) {
                JSONArray array = new JSONArray(response);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");

                    //orgName
                    String org = jsonObject.getString("orgName");
                    orgName.setText("" + org);

                    //institute
                    String ins = jsonObject.getString("insName");
                    insName.setText("" + ins);

                    //standard/division
                    String std = jsonObject.getString("usrStd");
                    String div = jsonObject.getString("usrDiv");
                    stdDivName.setText(std + " , " + div);
                    //user name
                    uName = jsonObject.getString("usrName");
                    myName.setText(uName);

                    //address
                    String uAddress = jsonObject.getString("usrAddress");
                    String myAddr = "<html><body</body></html>" + uAddress;
                    if (uAddress

                            .equals("")) {
                        myAddress.setText("Address: Not Applicable");

                    } else {
                        myAddress.setText("Address: " + Html.fromHtml(myAddr));
                    }

                    //contact no
                    String uContctNo = jsonObject.getString("usrContact");
                    String myMob = "" + uContctNo;
                    if (uContctNo.equals("")) {
                        myContact.setText("Contact Number: Not Applicable");
                    } else {
                        myContact.setText("Contact Number: " + Html.fromHtml(myMob));
                    }


                    //email
                    String uEmail = jsonObject.getString("usrEmail");
                    String myMailId = "<html><body></body></html>" + uEmail;
                    if (uEmail.equals("")) {
                        myEmail.setText("Email: Not Applicable");
                    } else {
                        myEmail.setText("Email: " + Html.fromHtml(myMailId));
                    }

                    //regno
                    String uRegNo = jsonObject.getString("usrRegNo");
                    String regNo = "<html><body></body></html>" + uRegNo;
                    if (uRegNo.equals("")) {

                        myRegNo.setText("Reg No: Not Applicable");
                    } else {
                        myRegNo.setText("Reg No: " + Html.fromHtml(regNo));
                    }


                    //dob
                    String uDob = jsonObject.getString("usrDob");
                    String dob = "<html><body></body></html>" + uDob;
                    if (uDob.equals("")) {

                        myDob.setText("D.O.B.: Not Applicable");
                    } else {
                        myDob.setText("D.O.B.: " + Html.fromHtml(dob));
                    }

                    //Doj
                    String uDoj = jsonObject.getString("usrJoiningDate");
                    String doj = "<html><body></body></html>" + uDoj;
                    if (uDoj.equals("")) {
                        myDoj.setText("D.O.J.: Not Applicable");

                    } else {
                        myDoj.setText("D.O.J.: " + Html.fromHtml(doj));
                    }

                    //image
                    byte[] byteData = null;
                    try {
                        imageString = jsonObject.getString("usrImg");
                        // Image Setting
                        String[] id = jsonObject.getString("usrImg").split(",");
                        byteData = new byte[id.length];
                        for (int i = 0; i < id.length; i++) {

                            byteData[i] = Byte.valueOf(id[i]);
                        }
                    } catch (Exception e) {
                        byteData = null;
                    }
                    if (byteData != null) {
                        ByteArrayInputStream bais = new ByteArrayInputStream(
                                byteData);
                        Bitmap btMap = BitmapFactory.decodeStream(bais);

                        Bitmap circleBitmap = Bitmap.createBitmap(btMap.getWidth(), btMap.getHeight(), Bitmap.Config.ARGB_8888);

                        BitmapShader shader = new BitmapShader(btMap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                        Paint paint = new Paint();
                        paint.setShader(shader);

                        Canvas c = new Canvas(circleBitmap);
                        c.drawCircle(btMap.getWidth() / 2, btMap.getHeight() / 2, btMap.getWidth() / 2, paint);
                        usrImg.setImageBitmap(btMap);


                    } else {
                        Bitmap noImage = BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.defaultuser);

                        usrImg.setImageBitmap(noImage);

                    }


                }
            } else {
           //     Toast.makeText(context, "Unable to load account details!", Toast.LENGTH_SHORT).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Unable to load account details!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_my_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.techled.teacheranalytics.ActivityList.TeachingLogModule;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.techled.teacheranalytics.ActivityList.EventModule.EventsActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.EventsPagerAdapter;
import com.techled.teacheranalytics.adapter.TeachingLogPagerAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;

public class TeachingLogsTabActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private String selectedDate;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context context;
    private LinearLayout layParent;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teaching_logs_tab);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Teaching Logs");
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        //get intent data
        Intent intent = getIntent();
        if (intent != null) {
            selectedDate = intent.getStringExtra("selectedDate");
        }
        context = this;
        layParent = (LinearLayout) findViewById(R.id.layParent);
        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Pending"));
        tabLayout.addTab(tabLayout.newTab().setText("Filled"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        //Creating our pager adapter
        TeachingLogPagerAdapter adapter = new TeachingLogPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(TeachingLogsTabActivity.this);

        //check internet connection
        CheckInternet checkInternet = new CheckInternet();
        boolean checkConnection = checkInternet.checkConnection(context);
        if (checkConnection) {

        } else {

            Snackbar snackbar = Snackbar
                    .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
            return;
        }

        //check poor internet connection
        CheckInternet checkPoorInternet = new CheckInternet();
        boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
        if (checkPoorConnection) {
            Snackbar snackbar = Snackbar
                    .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
            snackbar.setDuration(3000);
            snackbar.show();
        }

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
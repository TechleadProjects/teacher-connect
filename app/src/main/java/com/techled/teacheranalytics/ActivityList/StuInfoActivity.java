package com.techled.teacheranalytics.ActivityList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class StuInfoActivity extends AppCompatActivity {
    private int stuId;
    private String params;
    private ImageView btnRefresh;
    private LinearLayout layNoRecord, layParent;
    private int orgId = 0, insId = 0, dscId = 0, ayrYear = 0;
    private ProgressDialog progDailog;
    private Util util;
    private String response;
    private Context context;
    private CircleImageView stuImage;
    private TextView txtStdDiv, txtRegNo, txtRollNo, txtMotherName, txtFatherName, txtMobileNo,
            txtEmail, txtCurAdd, txtPerAdd, txtJoinDate, txtPob, txtDob, txtGender, txtBloodGroup, txtStuName;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stu_info);

        try {

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Student Info");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            //get Intent data
            Intent intent = getIntent();
            if (intent != null) {
                stuId = intent.getIntExtra("stuId", 0);

            }

            // get reference
            stuImage = (CircleImageView) findViewById(R.id.stuImage);
            txtStdDiv = (TextView) findViewById(R.id.txtStdDiv);
            txtRegNo = (TextView) findViewById(R.id.txtRegNo);
            txtRollNo = (TextView) findViewById(R.id.txtRollNo);
            txtMotherName = (TextView) findViewById(R.id.txtMotherName);
            txtFatherName = (TextView) findViewById(R.id.txtFatherName);
            txtMobileNo = (TextView) findViewById(R.id.txtMobileNo);
            txtEmail = (TextView) findViewById(R.id.txtEmail);
            txtCurAdd = (TextView) findViewById(R.id.txtCurAdd);
            txtPerAdd = (TextView) findViewById(R.id.txtPerAdd);
            txtJoinDate = (TextView) findViewById(R.id.txtJoinDate);
            txtPob = (TextView) findViewById(R.id.txtPob);
            txtDob = (TextView) findViewById(R.id.txtDob);
            txtGender = (TextView) findViewById(R.id.txtGender);
            txtBloodGroup = (TextView) findViewById(R.id.txtBloodGroup);
            txtStuName = (TextView) findViewById(R.id.txtStuName);
            layParent = (LinearLayout) findViewById(R.id.layParent);


            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //login details
            try {
                SharedPreferences loginDetails = getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);

                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);


                    //org shared pref
                    SharedPreferences orgPref = getSharedPreferences("Organization", 0);
                    if (orgPref != null) {
                        if (!orgPref.getString("Org_Id", "0").equals("")) {
                            orgId = Integer.parseInt(orgPref.getString("Org_Id", "0"));

                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        } else {
                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        }

                    }
                    //ins shared pref
                    SharedPreferences insPref = getSharedPreferences("Institute", 0);
                    if (insPref != null) {
                        if (!insPref.getString("Ins_Id", "0").equals("")) {
                            insId = Integer.parseInt(insPref.getString("Ins_Id", "0"));
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        } else {
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        }
                    }

                    ayrYear = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }



            //load information
            StuInfoActivity.loadStuInfo stuInfo = new StuInfoActivity.loadStuInfo();
            stuInfo.execute(null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class loadStuInfo extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading");
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                JSONArray jsonArray = new JSONArray(response);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {
                    JSONObject object2 = jsonArray.getJSONObject(1);

                    JSONObject object = object2.getJSONObject("data");


                    byte[] byteData = null;
                    try {
                        // Image Setting
                        String[] id = object.getString("stuImage").split(",");
                        byteData = new byte[id.length];
                        for (int i = 0; i < id.length; i++) {

                            byteData[i] = Byte.valueOf(id[i]);
                        }
                    } catch (Exception e) {
                        byteData = null;
                    }
                    if (byteData != null) {
                        ByteArrayInputStream bais = new ByteArrayInputStream(
                                byteData);
                        Bitmap btMap = BitmapFactory.decodeStream(bais);

                        Bitmap circleBitmap = Bitmap.createBitmap(btMap.getWidth(), btMap.getHeight(), Bitmap.Config.ARGB_8888);

                        BitmapShader shader = new BitmapShader(btMap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                        Paint paint = new Paint();
                        paint.setShader(shader);

                        Canvas c = new Canvas(circleBitmap);
                        c.drawCircle(btMap.getWidth() / 2, btMap.getHeight() / 2, btMap.getWidth() / 2, paint);
                        stuImage.setImageBitmap(btMap);


                    } else {
                        Bitmap noImage = BitmapFactory.decodeResource(context.getResources(),
                                R.drawable.defualt_user);

                        stuImage.setImageBitmap(noImage);

                    }
                    //stuName
                    txtStuName.setText(object.getString("stuName"));

                    //std/div
                    txtStdDiv.setText(object.getString("stdName") + " " + object.getString("divName"));

                    //roll no
                    txtRollNo.setText(object.getString("rollNo"));

                    //regNo
                    txtRegNo.setText(object.getString("regNo"));

                    //mother name
                    txtMotherName.setText(object.getString("motherName"));

                    //father name
                    txtFatherName.setText(object.getString("fatherName"));

                    //mobile no.
                    txtMobileNo.setText(object.getString("parMobile"));

                    //email
                    txtEmail.setText(object.getString("parEmail1") + " , " + object.getString("parEmail2"));

                    //cur address
                    txtCurAdd.setText(object.getString("curAddr"));

                    //per address
                    txtPerAdd.setText(object.getString("perAddr"));

                    //joining date
                    txtJoinDate.setText(object.getString("joiningDate"));

                    //pob
                    txtPob.setText(object.getString("pob"));

                    //dob
                    txtDob.setText(object.getString("dob"));

                    //gender
                    txtGender.setText(object.getString("gender"));

                    //blood group
                    txtBloodGroup.setText(object.getString("bloodGroup"));

                } else {
                  //  Toast.makeText(context, "Student information not found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Student information not found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }
                if (response == null) {
                   // Toast.makeText(context, "Student information not found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Student information not found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = util.getStudentInfo(orgId, insId, ayrYear, stuId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

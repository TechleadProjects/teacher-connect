package com.techled.teacheranalytics.ActivityList.MyLectures;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.icu.text.DateFormat;
import java.text.SimpleDateFormat;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;


import com.techled.teacheranalytics.ActivityList.SyllabusModule.SyllabusHomeActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.MyLecturesDetailsRecyAdapter;
import com.techled.teacheranalytics.adapter.SyllabusDetailsRecyAdapter;
import com.techled.teacheranalytics.pojo.MyLectureDetails;
import com.techled.teacheranalytics.pojo.Standards;
import com.techled.teacheranalytics.pojo.SubjectDetails;
import com.techled.teacheranalytics.pojo.SyllabusDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class MyLectures extends AppCompatActivity {

    private Context context;
    private Util util;
    private Dialog dialog;
    private ProgressDialog progDailog;
    private Spinner ayrYearSpinner, stdSpinner, subjectSpinner;
    private String resAcdemicYear, pastResAcdemicYear;
    private int orgId, insId, dscId, ayr, usrId, usrAssetId, stfId, stdId;
    private String lectureStartDate, lectureEndDate;
    private int[] ayrs;
    private JSONArray classTeacherOfJson;
    private String resUserInfo, resSubject;
    private ArrayList<MyLectureDetails> myLectureDetailsArrayList;
    private RecyclerView myLecturesRecyView;
    private LinearLayout layParent, layNoRecord;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lectures);

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("My Lectures");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            myLecturesRecyView = (RecyclerView) findViewById(R.id.myLecturesRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            myLecturesRecyView.setLayoutManager(layoutManager);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);


            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);


                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
                ayr = object.getInt("ayrYear");

            }

            //get data
            SharedPreferences prefUserInfo = getSharedPreferences("MyInfo", MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            //load syllabus details
            layParent.setVisibility(View.VISIBLE);
            layNoRecord.setVisibility(View.GONE);
            MyLectures.LoadMyLecturesDetails loadMyLectureDetails = new MyLectures.LoadMyLecturesDetails();
            loadMyLectureDetails.execute(null, null);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class LoadMyLecturesDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        //JSONObject dataObject = dataObj.getJSONObject("data");
                        JSONArray dataArray1 = dataObj.getJSONArray("data");
                        if(dataArray1.length() == 0)
                        {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Lectures not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        JSONObject dataObj1 = dataArray1.getJSONObject(0);
                        JSONArray dataArray = dataObj1.getJSONArray("logList");

                        myLectureDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            MyLectureDetails myLectureDetails = new MyLectureDetails();
                            JSONObject object = dataArray.getJSONObject(i);

                            myLectureDetails.setDiv(object.getString("divName"));
                            myLectureDetails.setLectureEndTime(object.getString("lectureEndTime"));
                            myLectureDetails.setLectureStartTime(object.getString("lectureStartTime"));
                            myLectureDetails.setSubName(object.getString("subName"));
                            myLectureDetails.setStd(object.getString("stdName"));

                            myLectureDetailsArrayList.add(myLectureDetails);

                        }

                        //set adapter
                        if (myLectureDetailsArrayList.size() > 0) {
                            layNoRecord.setVisibility(View.GONE);
                            layParent.setVisibility(View.VISIBLE);
                            MyLecturesDetailsRecyAdapter adapter = new MyLecturesDetailsRecyAdapter(orgId, insId, dscId, stfId, ayr,  myLectureDetailsArrayList, context);
                            myLecturesRecyView.setAdapter(adapter);
                        } else {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Lectures not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    } else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Lectures not found!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();
                lectureStartDate = lectureEndDate = sdf.format(date);
                resMyLectures = util.findTeachingLogForDay(orgId, insId, dscId, usrAssetId, ayr, lectureStartDate, lectureEndDate);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

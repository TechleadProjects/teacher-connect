package com.techled.teacheranalytics.ActivityList.TaskManagementModule;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.OpenableColumns;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.Divisions;
import com.techled.teacheranalytics.pojo.PredefinedTask;
import com.techled.teacheranalytics.pojo.Staff;
import com.techled.teacheranalytics.pojo.Standards;
import com.techled.teacheranalytics.pojo.Student;
import com.techled.teacheranalytics.pojo.TaskEventData;
import com.techled.teacheranalytics.util.Util;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddNewTaskActivity extends AppCompatActivity {

    private Context context;
    private Util util;
    private RadioGroup taskTypeRadioGroup, priorityRadioGroup, assignToMultipleRadioGroup, assignToRadioGroup, selectRadioGroup;
    private TextView assignerName, creationDate, assignedToNames;
    private Spinner predefinedTaskSpinner, staffSpinner, standardSpinner, divisionSpinner, studentSpinner;
    private EditText subject, description, progressNote;
    private Button startDate, endDate, btnUpload;
    private LinearLayout selectLayout, staffLayout, studentLayout, assignToLayout;
    private int orgId, insId, dscId, stfId, ayrYear, evtId;
    private String userName;
    private String predefinedTaskValue = "", staffValue = "", standardValue = "", divisionValue = "", studentValue = "";
    private String taskTypeValue = "", priorityValue = "", assignToMultipleValue = "", assignToValue = "", selectValue = "";
    private String subjectValue = "", descriptionValue = "", progressNoteValue = "";
    private String startDateValue = "", endDateValue = "", btnUploadValue = "";
    private String assignerNameValue = "", creationDateValue = "", sendSmsValue = "";
    //Pdf request code
    private int PICK_PDF_REQUEST = 1;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;
    private String base64StringForUploadFile = "", fileExtension = "", fileType = "", fileName = "";
    private Uri filePath;
    private final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private String whichDateSelected = "";
    private String response = "";
    private ProgressDialog progDailog;
    private String selectedPtkId = "", selectedStuId = "", selectedStfId = "", selectedStuIds = "", selectedStfIds = "", selectedStdId = "", selectedDivId = "";
    private CheckBox sendSmsCheckBox;
    private Spinner eventSpinner;
    private ArrayList<TaskEventData> taskEventDataArrayList;

    private ArrayList<PredefinedTask> predefinedTaskArrayList;
    private ArrayList<Staff> staffArrayList;
    private ArrayList<Standards> standardArrayList;
    private ArrayList<Divisions> divisionArrayList;
    private ArrayList<Student> studentArrayList;

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Add new task");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        context = this;
        util = new Util();

        //login details
        SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
        String params = loginDetails.getString("params", null);

        try {
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);


                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                stfId = object.getInt("assetId1");
                userName = object.getString("usrFullName");
                ayrYear = object.getInt("ayrYear");

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        //get references
        taskTypeRadioGroup = findViewById(R.id.taskTypeRadioGroup);
        priorityRadioGroup = findViewById(R.id.priorityRadioGroup);
        assignToMultipleRadioGroup = findViewById(R.id.assignToMultipleRadioGroup);
        assignToRadioGroup = findViewById(R.id.assignToRadioGroup);
        selectRadioGroup = findViewById(R.id.selectRadioGroup);

        assignerName = findViewById(R.id.assignerName);
        creationDate = findViewById(R.id.creationDate);

        predefinedTaskSpinner = findViewById(R.id.predefinedTaskSpinner);
        staffSpinner = findViewById(R.id.staffSpinner);
        standardSpinner = findViewById(R.id.standardSpinner);
        divisionSpinner = findViewById(R.id.divisionSpinner);
        studentSpinner = findViewById(R.id.studentSpinner);
        eventSpinner = findViewById(R.id.eventSpinner);

        subject = findViewById(R.id.subject);
        description = findViewById(R.id.description);
        progressNote = findViewById(R.id.progressNote);

        startDate = findViewById(R.id.startDate);
        endDate = findViewById(R.id.endDate);
        btnUpload = findViewById(R.id.btnUpload);

        selectLayout = findViewById(R.id.selectLayout);
        staffLayout = findViewById(R.id.staffLayout);
        studentLayout = findViewById(R.id.studentLayout);
        assignToLayout = findViewById(R.id.assignToLayout);
        assignedToNames = findViewById(R.id.assignedToNames);

        sendSmsCheckBox = findViewById(R.id.sendSmsCheckBox);

        //initialize array lists
        predefinedTaskArrayList = new ArrayList<>();
        staffArrayList = new ArrayList<>();
        standardArrayList = new ArrayList<>();
        divisionArrayList = new ArrayList<>();
        studentArrayList = new ArrayList<>();

        //set creation date
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        creationDate.setText(df.format(c));

        //set assigner name
        assignerName.setText(userName);

        //get task type radio value
        taskTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.task){
                    taskTypeValue = "T";
                }else if(checkedId == R.id.meeting){
                    taskTypeValue = "M";
                }
            }
        });

        //get task type radio value
        priorityRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.high){
                    priorityValue = "H";
                }else if(checkedId == R.id.medium){
                    priorityValue = "M";
                }else if(checkedId == R.id.low){
                    priorityValue = "L";
                }
            }
        });

        //get task type radio value
        assignToMultipleRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                //Clear all fields
                selectedStuId = "";
                selectedStuIds = "";
                selectedStfId = "";
                selectedStfIds = "";
                standardSpinner = findViewById(R.id.standardSpinner);
                divisionSpinner = findViewById(R.id.divisionSpinner);

                if(checkedId == R.id.btnYes){
                    assignToMultipleValue = "Y";
                    assignToLayout.setVisibility(View.VISIBLE);
                    selectLayout.setVisibility(View.GONE);
                    staffLayout.setVisibility(View.GONE);
                    studentLayout.setVisibility(View.GONE);

                }else if(checkedId == R.id.btnNo){
                    assignToMultipleValue = "N";
                    assignToLayout.setVisibility(View.GONE);
                    selectLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        //get task type radio value
        assignToRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                //Clear all fields
                selectedStuId = "";
                selectedStuIds = "";
                selectedStfId = "";
                selectedStfIds = "";
                standardSpinner = findViewById(R.id.standardSpinner);
                divisionSpinner = findViewById(R.id.divisionSpinner);

                if(checkedId == R.id.allTeachers){
                    assignToValue = "T";
                    assignedToNames.setVisibility(View.GONE);
                }else if(checkedId == R.id.classTeachers){
                    assignToValue = "C";
                    assignedToNames.setVisibility(View.GONE);
                }else if(checkedId == R.id.selectedParents){
                    assignToValue = "P";
                    assignedToNames.setVisibility(View.VISIBLE);
                    openStudentParentSelectionDialog();
                }else if(checkedId == R.id.selectedStaff){
                    assignToValue = "E";
                    assignedToNames.setVisibility(View.VISIBLE);
                    openStaffSelectionDialog();
                }else if(checkedId == R.id.selectedStudents){
                    assignToValue = "S";
                    assignedToNames.setVisibility(View.VISIBLE);
                    openStudentParentSelectionDialog();
                }
            }
        });

        //get task type radio value
        selectRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                //Clear all fields
                selectedStuId = "";
                selectedStuIds = "";
                selectedStfId = "";
                selectedStfIds = "";
                standardSpinner = findViewById(R.id.standardSpinner);
                divisionSpinner = findViewById(R.id.divisionSpinner);
                assignedToNames.setVisibility(View.GONE);

                if(checkedId == R.id.staff){
                    selectValue = "T";
                    staffLayout.setVisibility(View.VISIBLE);
                    studentLayout.setVisibility(View.GONE);
                    new LoadStaff().execute();
                }else if(checkedId == R.id.student){
                    selectValue = "S";
                    staffLayout.setVisibility(View.GONE);
                    studentLayout.setVisibility(View.VISIBLE);
                    new LoadStandards().execute();

                    divisionArrayList.clear();
                }
            }
        });

        sendSmsCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    sendSmsValue = "Y";
                }else {
                    sendSmsValue = "N";
                }
            }
        });

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        new LoadPredefinedTasks().execute();
    }

    private void openStaffSelectionDialog() {

        ArrayList<MultiSelectModel> multiSelectModel = new ArrayList<>();

        if(staffArrayList.size() == 0){
            Toast.makeText(context, "No staff available.", Toast.LENGTH_LONG).show();
            return;
        }

        for (Staff staff : staffArrayList){
            multiSelectModel.add(new MultiSelectModel(staff.getStfId(), staff.getStfName()));
        }

        final MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                .title("Select Staff") //setting title for dialog
                .titleSize(25)
                .positiveText("Done")
                .negativeText("Cancel") //List of ids that you need to be selected
                .multiSelectList(multiSelectModel) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(final ArrayList<Integer> selectedIds, final ArrayList<String> selectedNames, String dataString) {

                        selectedStfIds = selectedIds.toString();
                        selectedStfIds = selectedStfIds.replace("[", "");
                        selectedStfIds = selectedStfIds.replace("]", "");
                        selectedStfIds = selectedStfIds.replaceAll("\\s", "");
                        String staffNames = selectedNames.toString();

                        assignedToNames.setText(staffNames);
                    }

                    @Override
                    public void onCancel() {

                    }

                });

        multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
    }

    private void openStudentParentSelectionDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_standard_division_selection);
        dialog.show();
        Window window = dialog.getWindow();
        dialog.setCancelable(true);
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);

        standardSpinner = dialog.findViewById(R.id.standardSpinner);
        divisionSpinner = dialog.findViewById(R.id.divisionSpinner);
        Button button = dialog.findViewById(R.id.getStudentDetails);

        assignStandard();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(standardValue.equals("") || standardValue.equals("Choose standard...")){
                    Toast.makeText(context, "Please select standard...", Toast.LENGTH_SHORT).show();
                    return;
                }else if(divisionValue.equals("") || divisionValue.equals("Choose Standard")){
                    Toast.makeText(context, "Please select division...", Toast.LENGTH_SHORT).show();
                    return;
                }


                ArrayList<MultiSelectModel> multiSelectModel = new ArrayList<>();

                if(studentArrayList.size() == 0){
                    Toast.makeText(context, "No students available.", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    return;
                }else if("Students not found".equals(studentArrayList.get(0).toString())){
                    Toast.makeText(context, "No students available.", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                    return;
                }

                for (Student student : studentArrayList){
                    multiSelectModel.add(new MultiSelectModel(student.getStuId(), student.toString()));
                }

                final MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                        .title("Make Students") //setting title for dialog
                        .titleSize(25)
                        .positiveText("Done")
                        .negativeText("Cancel") //List of ids that you need to be selected
                        .multiSelectList(multiSelectModel) // the multi select model list with ids and name
                        .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                            @Override
                            public void onSelected(final ArrayList<Integer> selectedIds, final ArrayList<String> selectedNames, String dataString) {

                                dialog.dismiss();
                                selectedStuIds = selectedIds.toString();
                                selectedStuIds = selectedStuIds.replace("[", "");
                                selectedStuIds = selectedStuIds.replace("]", "");
                                selectedStuIds = selectedStuIds.replaceAll("\\s", "");
                                String stuNames = selectedNames.toString();

                                assignedToNames.setText(stuNames);
                            }

                            @Override
                            public void onCancel() {
                                dialog.dismiss();
                            }

                        });

                multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
            }
        });
    }

    public void assignPredefinedTasks(){

        ArrayAdapter<PredefinedTask> adapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, predefinedTaskArrayList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        predefinedTaskSpinner.setAdapter(adapter);

        predefinedTaskSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                predefinedTaskValue = predefinedTaskArrayList.get(position).getPtkSubject();
                selectedPtkId = String.valueOf(predefinedTaskArrayList.get(position).getPtkId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void assignStaff(){

        ArrayAdapter<Staff> adapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, staffArrayList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        staffSpinner.setAdapter(adapter);

        staffSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                staffValue = staffArrayList.get(position).getStfName();
                selectedStfId = String.valueOf(staffArrayList.get(position).getStfId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void assignStandard(){

        ArrayAdapter<Standards> adapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, standardArrayList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        standardSpinner.setAdapter(adapter);

        standardSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                standardValue = standardArrayList.get(position).getStdName();
                selectedStdId = standardArrayList.get(position).getStdId();
                if(!selectedStdId.equals("0")){
                    new LoadDivisions().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void assignDivision(){

        ArrayAdapter<Divisions> adapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, divisionArrayList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        divisionSpinner.setAdapter(adapter);

        divisionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                divisionValue = divisionArrayList.get(position).getDivName();
                selectedDivId = String.valueOf(divisionArrayList.get(position).getDivId());
                if(!selectedDivId.equals("0")){
                    new LoadStudents().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void assignStudent(){

        ArrayAdapter<Student> adapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, studentArrayList);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        studentSpinner.setAdapter(adapter);

        studentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                studentValue = studentArrayList.get(position).toString();
                selectedStuId = String.valueOf(studentArrayList.get(position).getStuId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void submitAddTask(View view) {

        subjectValue = subject.getText().toString();
        descriptionValue = description.getText().toString();
        assignerNameValue = assignerName.getText().toString();
        creationDateValue = creationDate.getText().toString();
        progressNoteValue = progressNote.getText().toString();

        if(taskTypeValue.equals("")){
            Toast.makeText(context, "Please select task type.", Toast.LENGTH_SHORT).show();
            return;
        }else if(subjectValue.equals("")){
            Toast.makeText(context, "Please enter subject.", Toast.LENGTH_SHORT).show();
            return;
        }else if(descriptionValue.equals("")){
            Toast.makeText(context, "Please enter description.", Toast.LENGTH_SHORT).show();
            return;
        }else if(priorityValue.equals("")){
            Toast.makeText(context, "Please select priority.", Toast.LENGTH_SHORT).show();
            return;
        }else if(startDateValue.equals("")){
            Toast.makeText(context, "Please select start date.", Toast.LENGTH_SHORT).show();
            return;
        }else if(endDateValue.equals("")){
            Toast.makeText(context, "Please select end date.", Toast.LENGTH_SHORT).show();
            return;
        }else if(assignToMultipleValue.equals("")){
            Toast.makeText(context, "Please select Assign to multiple staff / student.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(sendSmsValue.equals("")){
            sendSmsValue = "N";
        }

        if(assignToMultipleValue.equals("Y")){
            if(assignToValue.equals("")){
                Toast.makeText(context, "Please select Assign to.", Toast.LENGTH_SHORT).show();
                return;
            }

            if(assignToValue.equals("T")){

            }else if(assignToValue.equals("C")){

            }else if(assignToValue.equals("P")){
                if(selectedStuIds.equals("")){
                    Toast.makeText(context, "Please select parents.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }else if(assignToValue.equals("E")){
                if(selectedStfIds.equals("")){
                    Toast.makeText(context, "Please select staffs.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }else if(assignToValue.equals("S")){
                if(selectedStuIds.equals("")){
                    Toast.makeText(context, "Please select students.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }else if(assignToMultipleValue.equals("N")){
            if(selectValue.equals("")){
                Toast.makeText(context, "Please select Select.", Toast.LENGTH_SHORT).show();
                return;
            }

            if(selectValue.equals("T")){

                if(selectedStfId.equals("0")){
                    Toast.makeText(context, "Staff not selected or Staff not found.", Toast.LENGTH_SHORT).show();
                    return;
                }

            }else if(selectValue.equals("S")){

                if(standardValue.equals("") || standardValue.equals("Choose standard...")){
                    Toast.makeText(context, "Please select Standard.", Toast.LENGTH_SHORT).show();
                    return;
                }else if(divisionValue.equals("") || divisionValue.equals("Choose Standard")){
                    Toast.makeText(context, "Please select Division.", Toast.LENGTH_SHORT).show();
                    return;
                }else if(selectedStuId.equals("0")){
                    Toast.makeText(context, "Student not selected or Student not found.", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }

        new AddNewTask().execute();
    }

    public class AddNewTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    Toast.makeText(context, "Failed to add new task!", Toast.LENGTH_LONG).show();
                } else {


                    if (response.equals("SUCCESS")) {

                        Toast.makeText(context, "New task added successfully!", Toast.LENGTH_LONG).show();
                        finish();

                    } else {
                        Toast.makeText(context, "Failed to add new task.", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                if (base64StringForUploadFile == null || base64StringForUploadFile.equals("null"))
                {
                    base64StringForUploadFile = "";
                }

                response = util.addNewTaskByStaff(orgId, insId, dscId, stfId, ayrYear, String.valueOf(selectedPtkId), taskTypeValue, subjectValue, descriptionValue, creationDateValue, startDateValue, endDateValue, selectValue, selectedStfId, selectedStuId, sendSmsValue, selectedStuIds, selectedStfIds, priorityValue, assignToMultipleValue, assignToValue, progressNoteValue, "0", base64StringForUploadFile, fileExtension, fileType, fileName, evtId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void uploadFile(View view) {
        requestStoragePermission();

        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select file"), PICK_PDF_REQUEST);
    }

    public static byte[] getBytes(Context context, Uri uri) throws IOException {
        InputStream iStream = context.getContentResolver().openInputStream(uri);

        try {
            return getBytes(iStream);
        } finally {
            // close the stream
            try {
                iStream.close();
            } catch (IOException ignored) { /* do nothing */ }
        }
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {

        byte[] bytesResult = null;
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        try {
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            bytesResult = byteBuffer.toByteArray();
        } finally {
            // close the stream
            try {
                byteBuffer.close();
            } catch (IOException ignored) { /* do nothing */ }
        }
        return bytesResult;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == PICK_PDF_REQUEST && data != null && data.getData() != null) {
                try {

                    filePath = data.getData();

                    byte[] byteArray = getBytes(AddNewTaskActivity.this, filePath);
                    byte[] encoded = Base64.encodeBase64(byteArray);
                    base64StringForUploadFile = new String(encoded);
                    String[] mime = getMimeType(AddNewTaskActivity.this, filePath).split(",");
                    fileExtension = mime[0];
                    fileType = mime[1];
                    if (!fileExtension.startsWith(".")) {
                        fileExtension = "." + fileExtension;
                    }
                    //fileName = filePath.getPath().substring(filePath.getPath().lastIndexOf("/")+1);
                    fileName = getFileName(filePath);
                    btnUpload.setText("" + fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension, type = "-";

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
            type = mime.getMimeTypeFromExtension(extension);
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());
        }

        return extension + "," + type;
    }

    //Requesting permission
    private void requestStoragePermission() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                return;

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //If the user has denied the permission previously your code will come to this block
                //Here you can explain why you need this permission
                //Explain here why you need this permission
            }
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectStartDate(View view) {
        whichDateSelected = "start";
        new DatePickerDialog(AddNewTaskActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void selectEndDate(View view) {
        whichDateSelected = "end";
        new DatePickerDialog(AddNewTaskActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if(whichDateSelected.equals("start")){
            startDateValue = sdf.format(myCalendar.getTime());
            startDate.setText(startDateValue);
        }else if(whichDateSelected.equals("end")){
            endDateValue = sdf.format(myCalendar.getTime());
            endDate.setText(endDateValue);
        }
    }

    public class LoadStandards extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    Toast.makeText(context, "Failed to get standards.", Toast.LENGTH_SHORT).show();
                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        standardArrayList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Standards standards = new Standards();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            standards.setStdId(String.valueOf(jsonObject1.getInt("stdId")));
                            standards.setStdName(jsonObject1.getString("stdName"));

                            standardArrayList.add(standards);
                        }

                        if(standardArrayList.size() > 0){
                            assignStandard();
                        }else {
                            Toast.makeText(context, "Failed to get standards.", Toast.LENGTH_SHORT).show();
                            Standards standards = new Standards();
                            standards.setStdId("0");
                            standards.setStdName("Standards not found");
                            standardArrayList.clear();
                            standardArrayList.add(standards);
                            assignStandard();
                        }

                    } else {
                        Toast.makeText(context, "Failed to get standards.", Toast.LENGTH_SHORT).show();
                        Standards standards = new Standards();
                        standards.setStdId("0");
                        standards.setStdName("Standards not found");
                        standardArrayList.clear();
                        standardArrayList.add(standards);
                        assignStandard();
                    }
                }

                new LoadEvents().execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.loadStandards(orgId, insId, dscId);

                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadDivisions extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    Toast.makeText(context, "Failed to get divisions.", Toast.LENGTH_SHORT).show();
                } else {


                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        divisionArrayList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Divisions divisions = new Divisions();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            divisions.setDivId(jsonObject1.getInt("divId"));
                            divisions.setDivName(jsonObject1.getString("divName"));

                            divisionArrayList.add(divisions);
                        }

                        if(divisionArrayList.size() > 0){
                            assignDivision();
                        }else {
                            Toast.makeText(context, "Failed to get divisions.", Toast.LENGTH_SHORT).show();
                            Divisions divisions = new Divisions();
                            divisions.setDivId(0);
                            divisions.setDivName("Divisions not found");
                            divisionArrayList.clear();
                            divisionArrayList.add(divisions);
                            assignDivision();
                        }

                    } else {
                        Toast.makeText(context, "Failed to get divisions.", Toast.LENGTH_SHORT).show();
                        Divisions divisions = new Divisions();
                        divisions.setDivId(0);
                        divisions.setDivName("Divisions not found");
                        divisionArrayList.clear();
                        divisionArrayList.add(divisions);
                        assignDivision();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getDivisionByStandardId(orgId, insId, dscId, ayrYear, Integer.parseInt(selectedStdId));
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadStudents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    Toast.makeText(context, "Failed to get students.", Toast.LENGTH_SHORT).show();
                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("success")) {

                        studentArrayList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Student student = new Student();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            student.setStuId(jsonObject1.getInt("stuId"));
                            student.setStuRollNo(jsonObject1.getInt("stuRollNo"));
                            student.setStuName(jsonObject1.getString("stuName"));

                            studentArrayList.add(student);
                        }

                        if(studentArrayList.size() > 0){
                            assignStudent();
                        }else {
                            Toast.makeText(context, "Failed to get students.", Toast.LENGTH_SHORT).show();
                            Student student = new Student();
                            student.setStuId(0);
                            student.setStuRollNo(0);
                            student.setStuName("Students not found");
                            studentArrayList.clear();
                            studentArrayList.add(student);
                            assignStudent();
                        }

                    } else {
                        Toast.makeText(context, "Failed to get students.", Toast.LENGTH_SHORT).show();
                        Student student = new Student();
                        student.setStuId(0);
                        student.setStuRollNo(0);
                        student.setStuName("Students not found");
                        studentArrayList.clear();
                        studentArrayList.add(student);
                        assignStudent();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getStudents(orgId, insId, dscId, ayrYear, selectedStdId, selectedDivId);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadStaff extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    Toast.makeText(context, "Failed to add new task!", Toast.LENGTH_LONG).show();
                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        staffArrayList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Staff staff = new Staff();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            staff.setStfId(jsonObject1.getInt("stfId"));
                            staff.setStfName(jsonObject1.getString("stfName"));

                            staffArrayList.add(staff);
                        }

                        assignStaff();

                    } else {
                        Toast.makeText(context, "Failed to add new task.", Toast.LENGTH_SHORT).show();
                    }
                }

                new LoadStandards().execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getStaff(orgId, insId);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadPredefinedTasks extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    Toast.makeText(context, "Failed to get predefined tasks!", Toast.LENGTH_LONG).show();
                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("success")) {

                        predefinedTaskArrayList = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            PredefinedTask predefinedTask = new PredefinedTask();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            predefinedTask.setPtkId(jsonObject1.getInt("ptkId"));
                            predefinedTask.setPtkSubject(jsonObject1.getString("ptkSubject"));

                            predefinedTaskArrayList.add(predefinedTask);
                        }

                        assignPredefinedTasks();

                    } else {
                        Toast.makeText(context, "Failed to get predefined tasks.", Toast.LENGTH_SHORT).show();
                    }
                }

                new LoadStaff().execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getPredefinedTask(orgId, insId, dscId, stfId);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private class LoadEvents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (progDailog != null) {
                    progDailog.dismiss();
                }
                if (response != null) {

                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject statusObject = jsonArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {

                        taskEventDataArrayList = new ArrayList<>();

                        TaskEventData taskEvent = new TaskEventData();

                        taskEvent.setEvtId(0);
                        taskEvent.setEvtTitle("Choose Event");
                        taskEventDataArrayList.add(taskEvent);

                        JSONObject dataObject = jsonArray.getJSONObject(1);
                        JSONArray array = dataObject.getJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);

                            TaskEventData taskEventData = new TaskEventData();

                            taskEventData.setEvtId(jsonObject.getInt("evtId"));
                            taskEventData.setEvtTitle(jsonObject.getString("evtTitle"));

                            taskEventDataArrayList.add(taskEventData);
                        }

                        if (taskEventDataArrayList.size() > 0) {

                            //set adapter
                            ArrayAdapter<TaskEventData> orgAdaptor = new ArrayAdapter<>(AddNewTaskActivity.this,
                                    android.R.layout.simple_spinner_item, taskEventDataArrayList);
                            orgAdaptor
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            eventSpinner.setAdapter(orgAdaptor);
                            eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    evtId = taskEventDataArrayList.get(position).getEvtId();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        } else {

                            Toast.makeText(context, "Unable to load events, try Again later!", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        Toast.makeText(context, "Unable to load events, try Again later!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Unable to load events, try Again later!", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                response = util.findEventsForTask(orgId, insId, dscId, ayrYear);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

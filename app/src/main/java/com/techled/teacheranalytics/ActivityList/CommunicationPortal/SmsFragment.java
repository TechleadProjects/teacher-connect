package com.techled.teacheranalytics.ActivityList.CommunicationPortal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.SmsLogAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.SmsLogs;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class SmsFragment extends Fragment {

    private Util util;
    private ArrayList<SmsLogs> smsLogArrayList;
    private int orgId = 0, insId = 0, dscId = 0, ayr = 0, ugpId = 0, usrId = 0;
    private Context context;
    private String params, prefData, status;
    private LinearLayout layNoRecord, layParent;
    private ProgressDialog progDailog;
    private String resMyInfo, resSms, pastResSms;
    private RecyclerView recycleView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_sms_log, container, false);

        //get reference
        context = getActivity().getApplicationContext();
        util = new Util();
        recycleView = (RecyclerView) view.findViewById(R.id.recycleView);
        recycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(context);
        recycleView.setLayoutManager(layoutManager2);
        layParent = (LinearLayout) view.findViewById(R.id.layParent);
        layNoRecord = (LinearLayout) view.findViewById(R.id.layNoRecord);
        String events = null;

        //login details
        try {
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
            }


            //load stored sms details
            SharedPreferences smsPref = context.getSharedPreferences("sms", context.MODE_PRIVATE);
            if (smsPref != null) {
                pastResSms = smsPref.getString("response", null);
            }

            //load emails
            new LoadSmsLog().execute(null, null);


        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }


    public class LoadSmsLog extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog = new ProgressDialog(getActivity());
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected String doInBackground(String... aurl) {

            try {

                if (pastResSms == null || pastResSms.equals("")) {
                    resSms = util.getSmsLog(orgId, insId, dscId, usrId, "E");
                    SharedPreferences.Editor editor = context.getSharedPreferences("sms", context.MODE_PRIVATE).edit();
                    editor.putString("response", resSms);
                    editor.apply();
                } else {
                    resSms = pastResSms;
                }


                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String unused) {
            super.onPostExecute(unused);
            progDailog.dismiss();
            try {
                smsLogArrayList = new ArrayList<SmsLogs>();
                if (resSms.equals("")) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    //Toast.makeText(context, "No SMS found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No SMS found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }
                if (resSms != null) {

                    JSONArray jsonArray = new JSONArray(resSms);

                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    status = jsonObject.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject jsonObject12 = jsonArray.getJSONObject(1);
                        JSONArray jsonArray1 = jsonObject12.getJSONArray("data");


                        for (int i = 0; i < jsonArray1.length(); i++) {
                            SmsLogs smsLog = new SmsLogs();
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            smsLog.setAseId(jsonObject1.getInt("aseId"));
                            smsLog.setAssContactNo(jsonObject1.getString("assContactNo"));
                            smsLog.setAssMessage(jsonObject1.getString("assMessage"));
                            smsLog.setAssProcessId(jsonObject1.getInt("assProcessId"));
                            smsLog.setAssTimestampProcessed(jsonObject1.getString("assTimestampProcessed"));
                            smsLog.setAssTimestampRequested(jsonObject1.getString("assTimestampRequested"));
                            smsLogArrayList.add(smsLog);
                        }

                        if (smsLogArrayList.size() == 0) {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            // Toast.makeText(context, "No SMS found!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "No SMS found!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                    }
                } else {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    // Toast.makeText(context, "No SMS found!", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(layParent, "No SMS found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

                if (status.equals("FAILURE")) {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    //Toast.makeText(context, "No SMS found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No SMS found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                } else {
                    if (smsLogArrayList != null) {
                        //set adapter
                        layParent.setVisibility(View.VISIBLE);
                        layNoRecord.setVisibility(View.GONE);

                        SmsLogAdapter smsLogAdapter = new SmsLogAdapter(smsLogArrayList);
                        recycleView.setAdapter(smsLogAdapter);
                    } else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No SMS found!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        //Toast.makeText(context, "No SMS found!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}

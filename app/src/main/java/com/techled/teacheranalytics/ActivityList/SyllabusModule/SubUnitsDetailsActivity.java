package com.techled.teacheranalytics.ActivityList.SyllabusModule;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.SyllabusDetailsRecyAdapter;
import com.techled.teacheranalytics.adapter.SyllabusSubUnitsDetailsRecyAdapter;
import com.techled.teacheranalytics.pojo.SubUnitsSyllabusDetails;
import com.techled.teacheranalytics.pojo.SyllabusDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SubUnitsDetailsActivity extends AppCompatActivity {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private Context context;
    private Util util;
    private Dialog dialog;
    private ProgressDialog progDailog;
    private RecyclerView syllabusSubUnitRecyView;
    private LinearLayout layParent, layNoRecord;
    private ArrayList<SubUnitsSyllabusDetails> subUnitsSyllabusDetailsArrayList;
    private TextView txtUnitName;
    private int orgId, insId, dscId, ayr, usrId, unitId, usrAssetId, subId, stdId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_units_details);

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Syllabus Sub-Unit");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            syllabusSubUnitRecyView = (RecyclerView) findViewById(R.id.syllabusSubUnitRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            syllabusSubUnitRecyView.setLayoutManager(layoutManager);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            txtUnitName = (TextView) findViewById(R.id.txtUnitName);


            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);


                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");

            }

            Intent intent = getIntent();
            if (intent != null) {
                unitId = intent.getIntExtra("unitId", 0);
                stdId = intent.getIntExtra("stdId", 0);
                subId = intent.getIntExtra("subId", 0);
                ayr = intent.getIntExtra("ayr", 0);
                String unitDesc = intent.getStringExtra("unitDesc");
                txtUnitName.setText("" + unitDesc);

            }


            FloatingActionButton btnAddNewSyllabus = (FloatingActionButton) findViewById(R.id.btnAddNewSyllabus);
            btnAddNewSyllabus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(SubUnitsDetailsActivity.this, AddNewSubUnitActivity.class);
                    intent.putExtra("ayr", ayr);
                    intent.putExtra("stdId", stdId);
                    intent.putExtra("subId", subId);
                    intent.putExtra("unitId", unitId);

                    startActivity(intent);

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

        //load syllabus details
        LoadSyllabusDetails loadSyllabusDetails = new LoadSyllabusDetails();
        loadSyllabusDetails.execute(null, null);
    }

    public class LoadSyllabusDetails extends AsyncTask<String, String, String> {
        private String resSyllabus;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resSyllabus != null) {
                    JSONArray resArray = new JSONArray(resSyllabus);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        JSONArray dataArray = dataObj.getJSONArray("data");

                        subUnitsSyllabusDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            SubUnitsSyllabusDetails subUnitsSyllabusDetails = new SubUnitsSyllabusDetails();
                            JSONObject object = dataArray.getJSONObject(i);

                            subUnitsSyllabusDetails.setSunActivities(object.getString("sunActivities"));
                            subUnitsSyllabusDetails.setSunDescription(object.getString("sunDescription"));
                            //  syllabusDetails.setTerId(object.getInt("terId"));
                            subUnitsSyllabusDetails.setUniId(object.getInt("uniId"));
                            subUnitsSyllabusDetails.setSunId(object.getInt("sunId"));

                            subUnitsSyllabusDetails.setSunElcId(object.getString("sunElcId"));

                            String uniCompletionDate = object.getString("sunCompletionDate");

                            //set from - to date
                            if (uniCompletionDate == null || uniCompletionDate.equals(";") || uniCompletionDate.equals("")
                                    ) {
                                subUnitsSyllabusDetails.setTo("");
                                subUnitsSyllabusDetails.setFrom("");
                            } else {
                                String[] completionDate = uniCompletionDate.split(";");

                                if (completionDate[0].equals("")) {

                                    subUnitsSyllabusDetails.setFrom("");
                                } else {

                                    subUnitsSyllabusDetails.setFrom(completionDate[0]);
                                }

                                if (completionDate.length > 1) {
                                    if (completionDate[1].equals("")) {
                                        subUnitsSyllabusDetails.setTo("");
                                    } else {
                                        subUnitsSyllabusDetails.setTo(completionDate[1]);
                                    }
                                }
                            }

                            //show activeYn
                            subUnitsSyllabusDetails.setShowActiveYn("");
                            subUnitsSyllabusDetails.setShowContentYn("");
                            subUnitsSyllabusDetails.setCount("");
                            subUnitsSyllabusDetailsArrayList.add(subUnitsSyllabusDetails);

                        }

                        //set adapter
                        if (subUnitsSyllabusDetailsArrayList.size() > 0) {
                            layNoRecord.setVisibility(View.GONE);
                            layParent.setVisibility(View.VISIBLE);
                            SyllabusSubUnitsDetailsRecyAdapter adapter = new SyllabusSubUnitsDetailsRecyAdapter(orgId, insId, dscId, ayr, stdId, usrId, subId, subUnitsSyllabusDetailsArrayList, context);
                            syllabusSubUnitRecyView.setAdapter(adapter);
                        } else {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Syllabus sub units not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Syllabus sub units not found!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resSyllabus = util.getSubUnitsSyallabusDetails(orgId, insId, dscId, ayr, stdId, subId, unitId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

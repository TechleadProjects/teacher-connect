package com.techled.teacheranalytics.ActivityList.SendNorificationBroadcastModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule.AttendanceActivity;
import com.techled.teacheranalytics.ActivityList.UploadHomeworkModule.UploadHomeworkActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.StfGradesRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.Divisions;
import com.techled.teacheranalytics.pojo.FilePath;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.Standards;
import com.techled.teacheranalytics.pojo.StfGradesDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddNotificationToActivity extends AppCompatActivity {
    private Util util;
    private Context context;
    private String params, uName, notType;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId, stdId, divId;
    private Spinner spinnNotType;
    private String selectRbStuStf = "S", selectRbAddTimline = "Y", curDate;
    private EditText edtMessage, edtTitle;
    private Button btnNotify;
    private String base64StringForUploadFile, notificationS3Url, nType, resUserInfo, message, title, resSendNotification, notifyAllDivision = "N";
    private JSONArray classTeacherOfJson;
    private ArrayList<Standards> standardDivisionArrayList;
    private Spinner spinnStdDiv;
    private ProgressDialog progDailog;
    private LinearLayout layShowStudent;
    private Button btnUploadFile;
    private String fileExtension = "";
    //Pdf request code
    private int PICK_PDF_REQUEST = 1;
    //Uri to store the image uri
    private Uri filePath;
    private String fpath, ntfUploadContentType = "";
    private TextView txtFileName;
    private Spinner spinnContentType;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;
    private String stfGrades = null, stfIds = "";
    private LinearLayout layForStaff;
    private RecyclerView stfGradesRecyView;
    private String resStfGrades;
    private ArrayList<StfGradesDetails> stfGradesDetailsArrayList;
    private StfGradesRecyAdapter stfGradesRecyAdapter = null;
    private LinearLayout layStfSelection;
    private RadioGroup rgForSelStaff;
    private boolean flag = false;
    private ArrayList<Divisions> divisionsArrayList;
    private Spinner spinnDiv;
    private String stdName, divName, stfGradesName, notifiedStdDivName;
    private LinearLayout layParent;
    private String accessKey;
    private String secretKey;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notification_to);
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Notification Broadcast");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference

            context = this;
            util = new Util();

            spinnNotType = (Spinner) findViewById(R.id.spinnNotType);
            btnNotify = (Button) findViewById(R.id.btnNotify);
            edtTitle = (EditText) findViewById(R.id.edtTitle);
            edtMessage = (EditText) findViewById(R.id.edtMessage);
            layShowStudent = (LinearLayout) findViewById(R.id.layShowStudent);
            txtFileName = (TextView) findViewById(R.id.txtFileName);
            spinnContentType = (Spinner) findViewById(R.id.spinnContentType);
            layForStaff = (LinearLayout) findViewById(R.id.layForStaff);
            stfGradesRecyView = (RecyclerView) findViewById(R.id.stfGradesRecyView);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            stfGradesRecyView.setLayoutManager(linearLayoutManager);
            layStfSelection = (LinearLayout) findViewById(R.id.layStfSelection);
            rgForSelStaff = (RadioGroup) findViewById(R.id.rgForSelStaff);
            spinnDiv = (Spinner) findViewById(R.id.spinnDiv);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                uName = object.getString("usrFullName");
                usrAssetId = object.getInt("assetId1");
            }

            accessKey = loginDetails.getString("accessKey", null);
            secretKey = loginDetails.getString("secretKey", null);

            //get data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("applicableStds");

                }
            }

            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
              /*  Toast.makeText(this,
                        "Unable to load standards!",
                        Toast.LENGTH_LONG).show();
*/

                Snackbar snackbar = Snackbar
                        .make(layParent, "Unable to load standards!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            standardDivisionArrayList = new ArrayList<>();
            for (int i = 0; i < classTeacherOfJson.length(); i++) {

                JSONObject object = classTeacherOfJson.getJSONObject(i);

                Standards standards = new Standards();
                standards.setStdId(object.getString("value"));
                standards.setStdName(object.getString("label"));

          /*      StandardDivision standardDivision = new StandardDivision();
                standardDivision.setStdName(object.getString("label"));
                String stdDivName = object.getString("label");
                if (stdDivName.contains("-")) {
                    String splStr[] = stdDivName.split("-");
                    standardDivision.setStdName(splStr[0]);
                 //   standardDivision.setDivName(splStr[1]);
                } else {
                    standardDivision.setStdName(stdDivName);
                    standardDivision.setDivName(" ");
                }

                String stdDivIds = object.getString("value");
                String[] splStrs = stdDivIds.split("-");
                standardDivision.setStdId(Integer.parseInt(splStrs[0]));
                standardDivision.setDivId(Integer.parseInt(splStrs[1]));*/
                standardDivisionArrayList.add(standards);

            }


            spinnStdDiv = (Spinner) findViewById(R.id.spinnStdDiv);
            ArrayAdapter<Standards> stdDivAdapter = new ArrayAdapter<Standards>(AddNotificationToActivity.this,
                    android.R.layout.simple_spinner_item, standardDivisionArrayList);
            stdDivAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnStdDiv.setAdapter(stdDivAdapter);

            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    stdId = Integer.parseInt(standardDivisionArrayList.get(position).getStdId());

                    stdName = standardDivisionArrayList.get(position).getStdName();
                    // divId = standardDivisionArrayList.get(position).getDivId();

                    //load divisions by std id
                    if (stdId != 0) {
                        LoadDivisionByStdId loadDivisionByStdId = new LoadDivisionByStdId();
                        loadDivisionByStdId.execute(null, null);
                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            Calendar c1 = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            curDate = df.format(c1.getTime());

            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add("News");
            arrayList.add("Circular");
            arrayList.add("Announcement");

            ArrayAdapter<String> ayrAdaptor = new ArrayAdapter<String>(AddNotificationToActivity.this,
                    android.R.layout.simple_spinner_item, arrayList);
            ayrAdaptor
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnNotType.setAdapter(ayrAdaptor);
            spinnNotType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    notType = spinnNotType.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            rgForSelStaff.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbAll) {

                        flag = false;
                        layStfSelection.setVisibility(View.GONE);


                    } else if (checkedId == R.id.rbSelectStf) {

                        flag = true;
                        layStfSelection.setVisibility(View.VISIBLE);


                    }
                }
            });

            RadioGroup rgForStuStf = (RadioGroup) findViewById(R.id.rgForStuStf);
            rgForStuStf.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbStu) {
                        selectRbStuStf = "S";
                        //

                        layShowStudent.setVisibility(View.VISIBLE);
                        layForStaff.setVisibility(View.GONE);
                    } else {
                        selectRbStuStf = "E";
                        //
                        notifiedStdDivName = "";

                        //load stf grades
                        AddNotificationToActivity.GetStaffGrades getStaffGrades = new AddNotificationToActivity.GetStaffGrades();
                        getStaffGrades.execute(null, null);
                        layShowStudent.setVisibility(View.GONE);
                        layForStaff.setVisibility(View.VISIBLE);

                    }
                }
            });

            RadioGroup rgForAddTimeline = (RadioGroup) findViewById(R.id.rgForAddTimeline);
            rgForAddTimeline.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbYes) {
                        selectRbAddTimline = "Y";
                    } else {
                        selectRbAddTimline = "N";

                    }
                }
            });

         /*   RadioGroup rgNotifyStd = (RadioGroup) findViewById(R.id.rgNotifyStd);
            rgNotifyStd.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbNotifyYes) {
                        notifyAllDivision = "Y";
                    } else {
                        notifyAllDivision = "N";
                    }

                }
            });*/
            btnNotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        if (selectRbStuStf.equals("S")) {
                            //if users select All in division spinner then set Notify All Division to Y
                            if (divId == 0) {
                                notifyAllDivision = "Y";
                            } else {
                                notifyAllDivision = "N";
                            }
                        }

                        //get student and staff details after check radio button
                        if (selectRbStuStf.equals("S")) {
                            stfGrades = "";
                            stfIds = "";
                            //provide name of std div
                            notifiedStdDivName = stdName + " : " + divName;

                        } else if (selectRbStuStf.equals("E")) {
                            notifiedStdDivName = "";
                            stdId = 0;
                            divId = 0;
                        }

                        //message
                        message = edtMessage.getText().toString().trim();

                        //title
                        title = edtTitle.getText().toString().trim();

                        //standard division not empty
                        if (selectRbStuStf.equals("S")) {
                            if (stdId == 0) {
                                //  Toast.makeText(context, "Please select standard/division!", Toast.LENGTH_SHORT).show();

                                Snackbar snackbar = Snackbar
                                        .make(layParent, "Please select standard/division!", Snackbar.LENGTH_LONG);
                                snackbar.setDuration(3000);
                                snackbar.show();
                                return;
                            }
                        }

                        //message
                        if (message.equals("")) {
                            //Toast.makeText(context, "Notification message could not be empty!!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Notification message could not be empty!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        //title
                        if (title.equals("") || title == null) {
                            // Toast.makeText(context, "Notification title could not be empty!!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Notification title could not be empty!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();

                            return;
                        }

                        //notification type
                        if (notType.equals("News")) {
                            nType = "N";
                        } else if (notType.equals("Circular")) {
                            nType = "C";
                        } else if (notType.equals("Announcement")) {
                            nType = "A";
                        }

                        //get staff grades
                        if (flag == true) {
                            List<StfGradesDetails> finalList = null;
                            if (stfGradesRecyAdapter != null) {
                                List<StfGradesDetails> detailsArrayList = new ArrayList<>();
                                detailsArrayList = stfGradesRecyAdapter.getList();

                                finalList = new ArrayList<>();
                                for (int i = 0; i < detailsArrayList.size(); i++)

                                {

                                    if (detailsArrayList.get(i).getSetSelected().equals("Y")) {

                                        StfGradesDetails stfGradesDetails = new StfGradesDetails();
                                        stfGradesDetails.setStfCd(detailsArrayList.get(i).getStfCd());
                                        finalList.add(stfGradesDetails);

                                    }
                                }
                                stfGrades = finalList.toString();
                            }

                        } else {
                            List<StfGradesDetails> detailsArrayList = new ArrayList<>();
                            if (stfGradesRecyAdapter != null) {
                                detailsArrayList = stfGradesRecyAdapter.getList();
                                stfGrades = detailsArrayList.toString();
                            }


                        }

                        if (stfGrades != null) {
                            stfGrades = stfGrades.replace("[", "");
                            stfGrades = stfGrades.replace("]", "");
                            stfGrades = stfGrades.replace(", ", ",");
                        }


                        //send notification
                        AddNotificationToActivity.SendNotification notification = new AddNotificationToActivity.SendNotification();
                        notification.execute(null, null);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            //content type
            final ArrayList<String> arrayListContentType = new ArrayList<>();
            arrayListContentType.add("Choose Content Type");
            arrayListContentType.add("Video");
            arrayListContentType.add("Audio");
            arrayListContentType.add("Image");
            arrayListContentType.add("Text Document");
            arrayListContentType.add("Pdf Document");

            ArrayAdapter<String> contentTypeAdapter = new ArrayAdapter<String>(AddNotificationToActivity.this,
                    android.R.layout.simple_spinner_item, arrayListContentType);
            contentTypeAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnContentType.setAdapter(contentTypeAdapter);
            spinnContentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        String contentType = spinnContentType.getSelectedItem().toString();
                        if (contentType.equals("Video")) {
                            ntfUploadContentType = "V";
                            fileExtension = ".mp3";
                        } else if (contentType.equals("Audio")) {
                            ntfUploadContentType = "A";
                            fileExtension = ".mp3";
                        } else if (contentType.equals("Image")) {
                            ntfUploadContentType = "I";
                            fileExtension = ".png";

                        } else if (contentType.equals("Text Document")) {
                            ntfUploadContentType = "D";
                            fileExtension = ".txt";

                        } else if (contentType.equals("Pdf Document")) {
                            ntfUploadContentType = "D";
                            fileExtension = ".pdf";

                        }

                    } else {
                        // Toast.makeText(context, "Please select content type!", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            btnUploadFile = (Button) findViewById(R.id.btnUploadFile);
            //attach file
            btnUploadFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        requestStoragePermission();

                        Intent intent = new Intent();
                        intent.setType("*/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select file"), PICK_PDF_REQUEST);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Requesting permission
    private void requestStoragePermission() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                return;

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //If the user has denied the permission previously your code will come to this block
                //Here you can explain why you need this permission
                //Explain here why you need this permission
            }
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                //  Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                //    Toast.makeText(this, "You just denied the permission!", Toast.LENGTH_LONG).show();

                Snackbar snackbar = Snackbar
                        .make(layParent, "You just denied the permission!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }
        }
    }


    public static byte[] getBytes(Context context, Uri uri) throws IOException {
        InputStream iStream = context.getContentResolver().openInputStream(uri);

        try {
            return getBytes(iStream);
        } finally {
            // close the stream
            try {
                iStream.close();
            } catch (IOException ignored) { /* do nothing */ }
        }
    }

    public class LoadDivisionByStdId extends AsyncTask<String, String, String> {
        private String resDivisions;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resDivisions != null) {
                    JSONArray resArray = new JSONArray(resDivisions);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject datatObject = resArray.getJSONObject(1);
                        JSONArray dataArray = datatObject.getJSONArray("data");
                        divisionsArrayList = new ArrayList<>();

                        for (int i = 0; i < dataArray.length(); i++) {
                            Divisions divisions = new Divisions();
                            JSONObject divDetailsObject = dataArray.getJSONObject(i);

                            divisions.setDivId(divDetailsObject.getInt("divId"));
                            if (divDetailsObject.getString("divName").equals("Choose Standard")) {
                                divisions.setDivName("All");
                            } else {
                                divisions.setDivName(divDetailsObject.getString("divName"));
                            }
                            divisionsArrayList.add(divisions);
                        }

                        if (divisionsArrayList.size() != 0) {

                            ArrayAdapter<Divisions> stdDivAdapter = new ArrayAdapter<Divisions>(AddNotificationToActivity.this,
                                    android.R.layout.simple_spinner_item, divisionsArrayList);
                            stdDivAdapter
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnDiv.setAdapter(stdDivAdapter);

                            spinnDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    divId = divisionsArrayList.get(position).getDivId();
                                    divName = divisionsArrayList.get(position).getDivName();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading std div", "Teacher Connect", usrId, "Add Notification Broadcast");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resDivisions = util.getDivisionByStandardId(orgId, insId, dscId, ayr, stdId);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading std div", "Teacher Connect", usrId, "Add Notification Broadcast");
            }
            return null;
        }
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {

        byte[] bytesResult = null;
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        try {
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            bytesResult = byteBuffer.toByteArray();
        } finally {
            // close the stream
            try {
                byteBuffer.close();
            } catch (IOException ignored) { /* do nothing */ }
        }
        return bytesResult;
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
                try {

                    filePath = data.getData();

//                    byte[] bbbyte = getBytes(AddNotificationToActivity.this, filePath);
//                    byte[] encoded = org.apache.commons.codec.binary.Base64.encodeBase64(bbbyte);
//                    base64StringForUploadFile = new String(encoded);
                    fileExtension = getMimeType(AddNotificationToActivity.this, filePath);
                    if (!fileExtension.startsWith(".")) {
                        fileExtension = "." + fileExtension;
                    }

                    txtFileName.setText("" + filePath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class GetStaffGrades extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resStfGrades != null) {
                    JSONArray jsonArray = new JSONArray(resStfGrades);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    stfGradesDetailsArrayList = new ArrayList<>();
                    if (status.equals("SUCCESS")) {
                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);
                            StfGradesDetails stfGradesDetails = new StfGradesDetails();
                            stfGradesDetails.setStfCd(object.getString("grdCd"));
                            stfGradesDetails.setStfGradeName(object.getString("grdDesignation"));
                            stfGradesDetails.setSetSelected("N");
                            stfGradesDetailsArrayList.add(stfGradesDetails);


                        }
                        //set adapter
                        if (stfGradesDetailsArrayList.size() != 0) {
                            stfGradesRecyAdapter = new StfGradesRecyAdapter(context, stfGradesDetailsArrayList);
                            stfGradesRecyView.setAdapter(stfGradesRecyAdapter);
                            stfGradesRecyAdapter.notifyDataSetChanged();
                        } else {
                            //Toast.makeText(context, "Staff grades not found!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Staff grades not found!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();

                            layForStaff.setVisibility(View.GONE);
                            return;
                        }
                    } else {
                        //  Toast.makeText(context, "Staff grades not found!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Staff grades not found!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        layForStaff.setVisibility(View.GONE);
                        return;
                    }

                } else {
                    //Toast.makeText(context, "Staff grades not found!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Staff grades not found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layForStaff.setVisibility(View.GONE);
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading stf grades", "Teacher Connect", usrId, "Add Notification Broadcast");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resStfGrades = util.getStfGrades(orgId, insId);

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading stf grades", "Teacher Connect", usrId, "Add Notification Broadcast");
            }
            return null;
        }
    }

    public class SendNotification extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSendNotification.equals("") || resSendNotification == null) {
                    //make dialog box
                    Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.row_email_log);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

                    dialog.setCanceledOnTouchOutside(false);

                    TextView txtStatusResponse = (TextView) dialog.findViewById(R.id.txtStatusResponse);
                    TextView txtNotifiedTo = (TextView) dialog.findViewById(R.id.txtNotifiedTo);
                    TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
                    TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
                    Button btnOk = (Button) dialog.findViewById(R.id.btnOk);

                    //message
                    txtMessage.setText("Message : " + message);

                    //title
                    txtTitle.setText("Title" + title);

                    //response
                    txtStatusResponse.setTextColor(Color.RED);
                    if (selectRbStuStf.equals("S")) {
                        txtStatusResponse.setText("Failed to notify students");
                        util.SaveSystemLog(orgId, insId, "Failed to notify students", "Teacher Connect", usrId, "Add Notification Broadcast");
                    } else {
                        txtStatusResponse.setText("Failed to notify staffs");
                        util.SaveSystemLog(orgId, insId, "Failed to notify staffs", "Teacher Connect", usrId, "Add Notification Broadcast");
                    }


                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                            return;
                        }
                    });

                }

                if (resSendNotification.equals("SUCCESS")) {
                   /* Toast.makeText(AddNotificationToActivity.this, "Notification sent successfully!", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    return;*/

                    //make dialog box
                    Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_to_show_response_notification_broadcast);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    dialog.setCanceledOnTouchOutside(false);

                    TextView txtStatusResponse = (TextView) dialog.findViewById(R.id.txtStatusResponse);
                    TextView txtNotifiedTo = (TextView) dialog.findViewById(R.id.txtNotifiedTo);
                    TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
                    TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
                    Button btnOk = (Button) dialog.findViewById(R.id.btnOk);

                    //message
                    txtMessage.setText("Message : " + message);

                    //title
                    txtTitle.setText("Title : " + title);

                    //response
                    txtStatusResponse.setTextColor(Color.GREEN);
                    if (selectRbStuStf.equals("S")) {
                        txtStatusResponse.setText("Notification sent to students");
                    } else {
                        txtStatusResponse.setText("Notification sent to staffs");
                    }

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            Intent intent = new Intent(AddNotificationToActivity.this, NotificationBroadcastActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    });


                } else {

                    //make dialog box
                    Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_to_show_response_notification_broadcast);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

                    dialog.setCanceledOnTouchOutside(false);

                    TextView txtStatusResponse = (TextView) dialog.findViewById(R.id.txtStatusResponse);
                    TextView txtNotifiedTo = (TextView) dialog.findViewById(R.id.txtNotifiedTo);
                    TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
                    TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
                    Button btnOk = (Button) dialog.findViewById(R.id.btnOk);

                    //message
                    txtMessage.setText("Message : " + message);

                    //title
                    txtTitle.setText("Title" + title);

                    //response
                    txtStatusResponse.setTextColor(Color.RED);
                    if (selectRbStuStf.equals("S")) {
                        txtStatusResponse.setText("Failed to notify students");
                        util.SaveSystemLog(orgId, insId, "Failed to notify students", "Teacher Connect", usrId, "Add Notification Broadcast");
                    } else {
                        txtStatusResponse.setText("Failed to notify staffs");
                        util.SaveSystemLog(orgId, insId, "Failed to notify staffs", "Teacher Connect", usrId, "Add Notification Broadcast");
                    }

                    // txtStatusResponse.setText("Failed to send notification!");

                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                            return;
                        }
                    });
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while sending notification broadcast message", "Teacher Connect", usrId, "Add Notification Broadcast");
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                if (base64StringForUploadFile == null || base64StringForUploadFile.equals("null")) {
                    base64StringForUploadFile = "";
                }

                if (filePath == null) {

                    notificationS3Url = "";
                    resSendNotification = util.sendNotification(orgId, insId, dscId, ayr, stdId, divId, uName, "Y", "Y", selectRbStuStf, curDate, curDate, nType, selectRbAddTimline, "", ntfUploadContentType, title, message, notifyAllDivision, base64StringForUploadFile, fileExtension, stfGrades, stfIds, stdName, divName, usrId, notificationS3Url);
                } else {
                    File file = getFile(context, filePath);
                    String selectedFilePath = file.getPath();
                    SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-HH:mm:ss:SSS");
                    String fileName = "TC_NOT_".concat(String.valueOf(orgId)).concat("_").concat(String.valueOf(insId)).concat("_").concat(String.valueOf(dscId)).concat("_").concat(String.valueOf(usrAssetId)).concat("_").concat(String.valueOf(usrId)).concat("_").concat(ft.format(new Date())).concat(fileExtension);
                    boolean status = uploadFilesToS3(selectedFilePath, fileName);

                    if (status) {

                        notificationS3Url = "http://s3.ap-southeast-1.amazonaws.com/".concat("amoghcloudfront").concat("/").concat(fileName);
                        resSendNotification = util.sendNotification(orgId, insId, dscId, ayr, stdId, divId, uName, "Y", "Y", selectRbStuStf, curDate, curDate, nType, selectRbAddTimline, "", ntfUploadContentType, title, message, notifyAllDivision, base64StringForUploadFile, fileExtension, stfGrades, stfIds, stdName, divName, usrId, notificationS3Url);
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Failed to send notification!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                    }
                }

                //resSendNotification = util.sendNotification(orgId, insId, dscId, ayr, stdId, divId, uName, "Y", selectRbStuStf, curDate, curDate, nType, selectRbAddTimline, "", "", title, message, notifyAllDivision,base64StringForUploadFile,ntfUploadContentType);

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while sending notification broadcast message", "Teacher Connect", usrId, "Add Notification Broadcast");
            }
            return null;
        }
    }

    public boolean uploadFilesToS3(String filePath, String fileName){

        try {

            ClientConfiguration clientConfiguration = new ClientConfiguration()
                    .withMaxErrorRetry(1) // 1 retries
                    .withConnectionTimeout(200000) // 30,000 ms
                    .withSocketTimeout(200000); // 30,000 ms

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), clientConfiguration);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

            //PutObjectRequest por = new PutObjectRequest(bucketName.concat("/etechschool"),fileName, new File(filePath));
            //PutObjectRequest por = new PutObjectRequest(bucketName, fileName, new File(filePath));
            //All student uploaded documents will be uploaded on "amoghcloudfront" cloud bucket for now.
            PutObjectRequest por= new PutObjectRequest("amoghcloudfront", fileName, new File(filePath));
            por.withCannedAcl(CannedAccessControlList.PublicRead);
            s3Client.putObject(por);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public static File getFile(Context context, Uri uri) throws IOException {
        File destinationFilename = new File(context.getFilesDir().getPath() + File.separatorChar + queryName(context, uri));
        try (InputStream ins = context.getContentResolver().openInputStream(uri)) {
            createFileFromStream(ins, destinationFilename);
        } catch (Exception ex) {
            Log.e("Save File", ex.getMessage());
            ex.printStackTrace();
        }
        return destinationFilename;
    }

    public static void createFileFromStream(InputStream ins, File destination) {
        try (OutputStream os = new FileOutputStream(destination)) {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = ins.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
        } catch (Exception ex) {
            Log.e("Save File", ex.getMessage());
            ex.printStackTrace();
        }
    }

    private static String queryName(Context context, Uri uri) {
        Cursor returnCursor =
                context.getContentResolver().query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }
}

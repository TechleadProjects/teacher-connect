package com.techled.teacheranalytics.ActivityList.MyAttendanceModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.BookingDecorator;
import com.techled.teacheranalytics.pojo.DailyAttendance;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class MyAttendanceFragment extends Fragment {
    private ProgressDialog progDailog;
    private Context context;
    private Util util;
    private ArrayList<Date> dates;
    private HashSet<Date> datesSetForO, datesSetForW, datesSetForV, datesSetForP, datesSetForA, datesSetForF, datesSetForS, datesSetForX;
    private HashMap<Date, String> hashMap;
    private Date stringDate;
    private int orgId, insId, dscId, ayr, usrId, usrAssetId;
    private String params, response, resMyInfo;
    private String resDailyAttd, pastResDailyAttd;
    private int stdId, divId;
    private ArrayList<DailyAttendance> dailyAttendanceArrayList;
    private MaterialCalendarView mcv;
    private LinearLayout layParent;
    private LinearLayout layNoRecord;
    private String ayrStartDate, ayrEndDate;
    private Date startDate, endDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_monthly_attendance, container, false);

        try {

            context = getActivity();
            util = new Util();

            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);


                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
            }


            //my info
            SharedPreferences prefsForStdDiv = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefsForStdDiv != null) {
                resMyInfo = prefsForStdDiv.getString("response", null);
            } else {
                resMyInfo = util.getMyInfo(usrId, ayr);
            }


            if (resMyInfo != null) {
                JSONArray array = new JSONArray(resMyInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    usrId = jsonObject.getInt("usrAssetId");
                    stdId = jsonObject.getInt("stdId");
                    divId = jsonObject.getInt("divId");
                    if (jsonObject.getInt("payrollInsId") != 0) {
                        insId = jsonObject.getInt("payrollInsId");
                    }
                    if (jsonObject.getInt("payrollStfId") != 0) {
                        usrAssetId = jsonObject.getInt("payrollStfId");
                    }
                }


            }


            //get reference
            layParent = (LinearLayout) view.findViewById(R.id.layParent);
            mcv = (MaterialCalendarView) view.findViewById(R.id.cv);
            progDailog = new ProgressDialog(context);
            layNoRecord = (LinearLayout) view.findViewById(R.id.layNoRecord);


            //load Daily attendance from pref
            SharedPreferences dailyAttdprefs = context.getSharedPreferences("dailyAttd", context.MODE_PRIVATE);
            if (dailyAttdprefs != null) {
                pastResDailyAttd = dailyAttdprefs.getString("response", null);
            }


            //load daily attd
            new LoadDayWiseAttd(getActivity(), null).execute("");

            //defualt mcv
            try {
                mcv.state().edit()
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        .setCalendarDisplayMode(CalendarMode.MONTHS)
                        .commit();
                mcv.setShowOtherDates(0);
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;

    }


    public static Fragment newInstance(int i, String s) {
        MyAttendanceFragment fragmentFirst = new MyAttendanceFragment();
        return fragmentFirst;
    }

    public class LoadDayWiseAttd extends AsyncTask<String, String, String> {
        public LoadDayWiseAttd(FragmentActivity activity, Object o) {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();

            try {
                if (resDailyAttd != null) {

                    JSONObject jsonObject = new JSONObject(resDailyAttd);
                    JSONArray array = jsonObject.getJSONArray("staffAttd");

                    if (array.length() == 0) {
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                        //    Toast.makeText(context, "Daily attendance not marked!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Daily attendance not marked!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        util.SaveSystemLog(orgId,insId,"Daily attendance not marked","Teacher Connect",usrId,"My Attendance");
                        return;
                    }
                    dailyAttendanceArrayList = new ArrayList<>();
                    dates = new ArrayList<Date>();
                    datesSetForA = new HashSet<Date>();
                    datesSetForO = new HashSet<Date>();
                    datesSetForP = new HashSet<Date>();
                    datesSetForV = new HashSet<Date>();
                    datesSetForW = new HashSet<Date>();
                    datesSetForS = new HashSet<Date>();
                    datesSetForF = new HashSet<Date>();
                    datesSetForX = new HashSet<Date>();
                    hashMap = new HashMap();

                    JSONObject object1 = array.getJSONObject(0);
                    JSONObject object2 = array.getJSONObject(array.length()-1);
                    ayrStartDate = object1.getString("date");
                    ayrEndDate = object2.getString("date");

                    for (int i = 0; i < array.length(); i++) {

                        DailyAttendance dailyAttendance = new DailyAttendance();
                        JSONObject object = array.getJSONObject(i);

                        dailyAttendance.setDate(object.getString("date"));
                        dailyAttendance.setDesc(object.getString("desc"));
                        dailyAttendance.setStatus(object.getString("status"));
                        dailyAttendanceArrayList.add(dailyAttendance);
                        final String date1 = object.getString("date");

                        if (date1 == null) {

                        } else {

                            SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
                            stringDate = simpledateformat.parse(date1);
                            mcv.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
                            mcv.setSelectionColor(Color.TRANSPARENT);
                            startDate =  simpledateformat.parse(ayrStartDate);
                            endDate = simpledateformat.parse(ayrEndDate);
                            mcv.state().edit().setMaximumDate(endDate).commit();
                            mcv.state().edit().setMinimumDate(startDate).commit();

                            // datesSet.add(stringDate);
                            if (object.getString("status").equals("O")) {
                                datesSetForO.add(stringDate);
                            } else if (object.getString("status").equals("A")) {
                                datesSetForA.add(stringDate);
                            } else if (object.getString("status").equals("P")) {
                                datesSetForP.add(stringDate);
                            } else if (object.getString("status").equals("V")) {
                                datesSetForV.add(stringDate);
                            } else if (object.getString("status").equals("W")) {
                                datesSetForW.add(stringDate);
                            } else if (object.getString("status").equals("S")) {
                                datesSetForS.add(stringDate);
                            } else if (object.getString("status").equals("F")) {
                                datesSetForF.add(stringDate);
                            } else if (object.getString("status").equals("X")) {
                                datesSetForX.add(stringDate);
                            }
                            dates.add(stringDate);
                            hashMap.put(stringDate, object.getString("desc"));
                        }

                    }


                    mcv.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, datesSetForV, context, "V"));

                    mcv.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, datesSetForA, context, "A"));
                    mcv.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, datesSetForP, context, "P"));


                    mcv.addDecorator(new BookingDecorator(R.drawable.circle_orange, datesSetForW, context, "W"));
                    mcv.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForO, context, "O"));
                    mcv.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForS, context, "S"));
                    mcv.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForF, context, "F"));
                    mcv.addDecorator(new BookingDecorator(R.drawable.circle_green, datesSetForX, context, "X"));

                    mcv.setOnDateChangedListener(new OnDateSelectedListener() {
                        @Override
                        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                            Log.d("SIZE", "" + dates.size());
                            try {

                                String DATE = date.toString();
                                String[] d = DATE.split("-");
                                int dd = date.getDay();

                                int mm = date.getMonth() + 1;
                                int yyyy = date.getYear();

                                DATE = yyyy + "-" + mm + "-" + dd;
                                String ketDate = yyyy + "-" + date.getMonth() + "-" + dd;

                                SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
                                //   mcv.setSelected(false);
                                Date d1 = simpledateformat.parse(DATE);


                                if (dates.contains(d1)) {
                                    //Toast.makeText(application, "Right Value Holiday Present"+DATE, Toast.LENGTH_SHORT).show();

                                    for (Map.Entry<Date, String> entry : hashMap.entrySet()) {
                                        Date key = entry.getKey();
                                        if (key.equals(d1)) {
                                            String value = entry.getValue();
                                            //   textHoliday.setText("Today is holiday for "+value);
                                            //   Toast.makeText(context, "YES"+value, Toast.LENGTH_SHORT).show();
                                            Snackbar snackbar = Snackbar
                                                    .make(layParent, DATE + " ( " + value + " ) ", Snackbar.LENGTH_LONG);
                                            snackbar.setDuration(3000);
                                            snackbar.show();

                                        }

                                    }


                                } else {
                                    Snackbar snackbar = Snackbar
                                            .make(layParent, "Sorry! No information is available for this " + DATE, Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(3000);
                                    snackbar.show();



                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance history","Teacher Connect",usrId,"My Attendance");
                            }
                        }
                    });

                } else {
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    //     Toast.makeText(context, "Daily attendance not marked!", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(layParent, "Daily attendance not marked!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();

                    util.SaveSystemLog(orgId,insId,"Daily attendance not marked","Teacher Connect",usrId,"My Attendance");
                    return;

                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance history","Teacher Connect",usrId,"My Attendance");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                // if (pastResDailyAttd == null || pastResDailyAttd.equals("")) {
                resDailyAttd = util.getDayWiseMonthInfo(orgId, insId, dscId, ayr, usrAssetId);

                SharedPreferences.Editor editor = context.getSharedPreferences("dailyAttd", context.MODE_PRIVATE).edit();
                editor.putString("response", resDailyAttd);
                editor.apply();
//                } else {
//                    resDailyAttd = pastResDailyAttd;
//                }

            } catch (Exception e) {
                e.printStackTrace();

                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance history","Teacher Connect",usrId,"My Attendance");
            }
            return null;
        }
    }


}

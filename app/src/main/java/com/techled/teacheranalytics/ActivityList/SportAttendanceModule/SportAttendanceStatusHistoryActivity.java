package com.techled.teacheranalytics.ActivityList.SportAttendanceModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule.AttendanceActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.BookingDecorator;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.SportStdDivDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

public class SportAttendanceStatusHistoryActivity extends AppCompatActivity {
    private Util util;
    private Context context;
    private String params, resUserInfo, resMarkAttd;
    private MaterialCalendarView calView;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId, stdId, divId;
    private ProgressDialog progDailog;
    private ArrayList<Date> dates;
    private HashSet<Date> datesSetForNotMarkedAttd, attdMarkedSet;
    private HashMap<Date, String> hashMap;
    private HashSet<Date> workingDaysSet, sportsDaysSet, sportAttdMarkedSet;
    private Date stringDate, compareDate;
    private int lastDay, mMonth, mYear;
    private Date gDate;
    private LinearLayout layNoRecord, layParent;
    private Button btnMarkAttd;
    private String selectedDate = "", stdDivName;
    private Dialog dialog;
    private ArrayList<SportStdDivDetails> sportStdDivDetailsArrayList;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_sport_attendance_status);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Sport Attendance");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference

            context = this;
            util = new Util();



            dates = new ArrayList<Date>();
            datesSetForNotMarkedAttd = new HashSet<Date>();
            hashMap = new HashMap();
            calView = (MaterialCalendarView) findViewById(R.id.calView);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);


            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");


            }

            //get data
            SharedPreferences prefUserInfo = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    JSONArray classTeacherOfJson = jsonObject.getJSONArray("classTeacherOf");


//                    if (classTeacherOfJson != null || !classTeacherOfJson.equals("[]") || classTeacherOfJson.length() != 0) {
//
//                        Snackbar snackbar = Snackbar
//                                .make(layParent, "Error to load standard/division!", Snackbar.LENGTH_LONG);
//                        snackbar.setDuration(3000);
//                        snackbar.show();
//                        return;
//                    }

//                    JSONObject object = classTeacherOfJson.getJSONObject(0);
//                    String stdDivIds = object.getString("value");
//                    String[] splStrs = stdDivIds.split("-");
//
//                    stdId = Integer.parseInt(splStrs[0]);
//                    divId = Integer.parseInt(splStrs[1]);


                }
            }


            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            //load standard - division
            LoadSportStdDiv loadSportStdDiv = new LoadSportStdDiv();
            loadSportStdDiv.execute(null, null);

            btnMarkAttd = (Button) findViewById(R.id.btnMarkAttd);
            btnMarkAttd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (compareDate == null) {
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select date for attendance marking!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        CheckIsSportDay checkIsSportDay = new CheckIsSportDay();
                        checkIsSportDay.execute(null, null);
                        if (!workingDaysSet.contains(compareDate) && !sportAttdMarkedSet.contains(compareDate)) {

                            Calendar c = Calendar.getInstance();   // this takes current date
                            c.set(Calendar.DAY_OF_MONTH, 1);
                            System.out.println(c.getTime());
                            Date firstDateOfMonth = c.getTime();
                            if (compareDate.before(firstDateOfMonth)) {
                                Intent intent = new Intent(SportAttendanceStatusHistoryActivity.this, AttendanceActivity.class);
                                intent.putExtra("selectedDate", selectedDate);
                                startActivity(intent);
                                finish();
                                return;
                            } else {
                                //  Toast.makeText(context, selectedDate + " is not working day or its future date!", Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(layParent, selectedDate + " is not working day or its future date!", Snackbar.LENGTH_LONG);
                                snackbar.setDuration(3000);
                                snackbar.show();
                                return;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class CheckIsSportDay extends AsyncTask<String, String, String> {
        private String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                progDailog.dismiss();
                if (response != null) {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if (status.equals("NOT_A_SPORT_DAY")) {
                        //Toast.makeText(context, selectedDate+" not a sport day!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, selectedDate + " not a sport day!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();

                        return;

                    } else if (status.equals("ERROR")) {
                        //Toast.makeText(context, selectedDate+" not a sport day!", Toast.LENGTH_SHORT).show();

                        Snackbar snackbar = Snackbar
                                .make(layParent, selectedDate + " not a sport day!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;

                    }
                    {
                        Intent intent = new Intent(SportAttendanceStatusHistoryActivity.this, FinalMarkSportAttendanceActivity.class);
                        intent.putExtra("selectedDate", selectedDate);
                        intent.putExtra("stdId", stdId);
                        intent.putExtra("divId", divId);
                        intent.putExtra("stdDivName", stdDivName);
                        startActivity(intent);
                        finish();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {


                SimpleDateFormat spf = new SimpleDateFormat("dd-MMM-yyyy");
                Date newDate = spf.parse(selectedDate);
                spf = new SimpleDateFormat("yyyy-MM-dd");
                String date = spf.format(newDate);
                System.out.println(date);

                response = util.checkIsSportDay(orgId, insId, dscId, ayr, stdId, divId, date);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadSportStdDiv extends AsyncTask<String, String, String> {

        private String resSportStdDiv;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resSportStdDiv != null) {

                    JSONArray resArray = new JSONArray(resSportStdDiv);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        JSONArray dataArray = dataObj.getJSONArray("data");

                        if (dataArray.length() == 0) {
                            layNoRecord.setVisibility(View.VISIBLE);
                            layParent.setVisibility(View.GONE);
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Sorry, Only sport teacher can mark attendance!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        sportStdDivDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            SportStdDivDetails sportStdDivDetails = new SportStdDivDetails();
                            JSONObject object = dataArray.getJSONObject(i);

                            sportStdDivDetails.setDivId(object.getInt("divId"));
                            sportStdDivDetails.setStdId(object.getInt("stdId"));
                            sportStdDivDetails.setDivName(object.getString("divName"));
                            sportStdDivDetails.setStdName(object.getString("stdName"));
                            sportStdDivDetailsArrayList.add(sportStdDivDetails);

                        }

                        if (sportStdDivDetailsArrayList.size() == 0) {


                            layNoRecord.setVisibility(View.VISIBLE);
                            layParent.setVisibility(View.GONE);
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Sorry, Only sport teacher can mark attendance!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        } else {

                            //show standard division selection dialog
                            dialog = new Dialog(context);
                            dialog.setContentView(R.layout.dialog_load_std_div_for_markattendance);
                            dialog.show();
                            Window window = dialog.getWindow();
                            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                            dialog.setCanceledOnTouchOutside(false);
                            dialog.setCancelable(false);

                            Spinner spinnStdDiv = (Spinner) dialog.findViewById(R.id.spinnStdDiv);
                            ArrayAdapter<SportStdDivDetails> stdDivAdaptor = new ArrayAdapter<SportStdDivDetails>(SportAttendanceStatusHistoryActivity.this,
                                    android.R.layout.simple_spinner_item, sportStdDivDetailsArrayList);
                            stdDivAdaptor
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnStdDiv.setAdapter(stdDivAdaptor);

                            spinnStdDiv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    stdId = sportStdDivDetailsArrayList.get(position).getStdId();
                                    divId = sportStdDivDetailsArrayList.get(position).getDivId();
                                    stdDivName = sportStdDivDetailsArrayList.get(position).getStdName() + " - " + sportStdDivDetailsArrayList.get(position).getDivName();

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    if (stdId != 0 || divId != 0) {
                                        try {
                                            dialog.dismiss();
                                            //load mark atttd
                                            SportAttendanceStatusHistoryActivity.LoadMearkAttdData mearkAttdData = new SportAttendanceStatusHistoryActivity.LoadMearkAttdData();
                                            mearkAttdData.execute(null, null);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        // Toast.makeText(context, "Please select standard/division!", Toast.LENGTH_SHORT).show();
                                        Snackbar snackbar = Snackbar
                                                .make(layParent, "Please select standard/division!", Snackbar.LENGTH_LONG);
                                        snackbar.setDuration(3000);
                                        snackbar.show();
                                        return;
                                    }
                                }
                            });

                            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    onBackPressed();
                                }
                            });

                        }


                    } else {
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Sorry, Only sport teacher able to marked attendance!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }

                } else {

                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Sorry, Only sport teacher able to marked attendance!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                resSportStdDiv = util.loadSportStdDiv(orgId, insId, dscId, ayr, usrAssetId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class LoadMearkAttdData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {

                if (resMarkAttd == null || resMarkAttd.equals("")) {


                    Snackbar snackbar = Snackbar
                            .make(layParent, "Sport attendance status not found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    util.SaveSystemLog(orgId, insId, "Sport attendance status not found", "Teacher Connect", usrId, "Mark Attendance");
                    return;
                } else {
                    JSONArray jsonArray = new JSONArray(resMarkAttd);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {


                        workingDaysSet = new HashSet<>();
                        sportAttdMarkedSet = new HashSet<>();
                        sportsDaysSet = new HashSet<>();


                        //get working days array
                        JSONArray workingDaysArray = object1.getJSONArray("workingDays");

                        for (int m = 0; m < workingDaysArray.length(); m++) {
                            JSONObject object = workingDaysArray.getJSONObject(m);
                            String markDate = object.getString("workingDay");
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                            Date workingDate = format.parse(markDate);

                            workingDaysSet.add(workingDate);


                        }

                        //get sport attendance mark array
                        JSONArray sportAttdmarkedArray = object1.getJSONArray("data");
                        for (int i = 0; i < sportAttdmarkedArray.length(); i++) {
                            JSONObject sObj = sportAttdmarkedArray.getJSONObject(i);

                            String markDate = sObj.getString("date");
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                            Date sportAttdMarkedDate = format.parse(markDate);

                            if (workingDaysSet.contains(sportAttdMarkedDate)) {
                                sportAttdMarkedSet.add(sportAttdMarkedDate);
                                workingDaysSet.remove(sportAttdMarkedDate);
                            }


                        }


                        //get sport days array
                        JSONArray sportDaysArray = object1.getJSONArray("sportsDays");

                        for (int i = 0; i < sportDaysArray.length(); i++) {
                            JSONObject sObj = sportDaysArray.getJSONObject(i);

                            String markDate = sObj.getString("sportsDay");
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                            Date sportDate = format.parse(markDate);

                            if (workingDaysSet.contains(sportDate)) {
                                sportsDaysSet.add(sportDate);
                                workingDaysSet.remove(sportDate);
                            }


                        }
                    }


                    calView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_red, sportsDaysSet, context, "A"));
                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_green, sportAttdMarkedSet, context, "P"));
                    calView.addDecorator(
                            new BookingDecorator(R.drawable.circle_gray, workingDaysSet, context, "X"));
                    calView.state().edit()
                            .setCalendarDisplayMode(CalendarMode.MONTHS)
                            .commit();


                }


                calView.setOnDateChangedListener(new OnDateSelectedListener() {
                    @Override
                    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                        try {


                            selectedDate = date.toString();
                            int dd = date.getDay();
                            int mm = date.getMonth() + 1;
                            int yyyy = date.getYear();

                            selectedDate = dd + "-" + mm + "-" + yyyy;

                            DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
                            DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy");
                            compareDate = originalFormat.parse(selectedDate);
                            selectedDate = targetFormat.format(compareDate);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


            } catch (Exception e) {

                e.printStackTrace();
                // Toast.makeText(context, "Attendance status not found!", Toast.LENGTH_SHORT).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Sport attendance status not found!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                layNoRecord.setVisibility(View.VISIBLE);
                layParent.setVisibility(View.GONE);
                util.SaveSystemLog(orgId, insId, "Error occur while loading sport attendance history in graph view", "Teacher Connect", usrId, "Mark Attendance");
                return;
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                resMarkAttd = util.FetchSportAttendance(orgId, insId, dscId, ayr, stdId, divId, selectedDate);

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading attendance history in graph view", "Teacher Connect", usrId, "Mark Attendance");
            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

}

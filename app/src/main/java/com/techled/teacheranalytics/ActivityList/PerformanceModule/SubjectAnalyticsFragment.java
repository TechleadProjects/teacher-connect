package com.techled.teacheranalytics.ActivityList.PerformanceModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.SubjectAnalyticsRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.StudentInfo;
import com.techled.teacheranalytics.pojo.SubjectInfo;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class SubjectAnalyticsFragment extends Fragment {

    private int stdId, divId;
    private String stdName, divName;
    private TextView txtStdDiv, txtSubjectInfo, countGood, countAvg, countBad;
    private ProgressDialog progDailog;
    private Context context;
    private Util util;
    private int subId = 0;
    private String response;
    private String examTypes = "";
    private int orgId = 0, insId = 0, dscId = 0, ayrYear = 0;
    private String params;
    private RecyclerView subjectAnalyticsRecyView;
    private ArrayList<SubjectInfo> subjectInfoArrayList;
    private String subName;
    private ImageView btnRefresh;
    private LinearLayout parentLay;
    private int countGreen = 0, countOrange = 0, countRed = 0;
    private List<StudentInfo> studentInfoArrayList;
    private SharedPreferences.Editor editor;
    private String prefData = null;
    private LinearLayout layParent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            View view = inflater.inflate(R.layout.fragment_sub_analytics, container, false);
            context = getContext();
            util = new Util();
            //get reference
            subjectAnalyticsRecyView = (RecyclerView) view.findViewById(R.id.subjectAnalyticsRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
            subjectAnalyticsRecyView.setLayoutManager(layoutManager);
            layParent = (LinearLayout) view.findViewById(R.id.layParent);
            txtStdDiv = (TextView) view.findViewById(R.id.txtStdDiv);
            txtSubjectInfo = (TextView) view.findViewById(R.id.txtSubjectInfo);
            countGood = (TextView) view.findViewById(R.id.countGood);
            countAvg = (TextView) view.findViewById(R.id.countAvg);
            countBad = (TextView) view.findViewById(R.id.countBad);

            //login details
            try {
                SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
                params = loginDetails.getString("params", null);

                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);


                    //org shared pref
                    SharedPreferences orgPref = context.getSharedPreferences("Organization", 0);
                    if (orgPref != null) {
                        if (!orgPref.getString("Org_Id", "0").equals("")) {
                            orgId = Integer.parseInt(orgPref.getString("Org_Id", "0"));

                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        } else {
                            if (orgId == 0) {
                                orgId = object.getInt("orgId");
                            }
                        }

                    }
                    //ins shared pref
                    SharedPreferences insPref = context.getSharedPreferences("Institute", 0);
                    if (insPref != null) {
                        if (!insPref.getString("Ins_Id", "0").equals("")) {
                            insId = Integer.parseInt(insPref.getString("Ins_Id", "0"));
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        } else {
                            if (insId == 0) {
                                insId = object.getInt("insId");
                            }
                        }
                    }

                    ayrYear = object.getInt("ayrYear");
                    dscId = object.getInt("dscId");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            //get selected std,div,sub etc
            final Intent intent = getActivity().getIntent();
            if (intent != null) {
                stdId = intent.getIntExtra("stdId", 0);
                divId = intent.getIntExtra("divId", 0);
                stdName = intent.getStringExtra("stdName");
                divName = intent.getStringExtra("divName");
                subId = intent.getIntExtra("subId", 0);
                subName = intent.getStringExtra("subName");
                ayrYear = intent.getIntExtra("ayrYear", 0);

            }

            //retrive the data
            SharedPreferences prefs = context.getSharedPreferences("subjectAnalytics", context.MODE_PRIVATE);
            if (prefs != null) {
                String idsData = prefs.getString("ids", null);
                if (idsData != null) {
                    String Ids[] = idsData.split(",");
                    int STD_ID = Integer.parseInt(Ids[0]);
                    int DIV_ID = Integer.parseInt(Ids[1]);
                    int SUB_ID = Integer.parseInt(Ids[2]);


                    //if pass selected std /div and sub is same the load data from shared pref
                    if (STD_ID == stdId && DIV_ID == divId && SUB_ID == subId) {
                        prefData = prefs.getString("response", null);
                    }
                }

            }

            //store data
            editor = context.getSharedPreferences("subjectAnalytics", Context.MODE_PRIVATE).edit();
            String ids = stdId + "," + divId + "," + subId;
            editor.putString("ids", ids);
            editor.apply();


            txtStdDiv.setText("" + stdName + " " + divName);

            //intent to subject teacher
            LinearLayout layStdDivSub = (LinearLayout) view.findViewById(R.id.layStdDivSub);
            layStdDivSub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(context, SubjectTeacherInfoActivity.class);
                    intent1.putExtra("stdId", stdId);
                    intent1.putExtra("divId", divId);
                    intent1.putExtra("subId", subId);
                    intent1.putExtra("stdName", stdName);
                    intent1.putExtra("divName", divName);
                    intent1.putExtra("subName", subName);
                    intent1.putExtra("ayrYear", ayrYear);
                    ((context)).startActivity(intent1);
                }
            });


            try {

                //load data
                loadSubjectAnalyticsData();
                txtSubjectInfo.setText("" + subName);

            } catch (Exception e) {
                e.printStackTrace();
            }
            // Inflate the layout for this fragment
            return view;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void loadSubjectAnalyticsData() {
        new loadSubjectAnalyticsData().execute();
    }

    private class loadSubjectAnalyticsData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response != null) {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");
                    subjectInfoArrayList = new ArrayList<>();
                    if (status.equals("SUCCESS")) {
                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray dataArray = object2.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            SubjectInfo subjectInfo = new SubjectInfo();
                            JSONObject object = dataArray.getJSONObject(i);
                            subjectInfo.setMarks(object.getString("marks"));
                            subjectInfo.setColor(object.getString("color"));
                            subjectInfo.setSubId(object.getInt("subId"));
                            subjectInfo.setSubName(object.getString("subName"));
                            subjectInfo.setStdId(object.getInt("stdId"));
                            subjectInfo.setDivId(object.getInt("divId"));
                            subjectInfo.setStdName(object.getString("stdName"));
                            subjectInfo.setDivName(object.getString("divName"));


                            JSONArray array1 = object.getJSONArray("subjectInfo");
                            studentInfoArrayList = new ArrayList<>();
                            for (int j = 0; j < array1.length(); j++) {
                                JSONObject object3 = array1.getJSONObject(j);
                                StudentInfo studentInfo = new StudentInfo();
                                studentInfo.setSubName(object3.getString("subName"));
                                studentInfo.setMarks(object3.getString("marks"));
                                studentInfo.setColor(object3.getString("color"));
                                studentInfo.setSubId(object3.getInt("subId"));
                                studentInfo.setStdName(object3.getString("stdName"));
                                studentInfo.setStdId(object3.getInt("stdId"));
                                studentInfo.setDivName(object3.getString("divName"));
                                studentInfo.setDivId(object3.getInt("divId"));
                                studentInfo.setStuName(object3.getString("stuName"));
                                studentInfo.setStuId(object3.getInt("stuId"));
                                studentInfoArrayList.add(studentInfo);
                                subjectInfo.setStudentInfoArrayList(studentInfoArrayList);

                            }


                            //store data
                            SharedPreferences.Editor editor = getActivity().getSharedPreferences("subAnalyticsData", context.MODE_PRIVATE).edit();
                            editor.putString("response", response);
                            editor.apply();

                            if (subName.equals(object.getString("subName"))) {

                                subjectInfoArrayList.add(subjectInfo);


                                for (int m = 0; m < studentInfoArrayList.size(); m++) {

                                    if (studentInfoArrayList.get(m).getColor().equals("#43A047")) {
                                        countGreen++;
                                    } else if (studentInfoArrayList.get(m).getColor().equals("#FB8C00")) {
                                        countOrange++;
                                    } else {
                                        countRed++;
                                    }
                                }
                                countAvg.setText("Avg. : " + countOrange);
                                countBad.setText("Below Avg. : " + countRed);
                                countGood.setText("Above Avg. : " + countGreen);

                            }
                        }


                        //set adapter

                        SubjectAnalyticsRecyAdapter adapter = new SubjectAnalyticsRecyAdapter(subjectInfoArrayList, context);
                        subjectAnalyticsRecyView.setAdapter(adapter);


                    } else {
                        // Toast.makeText(context, "No data available for academic performance!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No data available for academic performance!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }


                } else {
                    //   Toast.makeText(context, "No data available for academic performance!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No data available for academic performance!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                if (prefData == null) {
                    response = util.getClassAnalytics(orgId, insId, dscId, ayrYear, stdId, divId, subId, examTypes);
                } else {
                    response = prefData;
                }
                editor.putString("response", response);
                editor.apply();


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    public static Fragment newInstance(int i, String s) {
        SubjectAnalyticsFragment fragmentFirst = new SubjectAnalyticsFragment();
        return fragmentFirst;
    }
}

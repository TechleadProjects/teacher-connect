package com.techled.teacheranalytics.ActivityList.EvaluateAnswerPaperModule;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.bumptech.glide.Glide;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.UploadedDocsForEvaluationRecyclerAdapter;
import com.techled.teacheranalytics.pojo.EvaluateDocumentData;
import com.techled.teacheranalytics.util.Util;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SubmittedPaperSelectionActivity extends AppCompatActivity {

    private Context context;
    private ArrayList<EvaluateDocumentData> uploadedDocs;
    private Util util;

    private ProgressDialog progDailog;
    private String response, params;
    private String accessKey;
    private String secretKey;

    private int orgId, insId, dscId, stfId;

    private RecyclerView recyclerView;
    private TextView studentExamName;
    private UploadedDocsForEvaluationRecyclerAdapter adapter;

    private final int PHOTO_EDITOR_REQUEST_CODE = 231;

    public static String outputImagePath = "";
    public static int index = 0;
    public static int exdId = 0;
    public static String originalFileName;
    private String errorMessage;

    private float totalMarksGiven;
    boolean isAllowedToEnterMarks = true;

    private String selectedAyrYear, selectedStandard, selectedDivision, selectedSubject, selectedExamType, selectedExamSchedule, stuId, examName, stuName;
    private String s3linkString, enteredMarks, enteredComment;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submitted_paper_selection);

        context = this;
        util = new Util();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Uploaded Papers");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        try {

            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                stfId = object.getInt("assetId1");

            }

            final SharedPreferences sharedPreferences = getSharedPreferences("user", 0);

            accessKey = sharedPreferences.getString("accessKey", null);
            secretKey = sharedPreferences.getString("secretKey", null);

            Thread t1 = new Thread(() -> Glide.get(context).clearDiskCache());
            t1.start();

            uploadedDocs = new ArrayList<>();

            recyclerView = findViewById(R.id.recyclerView);

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            studentExamName = findViewById(R.id.studentExamName);

            selectedAyrYear = getIntent().getStringExtra("ayrYear");
            selectedStandard = getIntent().getStringExtra("standard");
            selectedDivision = getIntent().getStringExtra("division");
            selectedSubject = getIntent().getStringExtra("subject");
            selectedExamType = getIntent().getStringExtra("examType");
            selectedExamSchedule = getIntent().getStringExtra("examSchedule");
            examName = getIntent().getStringExtra("examName");
            stuName = getIntent().getStringExtra("stuName");
            stuId = getIntent().getStringExtra("stuId");

            studentExamName.setText(stuName.concat(" > ").concat(examName));

            new LoadUploadedPapers().execute();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PHOTO_EDITOR_REQUEST_CODE) { // same code you used while starting

            if (data != null && data.getBooleanExtra("is_image_edited", false)) {

                Uri uri = Uri.fromFile(new File(outputImagePath));
                File file = new File(outputImagePath);

                final Dialog dialog = new Dialog(context);
                dialog.setTitle("Evaluate Answer Paper");
                dialog.setContentView(R.layout.dialog_evaluate_answer_paper);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                ImageView evaluatedPaper = dialog.findViewById(R.id.evaluatePaper);
                Button btnOk = dialog.findViewById(R.id.btnOk);
                Button btnCancel = dialog.findViewById(R.id.btnCancel);

                EditText marks = dialog.findViewById(R.id.marks);
                EditText comment = dialog.findViewById(R.id.comment);

                TextView maxMarks = dialog.findViewById(R.id.maxMarks);
                isAllowedToEnterMarks = true;

                evaluatedPaper.setImageURI(uri);

                if (uploadedDocs.get(index).getExdMaxMarks() != null && !uploadedDocs.get(index).getExdMaxMarks().equals("") && !uploadedDocs.get(index).getExdMaxMarks().equals("null") && !uploadedDocs.get(index).getExdMaxMarks().equals("0")) {

                    if (uploadedDocs.get(index).getExdMarks() == null || uploadedDocs.get(index).getExdMarks().equals("") || uploadedDocs.get(index).getExdMarks().equals("null") || uploadedDocs.get(index).getExdMarks().equals("0.0")) {

                        if (Float.parseFloat(uploadedDocs.get(index).getExdMaxMarks()) == totalMarksGiven) {
                            isAllowedToEnterMarks = false;
                            marks.setEnabled(false);
                            maxMarks.setVisibility(View.VISIBLE);
                            maxMarks.setText("You cannot give marks to this part as max marks have already been given to the paper.");
                        } else {

                            isAllowedToEnterMarks = true;
                            marks.setEnabled(true);
                            float marksCanBeGivenForThePart = Float.parseFloat(uploadedDocs.get(index).getExdMaxMarks()) - totalMarksGiven;

                            maxMarks.setVisibility(View.VISIBLE);
                            maxMarks.setText("Maximum ".concat(String.valueOf(marksCanBeGivenForThePart)).concat(" marks can be given to this part."));
                        }
                    } else {

                        isAllowedToEnterMarks = true;
                        marks.setEnabled(true);

                        totalMarksGiven = totalMarksGiven - Float.parseFloat(uploadedDocs.get(index).getExdMarks());

                        float marksCanBeGivenForThePart = Float.parseFloat(uploadedDocs.get(index).getExdMaxMarks()) - totalMarksGiven;

                        maxMarks.setVisibility(View.VISIBLE);
                        maxMarks.setText("Maximum ".concat(String.valueOf(marksCanBeGivenForThePart)).concat(" marks can be given to this part."));
                    }
                } else {
                    maxMarks.setVisibility(View.GONE);
                }

                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        enteredMarks = marks.getText().toString();
                        enteredComment = comment.getText().toString();

                        if (enteredMarks.equals("") && isAllowedToEnterMarks) {

                            Toast.makeText(context, "Please enter marks.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (!isAllowedToEnterMarks) {
                            enteredMarks = "0";
                        }

                        float marks = 0;

                        try {

                            if (isAllowedToEnterMarks) {

                                marks = Float.parseFloat(enteredMarks);
                                float marksCanBeGivenForThePart = Float.parseFloat(uploadedDocs.get(index).getExdMaxMarks()) - totalMarksGiven;

                                if (marks > marksCanBeGivenForThePart) {

                                    Toast.makeText(context, "Entered Marks cannot be greater than ".concat(String.valueOf(marksCanBeGivenForThePart)).concat("."), Toast.LENGTH_SHORT).show();
                                } else if (marks == marksCanBeGivenForThePart) {

                                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SubmittedPaperSelectionActivity.this);
                                    builder.setTitle("Student Connect");
                                    builder.setMessage("Please note that, Entered marks matches with the total marks allotted for this paper.\nYou won't be able to evaluate (i.e give marks) to remaining parts (if any).");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog1, int which) {
                                            dialog1.dismiss();
                                            dialog.dismiss();

//                            outputImagePath = "";
//                            index = 0;
//                            finish();
//                            startActivity(getIntent());

                                            uploadDocumentToServer(file);
                                        }
                                    });

                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();

                                        }
                                    });

                                    builder.show();

                                } else {
                                    dialog.dismiss();
                                    uploadDocumentToServer(file);
                                }

                            } else {

                                dialog.dismiss();

//                            outputImagePath = "";
//                            index = 0;
//                            finish();
//                            startActivity(getIntent());

                                uploadDocumentToServer(file);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                        Toast.makeText(context, "Paper evaluation cancelled.", Toast.LENGTH_SHORT).show();

                        outputImagePath = "";
                        index = 0;
                        exdId = 0;
                        originalFileName= "";

                        Intent intent = new Intent(SubmittedPaperSelectionActivity.this, SubmittedPaperSelectionActivity.class);
                        intent.putExtra("ayrYear", selectedAyrYear);
                        intent.putExtra("standard", selectedStandard);
                        intent.putExtra("division", selectedDivision);
                        intent.putExtra("subject", selectedSubject);
                        intent.putExtra("examType", selectedExamType);
                        intent.putExtra("examSchedule", selectedExamSchedule);
                        intent.putExtra("stuId", stuId);
                        intent.putExtra("examName", examName);
                        intent.putExtra("stuName", stuName);
                        startActivity(intent);
                        finish();
                    }
                });
            } else {

                outputImagePath = "";
                index = 0;
                exdId = 0;
                originalFileName = "";
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                Glide.get(context).clearDiskCache();
            }
        });
        t1.start();
    }

    public class LoadUploadedPapers extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading Uploaded Papers...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if ((response == null || response.equals(""))) {
                    Toast.makeText(context, "Failed to get documents for selected student.", Toast.LENGTH_SHORT).show();
                    finish();

                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    totalMarksGiven = 0;
                    uploadedDocs = new ArrayList<>();

                    if (status.equals("SUCCESS")) {

                        uploadedDocs = new ArrayList<>();

                        JSONObject object2 = array.getJSONObject(1);
                        JSONArray jsonArray = object2.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            EvaluateDocumentData evaluateDocumentData = new EvaluateDocumentData();
                            evaluateDocumentData.setExdId(jsonObject1.getInt("exdId"));
                            evaluateDocumentData.setSadRollNo(jsonObject1.getInt("sadRollNo"));
                            evaluateDocumentData.setExdDocLink(jsonObject1.getString("exdDocLink"));
                            evaluateDocumentData.setExdStatus(jsonObject1.getString("exdStatus"));
                            evaluateDocumentData.setExdStuComment(jsonObject1.getString("exdStuComment"));
                            evaluateDocumentData.setExdSource(jsonObject1.getString("exdSource"));
                            evaluateDocumentData.setExduploadedTime(jsonObject1.getString("exduploadedTime"));
                            evaluateDocumentData.setExdEvaluatedDocLink(jsonObject1.getString("exdEvaluatedDocLink"));
                            evaluateDocumentData.setExdEvaluatedComment(jsonObject1.getString("exdEvaluatedComment"));
                            evaluateDocumentData.setExdEvaluatedTime(jsonObject1.getString("exdEvaluatedTime"));
                            evaluateDocumentData.setExdMarks(jsonObject1.getString("exdMarks"));
                            evaluateDocumentData.setExdMaxMarks(String.valueOf(jsonObject1.getInt("exmMaxMarks")));

                            if (evaluateDocumentData.getExdMarks() != null && !evaluateDocumentData.getExdMarks().equals("") && !evaluateDocumentData.getExdMarks().equals("null")) {

                                totalMarksGiven = totalMarksGiven + Float.parseFloat(evaluateDocumentData.getExdMarks());
                            }

                            uploadedDocs.add(evaluateDocumentData);
                        }

                        if(uploadedDocs.size() > 0){

                            adapter = new UploadedDocsForEvaluationRecyclerAdapter(context, uploadedDocs, totalMarksGiven);
                            recyclerView.setAdapter(adapter);

                        }else {

                            Toast.makeText(context, "Documents not found for selected student.", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    } else {

                        Toast.makeText(context, "Documents not found for selected student.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

                Toast.makeText(context, "Failed to get documents for selected student.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.getStudentDocToEvaluate(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), selectedDivision, selectedStandard, stuId, selectedAyrYear, String.valueOf(selectedSubject), selectedExamSchedule, selectedExamType);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void uploadDocumentToServer(File file){

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Uploading files to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });

                String ext = FilenameUtils.getExtension(originalFileName);
                String filePath = file.getPath();

                SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-HH:mm:ss:SSS");
                String fileName = String.valueOf(orgId).concat(".").concat(String.valueOf(insId)).concat(".").concat(String.valueOf(dscId)).concat(".").concat(String.valueOf(stuId)).concat(".").concat(String.valueOf(stfId)).concat(".").concat(String.valueOf(selectedExamSchedule)).concat(".").concat(String.valueOf(selectedStandard).concat(".").concat(String.valueOf(selectedDivision).concat(".").concat(String.valueOf(selectedAyrYear)).concat("_").concat(ft.format(new Date())).concat(".").concat(ext)));
                boolean status = uploadFilesToS3(filePath, fileName);

                if(status){
                    s3linkString = "http://s3.ap-southeast-1.amazonaws.com/".concat("amoghcloudfront").concat("/").concat(fileName);
                }

                runOnUiThread(new Runnable() {
                    public void run() {

                        if (progDailog != null) {
                            progDailog.dismiss();
                        }

                        if(s3linkString != null && !s3linkString.equals("")){

                            new SaveUploadedPapersToServer().execute();

                        } else {

                            new AlertDialog.Builder(context)
                                    .setTitle("Student Connect")
                                    .setCancelable(false)
                                    .setMessage("There was a problem in uploading document(s) to server.\nThis may be a network issue. Try again in some time. If issue persists, then please share the following message to technical team - \n".concat(errorMessage))
                                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        }
                    }
                });
            }
        });
        t1.start();
    }

    public boolean uploadFilesToS3(String filePath, String fileName){

        try {

            ClientConfiguration clientConfiguration = new ClientConfiguration()
                    .withMaxErrorRetry(1) // 1 retries
                    .withConnectionTimeout(200000) // 30,000 ms
                    .withSocketTimeout(200000); // 30,000 ms

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), clientConfiguration);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

            //PutObjectRequest por = new PutObjectRequest(bucketName.concat("/etechschool"),fileName, new File(filePath));
            //PutObjectRequest por = new PutObjectRequest(bucketName, fileName, new File(filePath));
            //All student uploaded documents will be uploaded on "amoghcloudfront" cloud bucket for now.
            PutObjectRequest por = new PutObjectRequest("amoghcloudfront", fileName, new File(filePath));
            por.withCannedAcl(CannedAccessControlList.PublicRead);
            s3Client.putObject(por);
            return true;
        } catch (Exception e){
            errorMessage = "";
            errorMessage = e.toString();
            return false;
        }
    }

    public class SaveUploadedPapersToServer extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Saving details to server...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progDailog.dismiss();
            try {
                if ((response == null || response.equals(""))) {
                    Toast.makeText(context, "Failed to get documents for selected student.", Toast.LENGTH_SHORT).show();
                    finish();

                } else {

                    JSONArray array = new JSONArray(response);
                    JSONObject object1 = array.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        Toast.makeText(context, "Document uploaded successfully to server.", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(SubmittedPaperSelectionActivity.this, SubmittedPaperSelectionActivity.class);
                        intent.putExtra("ayrYear", selectedAyrYear);
                        intent.putExtra("standard", selectedStandard);
                        intent.putExtra("division", selectedDivision);
                        intent.putExtra("subject", selectedSubject);
                        intent.putExtra("examType", selectedExamType);
                        intent.putExtra("examSchedule", selectedExamSchedule);
                        intent.putExtra("stuId", stuId);
                        intent.putExtra("examName", examName);
                        intent.putExtra("stuName", stuName);
                        startActivity(intent);
                        finish();

                    } else {

                        Toast.makeText(context, "Failed to upload evaluated document to server.", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();

                Toast.makeText(context, "Failed to upload evaluated document to server.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                response = util.saveEvaluatedDoc(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), selectedDivision, selectedStandard, stuId, selectedAyrYear, String.valueOf(selectedSubject), selectedExamSchedule, String.valueOf(exdId), s3linkString, enteredMarks, enteredComment);
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}

package com.techled.teacheranalytics.ActivityList.StaffLeaveApplyModule;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.techled.teacheranalytics.R;

public class StaffLeaveActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Leave Records");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public void processApplyLeave(View view){
        Intent intent = new Intent(StaffLeaveActivity.this, LeaveStaffApplyActivity.class);
        startActivity(intent);
    }

    public void processViewLeaveSummary(View view){
        Intent intent = new Intent(StaffLeaveActivity.this, StaffLeaveSummaryActivity.class);
        startActivity(intent);
    }
}

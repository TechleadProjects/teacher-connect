package com.techled.teacheranalytics.ActivityList.ReportIssueModule;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.ModuleData;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MakeSuggestionActivity extends AppCompatActivity {

    private ArrayList<String> moduleList, priorityList;
    private Spinner moduleSpinner, prioritySpinner;
    private Context context;
    private Util util;
    private Button btnSubmit;
    private EditText edtTitle, edtDesc;
    private int dueDay, dueMonth, dueYear;
    private LinearLayout laySelectDueDate;
    private TextView txtDueDate;
    private String dueDate, mdlName, title, desc, priority;
    private ProgressDialog progDailog;
    private int orgId, insId, mdlId = -1, usrId, ayrYear, dscId;
    private String typeBe = "B";
    private RadioButton rbBug, rbEnhancement;
    private RadioGroup rgType;
    private ArrayList<ModuleData> moduleDataArrayList;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private Button dueDateButton;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_suggestion);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Report Issue");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //get reference
        context = this;
        util = new Util();
        moduleSpinner = (Spinner) findViewById(R.id.moduleSpinner);
        prioritySpinner = (Spinner) findViewById(R.id.prioritySpinner);
        edtTitle = (EditText) findViewById(R.id.edtTitle);
        edtDesc = (EditText) findViewById(R.id.edtDesc);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        laySelectDueDate = (LinearLayout) findViewById(R.id.laySelectDueDate);
        txtDueDate = (TextView) findViewById(R.id.txtDate);
        rgType = (RadioGroup) findViewById(R.id.rgType);
        dueDateButton = findViewById(R.id.dueDateButton);

        myCalendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(dueDateButton);
            }
        };

        //login details
        try {
            //get login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        //set current date as due date
        Calendar c1 = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String curDate = df.format(c1.getTime());
        dueYear = c1.get(Calendar.YEAR);
        dueMonth = c1.get(Calendar.MONTH);
        dueDay = c1.get(Calendar.DAY_OF_MONTH);
        txtDueDate.setText("" + curDate);

       /* //select due date from date picker
        laySelectDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        dueDateSetListener, dueYear, dueMonth, dueDay);
                datePickerDialog.show();
            }
        });*/


        //load module spinner

        LoadModules loadModules = new LoadModules();
        loadModules.execute(null, null);


        //load periority spinner
        priorityList = new ArrayList<>();
        priorityList.add("Choose Priority: ");
        priorityList.add("Low");
        priorityList.add("High");
        priorityList.add("Urgent");

        ArrayAdapter<String> prAdaptor = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, priorityList);
        prAdaptor
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySpinner.setAdapter(prAdaptor);


        //get type
        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rbBug) {
                    typeBe = "B";
                } else if (checkedId == R.id.rbEnhancement) {
                    typeBe = "E";
                }
            }
        });

        //get selected priority
        prioritySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                priority = prioritySpinner.getSelectedItem().toString().trim();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //get selected module
        moduleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mdlId = moduleDataArrayList.get(position).getMdlId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //module name
                if (mdlId == -1) {
                    Toast.makeText(context, "Please select module name!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(priority.contains("Choose")){
                    priority = "nil";
                } else if (priority.equals("Low")) {
                    priority = "L";
                } else if (priority.equals("High")) {
                    priority = "H";
                } else if (priority.equals("Urgent")) {
                    priority = "U";
                }

                //priority
                if (priority == null || priority.equals("") || priority.equals("nil")) {
                    Toast.makeText(context, "Please select priority!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //due date
                //dueDate = txtDueDate.getText().toString().trim();
                if (dueDate == null || dueDate.equals("")) {
                    Toast.makeText(context, "Please select due date!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //title
                title = edtTitle.getText().toString().trim();
                if (title == null || title.equals("")) {
                    Toast.makeText(context, "Title should not be blank!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //desc
                desc = edtDesc.getText().toString().trim();
                if (desc == null || desc.equals("")) {
                    Toast.makeText(context, "Description should not be blank!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //save issue
                SaveSuggestionRequest saveSuggestionRequest = new SaveSuggestionRequest();
                saveSuggestionRequest.execute(null, null);


            }
        });

    }

    private void updateLabel(Button button) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dueDate = sdf.format(myCalendar.getTime());
        button.setText(dueDate);
    }

    public class SaveSuggestionRequest extends AsyncTask<String, String, String> {

        private String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                progDailog.dismiss();

                if (response != null) {
                    JSONArray resArray = new JSONArray(response);
                    JSONObject statusObject = resArray.getJSONObject(0);

                    String status = statusObject.getString("status");
                    if (status.equals("success")) {

                        Toast.makeText(context, "You are successfully reported the issue!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MakeSuggestionActivity.this, ViewReportIssueActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(context, "Failed to report the issue!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    Toast.makeText(context, "Failed to report the issue!", Toast.LENGTH_SHORT).show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                String status = "N";
                response = util.SaveSuggestionRequest(orgId, insId, mdlId,
                        title, desc, dueDate, priority,
                        usrId, typeBe, status);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private DatePickerDialog.OnDateSetListener dueDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                    dueYear = year;
                    dueMonth = monthOfYear;
                    dueDay = dayOfMonth;

                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                    cal.set(Calendar.DAY_OF_MONTH, dueDay);
                    cal.set(Calendar.MONTH, dueMonth);
                    cal.set(Calendar.YEAR, dueYear);
                    cal.set(Calendar.HOUR, 00);
                    cal.set(Calendar.MINUTE, 00);
                    cal.set(Calendar.SECOND, 00);
                    cal.set(Calendar.MILLISECOND, 00);
                    Date dateFrom = cal.getTime();

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String dueDate = sdf.format(dateFrom);
                    txtDueDate.setText("" + dueDate);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    private class LoadModules extends AsyncTask<String, String, String> {
        private String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                if (progDailog != null) {
                    progDailog.dismiss();
                }

                if (response != null) {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    JSONObject object2 = jsonArray.getJSONObject(1);


                    moduleDataArrayList = new ArrayList<>();
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {
                        JSONArray array = object2.getJSONArray("data");

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);
                            ModuleData moduleData = new ModuleData();
                            moduleData.setMdlId(object.getInt("mdlId"));
                            moduleData.setMdlName(object.getString("mdlName"));
                            moduleDataArrayList.add(moduleData);

                        }

                        ModuleData moduleData = new ModuleData();
                        moduleData.setMdlId(0);
                        moduleData.setMdlName("Other");
                        moduleDataArrayList.add(moduleData);

                        //set adapter to spinner
                        ArrayAdapter<ModuleData> adapter = new ArrayAdapter<ModuleData>(MakeSuggestionActivity.this, R.layout.support_simple_spinner_dropdown_item, moduleDataArrayList);
                        moduleSpinner.setAdapter(adapter);
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                response = util.getModuleData();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }
    }

    public void openDate(View view){
        new DatePickerDialog(MakeSuggestionActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

}

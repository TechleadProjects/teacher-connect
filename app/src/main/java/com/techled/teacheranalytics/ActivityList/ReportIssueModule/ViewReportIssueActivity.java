package com.techled.teacheranalytics.ActivityList.ReportIssueModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.ViewReportIssuesRecyAdapter;
import com.techled.teacheranalytics.pojo.IssuesDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewReportIssueActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    private RecyclerView issuesDetailsRecyView;
    private Context context;
    private Util util;
    private ProgressDialog progDailog;
    private int orgId,insId,usrId;
    private ArrayList<IssuesDetails> issuesDetailsArrayList;
    private LinearLayout layNoRecord,layParent;
    private FloatingActionButton btnReportIssue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_report_issue);

        try {
            final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Report Issue");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();

            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                usrId = object.getInt("usrId");

            }


            layParent = (LinearLayout)findViewById(R.id.layParent);
            layNoRecord = (LinearLayout)findViewById(R.id.layNoRecord);
            btnReportIssue = (FloatingActionButton)findViewById(R.id.btnReportIssue);

            issuesDetailsRecyView = (RecyclerView) findViewById(R.id.issuesDetailsRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            issuesDetailsRecyView.setLayoutManager(layoutManager);

            //load issues
            LoadIssuesDetails loadIssuesDetails = new LoadIssuesDetails();
            loadIssuesDetails.execute(null, null);

            btnReportIssue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ViewReportIssueActivity.this, MakeSuggestionActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public class LoadIssuesDetails extends AsyncTask<String,String,String>
    {
        private String response;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try
            {
                progDailog.dismiss();

                if (response != null) {


                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject statusObject = jsonArray.getJSONObject(0);
                    String status = statusObject.getString("status");
                    if(status.equals("SUCCESS"))
                    {

                        issuesDetailsArrayList = new ArrayList<>();

                        JSONObject dataObject = jsonArray.getJSONObject(1);
                        JSONArray dataArray = dataObject.getJSONArray("data");
                        for(int i=0;i<dataArray.length();i++)
                        {
                            JSONObject object = dataArray.getJSONObject(i);
                            IssuesDetails issuesDetails = new IssuesDetails();
                            issuesDetails.setMdlName(object.getString("mdlName"));
                            issuesDetails.setSctDescription(object.getString("sctDescription"));
                            issuesDetails.setSctId(object.getInt("sctId"));
                            issuesDetails.setSctTitle(object.getString("sctTitle"));
                            issuesDetails.setSctReportingDate(object.getString("sctReportingDate"));
                            issuesDetails.setSctExpDueDate(object.getString("sctExpDueDate"));
                            issuesDetails.setSctPriorityUhml(object.getString("sctPriorityUhml"));
                            issuesDetails.setSctTypeBe(object.getString("sctTypeBe"));
                            issuesDetails.setSctProgressStatusNircj(object.getString("sctProgressStatusNircj"));
                            issuesDetailsArrayList.add(issuesDetails);

                        }

                        if(issuesDetailsArrayList.size() >= 1)
                        {
                            layParent.setVisibility(View.VISIBLE);
                            layNoRecord.setVisibility(View.GONE);
                            //set adapter
                            ViewReportIssuesRecyAdapter adapter = new ViewReportIssuesRecyAdapter(issuesDetailsArrayList,context);
                            issuesDetailsRecyView.setAdapter(adapter);

                        }else {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "No issue reported!", Toast.LENGTH_SHORT).show();
                            return;
                        }


                    }else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "No issue reported!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }else {
                    layParent.setVisibility(View.GONE);
                    layNoRecord.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "No issue reported!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try
            {
                response = util.getIssuesDetails(orgId,insId,usrId);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            return null;
        }
    }
}


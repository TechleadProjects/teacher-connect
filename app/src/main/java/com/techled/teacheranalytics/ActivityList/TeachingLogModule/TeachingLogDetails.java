package com.techled.teacheranalytics.ActivityList.TeachingLogModule;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.MyLectures.MyLectures;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.MyLecturesDetailsRecyAdapter;
import com.techled.teacheranalytics.adapter.TeachingLogDetailsRecyAdapter;
import com.techled.teacheranalytics.pojo.MyLectureDetails;
import com.techled.teacheranalytics.pojo.TeachingLog;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TeachingLogDetails extends AppCompatActivity {

    private String selectedDate,selectedDateCompare;
    private Context context;
    private LinearLayout layParent, layNoRecord;
    private Util util;
    private Dialog dialog;
    private ProgressDialog progDailog;
    private int orgId, insId, dscId, ayr, usrId, usrAssetId, stfId, stdId;
    private int[] ayrs;
    private JSONArray classTeacherOfJson;
    private String resUserInfo, resSubject;
    private ArrayList<TeachingLog> teachingLogDetailsArrayList;
    private RecyclerView teachingLogDetailsRecyView;
    private Button btnFillLog, btnViewLog, btnEditLog;
    private TeachingLog teachingLog;



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teaching_log_details);
        selectedDate = getIntent().getStringExtra("selectedDate");
        selectedDateCompare = getIntent().getStringExtra("selectedDateCompare");

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Lecters of the day");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();
            //get reference
            teachingLogDetailsRecyView = (RecyclerView) findViewById(R.id.myLecturesRecyView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            teachingLogDetailsRecyView.setLayoutManager(layoutManager);
            layNoRecord = (LinearLayout) findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            btnFillLog = (Button) findViewById(R.id.btnFillLog);
            btnEditLog = (Button) findViewById(R.id.btnEditLog);
            btnViewLog = (Button) findViewById(R.id.btnViewLog);

            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);


                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
                ayr = object.getInt("ayrYear");

            }

            //get data
            SharedPreferences prefUserInfo = getSharedPreferences("MyInfo", MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            //load syllabus details
            layParent.setVisibility(View.VISIBLE);
            layNoRecord.setVisibility(View.GONE);
            TeachingLogDetails.LoadTeachingLogDetails loadTeachingLogDetails = new TeachingLogDetails.LoadTeachingLogDetails();
            loadTeachingLogDetails.execute(null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class LoadTeachingLogDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        //JSONObject dataObject = dataObj.getJSONObject("data");
                        JSONArray dataArray1 = dataObj.getJSONArray("data");
                        if(dataArray1.length() == 0)
                        {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Lectures not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        JSONObject dataObj1 = dataArray1.getJSONObject(0);
                        JSONArray dataArray = dataObj1.getJSONArray("logList");

                        teachingLogDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            teachingLog = new TeachingLog();
                            JSONObject object = dataArray.getJSONObject(i);
                            teachingLog.setStdId(object.getInt("stdId"));
                            teachingLog.setDivId(object.getInt("divId"));
                            teachingLog.setSubId(object.getInt("subId"));
                            teachingLog.setDiv(object.getString("divName"));
                            teachingLog.setLectureEndTime(object.getString("lectureEndTime"));
                            teachingLog.setLectureStartTime(object.getString("lectureStartTime"));
                            teachingLog.setSubName(object.getString("subName"));
                            teachingLog.setStd(object.getString("stdName"));
                            teachingLog.setPrdId(object.getInt("prdId"));
                            teachingLog.setFilledLog(object.getBoolean("bTeachingLog"));
                            teachingLog.setAbsent(object.getBoolean("isAbsent"));
                            teachingLog.setHoliday(object.getBoolean("isHoliday"));
                            teachingLog.setEvent(object.getBoolean("isEvent"));
                            teachingLog.setLectureOff(object.getBoolean("isLectureOff"));
                            if(teachingLog.getFilledLog()==false)
                            {
                                teachingLog.setLogChecked(false);
                                teachingLog.setTeachingPlanStatus("P");
                            }
                            else if(teachingLog.getFilledLog()==true)
                            {
                                teachingLog.setScheduledTeachingDate(object.getString("scheduleTeachingDate"));
                                teachingLog.setActualTeaching(object.getString("telActualTeaching"));  // actualTeaching
                                teachingLog.setLogStatus(object.getString("teachingLogStatus"));
                                teachingLog.setComment(object.getString("telComment"));
                                teachingLog.setLogChecked(object.getString("telLogChecked").equals("Y")?true : false);  //logChecked
                                teachingLog.setLectureTaken(object.getString("telLectureConductedYn").equals("Y")?true : false);  //lectureConducted
                                teachingLog.setEventBased(object.getString("telLectureEventBasedYn").equals("Y")?true : false);
                                teachingLog.setLectureNotConductedReason(object.getString("telLectureNotConductedReason"));
                                teachingLog.setSpecialLog(object.getString("telSpecialLogYn").equals("Y") ? true : false);
                                teachingLog.setActualLogDate(object.getString("actualLogDate"));
                                teachingLog.setSyllabusBasted(object.getString("telSlbBokSb").equals("S")?true : false);
                                teachingLog.setScheduledTeachingPlan(object.getString("telScheduleTeaching"));  //scheduleTeaching
                                teachingLog.setTeachingPlanStatus(object.getString("teachingPlanStatus"));
                                teachingLog.setPrdIndex(object.getInt("tepPrdIndex"));
                                teachingLog.setApproverComment(object.getString("telApproverComment"));
                                teachingLog.setToolsMethods(object.getString("telEvalEctSubectIds"));
                                teachingLog.setCompletedSubUnit(object.getString("telCompletedUniSun"));
                                if(teachingLog.isSyllabusBasted()==true)
                                {
                                    //teachingLog.setGetUniSubUniId(object.getString("uniSubUnitId"));
                                    //teachingLog.setUniSubUniName(object.getString("uniSubUnit"));
                                    teachingLog.setPlannedUniSun(object.getString("telPlannedUniSun"));
                                    teachingLog.setActualTakenUniSun(object.getString("telUniSun"));
                                }
                            }



//                            try {
//                                teachingLog.setLogStatus(object.getString("telPlanApprovalApr"));
//                            }catch(Exception e)
//                            {
//                                teachingLog.setLogStatus("F");
//                            }

                            teachingLogDetailsArrayList.add(teachingLog);

                        }

                        //set adapter
                        if (teachingLogDetailsArrayList.size() > 0) {
                            layNoRecord.setVisibility(View.GONE);
                            layParent.setVisibility(View.VISIBLE);
                            TeachingLogDetailsRecyAdapter adapter = new TeachingLogDetailsRecyAdapter(orgId, insId, dscId, stfId, ayr, teachingLogDetailsArrayList, context, selectedDate, selectedDateCompare);
                            teachingLogDetailsRecyView.setAdapter(adapter);
                        } else {
                            layParent.setVisibility(View.GONE);
                            layNoRecord.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "Lectures not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                    } else {
                        layParent.setVisibility(View.GONE);
                        layNoRecord.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Lectures not found!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resMyLectures = util.findTeachingLogForDay(orgId, insId, dscId, usrAssetId, ayr, selectedDate, selectedDate);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

package com.techled.teacheranalytics.ActivityList.SyllabusModule;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AddNewUnitActivity extends AppCompatActivity {
    private EditText edtCount, edtFromDate, edtToDate, edtUnit, edtUnitDesc;
    private TextView btnAdd, btnSubtract;
    private int i = 0;
    private int orgId, insId, dscId, ayr, usrId, usrAssetId, subId, stdId;
    private Button btnSave;
    private String editable = "N", unitNumber, unitDesc, from, to, showContent = "", activeYN = "Y", unitNoOfUnitsAlloted = "0";
    private RadioGroup rgShowContent, rgActive;
    private int mMonth, mDay, mYear;
    private Context context;
    private Util util;
    private ProgressDialog progDailog;
    private LinearLayout layParent;
    private String hours;
    private int unitId = 0;
    private Button btnFrom, btnTo;
    private LinearLayout layRgActive;
    private RadioButton rbYes, rbNo, rbActYes, rbActNo;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_unit);

        try {

            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Syllabus");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }


            context = this;
            util = new Util();
            edtUnit = (EditText) findViewById(R.id.edtUnit);
            edtCount = (EditText) findViewById(R.id.edtCount);
            btnAdd = (TextView) findViewById(R.id.btnAdd);
            btnSubtract = (TextView) findViewById(R.id.btnSubtract);
            edtToDate = (EditText) findViewById(R.id.edtToDate);
            edtFromDate = (EditText) findViewById(R.id.edtFromDate);
            btnSave = (Button) findViewById(R.id.btnSave);
            rgShowContent = (RadioGroup) findViewById(R.id.rgShowContent);
            layRgActive = (LinearLayout) findViewById(R.id.layRgActive);
            edtUnitDesc = (EditText) findViewById(R.id.edtUnitDesc);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            btnFrom = (Button) findViewById(R.id.btnFrom);
            btnTo = (Button) findViewById(R.id.btnTo);
            rgActive = (RadioGroup) findViewById(R.id.rgActive);
            rbYes = (RadioButton) findViewById(R.id.rbYes);
            rbNo = (RadioButton) findViewById(R.id.rbNo);
            rbActYes = (RadioButton) findViewById(R.id.rbActYes);
            rbActNo = (RadioButton) findViewById(R.id.rbActNo);


            Calendar c1 = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String curDate = df.format(c1.getTime());
            mYear = c1.get(Calendar.YEAR);
            mMonth = c1.get(Calendar.MONTH);
            mDay = c1.get(Calendar.DAY_OF_MONTH);

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);


                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
            }


            //get intent data
            Intent intent = getIntent();
            if (intent != null) {
                editable = intent.getStringExtra("editable");
                unitNumber = intent.getStringExtra("unitNumber");
                unitDesc = intent.getStringExtra("unitDesc");
                from = intent.getStringExtra("fromDate");
                to = intent.getStringExtra("toDate");
                unitId = intent.getIntExtra("unitId", 0);

                ayr = intent.getIntExtra("ayr", 0);
                stdId = intent.getIntExtra("stdId", 0);
                subId = intent.getIntExtra("subId", 0);
                unitNoOfUnitsAlloted = intent.getStringExtra("count");

                activeYN = intent.getStringExtra("showActive");
                showContent = intent.getStringExtra("showContent");


                edtUnit.setText(unitNumber);
                edtFromDate.setText(from);
                edtToDate.setText(to);
                edtUnitDesc.setText(unitDesc);

                if (editable == null || editable.equals("")) {

                    layRgActive.setVisibility(View.GONE);

                } else {

                    if (editable.equals("Y")) {
                        layRgActive.setVisibility(View.VISIBLE);
                    } else {
                        layRgActive.setVisibility(View.GONE);
                    }

                }

                if (activeYN == null || activeYN.equals("")) {

                    rbActYes.setChecked(true);
                    rbActNo.setChecked(false);

                } else {
                    if (activeYN.equals("Y")) {

                        rbActYes.setChecked(true);
                        rbActNo.setChecked(false);

                    } else {

                        rbActYes.setChecked(false);
                        rbActNo.setChecked(true);
                    }
                }

                if (showContent == null || showContent.equals("")) {

                    rbYes.setChecked(false);
                    rbNo.setChecked(false);

                } else {
                    if (showContent.equals("Y")) {

                        rbYes.setChecked(true);
                        rbNo.setChecked(false);

                    } else {

                        rbYes.setChecked(false);
                        rbNo.setChecked(true);
                    }
                }

                if(unitNoOfUnitsAlloted == null || unitNoOfUnitsAlloted.equals(""))
                {
                    edtCount.setText("");
                }else {
                    edtCount.setText(unitNoOfUnitsAlloted);
                }


            }


            if (edtCount.getText().toString() == null || edtCount.getText().toString().equals("")) {
                i = 0;
            } else {
                i = Integer.parseInt(edtCount.getText().toString());
            }

            btnSubtract.setOnClickListener(new View.OnClickListener() {


                public void onClick(View v) {
                    String _stringVal;
                    Log.d("src", "Decreasing value...");
                    if (i > 0) {
                        i = i - 1;
                        _stringVal = String.valueOf(i);
                        edtCount.setText(_stringVal);
                    } else {
                        Log.d("src", "Value can't be less than 0");
                    }

                }
            });

            btnAdd.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String _stringVal;

                    Log.d("src", "Increasing value...");
                    i = i + 1;
                    _stringVal = String.valueOf(i);
                    edtCount.setText(_stringVal);
                }
            });

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //unit number
                    unitNumber = edtUnit.getText().toString().trim();
                    if (unitNumber == null || unitNumber.equals("")) {
                        Toast.makeText(AddNewUnitActivity.this, "Unit number should be blank!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    //unit description
                    unitDesc = edtUnitDesc.getText().toString();

                    //count
                    String count = edtCount.getText().toString().trim();
                    if (count == null || count.equals("")) {
                        unitNoOfUnitsAlloted = "0";
                    } else {
                        unitNoOfUnitsAlloted = count;
                    }

                    //from date
                    from = edtFromDate.getText().toString().trim();

                    //to date
                    to = edtToDate.getText().toString().trim();

                    if(activeYN == null || activeYN.equals(""))
                    {
                        activeYN = "Y";
                    }


                    SaveUnit saveUnit = new SaveUnit();
                    saveUnit.execute(null, null);

                }
            });

            rgShowContent.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbYes) {
                        showContent = "Y";
                    } else if (checkedId == R.id.rbNo) {
                        showContent = "N";
                    }
                }
            });

            rgActive.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbActYes) {
                        activeYN = "Y";
                    } else if (checkedId == R.id.rbActNo) {
                        activeYN = "N";
                    }
                }
            });

            //from date selection
            btnFrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setDateForFromDate();

                }
            });

            //to date selection
            btnTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setDateForToDate();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }


    public class SaveUnit extends AsyncTask<String, String, String> {
        private String resSave;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(true);
            progDailog.setCancelable(false);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {

                        new AlertDialog.Builder(context)
                                .setTitle("Teacher Connect")
                                .setMessage("Unit details successfully saved.")
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Intent a = new Intent(AddNewUnitActivity.this, SyllabusHomeActivity.class);
                                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(a);
                                        finish();

                                    }
                                }).show();

                    } else {
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Failed to save unit details!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Failed to save unit details!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                resSave = util.saveStdSubUnit(orgId, insId, dscId, ayr,
                        stdId, subId, unitDesc, unitNumber, activeYN, showContent,
                        unitNoOfUnitsAlloted, from, to, unitId);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void setDateForFromDate() {
        try {
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.timeDatePicker,
                    setDateForFromDateListener, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDateForToDate() {
        try {
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.timeDatePicker,
                    setDateForToDateListener, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DatePickerDialog.OnDateSetListener setDateForFromDateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                cal.set(Calendar.DAY_OF_MONTH, mDay);
                cal.set(Calendar.MONTH, mMonth);
                cal.set(Calendar.YEAR, mYear);
                cal.set(Calendar.HOUR, 00);
                cal.set(Calendar.MINUTE, 00);
                cal.set(Calendar.SECOND, 00);
                cal.set(Calendar.MILLISECOND, 00);
                Date dateFrom = cal.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String fromDate = sdf.format(dateFrom);

                edtFromDate.setText("" + fromDate);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private DatePickerDialog.OnDateSetListener setDateForToDateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                cal.set(Calendar.DAY_OF_MONTH, mDay);
                cal.set(Calendar.MONTH, mMonth);
                cal.set(Calendar.YEAR, mYear);
                cal.set(Calendar.HOUR, 00);
                cal.set(Calendar.MINUTE, 00);
                cal.set(Calendar.SECOND, 00);
                cal.set(Calendar.MILLISECOND, 00);
                Date dateFrom = cal.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String fromDate = sdf.format(dateFrom);

                edtToDate.setText("" + fromDate);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


}

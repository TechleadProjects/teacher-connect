package com.techled.teacheranalytics.ActivityList.TeachingLogModule;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.TeachingLogDetailsRecyAdapter;
import com.techled.teacheranalytics.pojo.Method;
import com.techled.teacheranalytics.pojo.SubUnitsSyllabusDetails;
import com.techled.teacheranalytics.pojo.SyllabusDetails;
import com.techled.teacheranalytics.pojo.TeachingLog;
import com.techled.teacheranalytics.pojo.Tool;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FillTeachingLogsActivity extends AppCompatActivity {


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private int orgId, insId, dscId, ayr, stfId, prdId, prdIndex, divId, subId, stdId, subUnitId, newLog;
    private CheckBox chkSyllabusBased, chkNone;
    private TextView txtActualSyllabus, txtPlannedSyllabus, edtReason,txtStdDiv, txtSubject, txtDate, txtAuthority, txtActualDate;
    private RadioGroup rgLecture, rgEventBased, rgSpecialLog, rgLogStatus, rgLogCheck, rgSyllabusBased, rgTeachingPlanStatus, rgTeachingLogStatus;
    private EditText  edtShdTeaching, edtComment, edtActlTeaching;
    private ProgressDialog progDailog;
    private Context context;
    private Button btnSave, btnSelectMethod, btnSelectTool, btnBack;
    private LinearLayout layPlannedSyllabusTxt,layTakenSyllabusTxt, laySyllabusBased, layParent, layReason, layEventBased, layCommentByHigherAuth, layTeachingPlanSatus, layTeachingLogSatus, layLogChecked;
    private FrameLayout frameLay1;
    private String completedUniSubuni,dateCompare,txtPlannedUniSubuniName="", txtActualUniSubuniName="",uniSubUnitId, date, selectedDate, stdDiv, reason, syllabusBased, lectureConducted, actualLogDate, eventBasedYesNo, schdlTeaching, actlTeaching, comment, teachingPlanStatus, logStatus, specialLog, unitName, subUnitName;
    private Util util;
    private ArrayList<SyllabusDetails> unitArrayList;
    private String TAG = "Cancel", resSave;
    private TeachingLog teachingLog;
    private boolean logCheck,isFuture;
    private SyllabusDetails unit;
    private SubUnitsSyllabusDetails subUnit;
    private int selectedUnit;
    private StringBuilder syllabusIds, toolsIds, methodsIds,PlannedSyllabusIds;
    private ArrayList<Tool> toolArrayList;
    private ArrayList<Method> methodArrayList;
    private Tool tool;
    private Method method;
    private ArrayList<Integer> selectedPreSubunits, selectedPreToolsMethods, selectedPrePlanned, completedUniSubIds;
    private ArrayList<Integer> selectedPlannedSubUnitIds= new ArrayList<>(),selectedPreTool= new ArrayList<>(), selectedPreMethod = new ArrayList<>();
    private ArrayList<Integer> selectedActualSubUnitIds1 = new ArrayList<>();
    private StringBuilder completedSyllabusIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_teaching_log);


        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Teaching Log Details");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference
            context = this;
            util = new Util();


            final ArrayList<Integer> selectedUnitIds = new ArrayList<>();

            txtStdDiv = (TextView) findViewById(R.id.txtStdDiv);
            txtSubject = (TextView) findViewById(R.id.txtSubject);
            txtDate = (TextView) findViewById(R.id.txtDate);
            txtAuthority = (TextView) findViewById(R.id.edtCommentByHigherAuth);
            txtActualDate = (TextView) findViewById(R.id.txtActualLogDate);
            txtActualSyllabus = (TextView) findViewById(R.id.txtActualSyllabus);
            txtPlannedSyllabus = (TextView) findViewById(R.id.txtPlannedSyllabus);

            rgLecture = (RadioGroup) findViewById(R.id.rgLecture);
            rgEventBased = (RadioGroup) findViewById(R.id.rgEventBased);
            rgSpecialLog = (RadioGroup) findViewById(R.id.rgSpecialLog);
            rgSyllabusBased =  (RadioGroup) findViewById(R.id.rgSyllabusBased);
            //rgLogStatus = (RadioGroup) findViewById(R.id.rgLogStatus);
            //rgLogCheck = (RadioGroup) findViewById(R.id.rgLogCheck);

            edtReason = (TextView) findViewById(R.id.edtReason);
            edtShdTeaching = (EditText) findViewById(R.id.edtSchdTeaching);
            edtActlTeaching = (EditText) findViewById(R.id.edtActlTeaching);
            edtComment = (EditText) findViewById(R.id.edtComment);

            btnSave = (Button) findViewById(R.id.btnSave);
            btnSelectMethod = (Button) findViewById(R.id.btnSelectMethod);
            btnSelectTool = (Button) findViewById(R.id.btnSelectTool);
            btnBack = (Button) findViewById(R.id.btnBack);
            btnBack.setVisibility(View.GONE);

            layReason = (LinearLayout) findViewById(R.id.layReason);
            layEventBased = (LinearLayout) findViewById(R.id.layEventBased);
            layParent = (LinearLayout) findViewById(R.id.layParent);
            frameLay1 = (FrameLayout) findViewById(R.id.frameLay1);
            layTeachingPlanSatus = (LinearLayout) findViewById(R.id.layTeachingPlanSatus);
            layTeachingLogSatus = (LinearLayout) findViewById(R.id.layTeachingLogSatus);
            layLogChecked = (LinearLayout) findViewById(R.id.layLogChecked);
            layCommentByHigherAuth = (LinearLayout) findViewById(R.id.layCommentByHigherAuth);
            layPlannedSyllabusTxt = (LinearLayout) findViewById(R.id.layPlannedSyllabusTxt);
            layTakenSyllabusTxt = (LinearLayout) findViewById(R.id.layTakenSyllabusTxt);
            layPlannedSyllabusTxt.setVisibility(View.GONE);
            layTakenSyllabusTxt.setVisibility(View.GONE);

            rgTeachingPlanStatus = (RadioGroup) findViewById(R.id.rgTeachingPlanStatus);
            ((RadioButton) findViewById(R.id.rgTeachingPlanStatusApproved)).setEnabled(false);
            ((RadioButton) findViewById(R.id.rgTeachingPlanStatusPending)).setEnabled(false);
            ((RadioButton) findViewById(R.id.rgTeachingPlanStatusRejected)).setEnabled(false);


            rgTeachingLogStatus = (RadioGroup) findViewById(R.id.rgTeachingLogStatus);
            ((RadioButton) findViewById(R.id.rgTeachingLogStatusApproved)).setEnabled(false);
            ((RadioButton) findViewById(R.id.rgTeachingLogStatusPending)).setEnabled(false);
            ((RadioButton) findViewById(R.id.rgTeachingLogStatusRejected)).setEnabled(false);

            ((RadioButton) findViewById(R.id.rgLogCheckedYes)).setEnabled(false);
            ((RadioButton) findViewById(R.id.rgLogCheckedNo)).setEnabled(false);

            //get values from Intent
            Intent intent = getIntent();

            //if edit log thin retrieve the object
            Type type = new TypeToken<TeachingLog>(){}.getType();
            Gson gson = new Gson();
            teachingLog = gson.fromJson(intent.getStringExtra("TeachingLogJsonObj"), type);
            String actualLoggDate = teachingLog.getActualLogDate();
            newLog = intent.getIntExtra("newLog",0);
            date = intent.getStringExtra("ScheduledDate");
            dateCompare = intent.getStringExtra("selectedDateCompare");
            selectedDate = date;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //" HH:mm"
            Date today = new Date();

            String date1 = dateCompare.concat(" "+ teachingLog.getLectureEndTime());
            SimpleDateFormat sdfTime = new SimpleDateFormat("dd-MM-yyyy HH:mm"); //" HH:mm"
            Date todayWithTime = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(date1);
            if(todayWithTime.compareTo(today) >= 0)
            {
                System.out.println("its future date and  time");
                //if future date then teacher should not able to enter actual teaching and comment for that lecture
                edtActlTeaching.setEnabled(false);
                edtComment.setEnabled(false);
                rgLecture.setEnabled(false);
                ((RadioButton) findViewById(R.id.rbConducted)).setEnabled(false);
                ((RadioButton) findViewById(R.id.rbNotConducted)).setEnabled(false);
                isFuture=true;

            }
            else if (todayWithTime.compareTo(today) < 0) {
                System.out.println("its past date and  time");
                //if past date then teacher should not able to enter plan/scheduled teaching for that lecture
                edtShdTeaching.setEnabled(false);
                if(newLog==1)
                {
                    rgSyllabusBased.setEnabled(false);
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedNone)).setEnabled(false);
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedYes)).setEnabled(false);
                }
                isFuture=false;
            }
//            else if (todayWithTime.compareTo(today) == 0) {
//                System.out.println("its current time");
//            } // ends time comparision

//            Only Date comparision
//            Date selectedDateToCompare = new SimpleDateFormat("yyyy-MM-dd").parse(date);
//            if(selectedDateToCompare.compareTo(today) > 0)
//            {
//                //if future date then teacher should not able to enter actual teaching and comment for that lecture
//                edtActlTeaching.setEnabled(false);
//                edtComment.setEnabled(false);
//                rgLecture.setEnabled(false);
//                ((RadioButton) findViewById(R.id.rbConducted)).setEnabled(false);
//                ((RadioButton) findViewById(R.id.rbNotConducted)).setEnabled(false);
//                isFuture=true;
//
//            }
//            else if (selectedDateToCompare.compareTo(today) <= 0)
//            {
//                //if past date then teacher should not able to enter plan/scheduled teaching for that lecture
//                edtShdTeaching.setEnabled(false);
//                if(newLog==1)
//                {
//                    rgSyllabusBased.setEnabled(false);
//                    ((RadioButton) findViewById(R.id.rbSyllabusBasedNone)).setEnabled(false);
//                    ((RadioButton) findViewById(R.id.rbSyllabusBasedYes)).setEnabled(false);
//                }
//                isFuture=false;
//            }

            stdDiv = teachingLog.getStd() + "-" + teachingLog.getDiv();
            txtStdDiv.setText(stdDiv);
            txtSubject.setText(teachingLog.getSubName());
            txtDate.setText(teachingLog.getScheduledTeachingDate());
            prdId = teachingLog.getPrdId();
            prdIndex = teachingLog.getPrdIndex();
            stdId = teachingLog.getStdId();;
            divId = teachingLog.getDivId();;
            subId = teachingLog.getSubId();;
            teachingPlanStatus = teachingLog.getTeachingPlanStatus();
            logCheck = teachingLog.isLogChecked();
            logStatus = teachingLog.getLogStatus();


            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            try{
                JSONArray jsonArray = new JSONArray(params);
                JSONObject object1 = jsonArray.getJSONObject(0);
                String status = object1.getString("status");
                if (status.equals("SUCCESS")) {

                    JSONObject object2 = jsonArray.getJSONObject(1);
                    JSONArray array = object2.getJSONArray("data");
                    JSONObject object = array.getJSONObject(0);


                    orgId = object.getInt("orgId");
                    insId = object.getInt("insId");
                    dscId = object.getInt("dscId");
                    stfId = object.getInt("assetId1");
                    ayr = object.getInt("ayrYear");
                }
            }catch(Exception e)
            {
                    e.printStackTrace();
            }

            if(newLog == 0)  // entering new log
            {
                ((RadioButton) findViewById(R.id.rgLogCheckedNo)).setChecked(true);
                ((RadioButton) findViewById(R.id.rgTeachingPlanStatusPending)).setChecked(true);
                txtStdDiv.setText(teachingLog.getStd()+" "+teachingLog.getDiv());
                txtSubject.setText(teachingLog.getSubName());
                txtActualDate.setText(actualLogDate);
                txtDate.setText(selectedDate);
            }
            else if(newLog == 2)  // entering view log
            {
                btnSave.setVisibility(View.GONE);
                btnBack.setVisibility(View.VISIBLE);

                ((RadioButton) findViewById(R.id.rgLogCheckedNo)).setChecked(true);
                ((RadioButton) findViewById(R.id.rgTeachingPlanStatusPending)).setChecked(true);
                txtStdDiv.setText(teachingLog.getStd()+" "+teachingLog.getDiv());
                txtSubject.setText(teachingLog.getSubName());
                txtDate.setText(selectedDate);
                if (teachingLog.isSyllabusBasted()==true)
                {
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedYes)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedYes)).setEnabled(false);
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedNone)).setEnabled(false);
                    syllabusBased = "S";
                }
                else {
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedNone)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedYes)).setEnabled(false);
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedNone)).setEnabled(false);
                    syllabusBased = "N";
                }
                if(logCheck == true) { ((RadioButton) findViewById(R.id.rgLogCheckedYes)).setChecked(true); }
                else{ ((RadioButton) findViewById(R.id.rgLogCheckedNo)).setChecked(true); }

                if(logStatus == "N") { ((RadioButton) findViewById(R.id.rgTeachingLogStatusPending)).setChecked(true); }
                else if(logStatus == "A") { ((RadioButton) findViewById(R.id.rgTeachingLogStatusApproved)).setChecked(true);  }
                else{((RadioButton) findViewById(R.id.rgTeachingLogStatusRejected)).setChecked(true); }

                if(teachingPlanStatus == "P"){((RadioButton) findViewById(R.id.rgTeachingPlanStatusPending)).setChecked(true); }
                else if(logStatus == "A") { ((RadioButton) findViewById(R.id.rgTeachingPlanStatusApproved)).setChecked(true); }
                else{((RadioButton) findViewById(R.id.rgTeachingPlanStatusPending)).setChecked(true);}

                edtShdTeaching.setText(teachingLog.getScheduledTeachingPlan());
                edtShdTeaching.setEnabled(false);
                edtActlTeaching.setText(teachingLog.getActualTeaching());
                edtActlTeaching.setEnabled(false);
                edtComment.setText(teachingLog.getComment());
                edtComment.setEnabled(false);
                edtReason.setText(teachingLog.getLectureNotConductedReason());
                edtReason.setEnabled(false);
                txtAuthority.setText(teachingLog.getHigherAuthComment());
                txtActualDate.setText(teachingLog.getActualLogDate());

                if(teachingLog.isLectureTaken()==false)
                {
                    ((RadioButton) findViewById(R.id.rbNotConducted)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rbNotConducted)).setEnabled(false);
                    ((RadioButton) findViewById(R.id.rbConducted)).setEnabled(false);
                    layEventBased.setVisibility(View.VISIBLE);
                    layReason.setVisibility(View.VISIBLE);
                    edtReason.setEnabled(false);
                    if(teachingLog.getEventBased().equals("Y"))
                    {
                        ((RadioButton) findViewById(R.id.rbEventBasedYes)).setChecked(true);
                        ((RadioButton) findViewById(R.id.rbEventBasedYes)).setEnabled(false);
                        ((RadioButton) findViewById(R.id.rbEventBasedNo)).setEnabled(false);
                    }
                    else {
                        ((RadioButton) findViewById(R.id.rbEventBasedNo)).setChecked(true);
                        ((RadioButton) findViewById(R.id.rbEventBasedYes)).setEnabled(false);
                        ((RadioButton) findViewById(R.id.rbEventBasedNo)).setEnabled(false);
                    }

                }

            }
            else if(newLog == 1)  // edit log
            {

                if(teachingLog.getSpecialLog().equals("N"))
                {
                    ((RadioButton) findViewById(R.id.rbSpecialLogNo)).setChecked(true);
                    specialLog = "N";
                }
                else {
                    {
                        ((RadioButton) findViewById(R.id.rbSpecialLogYes)).setChecked(true);
                        specialLog = "Y";
                    }
                }

                if (teachingLog.isSyllabusBasted()==true)
                {
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedYes)).setChecked(true);
                    syllabusBased = "S";
                    if(isFuture==true) {
                        openDialogSelectPlannedSyllabus();
//
                    }
                }
                else {
                    ((RadioButton) findViewById(R.id.rbSyllabusBasedNone)).setChecked(true);
                    syllabusBased = "N";
                }


                if(logCheck == true) { ((RadioButton) findViewById(R.id.rgLogCheckedYes)).setChecked(true); }
                else{ ((RadioButton) findViewById(R.id.rgLogCheckedNo)).setChecked(true); }

                if(logStatus == "N" || logStatus == "P") { ((RadioButton) findViewById(R.id.rgTeachingLogStatusPending)).setChecked(true); }
                else if(logStatus == "A") { ((RadioButton) findViewById(R.id.rgTeachingLogStatusApproved)).setChecked(true);  }
                else{((RadioButton) findViewById(R.id.rgTeachingLogStatusRejected)).setChecked(true); }

                if(teachingPlanStatus == "P" || teachingPlanStatus == "N"){((RadioButton) findViewById(R.id.rgTeachingPlanStatusPending)).setChecked(true); }
                else if(logStatus == "A") { ((RadioButton) findViewById(R.id.rgTeachingPlanStatusApproved)).setChecked(true); }
                else{((RadioButton) findViewById(R.id.rgTeachingPlanStatusPending)).setChecked(true);}

                edtShdTeaching.setText(teachingLog.getScheduledTeachingPlan());
                edtActlTeaching.setText(teachingLog.getActualTeaching());
                edtComment.setText(teachingLog.getComment());
                edtReason.setText(teachingLog.getLectureNotConductedReason());
                txtAuthority.setText(teachingLog.getHigherAuthComment());
                txtActualDate.setText(teachingLog.getActualLogDate());

                if(teachingLog.isLectureTaken()==false)
                {
                    ((RadioButton) findViewById(R.id.rbNotConducted)).setChecked(true);
                    lectureConducted = "N";
                    layEventBased.setVisibility(View.VISIBLE);
                    layReason.setVisibility(View.VISIBLE);
                    reason=edtReason.getText().toString().trim();
                    if(teachingLog.getEventBased().equals("Y"))
                    {
                        ((RadioButton) findViewById(R.id.rbEventBasedYes)).setChecked(true);
                        eventBasedYesNo="Y";
                    }
                    else {
                        ((RadioButton) findViewById(R.id.rbEventBasedNo)).setChecked(true);
                        eventBasedYesNo="N";
                    }

                }
                else
                {
                    ((RadioButton) findViewById(R.id.rbConducted)).setChecked(true);
                    layEventBased.setVisibility(View.GONE);
                    layReason.setVisibility(View.GONE);
                    lectureConducted = "Y";
                    if(syllabusBased.equals("S"))
                    {
                        openDialogSelectActualTakenSyllabus();
                        if(!txtActualUniSubuniName.isEmpty())
                        {
                            layTakenSyllabusTxt.setVisibility(View.VISIBLE);
                            txtActualSyllabus.setText(txtActualUniSubuniName);
                        }
                    }

                }

                //retrieving tools and methods data
                if(teachingLog.getToolsMethods()!=null)
                {
                    int tOrM = 0;
                    String toolsMethods = teachingLog.getToolsMethods();
                    String[] toolsMethodsSplitted = toolsMethods.split(",");
                    int size = toolsMethodsSplitted.length;
                    String[] subUni = new String[size];
                    selectedPreToolsMethods = new ArrayList<>();
                    for(int i=0;i<toolsMethodsSplitted.length;i++)
                    {
                        String[] s2 = toolsMethodsSplitted[i].split("-");
                        if(s2.length != 0)
                        {
                            tOrM = Integer.parseInt(s2[0]);
                            if (tOrM==1)
                            {

                                selectedPreTool.add(Integer.parseInt(s2[1]));
                            }
                            else if(tOrM==2)
                            {
                                selectedPreMethod.add(Integer.parseInt(s2[1]));
                            }
                        }

                    }
//                    if(teachingLog.getPlannedUniSun()!=null)
//                    {
//
//
//                    }
                }


                if(teachingLog.getCompletedSubUnit()!=null)
                     {
                    int selectedCompletedUnitId =0;
                    String completedUniSubuni = teachingLog.getToolsMethods();
                    String[] completedUniSubuniSplited = completedUniSubuni.split(",");
                    int size = completedUniSubuniSplited.length;
                    String[] subUni = new String[size];
                    completedUniSubIds = new ArrayList<>();
                    for(int i=0;i<completedUniSubuniSplited.length;i++)
                            {
                                String[] s2 = completedUniSubuniSplited[i].split("-");
                                selectedCompletedUnitId = Integer.parseInt(s2[0]);
                                subUni[i] = s2[1];
                                completedUniSubIds.add(Integer.parseInt(s2[1]));
                            }
                     }
                }


            //event based yes no
            rgEventBased.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbEventBasedYes) {
                        eventBasedYesNo = "Y";
                    } else if (checkedId == R.id.rbEventBasedNo) {
                        eventBasedYesNo = "N";
                    }
                }
            });

            //special log
            rgSpecialLog.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbSpecialLogYes) {
                        specialLog = "Y";
                    } else if (checkedId == R.id.rbSpecialLogNo) {
                        specialLog = "N";
                    }
                }
            });

            //syllabus based
            rgSyllabusBased.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                           @Override
                                                           public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                               if (checkedId == R.id.rbSyllabusBasedYes) {
                                                                   syllabusBased = "S";
                                                                   openDialogSelectPlannedSyllabus();
//                                                                   layPlannedSyllabusTxt.setVisibility(View.VISIBLE);
//                                                                   txtPlannedSyllabus.setText(txtPlannedUniSubuniName);
                                                               } else if (checkedId == R.id.rbSyllabusBasedNone) {
                                                                   syllabusBased = "N";
                                                               }
                                                           }
                                                       });

            //lecture conducted
            rgLecture.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbConducted) {
                        lectureConducted = "Y";
                        //layTakenSyllabusTxt.setVisibility(View.VISIBLE);
                        layEventBased.setVisibility(View.GONE);
                        layReason.setVisibility(View.GONE);
                        if(syllabusBased.equals("S"))
                        {
                            openDialogSelectActualTakenSyllabus();
//                            txtActualSyllabus.setText(txtActualUniSubuniName);
                        }
                    } else if (checkedId == R.id.rbNotConducted) {
                        lectureConducted = "N";
                        layEventBased.setVisibility(View.VISIBLE);
                        layReason.setVisibility(View.VISIBLE);
                    }
                }
            });

//            // Sonal's code..
//            if (intent != null) {
//                reason = intent.getStringExtra("reason");
//                if (reason == null || reason.equals(""))
//                {
//
//                } else {
//                    edtReason.setText(reason);
//                }
//            }

//        layReason.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                frameLay1.removeAllViews();
//                FragmentManager fragmentManager = getSupportFragmentManager();//Get Fragment Manager
//
//                Fragment argumentFragment = new EnterReasonFragment();//Get Fragment Instance
//
//                if(reason != null ) {
//                    Bundle data = new Bundle();//Use bundle to pass data
//                    data.putString("data", reason);//put string, int, etc in bundle with a key value
//                    argumentFragment.setArguments(data);//Finally set argument bundle to fragment
//
//                }
//                fragmentManager.beginTransaction().replace(R.id.frameLay1, argumentFragment).commit();//now replace the argument fragment
//
//            }
//        });

//            laySyllabusBased.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    // TODO Auto-generated method stub
//
//                    frameLay1.removeAllViews();
//                    getFragmentManager()
//                            .beginTransaction()
//
//                            .replace(R.id.frameLay1, new SelectSyllabusFragment())
//                            .commit();
//                }
//            });

            //log status
//        rgLogStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.rbApproved) {
//                    logStatus = "Approved";
//                } else if (checkedId == R.id.rbPending) {
//                    logStatus = "Pending";
//                } else if (checkedId == R.id.rbRejected) {
//                    logStatus = "Rejected";
//                }
//            }
//        });

            //log check
//        rgLogCheck.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.rbLogCheckYes) {
//                    logCheck = "Yes";
//                } else if (checkedId == R.id.rbLogCheckNo) {
//                    logCheck = "No";
//                }
//            }
//        });


            //

            btnSelectTool.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        resSave = util.getEvaluationForLessonPlan(orgId, insId, dscId, ayr);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // getting data from the webservice
                    toolArrayList = new ArrayList<Tool>();
                    Tool chooseTool = new Tool();
                    chooseTool.setEctId(-1);
                    chooseTool.setEcsHeader("Choose Tool");
                    toolArrayList.add(chooseTool);
                    final ArrayList<MultiSelectModel> toolsNames = new ArrayList();
                    //final ArrayList<Integer> toolsEcsIds = new ArrayList<>();
                    ArrayList<Integer> toolsIdArraylist = new ArrayList<>();

                    try {

                        if (resSave != null) {
                            JSONArray resArray = new JSONArray(resSave);
                            JSONObject statusObject = resArray.getJSONObject(0);
                            String status = statusObject.getString("status");

                            if (status.equals("SUCCESS")) {
                                JSONObject dataObj = resArray.getJSONObject(1);
                                //JSONObject dataObject = dataObj.getJSONObject("data");
                                JSONArray dataArray1 = dataObj.getJSONArray("data");
                                if (dataArray1.length() == 0) {
                                    layParent.setVisibility(View.GONE);
                                    // layNoRecord.setVisibility(View.VISIBLE);
                                    Toast.makeText(context, "Tools and methods not found!", Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                JSONObject dataObj1 = dataArray1.getJSONObject(0);
                                JSONArray dataArray = dataObj1.getJSONArray("subCatList");
                                for (int j = 0; j < dataArray.length(); j++) {
                                    tool = new Tool();
                                    JSONObject object = dataArray.getJSONObject(j);
                                    tool.setEctId(object.getInt("ectId"));
                                    tool.setEcsId(object.getInt("ecsId"));
                                    tool.setEcsHeader(object.getString("ecsHeader"));
                                    toolsIdArraylist.add(object.getInt("ecsId"));
                                    toolsNames.add(new MultiSelectModel(tool.getEcsId(),tool.getEcsHeader()));
                                    toolArrayList.add(tool);

                                }

//                                JSONObject dataObj2 = dataArray.getJSONObject(2);
//                                JSONArray dataArray2 = dataObj2.getJSONArray("subCatList");
//                                for (int j = 0; j < dataArray.length(); j++) {
//                                    method = new Method();
//                                    JSONObject object = dataArray.getJSONObject(j);
//                                    method.setEctId(object.getInt("ectId"));
//                                    method.setEcsId(object.getInt("ecsId"));
//                                    method.setEcsHeader(object.getString("ecsHeader"));
//
//                                    methodArrayList.add(method);
//                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(newLog==0)  // fill log
                    {
                        selectedPreTool = toolsIdArraylist;
                    }
                    else
                    {
                        selectedPreTool = selectedPreTool;
                    }

                    if(toolArrayList.size()>0)
                    {

                        toolsIds = new StringBuilder();
                        //tools selection
                        MultiSelectDialog multiSelectDialog1 = new MultiSelectDialog()
                                .title("Select Tools") //setting title for dialog
                                .titleSize(15)
                                .positiveText("Ok")
                                .negativeText("Cancel")
                                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                                .preSelectIDsList(selectedPreTool) //List of ids that you need to be selected
                                .multiSelectList(toolsNames) // the multi select model list with ids and name
                                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                    @Override
                                    public void onSelected(ArrayList<Integer> selectedPreTool, ArrayList<String> selectedNames,
                                                           String dataString) {
                                        //will return list of selected IDS
                                        //syllabusIds = new StringBuilder();
                                        for (int i = 0; i < selectedPreTool.size(); i++) {
                                            Toast.makeText(FillTeachingLogsActivity.this, "Selected Tool Ids : " + selectedPreTool.get(i) + "\n" +
                                                    "Selected tool name : " + selectedNames.get(i) + "\n" +
                                                    "DataString : " + dataString, Toast.LENGTH_SHORT).show();
                                            if(i==selectedPreTool.size()-1)
                                            {
                                                toolsIds = toolsIds.append("1"+"-"+selectedPreTool.get(i));
                                            }else{
                                                toolsIds = toolsIds.append("1"+"-"+selectedPreTool.get(i)+",");

                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.d(TAG, "Dialog cancelled");
                                    }
                                });

                        multiSelectDialog1.show(getSupportFragmentManager(), "multiSelectDialog");
                    }
                    else
                    {
                        Toast.makeText(context, "Tools not present..", Toast.LENGTH_SHORT).show();
                    }

                }
            });


            btnSelectMethod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        resSave = util.getEvaluationForLessonPlan(orgId, insId, dscId, ayr);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    methodArrayList = new ArrayList<Method>();
                    Method chooseMethod = new Method();
                    chooseMethod.setEctId(-1);
                    chooseMethod.setEcsHeader("Choose Method");
                    methodArrayList.add(chooseMethod);
                    final ArrayList<MultiSelectModel> methodsNames = new ArrayList();
                    final ArrayList<Integer> methodsEcsIds = new ArrayList<>();
                    ArrayList<Integer> methodsIdArraylist = new ArrayList<>();

                    try {

                        if (resSave != null) {
                            JSONArray resArray = new JSONArray(resSave);
                            JSONObject statusObject = resArray.getJSONObject(0);
                            String status = statusObject.getString("status");

                            if (status.equals("SUCCESS")) {
                                JSONObject dataObj = resArray.getJSONObject(1);
                                //JSONObject dataObject = dataObj.getJSONObject("data");
                                JSONArray dataArray1 = dataObj.getJSONArray("data");
                                if (dataArray1.length() == 0) {
                                    layParent.setVisibility(View.GONE);
                                    // layNoRecord.setVisibility(View.VISIBLE);
                                    Toast.makeText(context, "Tools and methods not found!", Toast.LENGTH_SHORT).show();
                                    return;
                                }


                                JSONObject dataObj2 = dataArray1.getJSONObject(1);
                                JSONArray dataArray2 = dataObj2.getJSONArray("subCatList");
                                for (int j = 0; j < dataArray2.length(); j++) {
                                    method = new Method();
                                    JSONObject object = dataArray2.getJSONObject(j);
                                    method.setEctId(object.getInt("ectId"));
                                    method.setEcsId(object.getInt("ecsId"));
                                    method.setEcsHeader(object.getString("ecsHeader"));
                                    methodsIdArraylist.add(object.getInt("ecsId"));
                                    methodsNames.add(new MultiSelectModel(method.getEcsId(),method.getEcsHeader()));
                                    methodArrayList.add(method);
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(newLog==0)  // fill log
                    {
                        selectedPreMethod = methodsIdArraylist;
                    }
                    else
                    {
                        selectedPreMethod = selectedPreMethod;
                    }

                    if(methodArrayList.size()>0)
                    {

                        methodsIds = new StringBuilder();
                        //Methods selection
                        MultiSelectDialog multiSelectDialog1 = new MultiSelectDialog()
                                .title("Select Methods") //setting title for dialog
                                .titleSize(15)
                                .positiveText("Ok")
                                .negativeText("Cancel")
                                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                                .preSelectIDsList(selectedPreMethod) //List of ids that you need to be selected
                                .multiSelectList(methodsNames) // the multi select model list with ids and name
                                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                    @Override
                                    public void onSelected(ArrayList<Integer> selectedPreMethod, ArrayList<String> selectedNames,
                                                           String dataString) {
                                        //will return list of selected IDS
                                        //syllabusIds = new StringBuilder();
                                        for (int i = 0; i < selectedPreMethod.size(); i++) {
                                            Toast.makeText(FillTeachingLogsActivity.this, "Selected Tool Ids : " + selectedPreMethod.get(i) + "\n" +
                                                    "Selected tool name : " + selectedNames.get(i) + "\n" +
                                                    "DataString : " + dataString, Toast.LENGTH_SHORT).show();
                                            if(i==selectedPreMethod.size()-1)
                                            {
                                                methodsIds = methodsIds.append("2"+"-"+selectedPreMethod.get(i));
                                            }else{
                                                methodsIds = methodsIds.append("2"+"-"+selectedPreMethod.get(i)+",");

                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.d(TAG, "Dialog cancelled");
                                    }
                                });

                        multiSelectDialog1.show(getSupportFragmentManager(), "multiSelectDialog");
                    }
                    else
                    {
                        Toast.makeText(context, "Methods not present..", Toast.LENGTH_SHORT).show();
                    }

                }

            });


            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        schdlTeaching = edtShdTeaching.getText().toString().trim();
                        actlTeaching = edtActlTeaching.getText().toString().trim();
                        comment = edtComment.getText().toString().trim();
                        reason = edtReason.getText().toString().trim();
                        FillTeachingLogsActivity.SaveTeachingLog saveTeachingLog = new FillTeachingLogsActivity.SaveTeachingLog();
                        saveTeachingLog.execute(null, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            btnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        Intent intent = new Intent(context, TeachingLogCalendarSummaryActivity.class);
                        context.startActivity(intent);
                        ((Activity)context).finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private void openDialogSelectPlannedSyllabus(){
//                    final Dialog dialogSyllabus = new Dialog(context);
//                    dialogSyllabus.setContentView(R.layout.dialog_select_syllabus);
//                    Window window =dialogSyllabus.getWindow();
//                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                    dialogSyllabus.show();
//                    dialogSyllabus.setCanceledOnTouchOutside(true);
//                    dialogSyllabus.setCancelable(true);

        //MultiSelectModel - unit names
        // webservice call for syllabus details
        int selectedUnitId =0;
        if(teachingLog.getPlannedUniSun()!=null)
        {
            String uniSubUniList = teachingLog.getPlannedUniSun();
            String[] uniSubUniListSplitted = uniSubUniList.split(",");
            int size = uniSubUniListSplitted.length;
            String[] subUni = new String[size];
            selectedPrePlanned = new ArrayList<>();
            for(int i=0;i<uniSubUniListSplitted.length;i++)
            {
                String[] s2 = uniSubUniListSplitted[i].split("-");
                selectedUnitId = Integer.parseInt(s2[0]);
                subUni[i] = s2[1];
                selectedPrePlanned.add(Integer.parseInt(s2[1]));
            }

        }
        try {
            resSave = util.getStdUnitsSubUnits(orgId, insId, dscId, ayr, stdId, subId);

        } catch (Exception e) {
            e.printStackTrace();
        }


        // getting data from the webservice
        unitArrayList = new ArrayList<SyllabusDetails>();
        SyllabusDetails choose = new SyllabusDetails();
        choose.setUniId(-1);
        choose.setUniDescription("Choose Unit name");
        unitArrayList.add(choose);

        ArrayList<SubUnitsSyllabusDetails> subUnitArrayList = new ArrayList<>();

        try {

            if (resSave != null) {
                JSONArray resArray = new JSONArray(resSave);
                JSONObject statusObject = resArray.getJSONObject(0);
                String status = statusObject.getString("status");

                if (status.equals("SUCCESS")) {
                    JSONObject dataObj = resArray.getJSONObject(1);
                    //JSONObject dataObject = dataObj.getJSONObject("data");
                    JSONArray dataArray1 = dataObj.getJSONArray("data");
                    if (dataArray1.length() == 0) {
                        layParent.setVisibility(View.GONE);
                        // layNoRecord.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Syllabus not found!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    for (int i = 0; i < dataArray1.length(); i++) {
                        JSONObject dataObj1 = dataArray1.getJSONObject(i);
                        unit = new SyllabusDetails();
                        unit.setUniDescription(dataObj1.getString("uniDescription"));
                        unit.setUniId(dataObj1.getInt("uniId"));
                        JSONArray dataArray = dataObj1.getJSONArray("SubUnitList");
                        for (int j = 0; j < dataArray.length(); j++) {
                            subUnit = new SubUnitsSyllabusDetails();
                            JSONObject object = dataArray.getJSONObject(j);
                            subUnit.setSunId(object.getInt("sunId"));
                            subUnit.setSunDescription(object.getString("sunDescription"));
                            subUnit.setUniId(object.getInt("uniId"));

                            subUnitArrayList.add(subUnit);
                            //subUnitNames.add(subUnit.getSunDescription());

                        }
                        unitArrayList.add(unit);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //Spinner for unit selection
//        AlertDialog.Builder builder = new AlertDialog.Builder(FillTeachingLogsActivity.this);
//        View mView = getLayoutInflater().inflate(R.layout.dialog_syllabus_selection, null);
//        builder.setTitle("Select Unit and Syllabus");
//        Spinner unitSpinner = (Spinner) mView.findViewById(R.id.unitSpinner);
//        ArrayAdapter<SyllabusDetails> unitDialogAdapeter = new ArrayAdapter<SyllabusDetails>(FillTeachingLogsActivity.this,
//                android.R.layout.simple_spinner_item, unitArrayList);
//        unitDialogAdapeter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        unitSpinner.setAdapter(unitDialogAdapeter);

        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Planed Syllabus");
        dialog.setContentView(R.layout.dialog_syllabus_selection);
        dialog.setCancelable(true);
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

        Spinner unitSpinner = (Spinner) dialog.findViewById(R.id.unitSpinner);
        TextView name = (TextView)dialog.findViewById(R.id.name);
        name.setText("Planned Syllabus");
        ArrayAdapter<SyllabusDetails> unitDialogAdapeter = new ArrayAdapter<SyllabusDetails>(FillTeachingLogsActivity.this,
                android.R.layout.simple_spinner_item, unitArrayList);
        unitDialogAdapeter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSpinner.setAdapter(unitDialogAdapeter);
        if(newLog==1)
        {
            unitSpinner.setSelection(selectedUnitId);
        }

        Button okay = dialog.findViewById(R.id.okay);
        Button cancel = dialog.findViewById(R.id.cancel);
        final ArrayList<SubUnitsSyllabusDetails> subUniList = subUnitArrayList;

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedUnit == -1)
                {
                    Toast.makeText(context, "Please select Unit..", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    final int selectedUnitId = selectedUnit;

                    ArrayList<String> subUnitIDDes = new ArrayList<>();
                    ArrayList<Integer> subUnitId = new ArrayList<>();
                    final ArrayList<MultiSelectModel> subUnitNames = new ArrayList();
                    ArrayList<Integer> subUnitIdArraylist = new ArrayList<>();

                    for(SubUnitsSyllabusDetails s : subUniList) {
                        if(selectedUnitId == s.getUniId()) {
                            subUnitIDDes.add(s.getSunId() + " " + s.getSunDescription());
                            subUnitId.add(s.getSunId());
                            subUnitNames.add(new MultiSelectModel(s.getSunId(),s.getSunDescription()));
                            subUnitIdArraylist.add(s.getSunId());
                        }
                    }


                    if(newLog==0)  // fill log
                    {
                        selectedPlannedSubUnitIds = subUnitIdArraylist;
                    }
                    else
                    {
                        selectedPlannedSubUnitIds = selectedPrePlanned;
                    }
                    if(subUnitNames.size()>0)
                    {

                        dialog.dismiss();

                        //sub unit selection
                        MultiSelectDialog multiSelectDialog1 = new MultiSelectDialog()
                                .title("Select Sub Unit") //setting title for dialog
                                .titleSize(15)
                                .positiveText("Ok")
                                .negativeText("Cancel")
                                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                                .preSelectIDsList(selectedPlannedSubUnitIds) //List of ids that you need to be selected
                                .multiSelectList(subUnitNames) // the multi select model list with ids and name
                                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                                    @Override
                                    public void onSelected(ArrayList<Integer> selectedPlannedSubUnitIds, ArrayList<String> selectedNames,
                                                           String dataString) {
                                        //will return list of selected IDS
                                        PlannedSyllabusIds = new StringBuilder();
                                        for (int i = 0; i < selectedPlannedSubUnitIds.size(); i++) {
                                            Toast.makeText(FillTeachingLogsActivity.this, "Selected Sub Unit Ids : " + selectedPlannedSubUnitIds.get(i) + "\n" +
                                                    "Selected Sub Unit Names : " + selectedNames.get(i) + "\n" +
                                                    "DataString : " + dataString, Toast.LENGTH_SHORT).show();
                                            txtPlannedUniSubuniName = dataString;
                                            if(!txtPlannedUniSubuniName.isEmpty())
                                            {
                                                layPlannedSyllabusTxt.setVisibility(View.VISIBLE);
                                                txtPlannedSyllabus.setText(txtPlannedUniSubuniName);
                                            }
                                            //txtPlannedUniSubuniName = txtPlannedUniSubuniName.concat(selectedNames.get(i)+" ");
                                            if(i==selectedPlannedSubUnitIds.size()-1)
                                            {
                                                PlannedSyllabusIds = PlannedSyllabusIds.append(selectedUnit+"-"+selectedPlannedSubUnitIds.get(i));
                                            }else{
                                                PlannedSyllabusIds = PlannedSyllabusIds.append(selectedUnit+"-"+selectedPlannedSubUnitIds.get(i)+",");

                                            }
                                        }
                                      //  syllabusIds.subSequence(0,syllabusIds.length()-1);
                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.d(TAG, "Dialog cancelled");
                                    }
                                });

                        multiSelectDialog1.show(getSupportFragmentManager(), "multiSelectDialog");
                    }
                    else
                    {
                        Toast.makeText(context, "Subunit Not present for this unit..", Toast.LENGTH_SHORT).show();
                    }
                    
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedUnit = unitArrayList.get(position).getUniId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




//                Toast.makeText(FillTeachingLogsActivity.this, "Selected Ids : " + selectedIds.get(i) + "\n" +
//                        "Selected Names : " + selectedNames.get(i) + "\n" +
//                        "DataString : " + dataString, Toast.LENGTH_SHORT).show();
    }

    public void openDialogSelectActualTakenSyllabus(){

        try {
            resSave = util.getStdUnitsSubUnits(orgId, insId, dscId, ayr, stdId, subId);

        } catch (Exception e) {
            e.printStackTrace();
        }


        // getting data from the webservice
        unitArrayList = new ArrayList<SyllabusDetails>();
        SyllabusDetails choose = new SyllabusDetails();
        choose.setUniId(-1);
        choose.setUniDescription("Choose Unit name");
        unitArrayList.add(choose);

        ArrayList<SubUnitsSyllabusDetails> subUnitArrayList = new ArrayList<>();

        try {

            if (resSave != null) {
                JSONArray resArray = new JSONArray(resSave);
                JSONObject statusObject = resArray.getJSONObject(0);
                String status = statusObject.getString("status");

                if (status.equals("SUCCESS")) {
                    JSONObject dataObj = resArray.getJSONObject(1);
                    //JSONObject dataObject = dataObj.getJSONObject("data");
                    JSONArray dataArray1 = dataObj.getJSONArray("data");
                    if (dataArray1.length() == 0) {
                        layParent.setVisibility(View.GONE);
                        // layNoRecord.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Syllabus not found!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    for (int i = 0; i < dataArray1.length(); i++) {
                        JSONObject dataObj1 = dataArray1.getJSONObject(i);
                        unit = new SyllabusDetails();
                        unit.setUniDescription(dataObj1.getString("uniDescription"));
                        unit.setUniId(dataObj1.getInt("uniId"));
                        JSONArray dataArray = dataObj1.getJSONArray("SubUnitList");
                        for (int j = 0; j < dataArray.length(); j++) {
                            subUnit = new SubUnitsSyllabusDetails();
                            JSONObject object = dataArray.getJSONObject(j);
                            subUnit.setSunId(object.getInt("sunId"));
                            subUnit.setSunDescription(object.getString("sunDescription"));
                            subUnit.setUniId(object.getInt("uniId"));

                            subUnitArrayList.add(subUnit);
                            //subUnitNames.add(subUnit.getSunDescription());

                        }
                        unitArrayList.add(unit);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        int selectedUnitId =0;
        selectedPreSubunits = new ArrayList<>();
        if(teachingLog.getActualTakenUniSun()!=null)
        {
            String uniSubUniList = teachingLog.getActualTakenUniSun();
            String[] uniSubUniListSplitted = uniSubUniList.split(",");
            int size = uniSubUniListSplitted.length;
            String[] subUni = new String[size];
            for(int i=0;i<uniSubUniListSplitted.length;i++)
            {
                String[] s2 = uniSubUniListSplitted[i].split("-");
                selectedUnitId = Integer.parseInt(s2[0]);
                subUni[i] = s2[1];
                selectedPreSubunits.add(Integer.parseInt(s2[1]));
            }
            System.out.println(selectedPreSubunits.size());
        }

        final Dialog dialog = new Dialog(context);
        dialog.setTitle("Actual Taken Syllabus");

        dialog.setContentView(R.layout.dialog_syllabus_selection);
        dialog.setCancelable(true);
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

        Spinner unitSpinner = (Spinner) dialog.findViewById(R.id.unitSpinner);
        TextView name = (TextView)dialog.findViewById(R.id.name);
        name.setText("Actual Taken Syllabus");
        ArrayAdapter<SyllabusDetails> unitDialogAdapeter = new ArrayAdapter<SyllabusDetails>(FillTeachingLogsActivity.this,
                android.R.layout.simple_spinner_item, unitArrayList);
        unitDialogAdapeter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSpinner.setAdapter(unitDialogAdapeter);
        if(newLog==1)
        {
            unitSpinner.setSelection(selectedUnitId);
        }

        Button okay = dialog.findViewById(R.id.okay);
        Button cancel = dialog.findViewById(R.id.cancel);
        final ArrayList<SubUnitsSyllabusDetails> subUniList = subUnitArrayList;

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedUnit == -1)
                {
                    Toast.makeText(context, "Please select Unit..", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    final int selectedUnitId = selectedUnit;

                    ArrayList<String> subUnitIDDes = new ArrayList<>();
                    ArrayList<Integer> subUnitId = new ArrayList<>();
                    final ArrayList<MultiSelectModel> subUnitNames = new ArrayList();
                    final ArrayList<Integer> selectedSubUnitIds = new ArrayList<>();
                    ArrayList<Integer> subUnitIdArraylist = new ArrayList<>();

                    for(SubUnitsSyllabusDetails s : subUniList) {
                        if(selectedUnitId == s.getUniId()) {
                            if(newLog==1)
                            {
                                if(!completedUniSubIds.contains(s.getSunId()))
                                {
                                    subUnitIDDes.add(s.getSunId() + " " + s.getSunDescription());
                                    subUnitId.add(s.getSunId());
                                    subUnitNames.add(new MultiSelectModel(s.getSunId(),s.getSunDescription()));
                                    subUnitIdArraylist.add(s.getSunId());
                                }
                            }else if(newLog==0)
                            {
                                subUnitIDDes.add(s.getSunId() + " " + s.getSunDescription());
                                subUnitId.add(s.getSunId());
                                subUnitNames.add(new MultiSelectModel(s.getSunId(),s.getSunDescription()));
                                subUnitIdArraylist.add(s.getSunId());
                            }

                        }
                    }
                    if(newLog==0)  // fill log
                    {
                        selectedActualSubUnitIds1 = subUnitIdArraylist;
                    }
                    else
                    {
                        selectedActualSubUnitIds1 = selectedPreSubunits;
                    }

                    if(subUnitNames.size()>0)
                    {

                        dialog.dismiss();

                        //sub unit selection
                        MultiSelectDialog multiSelectDialog1 = new MultiSelectDialog()
                                .title("Select Sub Unit") //setting title for dialog
                                .titleSize(15)
                                .positiveText("Ok")
                                .negativeText("Cancel")
                                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                                .preSelectIDsList(selectedActualSubUnitIds1) //List of ids that you need to be selected
                                .multiSelectList(subUnitNames) // the multi select model list with ids and name
                                .onSubmit(new MultiSelectDialog.SubmitCallbackListener()
                                {
                                    @Override
                                    public void onSelected(ArrayList<Integer> selectedActualSubUnitIds, ArrayList<String> selectedNames,
                                                           String dataString) {
                                        //will return list of selected IDS
                                        syllabusIds = new StringBuilder();
                                        for (int i = 0; i < selectedActualSubUnitIds.size(); i++) {
                                            Toast.makeText(FillTeachingLogsActivity.this, "Selected Sub Unit Ids : " + selectedActualSubUnitIds.get(i) + "\n" +
                                                    "Selected Sub Unit Names : " + selectedNames.get(i) + "\n" +
                                                    "DataString : " + dataString, Toast.LENGTH_SHORT).show();
                                            if(!dataString.isEmpty())
                                            {
                                                layTakenSyllabusTxt.setVisibility(View.VISIBLE);
                                                txtActualSyllabus.setText(dataString);
                                            }
//                                            txtActualUniSubuniName = txtActualUniSubuniName.concat(selectedNames.get(i)+" ");
                                            if(i==selectedActualSubUnitIds.size()-1)
                                            {
                                                syllabusIds = syllabusIds.append(selectedUnit+"-"+selectedActualSubUnitIds.get(i));
//                                                txtActualUniSubuniName = dataString;
//                                                System.out.println(txtActualUniSubuniName);
                                            }else{
                                                syllabusIds = syllabusIds.append(selectedUnit+"-"+selectedActualSubUnitIds.get(i)+",");

                                            }
                                           // dialog.dismiss();


                                        }
                                        //  syllabusIds.subSequence(0,syllabusIds.length()-1);
                                        openCompletedUniSubuniDialog(selectedActualSubUnitIds,selectedNames);
                                    }

                                    @Override
                                    public void onCancel() {
                                        Log.d(TAG, "Dialog cancelled");
                                    }
                                });

                        multiSelectDialog1.show(getSupportFragmentManager(), "multiSelectDialog");
                    }
                    else
                    {
                        Toast.makeText(context, "Subunit Not present for this unit..", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selectedUnit = unitArrayList.get(position).getUniId();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    });
    }

    private void openCompletedUniSubuniDialog(ArrayList<Integer> selectedActualSubUnitIds2, ArrayList<String> selectedNames1) {

        final ArrayList<MultiSelectModel> selectedCompletedNames = new ArrayList();
        for(int i = 0; i<selectedNames1.size();i++)
        {
            MultiSelectModel newSelectModel = new MultiSelectModel(selectedActualSubUnitIds2.get(i),selectedNames1.get(i));
            selectedCompletedNames.add(newSelectModel);
        }
        //sub unit selection
        MultiSelectDialog multiSelectDialog1 = new MultiSelectDialog()
                .title("Select Completed Sub unit") //setting title for dialog
                .titleSize(15)
                .positiveText("Ok")
                .negativeText("Cancel")
                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                .multiSelectList(selectedCompletedNames) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener()
                {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedActualSubUnitIds, ArrayList<String> selectedNames,
                                           String dataString) {
                        //will return list of selected IDS
                        completedSyllabusIds = new StringBuilder();
                        for (int i = 0; i < selectedActualSubUnitIds.size(); i++) {
                            System.out.println(i + " "+selectedActualSubUnitIds.size());
                            Toast.makeText(FillTeachingLogsActivity.this, "Selected Sub Unit Ids : " + selectedActualSubUnitIds.get(i) + "\n" +
                                    "Selected Sub Unit Names : " + selectedNames.get(i) + "\n" +
                                    "DataString : " + dataString, Toast.LENGTH_SHORT).show();
//                                            txtActualUniSubuniName = txtActualUniSubuniName.concat(selectedNames.get(i)+" ");
                            if(i==selectedActualSubUnitIds.size()-1)
                            {
                                completedSyllabusIds = completedSyllabusIds.append(selectedUnit+"-"+selectedActualSubUnitIds.get(i));
//                                                txtActualUniSubuniName = dataString;
//                                                System.out.println(txtActualUniSubuniName);
                            }else{
                                completedSyllabusIds = completedSyllabusIds.append(selectedUnit+"-"+selectedActualSubUnitIds.get(i)+",");

                            }


                        }
                        //  syllabusIds.subSequence(0,syllabusIds.length()-1);
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "Dialog cancelled");
                    }
                });

        multiSelectDialog1.show(getSupportFragmentManager(), "multiSelectDialog");
    }


    public class SaveTeachingLog extends AsyncTask<String, String, String> {
        private String resSave;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();
                if (resSave != null) {

                    JSONArray resArray = new JSONArray(resSave);
                    JSONObject statusObj = resArray.getJSONObject(0);
                    String status = statusObj.getString("status");
                    if (status.equals("SUCCESS")) {
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Teaching log details save successfully!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(4000);
                        snackbar.show();
                        Intent intent = new Intent(FillTeachingLogsActivity.this, TeachingLogCalendarSummaryActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Failed to save teaching log details!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                        return;
                    }
                } else {
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Failed to save teaching log details!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String telEvalEctSubEctIds;
            try {
                if(toolsIds!=null && methodsIds!=null) {
                    telEvalEctSubEctIds = toolsIds.toString().concat(",").concat(methodsIds.toString());
                }
                else if(toolsIds!=null){
                    telEvalEctSubEctIds = toolsIds.toString().concat(",").concat("");
                }
                else if(methodsIds!=null){
                    telEvalEctSubEctIds = "".concat(",").concat(methodsIds.toString());
                }
                else
                {
                    telEvalEctSubEctIds = "";
                }
                resSave = util.saveTeachingLogForDay(orgId, insId, dscId, stdId, stfId, divId, ayr,prdId, subId, specialLog, actlTeaching!=null?actlTeaching:"", comment!=null?comment:"", prdIndex, syllabusBased, syllabusIds!=null?syllabusIds.toString():"",schdlTeaching!=null?schdlTeaching:"", lectureConducted, eventBasedYesNo, selectedDate,telEvalEctSubEctIds!=null?telEvalEctSubEctIds:"",PlannedSyllabusIds!=null?PlannedSyllabusIds.toString():"",reason!=null?reason:"",completedSyllabusIds!=null?completedSyllabusIds.toString():"");
                return resSave;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;

        }
    }
}

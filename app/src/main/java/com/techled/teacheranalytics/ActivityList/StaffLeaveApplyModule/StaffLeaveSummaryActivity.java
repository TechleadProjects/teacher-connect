package com.techled.teacheranalytics.ActivityList.StaffLeaveApplyModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.TimetableModule.ViewStaffTimeTableActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.StaffLeaveSummaryRecyAdapter;
import com.techled.teacheranalytics.adapter.StaffTimetableRecyAdapter;
import com.techled.teacheranalytics.pojo.LeaveSummary;
import com.techled.teacheranalytics.pojo.MyLectureDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class StaffLeaveSummaryActivity extends AppCompatActivity {

    private List<LeaveSummary> leaveSummaryList = new ArrayList<>();
    private RecyclerView staffLeaveSummaryRecyView;
    private StaffLeaveSummaryRecyAdapter mStaffLeaveSummaryRecyAdapter;
    private ProgressDialog progDailog;
    private Context context;
    private Util util;
    private int orgId, insId, dscId, ayr, usrAssetId;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_leave_summary);

        context = this;
        util = new Util();

        try {

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("View Leave Summary");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            String params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                usrAssetId = object.getInt("assetId1");
                ayr = object.getInt("ayrYear");

            }

            staffLeaveSummaryRecyView = findViewById(R.id.staffLeaveSummaryRecyView);

            leaveSummaryList = new ArrayList<>();
            mStaffLeaveSummaryRecyAdapter = new StaffLeaveSummaryRecyAdapter(leaveSummaryList);
            staffLeaveSummaryRecyView.setLayoutManager(new LinearLayoutManager(this));
            staffLeaveSummaryRecyView.setItemAnimator(new DefaultItemAnimator());
            staffLeaveSummaryRecyView.setAdapter(mStaffLeaveSummaryRecyAdapter);

            new LoadMyLeaveSummaryDetails().execute();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public class LoadMyLeaveSummaryDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("SUCCESS")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        //JSONObject dataObject = dataObj.getJSONObject("data");
                        JSONArray dataArray1 = dataObj.getJSONArray("data");
                        if(dataArray1.length() == 0)
                        {
                            Toast.makeText(context, "Leave Summary not found!", Toast.LENGTH_SHORT).show();
                            finish();
                            return;
                        }

                        for (int i = 0; i < dataArray1.length(); i++) {
                            JSONObject object = dataArray1.getJSONObject(i);

                            LeaveSummary leaveSummary = new LeaveSummary();
                            leaveSummary.setLeaveType(object.getString("leaveType"));
                            leaveSummary.setLeavePeriod(object.getString("leaveStart") + " - " + object.getString("leaveEnd"));
                            leaveSummary.setLeaveApplicationDate(object.getString("applicationDate"));
                            leaveSummary.setLeaveAccountedDays(object.getString("accountedDays"));
                            leaveSummary.setLeaveStatus(object.getString("status"));
                            leaveSummary.setLeaveReason(object.getString("leaveReason"));

                            leaveSummaryList.add(leaveSummary);
                        }

                        if (leaveSummaryList.size() > 0){
                            mStaffLeaveSummaryRecyAdapter.notifyDataSetChanged();
                        }

                    } else {
                        Toast.makeText(context, "Leave Summary not found!", Toast.LENGTH_SHORT).show();
                        finish();
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resMyLectures = util.getMyLeavesStaff(orgId, insId, dscId, ayr, usrAssetId);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

package com.techled.teacheranalytics.ActivityList.SportAttendanceModule;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule.AttendanceActivity;
import com.techled.teacheranalytics.ActivityList.MarkStuAttendanceModule.MarkAttendanceStatusActivity;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.AttendanceManagementRecyAdapter2;
import com.techled.teacheranalytics.adapter.AttendanceStatusRecyAdapter;
import com.techled.teacheranalytics.adapter.SportAttendanceRecyAdapter;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.MarkAttendance;
import com.techled.teacheranalytics.pojo.MarkSportAttendance;
import com.techled.teacheranalytics.pojo.SportStdDivDetails;
import com.techled.teacheranalytics.pojo.StandardDivision;
import com.techled.teacheranalytics.pojo.StudentAttendanceStatus;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class FinalMarkSportAttendanceActivity extends AppCompatActivity {
    private RecyclerView recyViewSportAttd;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId;
    private SportAttendanceRecyAdapter adapter;
    private ArrayList<MarkSportAttendance> studentDetailsArrayList;
    public int stdId;
    public int divId;
    private int mYear, mMonth, mDay;
    private Button btnUpdate, btnViewNotifyStatus;
    private LinearLayout layDataNotFound, layParent;
    private Util util;
    private Context context;
    private String params;
    private int reqYear, reqMonth, reqDay;
    private String reqDate;
    private TextView txtDate;
    private Dialog dialog;
    private String notifyParents;
    private ProgressDialog progDailog, progDailog2;
    private String response, selectedSportMarkDate;
    private JSONArray classTeacherOfJson;
    private ArrayList<SportStdDivDetails> standardDivisionArrayList;
    private String resUserInfo, insName;
    private String resMarkAttd, markAttdDate;
    private List<MarkSportAttendance> stList;
    private String stdName, divName,stdDivName;
    private ArrayList<StudentAttendanceStatus> studentAttendanceStatusArrayList;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_mark_sport_attendance);

        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Sport Attendance");

            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            //get reference

            context = this;
            util = new Util();
            btnUpdate = (Button) findViewById(R.id.btnUpdate);
            recyViewSportAttd = (RecyclerView) findViewById(R.id.recyViewSportAttd);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            layoutManager.scrollToPositionWithOffset(0, 20);
            recyViewSportAttd.setLayoutManager(layoutManager);

            layParent = (LinearLayout) findViewById(R.id.layParent);
            layDataNotFound = (LinearLayout) findViewById(R.id.layDataNotFound);

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                // ayr = 2016;
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");

            }

            Intent intent = getIntent();
            if (intent != null) {
                selectedSportMarkDate = intent.getStringExtra("selectedDate");
                stdId = intent.getIntExtra("stdId", stdId);
                divId = intent.getIntExtra("divId", divId);
                stdDivName = intent.getStringExtra("stdDivName");

                TextView txtStdDiv = (TextView)findViewById(R.id.txtStdDiv);
                txtStdDiv.setText(stdDivName);

                TextView txtMarkDate = (TextView)findViewById(R.id.txtMarkDate);
                txtMarkDate.setText(selectedSportMarkDate);



            }



            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }


            //load sport attendance
            LoadAttendance loadAttendance = new LoadAttendance();
            loadAttendance.execute(null, null);

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog = new Dialog(context);
                    dialog.setContentView(R.layout.dialog_confirm_mark_attendance);
                    dialog.show();
                    Window window = dialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                    dialog.setCanceledOnTouchOutside(false);
                    notifyParents = "N";
                    // markAttdDate = txtDate.getText().toString().trim();

                    TextView txtConfirmDate = (TextView) dialog.findViewById(R.id.txtConfirmDate);
                    txtConfirmDate.setText("" + selectedSportMarkDate);


                    RadioGroup rgNotifyParents = (RadioGroup) dialog.findViewById(R.id.rgNotifyParents);
                    rgNotifyParents.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.rbYes) {
                                notifyParents = "Y";

                            } else if (checkedId == R.id.rbNo) {
                                notifyParents = "N";
                            }
                        }
                    });

                    Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                dialog.dismiss();

                                stList = ((SportAttendanceRecyAdapter) adapter)
                                        .getList();
                                Log.d("LIST", "" + stList);

                                if (stList.size() != 0) {

                                    //mark attendance
                                    markAttendance attendance = new markAttendance();
                                    attendance.execute(null, null);
                                } else {
                                    //  Toast.makeText(AttendanceActivity.this, "Failed to mark attendance!", Toast.LENGTH_SHORT).show();
                                    Snackbar snackbar = Snackbar
                                            .make(layParent, "Failed to mark sport attendance!", Snackbar.LENGTH_LONG);
                                    snackbar.setDuration(3000);
                                    snackbar.show();
                                    return;
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });


                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class markAttendance extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog2 = new ProgressDialog(context);
            progDailog2.setIndeterminate(false);
            progDailog2.setCancelable(false);
            progDailog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog2.setMessage("Loading...");
            progDailog2.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog2.dismiss();
            try {
                if (resMarkAttd != null) {


                    JSONObject object1 = new JSONObject(resMarkAttd);
                    String Status = object1.getString("status");
                    if (Status.equals("SPORT_ATTENDANCE_SUCCESSFULLY_UPDATED")) {
                        // Toast.makeText(context, "Attendance successfully marked!", Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(AttendanceActivity.this, MarkAttendanceStatusActivity.class);
//                        startActivity(intent);
//                        finish();
                        Toast.makeText(context, "Sport attendance successfully marked!", Toast.LENGTH_LONG).show();
                       /* Snackbar snackbar = Snackbar
                                .make(layParent, "Sport attendance successfully marked!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();*/

                        Intent intent = new Intent(FinalMarkSportAttendanceActivity.this, SportAttendanceStatusHistoryActivity.class);
                        startActivity(intent);
                        finish();


                    } else {
                        //  Toast.makeText(context, "Failed to mark attendance!", Toast.LENGTH_LONG).show();
                        Snackbar.make(layParent, "Failed to mark sport attendance!", Snackbar.LENGTH_LONG)
                                .setDuration(3000)
                                .show();
                        util.SaveSystemLog(orgId, insId, "Failed to mark sport attendance", "Teacher Connect", usrId, "Mark Attendance");
                        return;
                    }


                } else {
                    //Toast.makeText(context, "Failed to mark attendance!", Toast.LENGTH_LONG).show();
                    Snackbar.make(layParent, "Failed to mark sport attendance!", Snackbar.LENGTH_LONG)
                            .setDuration(3000)
                            .show();
                    util.SaveSystemLog(orgId, insId, "Failed to mark sport attendance", "Teacher Connect", usrId, "Mark Attendance");
                    return;
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while marking sport attendance", "Teacher Connect", usrId, "Mark Attendance");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                SimpleDateFormat spf = new SimpleDateFormat("dd-MMM-yyyy");
                Date newDate = spf.parse(selectedSportMarkDate);
                spf = new SimpleDateFormat("yyyy-MM-dd");
                String date = spf.format(newDate);
                System.out.println(date);

                String rolls1, rolls;
                rolls1 = stList.toString().replace("[", "");
                rolls = rolls1.toString().replace("]", "");
                resMarkAttd = util.markStudentSportAttd(orgId, insId, dscId, ayr, stdId, divId, rolls, date, notifyParents, usrAssetId);
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while marking sport attendance", "Teacher Connect", usrId, "Mark Attendance");

            }
            return null;
        }
    }


    public class LoadAttendance extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (response.equals("") || response == null) {
                    layParent.setVisibility(View.GONE);
                    layDataNotFound.setVisibility(View.VISIBLE);
                    //  Toast.makeText(AttendanceActivity.this, "Unable to load attendance history!", Toast.LENGTH_SHORT).show();

                    Snackbar.make(layParent, "Unable to load sport attendance history!", Snackbar.LENGTH_LONG)
                            .setDuration(3000)
                            .show();
                    util.SaveSystemLog(orgId, insId, "Unable to load sport attendance history", "Teacher Connect", usrId, "Mark Attendance");
                    return;
                } else {
                    JSONArray responseArray = new JSONArray(response);
                    JSONObject object1 = responseArray.getJSONObject(0);
                    String status = object1.getString("status");
                    if (status.equals("SUCCESS")) {

                        //   JSONObject object2 = responseArray.getJSONObject(1);
                        studentDetailsArrayList = new ArrayList<>();
                        JSONArray array = object1.getJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {
                            MarkSportAttendance markAttendance = new MarkSportAttendance();
                            JSONObject object = array.getJSONObject(i);
                            markAttendance.setStdId(object.getInt("stuId"));
                            markAttendance.setFlagPAL(object.getString("stuStatus"));
                            markAttendance.setStuName(object.getString("stuName"));
                            markAttendance.setRollNo(object.getInt("rollNo"));
                            markAttendance.setStuStatusDaily(object.getString("stuStatusDaily"));
                            studentDetailsArrayList.add(markAttendance);
                        }
                        if (studentDetailsArrayList.size() == 0) {
                            layParent.setVisibility(View.GONE);
                            layDataNotFound.setVisibility(View.VISIBLE);
                            //    Toast.makeText(AttendanceActivity.this, "Unable to load attendance history!", Toast.LENGTH_SHORT).show();
                            Snackbar.make(layParent, "Unable to sport load attendance history!", Snackbar.LENGTH_LONG)
                                    .setDuration(3000)
                                    .show();
                            util.SaveSystemLog(orgId, insId, "Unable to sport load attendance history", "Teacher Connect", usrId, "Mark Attendance");
                            return;
                        }

                        //set adapter
                        layParent.setVisibility(View.VISIBLE);
                        layDataNotFound.setVisibility(View.GONE);
                        adapter = new SportAttendanceRecyAdapter(context, studentDetailsArrayList, "");
                        recyViewSportAttd.setAdapter(adapter);
                        adapter.notifyDataSetChanged();


                    } else {

                        layParent.setVisibility(View.GONE);
                        layDataNotFound.setVisibility(View.VISIBLE);
                        //   Toast.makeText(AttendanceActivity.this, "Unable to load attendance history!", Toast.LENGTH_SHORT).show();
                        Snackbar.make(layParent, "Unable to load sport attendance history!", Snackbar.LENGTH_LONG)
                                .setDuration(3000)
                                .show();
                        util.SaveSystemLog(orgId, insId, "Unable to load sport attendance history", "Teacher Connect", usrId, "Mark Attendance");
                        return;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading sport attendance history", "Teacher Connect", usrId, "Mark Attendance");
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                response = util.fetchMarkedSportAttendance(orgId, insId, dscId, ayr, stdId, divId, selectedSportMarkDate);

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while loading sport attendance history", "Teacher Connect", usrId, "Mark Attendance");
            }
            return null;
        }
    }


}

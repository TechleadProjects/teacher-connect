package com.techled.teacheranalytics.ActivityList.TaskManagementModule;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.adapter.PendingTasksRecyAdapter;
import com.techled.teacheranalytics.pojo.PendingTaskDetails;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyTasksActivity extends AppCompatActivity {

    private Context context;
    private Dialog dialog;
    private Spinner filterSpinner;
    private String filterValue = "";
    private boolean highPriorityValue = false, mediumPriorityValue = false, lowPriorityValue = false;
    private boolean assignedProgressValue = false, inProgressProgressValue = false, reAssignedProgressValue = false, completedProgressValue = false;
    private ArrayList<String> filterArrayList;
    private final Calendar myCalendar = Calendar.getInstance();
    private Button btnName;
    private ImageView filterButton;
    private DatePickerDialog.OnDateSetListener date;
    private ArrayList<PendingTaskDetails> pendingTaskDetailsArrayList, filteredPendingTaskDetailsArrayList;
    private RecyclerView pending_tasks_recycler_view;
    private PendingTasksRecyAdapter pendingTasksRecyAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private String taskAssignedToMe, taskValue;
    private LinearLayout layParent, layNoData;
    private ProgressDialog progDailog;
    private Util util;
    private int orgId, insId, dscId, stfId;
    private FloatingActionButton addNewTask;
    ArrayAdapter<String> filterAdapter;

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pending_tasks);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Tasks");

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        context = this;
        util = new Util();

        pending_tasks_recycler_view = findViewById(R.id.pending_tasks_recycler_view);
        layParent = findViewById(R.id.layParent);
        layNoData = findViewById(R.id.layNoRecord);
        filterSpinner = findViewById(R.id.filterSpinner);
        addNewTask = findViewById(R.id.addNewTask);
        filterButton = findViewById(R.id.filterButton);

        //login details
        SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
        String params = loginDetails.getString("params", null);

        try {
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                dscId = object.getInt("dscId");
                stfId = object.getInt("assetId1");

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    filterValue = "To";
                    addNewTask.setVisibility(View.GONE);
                    new LoadMyPendingTaskDetails().execute();
                }else {
                    filterValue = "By";
                    addNewTask.setVisibility(View.VISIBLE);
                    new LoadMyPendingTaskDetails().execute();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(btnName);
            }

        };

        pendingTaskDetailsArrayList = new ArrayList<>();
        filteredPendingTaskDetailsArrayList = new ArrayList<>();

        filterArrayList = new ArrayList<>();
        filterArrayList.add("Task Assigned To Me");
        filterArrayList.add("Task Assigned By Me");

        filterAdapter =
                new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, filterArrayList);
        filterAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        filterSpinner.setAdapter(filterAdapter);

        //String pendingTasksString = getIntent().getStringExtra("taskList");
        taskAssignedToMe = getIntent().getStringExtra("byOrTo");

        if(taskAssignedToMe.equals("To")){
            addNewTask.setVisibility(View.GONE);
            filterSpinner.setSelection(0, true);
        }else {
            addNewTask.setVisibility(View.VISIBLE);
            filterSpinner.setSelection(1, true);
        }

        layoutManager = new LinearLayoutManager(context);
        pending_tasks_recycler_view.setLayoutManager(layoutManager);
        pending_tasks_recycler_view.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!filterValue.equals("")) {
            new LoadMyPendingTaskDetails().execute();
        }

    }

    public void applyFilters(View view) {

        filteredPendingTaskDetailsArrayList.clear();
        assignedProgressValue = false;
        inProgressProgressValue = false;
        reAssignedProgressValue = false;
        completedProgressValue = false;
        highPriorityValue = false;
        mediumPriorityValue = false;
        lowPriorityValue = false;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_apply_task_filters);
        dialog.show();
        Window window = dialog.getWindow();
        dialog.setCancelable(false);
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);

        final Button btnFrom = dialog.findViewById(R.id.btnFrom);
        final Button btnTo = dialog.findViewById(R.id.btnTo);

        LinearLayout priorityStatusLayout = dialog.findViewById(R.id.priorityLayout);
        //LinearLayout assignToLayout = dialog.findViewById(R.id.assignToLayout);

        if(filterValue.equals("By")){
            priorityStatusLayout.setVisibility(View.VISIBLE);
            //assignToLayout.setVisibility(View.VISIBLE);
        }else {
            priorityStatusLayout.setVisibility(View.GONE);
            //assignToLayout.setVisibility(View.GONE);
        }

        CheckBox highPriority = dialog.findViewById(R.id.highPriority);
        CheckBox mediumPriority = dialog.findViewById(R.id.mediumPriority);
        CheckBox lowPriority = dialog.findViewById(R.id.lowPriority);

        CheckBox assignedProgress = dialog.findViewById(R.id.assignedProgress);
        CheckBox inProgressProgress = dialog.findViewById(R.id.inProgressProgress);
        CheckBox reAssignedProgress = dialog.findViewById(R.id.reAssignedProgress);
        CheckBox completedProgress = dialog.findViewById(R.id.completedProgress);

        //RadioGroup assignToRadioGroup = dialog.findViewById(R.id.assignToRadioGroup);

        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);

        btnFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnName = btnFrom;
                new DatePickerDialog(MyTasksActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnName = btnTo;
                new DatePickerDialog(MyTasksActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        highPriority.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    highPriorityValue = true;
                }else {
                    highPriorityValue = false;
                }
            }
        });

        mediumPriority.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mediumPriorityValue = true;
                }else {
                    mediumPriorityValue = false;
                }
            }
        });

        highPriority.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    highPriorityValue = true;
                }else {
                    highPriorityValue = false;
                }
            }
        });

        lowPriority.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    lowPriorityValue = true;
                }else {
                    lowPriorityValue = false;
                }
            }
        });

        assignedProgress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    assignedProgressValue = true;
                }else {
                    assignedProgressValue = false;
                }
            }
        });

        inProgressProgress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    inProgressProgressValue = true;
                }else {
                    inProgressProgressValue = false;
                }
            }
        });

        reAssignedProgress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    reAssignedProgressValue = true;
                }else {
                    reAssignedProgressValue = false;
                }
            }
        });

        completedProgress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    completedProgressValue = true;
                }else {
                    completedProgressValue = false;
                }
            }
        });

//        assignToRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if(checkedId == R.id.allTeachers){
//                    assignToValue = "allTeachers";
//                }else if (checkedId == R.id.classTeachers){
//                    assignToValue = "classTeachers";
//                }
//            }
//        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filterValue.equals("To")){
                    if(btnFrom.getText().toString().equals("Select Date") && btnTo.getText().toString().equals("Select Date") && !assignedProgressValue && !inProgressProgressValue && !reAssignedProgressValue && !completedProgressValue){
                        Toast.makeText(context, "Please select at least one filter.", Toast.LENGTH_SHORT).show();
                    }else {
                        dialog.dismiss();
                        String fromDate = "", toDate = "";
                        if(!btnFrom.getText().toString().equals("Select Date")){
                            fromDate = btnFrom.getText().toString();
                        }

                        if(!btnTo.getText().toString().equals("Select Date")){
                            toDate = btnTo.getText().toString();
                        }

                        for(PendingTaskDetails pendingTaskDetails: pendingTaskDetailsArrayList){

                            String date = pendingTaskDetails.getDueDate();

                            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                            try{
                                Date date1 = formatter1.parse(date);

                                if(!fromDate.equals("") && !toDate.equals("")){
                                    Date fromD = formatter1.parse(fromDate);
                                    Date toD = formatter1.parse(toDate);

                                    if((date1.after(fromD) || fromD.equals(date1)) && (date1.before(toD) || toD.equals(date1))){
                                        if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){

                                            if(assignedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("A")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(reAssignedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("R")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(inProgressProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("P")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(completedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("C")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }
                                    }
                                }else if(fromDate.equals("") && !toDate.equals("")){
                                    Date toD = formatter1.parse(toDate);

                                    if(date1.before(toD) || toD.equals(date1)){
                                        if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){

                                            if(assignedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("A")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(reAssignedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("R")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(inProgressProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("P")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(completedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("C")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }
                                    }
                                }else if(!fromDate.equals("") && toDate.equals("")){
                                    Date fromD = formatter1.parse(fromDate);

                                    if(date1.after(fromD) || fromD.equals(date1)){
                                        if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){

                                            if(assignedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("A")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(reAssignedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("R")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(inProgressProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("P")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(completedProgressValue){
                                                if(pendingTaskDetails.getStatus().equals("C")){
                                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if(!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }
                                    }
                                }else if(fromDate.equals("") && toDate.equals("")){

                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){

                                        if(assignedProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("A")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(reAssignedProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("R")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(inProgressProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("P")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(completedProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("C")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue){
                                            if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                            }
                                        }
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        if(filteredPendingTaskDetailsArrayList.size() > 0){
                            filterButton.setVisibility(View.VISIBLE);
                            layNoData.setVisibility(View.GONE);
                            layParent.setVisibility(View.VISIBLE);
                            pendingTasksRecyAdapter = new PendingTasksRecyAdapter(context, filteredPendingTaskDetailsArrayList, filterValue, orgId, insId, dscId, stfId);
                            pending_tasks_recycler_view.setAdapter(pendingTasksRecyAdapter);
                            pendingTasksRecyAdapter.notifyDataSetChanged();
                        }else {
                            layNoData.setVisibility(View.VISIBLE);
                            layParent.setVisibility(View.GONE);
                        }
                    }
                }else {
                    if(btnFrom.getText().toString().equals("Select Date") && btnTo.getText().toString().equals("Select Date") && !assignedProgressValue && !inProgressProgressValue && !reAssignedProgressValue && !completedProgressValue && !highPriorityValue && !mediumPriorityValue && !lowPriorityValue){
                        Toast.makeText(context, "Please select at least one filter.", Toast.LENGTH_SHORT).show();
                    }else {
                        dialog.dismiss();
                        String fromDate = "", toDate = "";
                        if(!btnFrom.getText().toString().equals("Select Date")){
                            fromDate = btnFrom.getText().toString();
                        }

                        if(!btnTo.getText().toString().equals("Select Date")){
                            toDate = btnTo.getText().toString();
                        }

                        for(PendingTaskDetails pendingTaskDetails: pendingTaskDetailsArrayList) {

                            String date = pendingTaskDetails.getDueDate();

                            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date date1 = formatter1.parse(date);

                                if (!fromDate.equals("") && !toDate.equals("")) {
                                    Date fromD = formatter1.parse(fromDate);
                                    Date toD = formatter1.parse(toDate);

                                    if ((date1.after(fromD) || fromD.equals(date1)) && (date1.before(toD) || toD.equals(date1))) {
                                        if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {

                                            if (assignedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("A")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (reAssignedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("R")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (inProgressProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("P")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (completedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("C")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (highPriorityValue) {
                                                if (pendingTaskDetails.getPriority().equals("H")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (mediumPriorityValue) {
                                                if (pendingTaskDetails.getStatus().equals("M")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (lowPriorityValue) {
                                                if (pendingTaskDetails.getStatus().equals("L")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue && !highPriorityValue && !mediumPriorityValue && !lowPriorityValue) {
                                                if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }
                                    }
                                } else if (fromDate.equals("") && !toDate.equals("")) {
                                    Date toD = formatter1.parse(toDate);

                                    if (date1.before(toD) || toD.equals(date1)) {
                                        if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {

                                            if (assignedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("A")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (reAssignedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("R")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (inProgressProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("P")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (completedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("C")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (highPriorityValue) {
                                                if (pendingTaskDetails.getPriority().equals("H")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (mediumPriorityValue) {
                                                if (pendingTaskDetails.getStatus().equals("M")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (lowPriorityValue) {
                                                if (pendingTaskDetails.getStatus().equals("L")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue && !highPriorityValue && !mediumPriorityValue && !lowPriorityValue) {
                                                if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }
                                    }
                                } else if (!fromDate.equals("") && toDate.equals("")) {
                                    Date fromD = formatter1.parse(fromDate);

                                    if (date1.after(fromD) || fromD.equals(date1)) {
                                        if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {

                                            if (assignedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("A")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (reAssignedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("R")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (inProgressProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("P")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (completedProgressValue) {
                                                if (pendingTaskDetails.getStatus().equals("C")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (highPriorityValue) {
                                                if (pendingTaskDetails.getPriority().equals("H")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (mediumPriorityValue) {
                                                if (pendingTaskDetails.getStatus().equals("M")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (lowPriorityValue) {
                                                if (pendingTaskDetails.getStatus().equals("L")) {
                                                    if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                        filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                    }
                                                }
                                            }

                                            if (!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue && !highPriorityValue && !mediumPriorityValue && !lowPriorityValue) {
                                                if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }
                                    }
                                }else if(fromDate.equals("") && toDate.equals("")){

                                    if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){

                                        if(assignedProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("A")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(reAssignedProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("R")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(inProgressProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("P")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(completedProgressValue){
                                            if(pendingTaskDetails.getStatus().equals("C")){
                                                if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if (highPriorityValue) {
                                            if (pendingTaskDetails.getPriority().equals("H")) {
                                                if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if (mediumPriorityValue) {
                                            if (pendingTaskDetails.getStatus().equals("M")) {
                                                if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if (lowPriorityValue) {
                                            if (pendingTaskDetails.getStatus().equals("L")) {
                                                if (!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)) {
                                                    filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                                }
                                            }
                                        }

                                        if(!assignedProgressValue && !reAssignedProgressValue && !inProgressProgressValue && !completedProgressValue && !highPriorityValue && !mediumPriorityValue && !lowPriorityValue){
                                            if(!filteredPendingTaskDetailsArrayList.contains(pendingTaskDetails)){
                                                filteredPendingTaskDetailsArrayList.add(pendingTaskDetails);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {

                            }
                        }
                    }
                }

                if(filteredPendingTaskDetailsArrayList.size() > 0){
                    filterButton.setVisibility(View.VISIBLE);
                    layNoData.setVisibility(View.GONE);
                    layParent.setVisibility(View.VISIBLE);
                    pendingTasksRecyAdapter = new PendingTasksRecyAdapter(context, filteredPendingTaskDetailsArrayList, filterValue, orgId, insId, dscId, stfId);
                    pending_tasks_recycler_view.setAdapter(pendingTasksRecyAdapter);
                    pendingTasksRecyAdapter.notifyDataSetChanged();
                }else {
                    layNoData.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void updateLabel(Button btnName){
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        btnName.setText(sdf.format(myCalendar.getTime()));
    }

    public void addNewTask(View view) {

        Intent intent = new Intent(MyTasksActivity.this, AddNewTaskActivity.class);
        startActivity(intent);
    }

    public class LoadMyPendingTaskDetails extends AsyncTask<String, String, String> {
        private String resMyLectures;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                progDailog.dismiss();

                if (resMyLectures != null) {
                    JSONArray resArray = new JSONArray(resMyLectures);
                    JSONObject statusObject = resArray.getJSONObject(0);
                    String status = statusObject.getString("status");

                    if (status.equals("success")) {
                        JSONObject dataObj = resArray.getJSONObject(1);
                        //JSONObject dataObject = dataObj.getJSONObject("data");
                        JSONArray dataArray1 = dataObj.getJSONArray("data");
                        if(dataArray1.length() == 0)
                        {
                            Toast.makeText(context, "Pending tasks not found!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        pendingTaskDetailsArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray1.length(); i++) {
                            PendingTaskDetails pendingTaskDetails = new PendingTaskDetails();
                            JSONObject object = dataArray1.getJSONObject(i);

                            if(filterValue.contains("By")){
                                pendingTaskDetails.setTskId(object.getInt("tskId"));
                                pendingTaskDetails.setSubject(object.getString("tskSubject"));
                                pendingTaskDetails.setPriority(object.getString("tskPriority"));
                                pendingTaskDetails.setPercentComplete(object.getString("tskPercentComplete"));
                                pendingTaskDetails.setStatus(object.getString("tskStatus"));
                                pendingTaskDetails.setStartDate(object.getString("tskStartDate"));
                                pendingTaskDetails.setDueDate(object.getString("tskEndDate"));
                            }else if(filterValue.contains("To")) {
                                pendingTaskDetails.setAssignerId(object.getInt("assignerId"));
                                pendingTaskDetails.setTskId(object.getInt("tskId"));
                                pendingTaskDetails.setTssId(object.getInt("tssId"));
                                pendingTaskDetails.setSubject(object.getString("tssSubject"));
                                pendingTaskDetails.setAssigneeName(object.getString("assigner"));
                                pendingTaskDetails.setPercentComplete(object.getString("tssPercentComplete"));
                                pendingTaskDetails.setStatus(object.getString("tssStatus"));
                                pendingTaskDetails.setDueDate(object.getString("dueDate"));
                                pendingTaskDetails.setColorCode(object.getString("colourCode"));
                            }

                            pendingTaskDetailsArrayList.add(pendingTaskDetails);

                        }

                        //set adapter
                        if (pendingTaskDetailsArrayList.size() > 0) {
                            filterButton.setVisibility(View.VISIBLE);
                            layNoData.setVisibility(View.GONE);
                            layParent.setVisibility(View.VISIBLE);
                            pendingTasksRecyAdapter = new PendingTasksRecyAdapter(context, pendingTaskDetailsArrayList, filterValue, orgId, insId, dscId, stfId);
                            pending_tasks_recycler_view.setAdapter(pendingTasksRecyAdapter);
                            pendingTasksRecyAdapter.notifyDataSetChanged();

                        } else {
                            Toast.makeText(context, "Pending tasks not found!", Toast.LENGTH_SHORT).show();
                            filterButton.setVisibility(View.GONE);
                            layParent.setVisibility(View.GONE);
                            layNoData.setVisibility(View.VISIBLE);
                        }

                    } else {
                        filterButton.setVisibility(View.GONE);
                        layParent.setVisibility(View.GONE);
                        layNoData.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Pending tasks not found!", Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                String isTaskToMe;

                if(filterValue.equals("To")){
                    isTaskToMe = "Y";
                }else {
                    isTaskToMe = "N";
                }

                resMyLectures = util.findPendingTasks(orgId, insId, dscId, stfId, isTaskToMe);
                return resMyLectures;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
package com.techled.teacheranalytics.ActivityList.UploadHomeworkModule;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.OpenableColumns;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.CheckInternet;
import com.techled.teacheranalytics.pojo.Divisions;
import com.techled.teacheranalytics.pojo.SubjectDetails;
import com.techled.teacheranalytics.util.Util;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UploadLevelSubjectHomeworkActivity extends AppCompatActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private Util util;
    private Context context;
    private String params;
    private int orgId, insId, usrId, dscId, ayr, ugpId, usrAssetId, subId;
    private Spinner spinnSubject;
    private EditText edtTitle, edtDescription;
    private TextView txtAssignmentDate, txtDueDate, txtAttachment;
    private RadioGroup rgUploadContentType, rgFlippedClassroom, rbSubmissionType;
    private Button btnSave, btnChooseAttachment;
    private String fileExtension = "";
    private String response, date, resHwkUpload;
    private JSONArray classTeacherOfJson;
    private String resUserInfo;
    private int mYear, mMonth, mDay;
    private String assDate, dueDate, resSubject, hwkTitle, hwkDesc, hwkAssignedDate, hwkDueDate, hwkSubType = "", hwkFlippedClassrooom = "N", hwkUploadContentType = "", hwkUrl;
    private ProgressDialog progDailog;
    private ArrayList<SubjectDetails> subjectDetailsArrayList;
    private Spinner spinnSubmissionType, spinnContentType;
    private String resViewHwk;

    private String accessKey;
    private String secretKey;

    private String resAcdemicYear;
    private Spinner ayrYearSpinner;
    private String[] academicYears;
    private int[] ayrs;

    private String image, base64StringForUploadFile = "", hwkS3Url = "";
    //Pdf request code
    private int PICK_PDF_REQUEST = 1;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Uri to store the image uri
    private Uri filePath;
    private String notifyAllDivision = "N", notifyParents = "Y";
    private Button btnUploadFile;
    private TextView txtFileName;

    private ArrayList<Divisions> divisionsArrayList;
    private LinearLayout layParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_level_subject_homework);

        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Upload Homework");
            setSupportActionBar(toolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }

            context = this;
            util = new Util();

            //request runtime permission
            requestStoragePermission();

            spinnSubject = (Spinner) findViewById(R.id.spinnSubject);
            spinnSubmissionType = (Spinner) findViewById(R.id.spinnSubmissionType);
            spinnContentType = (Spinner) findViewById(R.id.spinnContentType);
            edtTitle = (EditText) findViewById(R.id.edtTitle);
            edtDescription = (EditText) findViewById(R.id.edtDescription);
            ayrYearSpinner = (Spinner) findViewById(R.id.ayrYearSpinner);

            txtAssignmentDate = (TextView) findViewById(R.id.txtAssignmentDate);
            txtDueDate = (TextView) findViewById(R.id.txtDueDate);
            txtAttachment = (TextView) findViewById(R.id.txtAttachment);

            rgUploadContentType = (RadioGroup) findViewById(R.id.rgUploadContentType);
            rgFlippedClassroom = (RadioGroup) findViewById(R.id.rgFlippedClassroom);
            rbSubmissionType = (RadioGroup) findViewById(R.id.rbSubmissionType);

            btnSave = (Button) findViewById(R.id.btnSave);
            btnChooseAttachment = (Button) findViewById(R.id.btnChooseAttachment);
            btnUploadFile = (Button) findViewById(R.id.btnUploadFile);
            txtFileName = (TextView) findViewById(R.id.txtFileName);
            layParent = (LinearLayout) findViewById(R.id.layParent);

            //check internet connection
            CheckInternet checkInternet = new CheckInternet();
            boolean checkConnection = checkInternet.checkConnection(context);
            if (checkConnection) {

            } else {

                Snackbar snackbar = Snackbar
                        .make(layParent, "Please check your internet connection or try again later!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //check poor internet connection
            CheckInternet checkPoorInternet = new CheckInternet();
            boolean checkPoorConnection = checkPoorInternet.CheckPoorInternetConnection(context);
            if (checkPoorConnection) {
                Snackbar snackbar = Snackbar
                        .make(layParent, "Poor internet connection!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
            }

            //login details
            SharedPreferences loginDetails = getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);
            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                //   ayr = 2016;
                dscId = object.getInt("dscId");
                ugpId = object.getInt("ugpId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
            }

            accessKey = loginDetails.getString("accessKey", null);
            secretKey = loginDetails.getString("secretKey", null);

            //homework upload content type
            rgUploadContentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbVedio) {
                        hwkUploadContentType = "V";
                    } else if (checkedId == R.id.rbAudio) {
                        hwkUploadContentType = "A";
                    } else if (checkedId == R.id.rbImage) {
                        hwkUploadContentType = "I";
                    } else if (checkedId == R.id.rbDocument) {
                        hwkUploadContentType = "D";
                    }
                }
            });

            //homework flipped classroom
            rgFlippedClassroom.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbYes) {
                        hwkFlippedClassrooom = "Y";
                    } else if (checkedId == R.id.rbNo) {
                        hwkFlippedClassrooom = "N";
                    }
                }
            });

            //homework submission type
            rbSubmissionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbHardCopy) {
                        hwkSubType = "H";
                    } else if (checkedId == R.id.rbSoftCopy) {
                        hwkSubType = "S";
                    } else if (checkedId == R.id.rbReading) {
                        hwkSubType = "R";
                    } else if (checkedId == R.id.rbOther) {
                        hwkSubType = "O";
                    }
                }
            });

            final ArrayList<String> arrayListSubType = new ArrayList<>();
            arrayListSubType.add("Choose Submission Type");
            arrayListSubType.add("HardCopy/NoteBook");
            arrayListSubType.add("SoftCopy");
            arrayListSubType.add("Reading");
            arrayListSubType.add("Other");

            ArrayAdapter<String> subTypeAdapter = new ArrayAdapter<String>(UploadLevelSubjectHomeworkActivity.this,
                    android.R.layout.simple_spinner_item, arrayListSubType);
            subTypeAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnSubmissionType.setAdapter(subTypeAdapter);
            spinnSubmissionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        String subType = spinnSubmissionType.getSelectedItem().toString();
                        if (subType.equals("HardCopy/NoteBook")) {
                            hwkSubType = "H";
                        } else if (subType.equals("SoftCopy")) {
                            hwkSubType = "S";
                        } else if (subType.equals("Reading")) {
                            hwkSubType = "R";

                        } else if (subType.equals("Other")) {
                            hwkSubType = "O";
                        }

                    } else {
                        //Toast.makeText(context, "Please select submission type!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //content type
            final ArrayList<String> arrayListContentType = new ArrayList<>();
            arrayListContentType.add("Choose Content Type");
            arrayListContentType.add("Video");
            arrayListContentType.add("Audio");
            arrayListContentType.add("Image");
            arrayListContentType.add("Text Document");
            arrayListContentType.add("Pdf Document");

            ArrayAdapter<String> contentTypeAdapter = new ArrayAdapter<String>(UploadLevelSubjectHomeworkActivity.this,
                    android.R.layout.simple_spinner_item, arrayListContentType);
            contentTypeAdapter
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnContentType.setAdapter(contentTypeAdapter);
            spinnContentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        String contentType = spinnContentType.getSelectedItem().toString();
                        if (contentType.equals("Video")) {
                            hwkUploadContentType = "V";
                            // fileExtension = ".mp3";
                        } else if (contentType.equals("Audio")) {
                            hwkUploadContentType = "A";
                            //  fileExtension = ".mp3";
                        } else if (contentType.equals("Image")) {
                            hwkUploadContentType = "I";
                            //fileExtension = ".png";

                        } else if (contentType.equals("Text Document")) {
                            hwkUploadContentType = "D";
                            // fileExtension = ".txt";

                        } else if (contentType.equals("Pdf Document")) {
                            hwkUploadContentType = "D";
                            // fileExtension = ".pdf";
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //get data
            SharedPreferences prefUserInfo = getSharedPreferences("MyInfo", MODE_PRIVATE);
            if (prefUserInfo != null) {
                resUserInfo = prefUserInfo.getString("response", null);
            } else {
                resUserInfo = util.getMyInfo(usrId, ayr);
            }

            if (resUserInfo != null) {
                JSONArray array = new JSONArray(resUserInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");
                    classTeacherOfJson = jsonObject.getJSONArray("applicableStds");

                }
            }

            if (classTeacherOfJson == null || classTeacherOfJson.equals("")) {
//                Toast.makeText(this,
//                        "Error to load standard/division!",
//                        Toast.LENGTH_LONG).show();

                Snackbar snackbar = Snackbar
                        .make(layParent, "Error to load standard/division!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                return;
            }

            //load assignment date,due date as current date
            //set current date
            Calendar c1 = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String curDate = df.format(c1.getTime());
            mYear = c1.get(Calendar.YEAR);
            mMonth = c1.get(Calendar.MONTH);
            mDay = c1.get(Calendar.DAY_OF_MONTH);

            txtAssignmentDate.setText("" + curDate);
            //   txtDueDate.setText("" + curDate);

          /*  RadioGroup rgNotifyStd = (RadioGroup) findViewById(R.id.rgNotifyStd);
            rgNotifyStd.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == R.id.rbNotifyYes) {
                        notifyAllDivision = "Y";
                    } else {
                        notifyAllDivision = "N";
                    }

                }
            });*/

            //attach file
            btnUploadFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        requestStoragePermission();

                        Intent intent = new Intent();
                        intent.setType("*/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select file"), PICK_PDF_REQUEST);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            //save / upload homework
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        //ayrYear
                        if (ayr == 0) {
                            // Toast.makeText(context, "Please select subject!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select academic year!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }
                        //subject
                        if (subId == 0) {
                            // Toast.makeText(context, "Please select subject!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select subject!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }
                        //title
                        hwkTitle = edtTitle.getText().toString().trim();
                        if (hwkTitle.equals("") || hwkTitle == null) {
                            // Toast.makeText(context, "Please enter homework title!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please enter homework title!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        //description
                        hwkDesc = edtDescription.getText().toString().trim();
                        if (hwkDesc.equals("")) {
                            // Toast.makeText(context, "Please enter homework title!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please enter homework description!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        //assigned date
                        hwkAssignedDate = txtAssignmentDate.getText().toString().trim();

                        if (hwkAssignedDate.equals("") || hwkAssignedDate == null) {
                            //Toast.makeText(context, "Please select homework assigned date!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select homework assigned date!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;

                        }
                        //due date
                        hwkDueDate = txtDueDate.getText().toString().trim();
                        if (hwkDueDate.equals("") || hwkDueDate == null) {
                            //  Toast.makeText(context, "Please select homework due date!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select homework due date!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        //flipped classroom
                        if (hwkFlippedClassrooom.equals("") || hwkFlippedClassrooom == null) {
                            //  Toast.makeText(context, "Please select flipped classroom!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select flipped classroom!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        //submission type
                        if (hwkSubType.equals("") || hwkSubType == null) {
                            //  Toast.makeText(context, "Please select homework submission type!", Toast.LENGTH_SHORT).show();
                            Snackbar snackbar = Snackbar
                                    .make(layParent, "Please select homework submission type!", Snackbar.LENGTH_LONG);
                            snackbar.setDuration(3000);
                            snackbar.show();
                            return;
                        }

                        //due date
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            Date DueDate = format.parse(hwkDueDate);
                            Date AssignedDate = format.parse(hwkAssignedDate);

                            if (DueDate.before(AssignedDate)) {
                                // Toast.makeText(context, "Due date should not be less than assigned date!", Toast.LENGTH_SHORT).show();
                                Snackbar snackbar = Snackbar
                                        .make(layParent, "Due date should not be less than assigned date!", Snackbar.LENGTH_LONG);
                                snackbar.setDuration(3000);
                                snackbar.show();
                                return;
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.dialog_confirm_upload_homework);
                        dialog.show();
                        Window window = dialog.getWindow();
                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                        dialog.setCanceledOnTouchOutside(false);

                        RadioGroup rgNotifyParents = (RadioGroup) dialog.findViewById(R.id.rgNotifyParents);
                        rgNotifyParents.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if (checkedId == R.id.rbYes) {
                                    notifyParents = "Y";

                                } else if (checkedId == R.id.rbNo) {
                                    notifyParents = "N";
                                }
                            }
                        });

                        Button btnUpload = (Button) dialog.findViewById(R.id.btnUpload);
                        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        btnUpload.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                new UploadHomework().execute();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });

            new LoadAcdYears().execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static byte[] getBytes(Context context, Uri uri) throws IOException {
        InputStream iStream = context.getContentResolver().openInputStream(uri);

        try {
            return getBytes(iStream);
        } finally {
            // close the stream
            try {
                iStream.close();
            } catch (IOException ignored) { /* do nothing */ }
        }
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {

        byte[] bytesResult = null;
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        try {
            int len;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            bytesResult = byteBuffer.toByteArray();
        } finally {
            // close the stream
            try {
                byteBuffer.close();
            } catch (IOException ignored) { /* do nothing */ }
        }
        return bytesResult;
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
                try {

                    filePath = data.getData();

//                    byte[] bbbyte = getBytes(UploadLevelSubjectHomeworkActivity.this, filePath);
//                    byte[] encoded = Base64.encodeBase64(bbbyte);
//                    base64StringForUploadFile = new String(encoded);
                    fileExtension = getMimeType(UploadLevelSubjectHomeworkActivity.this, filePath);
                    if (!fileExtension.startsWith(".")) {
                        fileExtension = "." + fileExtension;
                    }

                    txtFileName.setText("" + filePath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class UploadHomework extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resHwkUpload.equals("") || resHwkUpload == null) {
                    //  Toast.makeText(context, "Failed to upload homework!", Toast.LENGTH_LONG).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "Failed to upload homework!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                    snackbar.show();
                    util.SaveSystemLog(orgId, insId, "Failed to upload Homework", "TCANDROID", usrId, "Homework");

                    return;
                } else {

                    Snackbar snackbar;
                    if (resHwkUpload.equals("SUCCESS")) {

                        //  Toast.makeText(context, "Homework uploaded successfully!", Toast.LENGTH_LONG).show();
                        snackbar = Snackbar
                                .make(layParent, "Homework uploaded successfully!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();

                        //show alert for notify parents
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                        // Setting Dialog Title
                        alertDialog.setTitle("Alert..");

                        // Setting Dialog Message
                        alertDialog.setMessage("Homework uploaded successfully! Check details on homework details screen.");

                        // Setting Icon to Dialog
                        alertDialog.setIcon(R.drawable.notification);

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                Intent intent = new Intent(UploadLevelSubjectHomeworkActivity.this, ViewHomeworkActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
                        // onBackPressed();

                    } else {

                        //  Toast.makeText(context, "Failed to upload Homework!", Toast.LENGTH_LONG).show();
                        snackbar = Snackbar
                                .make(layParent, "Failed to upload Homework!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();

                        util.SaveSystemLog(orgId, insId, "Failed to upload Homework", "TCANDROID", usrId, "Homework");

                    }
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId, insId, "Error occur while uploading homework", "TCANDROID", usrId, "Homework");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {

                if (base64StringForUploadFile == null || base64StringForUploadFile.equals("null")) {
                    base64StringForUploadFile = "";
                }

                if (filePath == null) {

                    hwkS3Url = "";
                    resHwkUpload = util.uploadHomeworkByStaffV1(orgId, insId, dscId, 0, 0, subId, ayr, 0, hwkTitle, hwkDesc, hwkSubType,
                            base64StringForUploadFile, hwkUploadContentType, hwkDueDate, usrAssetId, hwkFlippedClassrooom, hwkAssignedDate, fileExtension, notifyAllDivision, notifyParents, hwkS3Url);
                } else {
                    File file = getFile(context, filePath);
                    String selectedFilePath = file.getPath();
                    SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-HH:mm:ss:SSS");
                    String fileName = "TC_HW_".concat(String.valueOf(orgId)).concat("_").concat(String.valueOf(insId)).concat("_").concat(String.valueOf(dscId)).concat("_").concat(String.valueOf(usrAssetId)).concat("_").concat(String.valueOf(usrId)).concat("_").concat(String.valueOf(subId).concat("_").concat(ft.format(new Date())).concat(fileExtension));
                    boolean status = uploadFilesToS3(selectedFilePath, fileName);

                    if (status) {

                        hwkS3Url = "http://s3.ap-southeast-1.amazonaws.com/".concat("amoghcloudfront").concat("/").concat(fileName);

                        resHwkUpload = util.uploadHomeworkByStaffV1(orgId, insId, dscId, 0, 0, subId, ayr, 0, hwkTitle, hwkDesc, hwkSubType,
                                base64StringForUploadFile, hwkUploadContentType, hwkDueDate, usrAssetId, hwkFlippedClassrooom, hwkAssignedDate, fileExtension, notifyAllDivision, notifyParents, hwkS3Url);
                    } else {
                        Snackbar snackbar = Snackbar
                                .make(layParent, "Failed to upload Homework!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                Snackbar snackbar = Snackbar
                        .make(layParent, "Failed to upload Homework!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();
                util.SaveSystemLog(orgId, insId, "Error occur while uploading homework", "TCANDROID", usrId, "Homework");
            }
            return null;
        }
    }

    public boolean uploadFilesToS3(String filePath, String fileName){

        try {

            ClientConfiguration clientConfiguration = new ClientConfiguration()
                    .withMaxErrorRetry(1) // 1 retries
                    .withConnectionTimeout(200000) // 30,000 ms
                    .withSocketTimeout(200000); // 30,000 ms

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey), clientConfiguration);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));

            //PutObjectRequest por = new PutObjectRequest(bucketName.concat("/etechschool"),fileName, new File(filePath));
            //PutObjectRequest por = new PutObjectRequest(bucketName, fileName, new File(filePath));
            //All student uploaded documents will be uploaded on "amoghcloudfront" cloud bucket for now.
            PutObjectRequest por= new PutObjectRequest("amoghcloudfront", fileName, new File(filePath));
            por.withCannedAcl(CannedAccessControlList.PublicRead);
            s3Client.putObject(por);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public static File getFile(Context context, Uri uri) throws IOException {
        File destinationFilename = new File(context.getFilesDir().getPath() + File.separatorChar + queryName(context, uri));
        try (InputStream ins = context.getContentResolver().openInputStream(uri)) {
            createFileFromStream(ins, destinationFilename);
        } catch (Exception ex) {
            Log.e("Save File", ex.getMessage());
            ex.printStackTrace();
        }
        return destinationFilename;
    }

    public static void createFileFromStream(InputStream ins, File destination) {
        try (OutputStream os = new FileOutputStream(destination)) {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = ins.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
        } catch (Exception ex) {
            Log.e("Save File", ex.getMessage());
            ex.printStackTrace();
        }
    }

    private static String queryName(Context context, Uri uri) {
        Cursor returnCursor =
                context.getContentResolver().query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

    public void setDateForAssignedDate(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.timeDatePicker,
                setDateForAssignedDateListener, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void setDateForDueDate(View view) {
        try {
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.timeDatePicker,
                    setDateForDueDateListener, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DatePickerDialog.OnDateSetListener setDateForAssignedDateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                assDate = String.valueOf(mYear) + "-" + String.valueOf(mMonth + 1)
                        + "-" + String.valueOf(mDay);

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                cal.set(Calendar.DAY_OF_MONTH, mDay);
                cal.set(Calendar.MONTH, mMonth);
                cal.set(Calendar.YEAR, mYear);
                cal.set(Calendar.HOUR, 00);
                cal.set(Calendar.MINUTE, 00);
                cal.set(Calendar.SECOND, 00);
                cal.set(Calendar.MILLISECOND, 00);
                Date dateFrom = cal.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                assDate = sdf.format(dateFrom);

                txtAssignmentDate.setText("" + assDate);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private DatePickerDialog.OnDateSetListener setDateForDueDateListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;

                dueDate = String.valueOf(mYear) + "-" + String.valueOf(mMonth + 1)
                        + "-" + String.valueOf(mDay);

                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Kolkata"));
                cal.set(Calendar.DAY_OF_MONTH, mDay);
                cal.set(Calendar.MONTH, mMonth);
                cal.set(Calendar.YEAR, mYear);
                cal.set(Calendar.HOUR, 00);
                cal.set(Calendar.MINUTE, 00);
                cal.set(Calendar.SECOND, 00);
                cal.set(Calendar.MILLISECOND, 00);
                Date dateFrom = cal.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                dueDate = sdf.format(dateFrom);

                txtDueDate.setText("" + dueDate);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    //Requesting permission
    private void requestStoragePermission() {
        try {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                return;

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //If the user has denied the permission previously your code will come to this block
                //Here you can explain why you need this permission
                //Explain here why you need this permission
            }
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                //  Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                //  Toast.makeText(this, "You just denied the permission!", Toast.LENGTH_LONG).show();
                Snackbar snackbar = Snackbar
                        .make(layParent, "You just denied the permission!", Snackbar.LENGTH_LONG);
                snackbar.setDuration(3000);
                snackbar.show();

            }
        }
    }

    public class LoadSubjects extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
//            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resViewHwk != null) {

                    JSONArray jsonArray = new JSONArray(resViewHwk);
                    JSONObject object1 = jsonArray.getJSONObject(0);
                    String status = object1.getString("status");

                    if (status.equals("SUCCESS")) {

                        subjectDetailsArrayList = new ArrayList<>();

                        SubjectDetails sD = new SubjectDetails();
                        sD.setSubId(0);
                        sD.setSubName("Select Subject");

                        subjectDetailsArrayList.add(sD);

                        JSONObject object2 = jsonArray.getJSONObject(1);
                        JSONArray array = object2.getJSONArray("data");

                        for (int i=0; i<array.length(); i++){

                            JSONObject object = array.getJSONObject(i);

                            SubjectDetails subjectDetails = new SubjectDetails();
                            subjectDetails.setSubId(object.getInt("subId"));
                            subjectDetails.setSubName(object.getString("subName"));

                            subjectDetailsArrayList.add(subjectDetails);
                        }

                        if(subjectDetailsArrayList.size() > 0){

                            ArrayAdapter<SubjectDetails> ayrAdaptor = new ArrayAdapter<SubjectDetails>(UploadLevelSubjectHomeworkActivity.this,
                                    android.R.layout.simple_spinner_item, subjectDetailsArrayList);
                            ayrAdaptor
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnSubject.setAdapter(ayrAdaptor);

                            spinnSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    if (position != 0) {
                                        subId = subjectDetailsArrayList.get(position).getSubId();
                                    } else {
                                        subId = 0;
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            
                        }else {
                            Toast.makeText(context, "Subject details not found!", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    } else {

                        //  Toast.makeText(context, "No Homework Assigned!", Toast.LENGTH_SHORT).show();
                        Snackbar snackbar = Snackbar
                                .make(layParent, "No Subjects Found!", Snackbar.LENGTH_LONG);
                        snackbar.setDuration(3000);
                        snackbar.show();

                    }

                } else {

                    // Toast.makeText(context, "No Homework Assigned!", Toast.LENGTH_SHORT).show();
                    Snackbar snackbar = Snackbar
                            .make(layParent, "No Subjects Found!", Snackbar.LENGTH_LONG);
                    snackbar.setDuration(3000);
                }

            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading Subject details","TCANDROID",usrAssetId,"Homework");
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                resViewHwk = util.getLevelSubsForTeacher(String.valueOf(orgId), String.valueOf(insId), String.valueOf(dscId), String.valueOf(usrAssetId), "L");
            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading homework details","TCANDROID",usrAssetId,"Homework");

            }
            return null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (progDailog != null) {
            progDailog.dismiss();
        }
    }

    private class LoadAcdYears extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setMessage("Loading...");
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
//            progDailog.setIndeterminate(false);
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resAcdemicYear == null || resAcdemicYear.equals("")) {
                    Toast.makeText(context, "Academic year not found!", Toast.LENGTH_SHORT).show();
                } else {

                    String[] splitIns = resAcdemicYear.split(";");

                    academicYears = new String[splitIns.length];
                    ayrs = new int[splitIns.length];

                    /* fill organization, orgs list */
                    for (int iterator = 0; iterator < splitIns.length; iterator++) {
                        String[] record = splitIns[iterator].split(":");

                        academicYears[iterator] = record[0];
                        ayrs[iterator] = Integer.valueOf(record[1]);
                    }

                    /* create adaptor for dsc spinner */
                    ArrayAdapter<String> ayrAdaptor = new ArrayAdapter<String>(UploadLevelSubjectHomeworkActivity.this,
                            android.R.layout.simple_spinner_item, academicYears);
                    ayrAdaptor
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    ayrYearSpinner.setAdapter(ayrAdaptor);

                    ayrYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ayr = ayrs[position];

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    new LoadSubjects().execute();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                //
                // if (pastResAcdemicYear == null || pastResAcdemicYear.equals("")) {

                resAcdemicYear = util.getAcademicYears(orgId, insId, dscId);

                    /*SharedPreferences.Editor syllabusPref = getSharedPreferences("AYR_YEAR", MODE_PRIVATE).edit();
                    syllabusPref.putString("Response", resAcdemicYear);
                    syllabusPref.commit();

                } else {

                    resAcdemicYear = pastResAcdemicYear;
                }
*/

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

package com.techled.teacheranalytics.ActivityList.MyAttendanceModule;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.techled.teacheranalytics.R;
import com.techled.teacheranalytics.pojo.DailyAttendance;
import com.techled.teacheranalytics.pojo.MonthlyAttendanceHistory;
import com.techled.teacheranalytics.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class MyAttendanceGraphFragment extends Fragment implements OnChartValueSelectedListener {
    private ProgressDialog progDailog;
    private Context context;
    private Util util;
    private int orgId, insId, dscId, ayr, usrId, usrAssetId;
    private String params;
    private ArrayList<MonthlyAttendanceHistory> monthlyAttendanceHistoryArrayList;
    private PieChart chart;
    private String totalWorking, totalPresent;
    private double totalPercentage, totalBlank;
    private TextView txtPresentDays, txtAbsentDays;
    private String resMonthlyAttd, pastResMonthlyAttd, resMyInfo;
    private LinearLayout layNoRecord, layParent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_layout_graph, container, false);

        try {

            context = getActivity();
            util = new Util();

            //login details
            SharedPreferences loginDetails = context.getSharedPreferences("user", 0);
            params = loginDetails.getString("params", null);

            JSONArray jsonArray = new JSONArray(params);
            JSONObject object1 = jsonArray.getJSONObject(0);
            String status = object1.getString("status");
            if (status.equals("SUCCESS")) {

                JSONObject object2 = jsonArray.getJSONObject(1);
                JSONArray array = object2.getJSONArray("data");
                JSONObject object = array.getJSONObject(0);

                orgId = object.getInt("orgId");
                insId = object.getInt("insId");
                ayr = object.getInt("ayrYear");
                dscId = object.getInt("dscId");
                usrId = object.getInt("usrId");
                usrAssetId = object.getInt("assetId1");
            }


            //my info
            SharedPreferences prefsForStdDiv = context.getSharedPreferences("MyInfo", context.MODE_PRIVATE);
            if (prefsForStdDiv != null) {
                resMyInfo = prefsForStdDiv.getString("response", null);
            } else {
                resMyInfo = util.getMyInfo(usrId, ayr);
            }


            if (resMyInfo != null) {
                JSONArray array = new JSONArray(resMyInfo);
                JSONObject jsonObject1 = array.getJSONObject(0);
                String Status = jsonObject1.getString("status");
                if (Status.equals("SUCCESS")) {
                    JSONObject jsonObject2 = array.getJSONObject(1);
                    JSONObject jsonObject = jsonObject2.getJSONObject("data");


                    if (jsonObject.getInt("payrollInsId") != 0) {
                        insId = jsonObject.getInt("payrollInsId");
                    }
                    if (jsonObject.getInt("payrollStfId") != 0) {
                        usrAssetId = jsonObject.getInt("payrollStfId");
                    }
                }


            }
            //get reference
            txtAbsentDays = (TextView) view.findViewById(R.id.txtAbsentDays);
            txtPresentDays = (TextView) view.findViewById(R.id.txtPresentDays);
            chart = (PieChart) view.findViewById(R.id.chart);
            progDailog = new ProgressDialog(context);
            layNoRecord = (LinearLayout) view.findViewById(R.id.layNoRecord);
            layParent = (LinearLayout) view.findViewById(R.id.layParent);


            //load monthly attendance from pref
            SharedPreferences monthlyAttdprefs = context.getSharedPreferences("monthlyAttd", context.MODE_PRIVATE);
            if (monthlyAttdprefs != null) {
                pastResMonthlyAttd = monthlyAttdprefs.getString("response", null);
            }


            //load monthly attd
            //MonthlyAttendanceActivity.loadMonthlyAttd loadMonthlyAttd = new MonthlyAttendanceActivity.loadMonthlyAttd();
            new LoadMonthlyAttd(getActivity(), null).execute("");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;

    }

    @Override
    public void onValueSelected(Entry e, int i, Highlight highlight) {
        if (e == null) {
            return;
        } else {
            Log.i("VAL SELECTED",
                    "Value: " + e.getVal() + ", xIndex: " + e.getXIndex()
                            + ", DataSet index: " + i);

        }
    }

    @Override
    public void onNothingSelected() {

    }


    public static Fragment newInstance(int i, String s) {
        MyAttendanceGraphFragment fragmentFirst = new MyAttendanceGraphFragment();
        return fragmentFirst;
    }


    public class LoadMonthlyAttd extends AsyncTask<String, String, String> {
        public LoadMonthlyAttd(Activity activity, Object o) {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progDailog.setIndeterminate(false);
            progDailog.setCancelable(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setMessage("Loading...");
            progDailog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progDailog.dismiss();
            try {
                if (resMonthlyAttd == null || resMonthlyAttd.equals("")) {
                    layNoRecord.setVisibility(View.VISIBLE);
                    layParent.setVisibility(View.GONE);
                    return;
                } else {
                    JSONObject object1 = new JSONObject(resMonthlyAttd);
                    totalWorking = object1.getString("totalWorking");
                    totalPresent = object1.getString("totalPresent");
                    totalPercentage = object1.getDouble("totalPercentage");
                    if (totalPercentage == 0.0 || totalPercentage == 0) {
                        layNoRecord.setVisibility(View.VISIBLE);
                        layParent.setVisibility(View.GONE);
                        return;
                    }
                    totalBlank = 100 - totalPercentage;

                    txtPresentDays.setText("Present Days(%) : " + totalPercentage);
                    txtAbsentDays.setText("Absent Days(%) : " + totalBlank);


                    chart.setUsePercentValues(true);

                    ArrayList<Entry> yValue = new ArrayList<>();
                    yValue.add(new Entry((float) totalPercentage, 0));
                    yValue.add(new Entry((float) totalBlank, 1));

                    ArrayList<String> xValue = new ArrayList<>();
                    xValue.add("");
                    xValue.add("");
                    PieDataSet dataSet = new PieDataSet(yValue, "");
                    dataSet.setColors(new int[]{ColorTemplate.rgb("#7ABF60"), ColorTemplate.rgb("#F6546A")});
                    PieData data = new PieData(xValue, dataSet);
                    data.setValueFormatter(new PercentFormatter());
                    chart.setData(data);

                    chart.setDrawHoleEnabled(true);
                    chart.setTransparentCircleRadius(30f);
                    chart.setHoleRadius(30f);
                    data.setValueTextSize(13f);
                    chart.setDescription("");
                    data.setValueTextColor(Color.DKGRAY);
                    chart.getLegend().setEnabled(false);
                    chart.animateXY(1400, 1400);
                    chart.setOnChartValueSelectedListener((OnChartValueSelectedListener) getActivity());

                    JSONArray array = object1.getJSONArray("studentAttendance");
                    monthlyAttendanceHistoryArrayList = new ArrayList<>();

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        MonthlyAttendanceHistory monthlyAttendanceHistory = new MonthlyAttendanceHistory();
                        monthlyAttendanceHistory.setMonthName(object.getString("monthName"));
                        monthlyAttendanceHistory.setPresentCount(object.getString("presentCount"));
                        monthlyAttendanceHistory.setWorkingCount(object.getString("workingCount"));


                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance in graph","Teacher Connect",usrId,"My Attendance");
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                if (pastResMonthlyAttd == null) {
                    resMonthlyAttd = util.getStudentMonthlyAttendance(orgId, insId, dscId, ayr, usrAssetId);

                    SharedPreferences.Editor editor = context.getSharedPreferences("monthlyAttd", context.MODE_PRIVATE).edit();
                    editor.putString("response", resMonthlyAttd);
                    editor.apply();

                } else {

                    resMonthlyAttd = pastResMonthlyAttd;
                }


            } catch (Exception e) {
                e.printStackTrace();
                util.SaveSystemLog(orgId,insId,"Error occur while loading my attendance in graph","Teacher Connect",usrId,"My Attendance");
            }
            return null;
        }
    }


}

package iamutkarshtiwari.github.io.ananas.editimage.fragment.paint;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;

import iamutkarshtiwari.github.io.ananas.BaseActivity;
import iamutkarshtiwari.github.io.ananas.R;
import iamutkarshtiwari.github.io.ananas.editimage.EditImageActivity;
import iamutkarshtiwari.github.io.ananas.editimage.ModuleConfig;
import iamutkarshtiwari.github.io.ananas.editimage.fragment.BaseEditFragment;
import iamutkarshtiwari.github.io.ananas.editimage.fragment.MainMenuFragment;
import iamutkarshtiwari.github.io.ananas.editimage.interfaces.OnMainBitmapChangeListener;
import iamutkarshtiwari.github.io.ananas.editimage.layout.ZoomLayout;
import iamutkarshtiwari.github.io.ananas.editimage.utils.Matrix3;
import iamutkarshtiwari.github.io.ananas.editimage.view.CustomPaintView;
import iamutkarshtiwari.github.io.ananas.editimage.view.TextStickerView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PaintFragment extends BaseEditFragment implements View.OnClickListener, BrushConfigDialog.Properties, EraserConfigDialog.Properties, OnMainBitmapChangeListener {

    public static final int INDEX = ModuleConfig.INDEX_PAINT;
    public static final String TAG = PaintFragment.class.getName();

    private static final float MAX_PERCENT = 100;
    private static final float MAX_ALPHA = 255;
    private static final float INITIAL_WIDTH = 15;

    private View mainView;

    private boolean isEraser = false;

    private View backToMenu;
    private CustomPaintView customPaintView;
    private LinearLayout eraserView;
    private LinearLayout brushView;
    private LinearLayout zoomView;

    private TextStickerView customPaintParentView;
    private ZoomLayout zoomLayout;

    private BrushConfigDialog brushConfigDialog;
    private EraserConfigDialog eraserConfigDialog;
    private Dialog loadingDialog;

    private float brushSize = INITIAL_WIDTH;
    private float eraserSize = INITIAL_WIDTH;
    private float brushAlpha = MAX_ALPHA;
    private int brushColor = Color.RED;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public static PaintFragment newInstance() {
        return new PaintFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_edit_paint, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadingDialog = BaseActivity.getLoadingDialog(getActivity(), R.string.iamutkarshtiwari_github_io_ananas_loading,
                false);
        customPaintView = ensureEditActivity().findViewById(R.id.custom_paint_view);
        backToMenu = mainView.findViewById(R.id.back_to_main);
        eraserView = mainView.findViewById(R.id.eraser_btn);
        brushView = mainView.findViewById(R.id.brush_btn);
        zoomView = mainView.findViewById(R.id.zoom_btn);
        mainView.findViewById(R.id.settings).setOnClickListener(this);
        setupOptionsConfig();

        customPaintParentView = ensureEditActivity().findViewById(R.id.custom_paint_panel);
        customPaintParentView.setDrawingCacheEnabled(true);

        zoomLayout = ensureEditActivity().findViewById(R.id.custom_paint_panel_frame);

        backToMenu.setOnClickListener(this);

        setClickListeners();
        initStroke();
    }

    private void setupOptionsConfig() {
        brushConfigDialog = new BrushConfigDialog(Math.round(INITIAL_WIDTH));
        brushConfigDialog.setPropertiesChangeListener(this);

        eraserConfigDialog = new EraserConfigDialog();
        eraserConfigDialog.setPropertiesChangeListener(this);
    }

    private void setClickListeners() {
        brushView.setOnClickListener(this);
        eraserView.setOnClickListener(this);
        zoomView.setOnClickListener(this);
    }

    private void initStroke() {
        customPaintView.setWidth(INITIAL_WIDTH);
        customPaintView.setColor(brushColor);
        customPaintView.setStrokeAlpha(MAX_ALPHA);
        customPaintView.setEraserStrokeWidth(INITIAL_WIDTH);
    }

    @Override
    public void onClick(View view) {
        if (view == backToMenu) {
            activity.applyBtn.setVisibility(View.VISIBLE);
            backToMain();
        } else if (view == eraserView) {
            activity.applyBtn.setVisibility(View.VISIBLE);
            hideZoomViews();
            if (!isEraser) {
                toggleButtons();
            }
            ((ImageView) zoomView.findViewById(R.id.zoom_icon)).setImageResource(R.drawable.zoom_gray);
        } else if (view == brushView) {
            activity.applyBtn.setVisibility(View.VISIBLE);
            toggleViews(true);
            if (isEraser) {
                toggleButtons();
            }
            ((ImageView) zoomView.findViewById(R.id.zoom_icon)).setImageResource(R.drawable.zoom_gray);
        } else if (view == zoomView) {
            activity.applyBtn.setVisibility(View.GONE);
            toggleViews(false);
            ((ImageView) zoomView.findViewById(R.id.zoom_icon)).setImageResource(R.drawable.zoom_white);
            ((ImageView) eraserView.findViewById(R.id.eraser_icon)).setImageResource(R.drawable.ic_eraser_disabled);
            ((ImageView) brushView.findViewById(R.id.brush_icon)).setImageResource(R.drawable.ic_brush_grey_24dp);
        } else if (view.getId() == R.id.settings) {
            showDialog(isEraser ? eraserConfigDialog : brushConfigDialog);
        }
    }

    public void hideZoomViews() {

        customPaintParentView.setVisibility(View.GONE);
        zoomLayout.setVisibility(View.GONE);

        activity.mainImage.setVisibility(View.VISIBLE);
        customPaintView.setVisibility(View.VISIBLE);
    }

    private void showDialog(BottomSheetDialogFragment dialogFragment) {
        String tag = dialogFragment.getTag();

        // Avoid IllegalStateException "Fragment already added"
        if (dialogFragment.isAdded()) return;

        dialogFragment.show(requireFragmentManager(), tag);

        if (isEraser) {
            updateEraserSize();
        } else {
            updateBrushParams();
        }
    }

    @Override
    public void onPause() {
        compositeDisposable.clear();
        super.onPause();
    }

    public void backToMain() {
        activity.mode = EditImageActivity.MODE_NONE;
        activity.bottomGallery.setCurrentItem(MainMenuFragment.INDEX);
        activity.mainImage.setVisibility(View.VISIBLE);
        activity.bannerFlipper.showPrevious();

        customPaintView.reset();
        hideZoomViews();
        customPaintView.setVisibility(View.GONE);
    }

    public void onShow() {
        activity.mode = EditImageActivity.MODE_PAINT;
        activity.mainImage.setImageBitmap(activity.getMainBit());

        activity.mainImage.setVisibility(View.VISIBLE);
        customPaintView.setVisibility(View.VISIBLE);
        customPaintParentView.setVisibility(View.GONE);
        zoomLayout.setVisibility(View.GONE);
    }

    public void onShowV2() {
        activity.mode = EditImageActivity.MODE_PAINT;
        activity.mainImage.setVisibility(View.GONE);
        customPaintParentView.updateImageBitmap(activity.getMainBit());
        customPaintParentView.setVisibility(View.VISIBLE);
        zoomLayout.setVisibility(View.VISIBLE);

        customPaintView.setVisibility(View.GONE);
        autoScaleImageToFitBounds();
    }

    public void toggleViews(boolean isPaint) {

        if (isPaint) {

            onShow();
        } else {

            onShowV2();
        }
    }

    private void autoScaleImageToFitBounds() {
        customPaintParentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                customPaintParentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                scaleImage();
            }
        });
    }

    private void scaleImage() {
        final float zoomLayoutWidth = zoomLayout.getWidth();
        final float zoomLayoutHeight = zoomLayout.getHeight();

        final float imageViewWidth = customPaintParentView.getWidth();
        final float imageViewHeight = customPaintParentView.getHeight();

        // To avoid divideByZero exception
        if (imageViewHeight != 0 && imageViewWidth != 0 && zoomLayoutHeight != 0 && zoomLayoutWidth != 0) {
            final float offsetFactorX = zoomLayoutWidth / imageViewWidth;
            final float offsetFactorY = zoomLayoutHeight / imageViewHeight;

            float scaleFactor = Math.min(offsetFactorX, offsetFactorY);
            zoomLayout.setChildScale(scaleFactor);
        }
    }

    private void toggleButtons() {
        isEraser = !isEraser;
        customPaintView.setEraser(isEraser);
        ((ImageView) eraserView.findViewById(R.id.eraser_icon)).setImageResource(isEraser ? R.drawable.ic_eraser_enabled : R.drawable.ic_eraser_disabled);
        ((ImageView) brushView.findViewById(R.id.brush_icon)).setImageResource(isEraser ? R.drawable.ic_brush_grey_24dp : R.drawable.ic_brush_white_24dp);
    }

    public void savePaintImage() {
        compositeDisposable.clear();

        Disposable applyPaintDisposable = applyPaint(activity.getMainBit())
                .flatMap(bitmap -> {
                    if (bitmap == null) {
                        return Single.error(new Throwable("Error occurred while applying paint"));
                    } else {
                        return Single.just(bitmap);
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscriber -> loadingDialog.show())
                .doFinally(() -> loadingDialog.dismiss())
                .subscribe(bitmap -> {
                    customPaintView.reset();
                    activity.changeMainBitmap(bitmap, true);
                    backToMain();
                }, e -> {
                    // Do nothing on error
                });

        compositeDisposable.add(applyPaintDisposable);
    }

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    private Single<Bitmap> applyPaint(Bitmap mainBitmap) {
        return Single.fromCallable(() -> {
            Matrix touchMatrix = activity.mainImage.getImageViewMatrix();

            Bitmap resultBit = Bitmap.createBitmap(mainBitmap).copy(
                    Bitmap.Config.ARGB_8888, true);
            Canvas canvas = new Canvas(resultBit);

            float[] data = new float[9];
            touchMatrix.getValues(data);
            Matrix3 cal = new Matrix3(data);
            Matrix3 inverseMatrix = cal.inverseMatrix();
            Matrix matrix = new Matrix();
            matrix.setValues(inverseMatrix.getValues());

            handleImage(canvas, matrix);

            return resultBit;
        });
    }

    private void handleImage(Canvas canvas, Matrix matrix) {
        float[] f = new float[9];
        matrix.getValues(f);

        int dx = (int) f[Matrix.MTRANS_X];
        int dy = (int) f[Matrix.MTRANS_Y];

        float scale_x = f[Matrix.MSCALE_X];
        float scale_y = f[Matrix.MSCALE_Y];

        canvas.save();
        canvas.translate(dx, dy);
        canvas.scale(scale_x, scale_y);

        if (customPaintView.getPaintBit() != null) {
            canvas.drawBitmap(customPaintView.getPaintBit(), 0, 0, null);
        }
        canvas.restore();
    }

    @Override
    public void onColorChanged(int colorCode) {
        brushColor = colorCode;
        updateBrushParams();
    }

    @Override
    public void onOpacityChanged(int opacity) {
        brushAlpha = (opacity / MAX_PERCENT) * MAX_ALPHA;
        updateBrushParams();
    }

    @Override
    public void onBrushSizeChanged(int brushSize) {
        if (isEraser) {
            this.eraserSize = brushSize;
            updateEraserSize();
        } else {
            this.brushSize = brushSize;
            updateBrushParams();
        }
    }

    private void updateBrushParams() {
        customPaintView.setColor(brushColor);
        customPaintView.setWidth(brushSize);
        customPaintView.setStrokeAlpha(brushAlpha);
    }

    private void updateEraserSize() {
        customPaintView.setEraserStrokeWidth(eraserSize);
    }

    @Override
    public void onMainBitmapChange() {
        customPaintParentView.updateImageBitmap(activity.getMainBit());
    }
}
